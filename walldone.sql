-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Oct 02, 2018 at 11:25 PM
-- Server version: 5.5.42
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `walldone`
--

-- --------------------------------------------------------

--
-- Table structure for table `wd_cf7_vdata`
--

CREATE TABLE `wd_cf7_vdata` (
  `id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wd_cf7_vdata_entry`
--

CREATE TABLE `wd_cf7_vdata_entry` (
  `id` int(11) NOT NULL,
  `cf7_id` int(11) NOT NULL,
  `data_id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wd_commentmeta`
--

CREATE TABLE `wd_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wd_commentmeta`
--

INSERT INTO `wd_commentmeta` (`meta_id`, `comment_id`, `meta_key`, `meta_value`) VALUES
(1, 2, 'rating', '5'),
(2, 2, 'verified', '0'),
(3, 3, 'rating', '4'),
(4, 3, 'verified', '0'),
(5, 4, 'rating', '3'),
(6, 4, 'verified', '0');

-- --------------------------------------------------------

--
-- Table structure for table `wd_comments`
--

CREATE TABLE `wd_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wd_comments`
--

INSERT INTO `wd_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'Um comentarista do WordPress', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2018-08-23 11:05:40', '2018-08-23 14:05:40', 'Olá, isso é um comentário.\nPara começar a moderar, editar e deletar comentários, visite a tela de Comentários no painel.\nAvatares de comentaristas vêm a partir do <a href="https://gravatar.com">Gravatar</a>.', 0, '1', '', '', 0, 0),
(2, 99, 'agencia904', 'web@904.ag', '', '::1', '2018-08-23 15:01:11', '2018-08-23 18:01:11', 'Testando avaliação 123', 0, '1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '', 0, 1),
(3, 99, 'agencia904', 'web@904.ag', '', '::1', '2018-08-23 15:42:23', '2018-08-23 18:42:23', 'jdsahjhdja kjsa', 0, '1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '', 0, 1),
(4, 96, 'agencia904', 'web@904.ag', '', '::1', '2018-08-25 18:01:34', '2018-08-25 21:01:34', 'teste', 0, '1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '', 0, 1),
(5, 353, 'WooCommerce', '', '', '', '2018-09-27 15:32:47', '2018-09-27 18:32:47', 'Aguardando pagamento em conta Status do pedido alterado de Pagamento pendente para Aguardando.', 0, '1', 'WooCommerce', 'order_note', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wd_links`
--

CREATE TABLE `wd_links` (
  `link_id` bigint(20) unsigned NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wd_options`
--

CREATE TABLE `wd_options` (
  `option_id` bigint(20) unsigned NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB AUTO_INCREMENT=2152 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wd_options`
--

INSERT INTO `wd_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://walldone/', 'yes'),
(2, 'home', 'http://walldone/', 'yes'),
(3, 'blogname', 'Wall Done', 'yes'),
(4, 'blogdescription', 'Só mais um site WordPress', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'web@904.ag', 'yes'),
(7, 'start_of_week', '0', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '1', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'j \\d\\e F \\d\\e Y', 'yes'),
(24, 'time_format', 'H:i', 'yes'),
(25, 'links_updated_date_format', 'j \\d\\e F \\d\\e Y, H:i', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:210:{s:24:"^wc-auth/v([1]{1})/(.*)?";s:63:"index.php?wc-auth-version=$matches[1]&wc-auth-route=$matches[2]";s:22:"^wc-api/v([1-3]{1})/?$";s:51:"index.php?wc-api-version=$matches[1]&wc-api-route=/";s:24:"^wc-api/v([1-3]{1})(.*)?";s:61:"index.php?wc-api-version=$matches[1]&wc-api-route=$matches[2]";s:7:"loja/?$";s:27:"index.php?post_type=product";s:37:"loja/feed/(feed|rdf|rss|rss2|atom)/?$";s:44:"index.php?post_type=product&feed=$matches[1]";s:32:"loja/(feed|rdf|rss|rss2|atom)/?$";s:44:"index.php?post_type=product&feed=$matches[1]";s:24:"loja/page/([0-9]{1,})/?$";s:45:"index.php?post_type=product&paged=$matches[1]";s:11:"^wp-json/?$";s:22:"index.php?rest_route=/";s:14:"^wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:21:"^index.php/wp-json/?$";s:22:"index.php?rest_route=/";s:24:"^index.php/wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:26:"^produtos-do-kit/([\\w]+)/?";s:42:"index.php?page_id=248&woosw_id=$matches[1]";s:13:"depoimento/?$";s:30:"index.php?post_type=depoimento";s:43:"depoimento/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?post_type=depoimento&feed=$matches[1]";s:38:"depoimento/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?post_type=depoimento&feed=$matches[1]";s:30:"depoimento/page/([0-9]{1,})/?$";s:48:"index.php?post_type=depoimento&paged=$matches[1]";s:49:"categorias/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:44:"categorias/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:25:"categorias/(.+?)/embed/?$";s:46:"index.php?category_name=$matches[1]&embed=true";s:37:"categorias/(.+?)/page/?([0-9]{1,})/?$";s:53:"index.php?category_name=$matches[1]&paged=$matches[2]";s:34:"categorias/(.+?)/wc-api(/(.*))?/?$";s:54:"index.php?category_name=$matches[1]&wc-api=$matches[3]";s:19:"categorias/(.+?)/?$";s:35:"index.php?category_name=$matches[1]";s:44:"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?product_tag=$matches[1]&feed=$matches[2]";s:39:"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?product_tag=$matches[1]&feed=$matches[2]";s:20:"tag/([^/]+)/embed/?$";s:44:"index.php?product_tag=$matches[1]&embed=true";s:32:"tag/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?product_tag=$matches[1]&paged=$matches[2]";s:29:"tag/([^/]+)/wc-api(/(.*))?/?$";s:44:"index.php?tag=$matches[1]&wc-api=$matches[3]";s:14:"tag/([^/]+)/?$";s:33:"index.php?product_tag=$matches[1]";s:45:"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:40:"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:21:"type/([^/]+)/embed/?$";s:44:"index.php?post_format=$matches[1]&embed=true";s:33:"type/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?post_format=$matches[1]&paged=$matches[2]";s:15:"type/([^/]+)/?$";s:33:"index.php?post_format=$matches[1]";s:48:"categoria/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?product_cat=$matches[1]&feed=$matches[2]";s:43:"categoria/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?product_cat=$matches[1]&feed=$matches[2]";s:24:"categoria/(.+?)/embed/?$";s:44:"index.php?product_cat=$matches[1]&embed=true";s:36:"categoria/(.+?)/page/?([0-9]{1,})/?$";s:51:"index.php?product_cat=$matches[1]&paged=$matches[2]";s:18:"categoria/(.+?)/?$";s:33:"index.php?product_cat=$matches[1]";s:35:"produto/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:45:"produto/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:65:"produto/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:60:"produto/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:60:"produto/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:41:"produto/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:24:"produto/([^/]+)/embed/?$";s:40:"index.php?product=$matches[1]&embed=true";s:28:"produto/([^/]+)/trackback/?$";s:34:"index.php?product=$matches[1]&tb=1";s:48:"produto/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:46:"index.php?product=$matches[1]&feed=$matches[2]";s:43:"produto/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:46:"index.php?product=$matches[1]&feed=$matches[2]";s:36:"produto/([^/]+)/page/?([0-9]{1,})/?$";s:47:"index.php?product=$matches[1]&paged=$matches[2]";s:43:"produto/([^/]+)/comment-page-([0-9]{1,})/?$";s:47:"index.php?product=$matches[1]&cpage=$matches[2]";s:33:"produto/([^/]+)/wc-api(/(.*))?/?$";s:48:"index.php?product=$matches[1]&wc-api=$matches[3]";s:39:"produto/[^/]+/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:50:"produto/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:32:"produto/([^/]+)(?:/([0-9]+))?/?$";s:46:"index.php?product=$matches[1]&page=$matches[2]";s:24:"produto/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:34:"produto/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:54:"produto/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:49:"produto/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:49:"produto/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:30:"produto/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:34:"slider/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:44:"slider/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:64:"slider/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:59:"slider/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:59:"slider/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:40:"slider/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:23:"slider/([^/]+)/embed/?$";s:39:"index.php?slider=$matches[1]&embed=true";s:27:"slider/([^/]+)/trackback/?$";s:33:"index.php?slider=$matches[1]&tb=1";s:35:"slider/([^/]+)/page/?([0-9]{1,})/?$";s:46:"index.php?slider=$matches[1]&paged=$matches[2]";s:42:"slider/([^/]+)/comment-page-([0-9]{1,})/?$";s:46:"index.php?slider=$matches[1]&cpage=$matches[2]";s:32:"slider/([^/]+)/wc-api(/(.*))?/?$";s:47:"index.php?slider=$matches[1]&wc-api=$matches[3]";s:38:"slider/[^/]+/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:49:"slider/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:31:"slider/([^/]+)(?:/([0-9]+))?/?$";s:45:"index.php?slider=$matches[1]&page=$matches[2]";s:23:"slider/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:33:"slider/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:53:"slider/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:48:"slider/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:48:"slider/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:29:"slider/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:39:"slider-blog/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:49:"slider-blog/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:69:"slider-blog/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:64:"slider-blog/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:64:"slider-blog/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:45:"slider-blog/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:28:"slider-blog/([^/]+)/embed/?$";s:44:"index.php?slider_blog=$matches[1]&embed=true";s:32:"slider-blog/([^/]+)/trackback/?$";s:38:"index.php?slider_blog=$matches[1]&tb=1";s:40:"slider-blog/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?slider_blog=$matches[1]&paged=$matches[2]";s:47:"slider-blog/([^/]+)/comment-page-([0-9]{1,})/?$";s:51:"index.php?slider_blog=$matches[1]&cpage=$matches[2]";s:37:"slider-blog/([^/]+)/wc-api(/(.*))?/?$";s:52:"index.php?slider_blog=$matches[1]&wc-api=$matches[3]";s:43:"slider-blog/[^/]+/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:54:"slider-blog/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:36:"slider-blog/([^/]+)(?:/([0-9]+))?/?$";s:50:"index.php?slider_blog=$matches[1]&page=$matches[2]";s:28:"slider-blog/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:38:"slider-blog/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:58:"slider-blog/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:53:"slider-blog/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:53:"slider-blog/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:34:"slider-blog/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:36:"depoimento/.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:46:"depoimento/.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:66:"depoimento/.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:61:"depoimento/.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:61:"depoimento/.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:42:"depoimento/.+?/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:25:"depoimento/(.+?)/embed/?$";s:43:"index.php?depoimento=$matches[1]&embed=true";s:29:"depoimento/(.+?)/trackback/?$";s:37:"index.php?depoimento=$matches[1]&tb=1";s:49:"depoimento/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?depoimento=$matches[1]&feed=$matches[2]";s:44:"depoimento/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?depoimento=$matches[1]&feed=$matches[2]";s:37:"depoimento/(.+?)/page/?([0-9]{1,})/?$";s:50:"index.php?depoimento=$matches[1]&paged=$matches[2]";s:44:"depoimento/(.+?)/comment-page-([0-9]{1,})/?$";s:50:"index.php?depoimento=$matches[1]&cpage=$matches[2]";s:34:"depoimento/(.+?)/wc-api(/(.*))?/?$";s:51:"index.php?depoimento=$matches[1]&wc-api=$matches[3]";s:40:"depoimento/.+?/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:51:"depoimento/.+?/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:33:"depoimento/(.+?)(?:/([0-9]+))?/?$";s:49:"index.php?depoimento=$matches[1]&page=$matches[2]";s:12:"robots\\.txt$";s:18:"index.php?robots=1";s:48:".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$";s:18:"index.php?feed=old";s:20:".*wp-app\\.php(/.*)?$";s:19:"index.php?error=403";s:18:".*wp-register.php$";s:23:"index.php?register=true";s:32:"feed/(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:27:"(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:8:"embed/?$";s:21:"index.php?&embed=true";s:20:"page/?([0-9]{1,})/?$";s:28:"index.php?&paged=$matches[1]";s:17:"wc-api(/(.*))?/?$";s:29:"index.php?&wc-api=$matches[2]";s:41:"comments/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:36:"comments/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:17:"comments/embed/?$";s:21:"index.php?&embed=true";s:26:"comments/wc-api(/(.*))?/?$";s:29:"index.php?&wc-api=$matches[2]";s:44:"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:39:"search/(.+)/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:20:"search/(.+)/embed/?$";s:34:"index.php?s=$matches[1]&embed=true";s:32:"search/(.+)/page/?([0-9]{1,})/?$";s:41:"index.php?s=$matches[1]&paged=$matches[2]";s:29:"search/(.+)/wc-api(/(.*))?/?$";s:42:"index.php?s=$matches[1]&wc-api=$matches[3]";s:14:"search/(.+)/?$";s:23:"index.php?s=$matches[1]";s:47:"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:42:"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:23:"author/([^/]+)/embed/?$";s:44:"index.php?author_name=$matches[1]&embed=true";s:35:"author/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?author_name=$matches[1]&paged=$matches[2]";s:32:"author/([^/]+)/wc-api(/(.*))?/?$";s:52:"index.php?author_name=$matches[1]&wc-api=$matches[3]";s:17:"author/([^/]+)/?$";s:33:"index.php?author_name=$matches[1]";s:69:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:45:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$";s:74:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]";s:54:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/wc-api(/(.*))?/?$";s:82:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&wc-api=$matches[5]";s:39:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$";s:63:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]";s:56:"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:51:"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:32:"([0-9]{4})/([0-9]{1,2})/embed/?$";s:58:"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true";s:44:"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]";s:41:"([0-9]{4})/([0-9]{1,2})/wc-api(/(.*))?/?$";s:66:"index.php?year=$matches[1]&monthnum=$matches[2]&wc-api=$matches[4]";s:26:"([0-9]{4})/([0-9]{1,2})/?$";s:47:"index.php?year=$matches[1]&monthnum=$matches[2]";s:43:"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:38:"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:19:"([0-9]{4})/embed/?$";s:37:"index.php?year=$matches[1]&embed=true";s:31:"([0-9]{4})/page/?([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&paged=$matches[2]";s:28:"([0-9]{4})/wc-api(/(.*))?/?$";s:45:"index.php?year=$matches[1]&wc-api=$matches[3]";s:13:"([0-9]{4})/?$";s:26:"index.php?year=$matches[1]";s:27:".?.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:".?.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:".?.+?/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:16:"(.?.+?)/embed/?$";s:41:"index.php?pagename=$matches[1]&embed=true";s:20:"(.?.+?)/trackback/?$";s:35:"index.php?pagename=$matches[1]&tb=1";s:40:"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:35:"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:28:"(.?.+?)/page/?([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&paged=$matches[2]";s:35:"(.?.+?)/comment-page-([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&cpage=$matches[2]";s:25:"(.?.+?)/wc-api(/(.*))?/?$";s:49:"index.php?pagename=$matches[1]&wc-api=$matches[3]";s:28:"(.?.+?)/pagamento(/(.*))?/?$";s:52:"index.php?pagename=$matches[1]&pagamento=$matches[3]";s:34:"(.?.+?)/pedido-recebido(/(.*))?/?$";s:58:"index.php?pagename=$matches[1]&pedido-recebido=$matches[3]";s:26:"(.?.+?)/pedidos(/(.*))?/?$";s:50:"index.php?pagename=$matches[1]&pedidos=$matches[3]";s:29:"(.?.+?)/ver-pedido(/(.*))?/?$";s:53:"index.php?pagename=$matches[1]&ver-pedido=$matches[3]";s:28:"(.?.+?)/downloads(/(.*))?/?$";s:52:"index.php?pagename=$matches[1]&downloads=$matches[3]";s:30:"(.?.+?)/edita-conta(/(.*))?/?$";s:54:"index.php?pagename=$matches[1]&edita-conta=$matches[3]";s:34:"(.?.+?)/editar-endereco(/(.*))?/?$";s:58:"index.php?pagename=$matches[1]&editar-endereco=$matches[3]";s:39:"(.?.+?)/metodos-de-pagamento(/(.*))?/?$";s:63:"index.php?pagename=$matches[1]&metodos-de-pagamento=$matches[3]";s:38:"(.?.+?)/esqueci-minha-senha(/(.*))?/?$";s:62:"index.php?pagename=$matches[1]&esqueci-minha-senha=$matches[3]";s:23:"(.?.+?)/sair(/(.*))?/?$";s:47:"index.php?pagename=$matches[1]&sair=$matches[3]";s:48:"(.?.+?)/adicionar-metodo-de-pagamento(/(.*))?/?$";s:72:"index.php?pagename=$matches[1]&adicionar-metodo-de-pagamento=$matches[3]";s:46:"(.?.+?)/excluir-metodo-de-pagamento(/(.*))?/?$";s:70:"index.php?pagename=$matches[1]&excluir-metodo-de-pagamento=$matches[3]";s:53:"(.?.+?)/definir-metodo-de-pagamento-padrao(/(.*))?/?$";s:77:"index.php?pagename=$matches[1]&definir-metodo-de-pagamento-padrao=$matches[3]";s:31:".?.+?/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:42:".?.+?/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:24:"(.?.+?)(?:/([0-9]+))?/?$";s:47:"index.php?pagename=$matches[1]&page=$matches[2]";s:27:"[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:"[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:"[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:16:"([^/]+)/embed/?$";s:37:"index.php?name=$matches[1]&embed=true";s:20:"([^/]+)/trackback/?$";s:31:"index.php?name=$matches[1]&tb=1";s:40:"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:35:"([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:28:"([^/]+)/page/?([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&paged=$matches[2]";s:35:"([^/]+)/comment-page-([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&cpage=$matches[2]";s:25:"([^/]+)/wc-api(/(.*))?/?$";s:45:"index.php?name=$matches[1]&wc-api=$matches[3]";s:31:"[^/]+/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:42:"[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:24:"([^/]+)(?:/([0-9]+))?/?$";s:43:"index.php?name=$matches[1]&page=$matches[2]";s:16:"[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:26:"[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:46:"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:22:"[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:16:{i:0;s:35:"advanced-cf7-db/advanced-cf7-db.php";i:1;s:30:"advanced-custom-fields/acf.php";i:3;s:37:"breadcrumb-trail/breadcrumb-trail.php";i:4;s:36:"contact-form-7/wp-contact-form-7.php";i:5;s:59:"cresta-social-share-counter/cresta-social-share-counter.php";i:6;s:32:"duplicate-page/duplicatepage.php";i:7;s:59:"product-import-export-for-woo/product-csv-import-export.php";i:8;s:45:"user-submitted-posts/user-submitted-posts.php";i:9;s:26:"wc-fields-factory/wcff.php";i:10;s:28:"woo-smart-wishlist/index.php";i:11;s:37:"woocommerce-ajax-cart/wooajaxcart.php";i:12;s:48:"woocommerce-ajax-filters/woocommerce-filters.php";i:13;s:45:"woocommerce-correios/woocommerce-correios.php";i:14;s:51:"woocommerce-product-image-flipper/image-flipper.php";i:15;s:67:"woocommerce-simple-registration/woocommerce-simple-registration.php";i:16;s:27:"woocommerce/woocommerce.php";}', 'yes'),
(34, 'category_base', '/categorias', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'walldone', 'yes'),
(41, 'stylesheet', 'walldone', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '38590', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'posts', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:"title";s:0:"";s:5:"count";i:0;s:12:"hierarchical";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(79, 'widget_text', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:3:{s:48:"woocommerce-ajax-filters/woocommerce-filters.php";a:2:{i:0;s:13:"BeRocket_AAPF";i:1;s:24:"br_delete_plugin_options";}s:79:"woo-simply-add-related-products-to-blog-posts/woo-related-products-on-posts.php";s:20:"wrpp_pluginUninstall";s:51:"ti-woocommerce-wishlist/ti-woocommerce-wishlist.php";s:23:"uninstall_tinv_wishlist";}', 'no'),
(82, 'timezone_string', 'America/Sao_Paulo', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '0', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '0', 'yes'),
(93, 'initial_db_version', '38590', 'yes'),
(94, 'wd_user_roles', 'a:7:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:119:{s:13:"switch_themes";b:1;s:11:"edit_themes";b:1;s:16:"activate_plugins";b:1;s:12:"edit_plugins";b:1;s:10:"edit_users";b:1;s:10:"edit_files";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:6:"import";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:8:"level_10";b:1;s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:12:"delete_users";b:1;s:12:"create_users";b:1;s:17:"unfiltered_upload";b:1;s:14:"edit_dashboard";b:1;s:14:"update_plugins";b:1;s:14:"delete_plugins";b:1;s:15:"install_plugins";b:1;s:13:"update_themes";b:1;s:14:"install_themes";b:1;s:11:"update_core";b:1;s:10:"list_users";b:1;s:12:"remove_users";b:1;s:13:"promote_users";b:1;s:18:"edit_theme_options";b:1;s:13:"delete_themes";b:1;s:6:"export";b:1;s:18:"manage_woocommerce";b:1;s:24:"view_woocommerce_reports";b:1;s:12:"edit_product";b:1;s:12:"read_product";b:1;s:14:"delete_product";b:1;s:13:"edit_products";b:1;s:20:"edit_others_products";b:1;s:16:"publish_products";b:1;s:21:"read_private_products";b:1;s:15:"delete_products";b:1;s:23:"delete_private_products";b:1;s:25:"delete_published_products";b:1;s:22:"delete_others_products";b:1;s:21:"edit_private_products";b:1;s:23:"edit_published_products";b:1;s:20:"manage_product_terms";b:1;s:18:"edit_product_terms";b:1;s:20:"delete_product_terms";b:1;s:20:"assign_product_terms";b:1;s:15:"edit_shop_order";b:1;s:15:"read_shop_order";b:1;s:17:"delete_shop_order";b:1;s:16:"edit_shop_orders";b:1;s:23:"edit_others_shop_orders";b:1;s:19:"publish_shop_orders";b:1;s:24:"read_private_shop_orders";b:1;s:18:"delete_shop_orders";b:1;s:26:"delete_private_shop_orders";b:1;s:28:"delete_published_shop_orders";b:1;s:25:"delete_others_shop_orders";b:1;s:24:"edit_private_shop_orders";b:1;s:26:"edit_published_shop_orders";b:1;s:23:"manage_shop_order_terms";b:1;s:21:"edit_shop_order_terms";b:1;s:23:"delete_shop_order_terms";b:1;s:23:"assign_shop_order_terms";b:1;s:16:"edit_shop_coupon";b:1;s:16:"read_shop_coupon";b:1;s:18:"delete_shop_coupon";b:1;s:17:"edit_shop_coupons";b:1;s:24:"edit_others_shop_coupons";b:1;s:20:"publish_shop_coupons";b:1;s:25:"read_private_shop_coupons";b:1;s:19:"delete_shop_coupons";b:1;s:27:"delete_private_shop_coupons";b:1;s:29:"delete_published_shop_coupons";b:1;s:26:"delete_others_shop_coupons";b:1;s:25:"edit_private_shop_coupons";b:1;s:27:"edit_published_shop_coupons";b:1;s:24:"manage_shop_coupon_terms";b:1;s:22:"edit_shop_coupon_terms";b:1;s:24:"delete_shop_coupon_terms";b:1;s:24:"assign_shop_coupon_terms";b:1;s:19:"cf7_db_form_view102";b:1;s:20:"cf7_db_form_edit_102";b:1;s:23:"tinvwl_general_settings";b:1;s:21:"tinvwl_style_settings";b:1;s:14:"tinvwl_upgrade";b:1;}}s:6:"editor";a:2:{s:4:"name";s:6:"Editor";s:12:"capabilities";a:34:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;}}s:6:"author";a:2:{s:4:"name";s:6:"Author";s:12:"capabilities";a:10:{s:12:"upload_files";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;}}s:11:"contributor";a:2:{s:4:"name";s:11:"Contributor";s:12:"capabilities";a:5:{s:10:"edit_posts";b:1;s:4:"read";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;}}s:10:"subscriber";a:2:{s:4:"name";s:10:"Subscriber";s:12:"capabilities";a:2:{s:4:"read";b:1;s:7:"level_0";b:1;}}s:8:"customer";a:2:{s:4:"name";s:8:"Customer";s:12:"capabilities";a:1:{s:4:"read";b:1;}}s:12:"shop_manager";a:2:{s:4:"name";s:12:"Shop manager";s:12:"capabilities";a:93:{s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:4:"read";b:1;s:18:"read_private_pages";b:1;s:18:"read_private_posts";b:1;s:10:"edit_users";b:1;s:10:"edit_posts";b:1;s:10:"edit_pages";b:1;s:20:"edit_published_posts";b:1;s:20:"edit_published_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"edit_private_posts";b:1;s:17:"edit_others_posts";b:1;s:17:"edit_others_pages";b:1;s:13:"publish_posts";b:1;s:13:"publish_pages";b:1;s:12:"delete_posts";b:1;s:12:"delete_pages";b:1;s:20:"delete_private_pages";b:1;s:20:"delete_private_posts";b:1;s:22:"delete_published_pages";b:1;s:22:"delete_published_posts";b:1;s:19:"delete_others_posts";b:1;s:19:"delete_others_pages";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:17:"moderate_comments";b:1;s:12:"upload_files";b:1;s:6:"export";b:1;s:6:"import";b:1;s:10:"list_users";b:1;s:18:"manage_woocommerce";b:1;s:24:"view_woocommerce_reports";b:1;s:12:"edit_product";b:1;s:12:"read_product";b:1;s:14:"delete_product";b:1;s:13:"edit_products";b:1;s:20:"edit_others_products";b:1;s:16:"publish_products";b:1;s:21:"read_private_products";b:1;s:15:"delete_products";b:1;s:23:"delete_private_products";b:1;s:25:"delete_published_products";b:1;s:22:"delete_others_products";b:1;s:21:"edit_private_products";b:1;s:23:"edit_published_products";b:1;s:20:"manage_product_terms";b:1;s:18:"edit_product_terms";b:1;s:20:"delete_product_terms";b:1;s:20:"assign_product_terms";b:1;s:15:"edit_shop_order";b:1;s:15:"read_shop_order";b:1;s:17:"delete_shop_order";b:1;s:16:"edit_shop_orders";b:1;s:23:"edit_others_shop_orders";b:1;s:19:"publish_shop_orders";b:1;s:24:"read_private_shop_orders";b:1;s:18:"delete_shop_orders";b:1;s:26:"delete_private_shop_orders";b:1;s:28:"delete_published_shop_orders";b:1;s:25:"delete_others_shop_orders";b:1;s:24:"edit_private_shop_orders";b:1;s:26:"edit_published_shop_orders";b:1;s:23:"manage_shop_order_terms";b:1;s:21:"edit_shop_order_terms";b:1;s:23:"delete_shop_order_terms";b:1;s:23:"assign_shop_order_terms";b:1;s:16:"edit_shop_coupon";b:1;s:16:"read_shop_coupon";b:1;s:18:"delete_shop_coupon";b:1;s:17:"edit_shop_coupons";b:1;s:24:"edit_others_shop_coupons";b:1;s:20:"publish_shop_coupons";b:1;s:25:"read_private_shop_coupons";b:1;s:19:"delete_shop_coupons";b:1;s:27:"delete_private_shop_coupons";b:1;s:29:"delete_published_shop_coupons";b:1;s:26:"delete_others_shop_coupons";b:1;s:25:"edit_private_shop_coupons";b:1;s:27:"edit_published_shop_coupons";b:1;s:24:"manage_shop_coupon_terms";b:1;s:22:"edit_shop_coupon_terms";b:1;s:24:"delete_shop_coupon_terms";b:1;s:24:"assign_shop_coupon_terms";b:1;s:14:"manage_options";b:1;}}}', 'yes'),
(95, 'fresh_site', '0', 'yes'),
(96, 'WPLANG', 'pt_BR', 'yes'),
(97, 'widget_search', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(98, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(99, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(100, 'widget_archives', 'a:2:{i:2;a:3:{s:5:"title";s:0:"";s:5:"count";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(101, 'widget_meta', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(102, 'sidebars_widgets', 'a:4:{s:12:";eft_sidebar";a:0:{}s:19:"wp_inactive_widgets";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:9:"sidebar-1";a:4:{i:0;s:33:"woocommerce_layered_nav_filters-2";i:1;s:32:"woocommerce_product_categories-2";i:2;s:22:"berocket_aapf_widget-2";i:3;s:22:"berocket_aapf_widget-3";}s:13:"array_version";i:3;}', 'yes'),
(103, 'widget_pages', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(104, 'widget_calendar', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(105, 'widget_media_audio', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(106, 'widget_media_image', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(107, 'widget_media_gallery', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(108, 'widget_media_video', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(109, 'widget_tag_cloud', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(110, 'widget_nav_menu', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(111, 'widget_custom_html', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(112, 'cron', 'a:12:{i:1538535600;a:1:{s:27:"woocommerce_scheduled_sales";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1538535940;a:1:{s:34:"wp_privacy_delete_old_export_files";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:6:"hourly";s:4:"args";a:0:{}s:8:"interval";i:3600;}}}i:1538536202;a:1:{s:32:"woocommerce_cancel_unpaid_orders";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:2:{s:8:"schedule";b:0;s:4:"args";a:0:{}}}}i:1538552220;a:1:{s:28:"woocommerce_cleanup_sessions";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1538573820;a:1:{s:33:"woocommerce_cleanup_personal_data";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1538573830;a:1:{s:30:"woocommerce_tracker_send_event";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1538575540;a:3:{s:16:"wp_version_check";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:17:"wp_update_plugins";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:16:"wp_update_themes";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1538575551;a:2:{s:19:"wp_scheduled_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}s:25:"delete_expired_transients";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1538576408;a:1:{s:30:"wp_scheduled_auto_draft_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1538584620;a:1:{s:24:"woocommerce_cleanup_logs";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1541073600;a:1:{s:25:"woocommerce_geoip_updater";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:7:"monthly";s:4:"args";a:0:{}s:8:"interval";i:2635200;}}}s:7:"version";i:2;}', 'yes'),
(113, 'theme_mods_twentyseventeen', 'a:2:{s:18:"custom_css_post_id";i:-1;s:16:"sidebars_widgets";a:2:{s:4:"time";i:1535033483;s:4:"data";a:4:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:9:"sidebar-2";a:0:{}s:9:"sidebar-3";a:0:{}}}}', 'yes'),
(127, 'can_compress_scripts', '1', 'no'),
(131, 'recently_activated', 'a:7:{s:59:"banner-management-for-woocommerce/woo-banner-management.php";i:1538405202;s:40:"yith-woocommerce-featured-video/init.php";i:1538136815;s:100:"woocommerce-embed-videos-to-product-image-gallery/woocommerce-embed-videos-product-image-gallery.php";i:1538136306;s:35:"redux-framework/redux-framework.php";i:1538134343;s:31:"smart-variations-images/svi.php";i:1538134243;s:47:"woo-variation-gallery/woo-variation-gallery.php";i:1538133755;s:20:"wp-dropzone/init.php";i:1537881952;}', 'yes'),
(150, 'woocommerce_store_address', 'Av. Presidente Affonso Camargo, 633', 'yes'),
(151, 'woocommerce_store_address_2', '', 'yes'),
(152, 'woocommerce_store_city', 'Curitiba', 'yes'),
(153, 'woocommerce_default_country', 'BR:PR', 'yes'),
(154, 'woocommerce_store_postcode', '80050-370', 'yes'),
(155, 'woocommerce_allowed_countries', 'all', 'yes'),
(156, 'woocommerce_all_except_countries', '', 'yes'),
(157, 'woocommerce_specific_allowed_countries', '', 'yes'),
(158, 'woocommerce_ship_to_countries', '', 'yes'),
(159, 'woocommerce_specific_ship_to_countries', '', 'yes'),
(160, 'woocommerce_default_customer_address', 'geolocation', 'yes'),
(161, 'woocommerce_calc_taxes', 'no', 'yes'),
(162, 'woocommerce_enable_coupons', 'yes', 'yes'),
(163, 'woocommerce_calc_discounts_sequentially', 'no', 'no'),
(164, 'woocommerce_currency', 'BRL', 'yes'),
(165, 'woocommerce_currency_pos', 'left', 'yes'),
(166, 'woocommerce_price_thousand_sep', '.', 'yes'),
(167, 'woocommerce_price_decimal_sep', ',', 'yes'),
(168, 'woocommerce_price_num_decimals', '2', 'yes'),
(169, 'woocommerce_shop_page_id', '5', 'yes'),
(170, 'woocommerce_cart_redirect_after_add', 'no', 'yes'),
(171, 'woocommerce_enable_ajax_add_to_cart', 'yes', 'yes'),
(172, 'woocommerce_weight_unit', 'kg', 'yes'),
(173, 'woocommerce_dimension_unit', 'cm', 'yes'),
(174, 'woocommerce_enable_reviews', 'yes', 'yes'),
(175, 'woocommerce_review_rating_verification_label', 'yes', 'no'),
(176, 'woocommerce_review_rating_verification_required', 'no', 'no'),
(177, 'woocommerce_enable_review_rating', 'yes', 'yes'),
(178, 'woocommerce_review_rating_required', 'yes', 'no'),
(179, 'woocommerce_manage_stock', 'yes', 'yes'),
(180, 'woocommerce_hold_stock_minutes', '60', 'no'),
(181, 'woocommerce_notify_low_stock', 'yes', 'no'),
(182, 'woocommerce_notify_no_stock', 'yes', 'no'),
(183, 'woocommerce_stock_email_recipient', 'web@904.ag', 'no'),
(184, 'woocommerce_notify_low_stock_amount', '2', 'no'),
(185, 'woocommerce_notify_no_stock_amount', '0', 'yes'),
(186, 'woocommerce_hide_out_of_stock_items', 'no', 'yes'),
(187, 'woocommerce_stock_format', '', 'yes'),
(188, 'woocommerce_file_download_method', 'force', 'no'),
(189, 'woocommerce_downloads_require_login', 'no', 'no'),
(190, 'woocommerce_downloads_grant_access_after_payment', 'yes', 'no'),
(191, 'woocommerce_prices_include_tax', 'no', 'yes'),
(192, 'woocommerce_tax_based_on', 'shipping', 'yes'),
(193, 'woocommerce_shipping_tax_class', 'inherit', 'yes'),
(194, 'woocommerce_tax_round_at_subtotal', 'no', 'yes'),
(195, 'woocommerce_tax_classes', 'Taxa reduzida\nTaxa zero', 'yes'),
(196, 'woocommerce_tax_display_shop', 'excl', 'yes'),
(197, 'woocommerce_tax_display_cart', 'excl', 'yes'),
(198, 'woocommerce_price_display_suffix', '', 'yes'),
(199, 'woocommerce_tax_total_display', 'itemized', 'no'),
(200, 'woocommerce_enable_shipping_calc', 'yes', 'no'),
(201, 'woocommerce_shipping_cost_requires_address', 'no', 'yes'),
(202, 'woocommerce_ship_to_destination', 'billing', 'no'),
(203, 'woocommerce_shipping_debug_mode', 'no', 'yes'),
(204, 'woocommerce_enable_guest_checkout', 'yes', 'no'),
(205, 'woocommerce_enable_checkout_login_reminder', 'no', 'no'),
(206, 'woocommerce_enable_signup_and_login_from_checkout', 'no', 'no'),
(207, 'woocommerce_enable_myaccount_registration', 'yes', 'no'),
(208, 'woocommerce_registration_generate_username', 'yes', 'no'),
(209, 'woocommerce_registration_generate_password', 'yes', 'no'),
(210, 'woocommerce_erasure_request_removes_order_data', 'no', 'no'),
(211, 'woocommerce_erasure_request_removes_download_data', 'no', 'no'),
(212, 'woocommerce_registration_privacy_policy_text', 'Seus dados pessoais serão usados para aprimorar a sua experiência em todo este site, para gerenciar o acesso a sua conta e para outros propósitos, como descritos em nossa [privacy_policy].', 'yes'),
(213, 'woocommerce_checkout_privacy_policy_text', 'Os seus dados pessoais serão utilizados para processar a sua compra, apoiar a sua experiência em todo este site e para outros fins descritos na nossa [privacy_policy].', 'yes'),
(214, 'woocommerce_delete_inactive_accounts', 'a:2:{s:6:"number";s:0:"";s:4:"unit";s:6:"months";}', 'no'),
(215, 'woocommerce_trash_pending_orders', 'a:2:{s:6:"number";s:0:"";s:4:"unit";s:4:"days";}', 'no'),
(216, 'woocommerce_trash_failed_orders', 'a:2:{s:6:"number";s:0:"";s:4:"unit";s:4:"days";}', 'no'),
(217, 'woocommerce_trash_cancelled_orders', 'a:2:{s:6:"number";s:0:"";s:4:"unit";s:4:"days";}', 'no'),
(218, 'woocommerce_anonymize_completed_orders', 'a:2:{s:6:"number";s:0:"";s:4:"unit";s:6:"months";}', 'no'),
(219, 'woocommerce_email_from_name', 'Wall Done', 'no'),
(220, 'woocommerce_email_from_address', 'web@904.ag', 'no'),
(221, 'woocommerce_email_header_image', '', 'no'),
(222, 'woocommerce_email_footer_text', '{site_title}', 'no'),
(223, 'woocommerce_email_base_color', '#96588a', 'no'),
(224, 'woocommerce_email_background_color', '#f7f7f7', 'no'),
(225, 'woocommerce_email_body_background_color', '#ffffff', 'no'),
(226, 'woocommerce_email_text_color', '#3c3c3c', 'no'),
(227, 'woocommerce_cart_page_id', '6', 'yes'),
(228, 'woocommerce_checkout_page_id', '7', 'yes'),
(229, 'woocommerce_myaccount_page_id', '8', 'yes'),
(230, 'woocommerce_terms_page_id', '', 'no'),
(231, 'woocommerce_force_ssl_checkout', 'no', 'yes'),
(232, 'woocommerce_unforce_ssl_checkout', 'no', 'yes'),
(233, 'woocommerce_checkout_pay_endpoint', 'pagamento', 'yes'),
(234, 'woocommerce_checkout_order_received_endpoint', 'pedido-recebido', 'yes'),
(235, 'woocommerce_myaccount_add_payment_method_endpoint', 'adicionar-metodo-de-pagamento', 'yes'),
(236, 'woocommerce_myaccount_delete_payment_method_endpoint', 'excluir-metodo-de-pagamento', 'yes'),
(237, 'woocommerce_myaccount_set_default_payment_method_endpoint', 'definir-metodo-de-pagamento-padrao', 'yes'),
(238, 'woocommerce_myaccount_orders_endpoint', 'pedidos', 'yes'),
(239, 'woocommerce_myaccount_view_order_endpoint', 'ver-pedido', 'yes'),
(240, 'woocommerce_myaccount_downloads_endpoint', 'downloads', 'yes'),
(241, 'woocommerce_myaccount_edit_account_endpoint', 'edita-conta', 'yes'),
(242, 'woocommerce_myaccount_edit_address_endpoint', 'editar-endereco', 'yes'),
(243, 'woocommerce_myaccount_payment_methods_endpoint', 'metodos-de-pagamento', 'yes'),
(244, 'woocommerce_myaccount_lost_password_endpoint', 'esqueci-minha-senha', 'yes'),
(245, 'woocommerce_logout_endpoint', 'sair', 'yes'),
(246, 'woocommerce_api_enabled', 'no', 'yes'),
(247, 'woocommerce_single_image_width', '600', 'yes'),
(248, 'woocommerce_thumbnail_image_width', '300', 'yes'),
(249, 'woocommerce_checkout_highlight_required_fields', 'yes', 'yes'),
(250, 'woocommerce_demo_store', 'no', 'no'),
(251, 'woocommerce_permalinks', 'a:5:{s:12:"product_base";s:8:"/produto";s:13:"category_base";s:9:"categoria";s:8:"tag_base";s:3:"tag";s:14:"attribute_base";s:0:"";s:22:"use_verbose_page_rules";b:0;}', 'yes'),
(252, 'current_theme_supports_woocommerce', 'yes', 'yes'),
(253, 'woocommerce_queue_flush_rewrite_rules', 'no', 'yes'),
(256, 'default_product_cat', '15', 'yes'),
(261, 'woocommerce_admin_notices', 'a:1:{i:0;s:20:"no_secure_connection";}', 'yes'),
(262, '_transient_woocommerce_webhook_ids', 'a:0:{}', 'yes'),
(263, 'widget_woocommerce_widget_cart', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(264, 'widget_woocommerce_layered_nav_filters', 'a:2:{i:2;a:1:{s:5:"title";s:14:"Ativar filtros";}s:12:"_multiwidget";i:1;}', 'yes'),
(265, 'widget_woocommerce_layered_nav', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(266, 'widget_woocommerce_price_filter', 'a:2:{i:2;a:1:{s:5:"title";s:18:"Filtrar por preço";}s:12:"_multiwidget";i:1;}', 'yes'),
(267, 'widget_woocommerce_product_categories', 'a:2:{i:2;a:8:{s:5:"title";s:10:"Categorias";s:7:"orderby";s:4:"name";s:8:"dropdown";i:0;s:5:"count";i:0;s:12:"hierarchical";i:1;s:18:"show_children_only";i:0;s:10:"hide_empty";i:0;s:9:"max_depth";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(268, 'widget_woocommerce_product_search', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(269, 'widget_woocommerce_product_tag_cloud', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(270, 'widget_woocommerce_products', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(271, 'widget_woocommerce_recently_viewed_products', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(272, 'widget_woocommerce_top_rated_products', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(273, 'widget_woocommerce_recent_reviews', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(274, 'widget_woocommerce_rating_filter', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(284, 'woocommerce_product_type', 'both', 'yes'),
(285, 'woocommerce_allow_tracking', 'yes', 'yes'),
(287, 'woocommerce_tracker_last_send', '1538060282', 'yes'),
(288, 'woocommerce_ppec_paypal_settings', 'a:2:{s:16:"reroute_requests";b:0;s:5:"email";b:0;}', 'yes'),
(289, 'woocommerce_cheque_settings', 'a:1:{s:7:"enabled";s:2:"no";}', 'yes'),
(290, 'woocommerce_bacs_settings', 'a:11:{s:7:"enabled";s:3:"yes";s:5:"title";s:31:"Transferência bancária direta";s:11:"description";s:251:"Faça seu pagamento diretamente em nossa conta bancária. Se possível informe o ID do seu pedido como identificação do seu depósito ou transferência. Para pagamentos via DOC, seu pedido não será enviado enquanto o pagamento não for compensado.";s:12:"instructions";s:0:"";s:12:"account_name";s:0:"";s:14:"account_number";s:0:"";s:9:"sort_code";s:0:"";s:9:"bank_name";s:0:"";s:4:"iban";s:0:"";s:3:"bic";s:0:"";s:15:"account_details";s:0:"";}', 'yes'),
(291, 'woocommerce_cod_settings', 'a:1:{s:7:"enabled";s:2:"no";}', 'yes'),
(293, '_transient_shipping-transient-version', '1535033341', 'yes'),
(308, 'current_theme', 'Wall Done', 'yes'),
(309, 'theme_mods_walldone', 'a:3:{i:0;b:0;s:18:"nav_menu_locations";a:2:{s:10:"store_menu";i:47;s:11:"mobile_menu";i:48;}s:18:"custom_css_post_id";i:-1;}', 'yes'),
(310, 'theme_switched', '', 'yes'),
(311, 'woocommerce_maybe_regenerate_images_hash', '991b1ca641921cf0f5baf7a2fe85861b', 'yes'),
(317, 'xa_pipe_plugin_installed_date', '2018-08-23 14:14:15', 'yes');
INSERT INTO `wd_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(320, 'wf_prod_csv_imp_exp_mapping', 'a:2:{i:0;a:56:{s:2:"id";s:2:"ID";s:10:"post_title";s:10:"post_title";s:9:"post_name";s:9:"post_name";s:11:"post_status";s:11:"post_status";s:12:"post_content";s:12:"post_content";s:12:"post_excerpt";s:12:"post_excerpt";s:9:"post_date";s:9:"post_date";s:3:"sku";s:3:"sku";s:13:"post_password";s:0:"";s:11:"post_author";s:11:"post_author";s:10:"menu_order";s:10:"menu_order";s:14:"comment_status";s:14:"comment_status";s:12:"downloadable";s:12:"downloadable";s:7:"virtual";s:7:"virtual";s:10:"visibility";s:10:"visibility";s:8:"featured";s:8:"featured";s:5:"stock";s:5:"stock";s:12:"stock_status";s:12:"stock_status";s:10:"backorders";s:10:"backorders";s:12:"manage_stock";s:12:"manage_stock";s:10:"sale_price";s:10:"sale_price";s:13:"regular_price";s:13:"regular_price";s:21:"sale_price_dates_from";s:21:"sale_price_dates_from";s:19:"sale_price_dates_to";s:19:"sale_price_dates_to";s:6:"weight";s:6:"weight";s:6:"length";s:6:"length";s:5:"width";s:5:"width";s:6:"height";s:6:"height";s:10:"tax_status";s:10:"tax_status";s:9:"tax_class";s:9:"tax_class";s:10:"upsell_ids";s:10:"upsell_ids";s:13:"crosssell_ids";s:13:"crosssell_ids";s:10:"file_paths";s:0:"";s:18:"downloadable_files";s:18:"downloadable_files";s:14:"download_limit";s:14:"download_limit";s:15:"download_expiry";s:15:"download_expiry";s:11:"product_url";s:11:"product_url";s:11:"button_text";s:11:"button_text";s:6:"images";s:6:"images";s:16:"tax:product_type";s:16:"tax:product_type";s:15:"tax:product_cat";s:15:"tax:product_cat";s:15:"tax:product_tag";s:15:"tax:product_tag";s:26:"tax:product_shipping_class";s:26:"tax:product_shipping_class";s:22:"tax:product_visibility";s:22:"tax:product_visibility";s:16:"attribute:cantos";s:16:"attribute:Cantos";s:21:"attribute_data:cantos";s:21:"attribute_data:Cantos";s:24:"attribute_default:cantos";s:24:"attribute_default:Cantos";s:15:"attribute:cores";s:15:"attribute:Cores";s:20:"attribute_data:cores";s:20:"attribute_data:Cores";s:23:"attribute_default:cores";s:23:"attribute_default:Cores";s:18:"attribute:molduras";s:18:"attribute:Molduras";s:23:"attribute_data:molduras";s:23:"attribute_data:Molduras";s:26:"attribute_default:molduras";s:26:"attribute_default:Molduras";s:17:"attribute:tamanho";s:17:"attribute:Tamanho";s:22:"attribute_data:tamanho";s:22:"attribute_data:Tamanho";s:25:"attribute_default:tamanho";s:25:"attribute_default:Tamanho";}i:1;a:56:{s:2:"id";s:0:"";s:10:"post_title";s:0:"";s:9:"post_name";s:0:"";s:11:"post_status";s:0:"";s:12:"post_content";s:0:"";s:12:"post_excerpt";s:0:"";s:9:"post_date";s:0:"";s:3:"sku";s:0:"";s:13:"post_password";s:0:"";s:11:"post_author";s:0:"";s:10:"menu_order";s:0:"";s:14:"comment_status";s:0:"";s:12:"downloadable";s:0:"";s:7:"virtual";s:0:"";s:10:"visibility";s:0:"";s:8:"featured";s:0:"";s:5:"stock";s:0:"";s:12:"stock_status";s:0:"";s:10:"backorders";s:0:"";s:12:"manage_stock";s:0:"";s:10:"sale_price";s:0:"";s:13:"regular_price";s:0:"";s:21:"sale_price_dates_from";s:0:"";s:19:"sale_price_dates_to";s:0:"";s:6:"weight";s:0:"";s:6:"length";s:0:"";s:5:"width";s:0:"";s:6:"height";s:0:"";s:10:"tax_status";s:0:"";s:9:"tax_class";s:0:"";s:10:"upsell_ids";s:0:"";s:13:"crosssell_ids";s:0:"";s:10:"file_paths";s:0:"";s:18:"downloadable_files";s:0:"";s:14:"download_limit";s:0:"";s:15:"download_expiry";s:0:"";s:11:"product_url";s:0:"";s:11:"button_text";s:0:"";s:6:"images";s:0:"";s:16:"tax:product_type";s:0:"";s:15:"tax:product_cat";s:0:"";s:15:"tax:product_tag";s:0:"";s:26:"tax:product_shipping_class";s:0:"";s:22:"tax:product_visibility";s:0:"";s:16:"attribute:cantos";s:0:"";s:21:"attribute_data:cantos";s:0:"";s:24:"attribute_default:cantos";s:0:"";s:15:"attribute:cores";s:0:"";s:20:"attribute_data:cores";s:0:"";s:23:"attribute_default:cores";s:0:"";s:18:"attribute:molduras";s:0:"";s:23:"attribute_data:molduras";s:0:"";s:26:"attribute_default:molduras";s:0:"";s:17:"attribute:tamanho";s:0:"";s:22:"attribute_data:tamanho";s:0:"";s:25:"attribute_default:tamanho";s:0:"";}}', 'yes'),
(321, '_transient_product_query-transient-version', '1538141629', 'yes'),
(324, '_transient_product-transient-version', '1538141630', 'yes'),
(335, '_transient_timeout_wc_var_prices_93', '1540733703', 'no'),
(336, '_transient_wc_var_prices_93', '{"version":"1538141630","676dce7cb3a82cb252d26c1518287bc9":{"price":[],"regular_price":[],"sale_price":[]}}', 'no'),
(339, '_transient_timeout_wc_var_prices_90', '1540733633', 'no'),
(340, '_transient_wc_var_prices_90', '{"version":"1538141630","676dce7cb3a82cb252d26c1518287bc9":{"price":[],"regular_price":[],"sale_price":[]}}', 'no'),
(343, '_transient_timeout_wc_var_prices_50', '1540733703', 'no'),
(344, '_transient_wc_var_prices_50', '{"version":"1538141630","676dce7cb3a82cb252d26c1518287bc9":{"price":[],"regular_price":[],"sale_price":[]}}', 'no'),
(347, '_transient_timeout_wc_var_prices_38', '1540733633', 'no'),
(348, '_transient_wc_var_prices_38', '{"version":"1538141630","676dce7cb3a82cb252d26c1518287bc9":{"price":[],"regular_price":[],"sale_price":[]}}', 'no'),
(365, 'wpcf7', 'a:2:{s:7:"version";s:5:"5.0.4";s:13:"bulk_validate";a:4:{s:9:"timestamp";d:1535023170;s:7:"version";s:5:"5.0.3";s:11:"count_valid";i:1;s:13:"count_invalid";i:0;}}', 'yes'),
(366, 'vsz_cf7_db_version', '1.4.4', 'yes'),
(367, 'selected_button', 'facebook,tweet,gplus,linkedin,pinterest', 'yes'),
(368, 'cresta_social_shares_selected_page', 'post,page,product', 'yes'),
(369, 'cresta_social_shares_float', 'left', 'yes'),
(370, 'cresta_social_shares_float_buttons', 'right', 'yes'),
(371, 'cresta_social_shares_style', 'first_style', 'yes'),
(372, 'cresta_social_shares_position_top', '20', 'yes'),
(373, 'cresta_social_shares_position_left', '20', 'yes'),
(374, 'cresta_social_shares_twitter_username', '', 'yes'),
(375, 'cresta_social_shares_show_counter', '', 'yes'),
(376, 'cresta_social_shares_show_ifmorezero', '', 'yes'),
(377, 'cresta_social_shares_show_ifmorenumber', '0', 'yes'),
(378, 'cresta_social_shares_show_total', '1', 'yes'),
(379, 'cresta_social_shares_total_text', 'Shares', 'yes'),
(380, 'cresta_social_shares_disable_mobile', '', 'yes'),
(381, 'cresta_social_shares_enable_animation', '', 'yes'),
(382, 'cresta_social_shares_enable_samecolors', '', 'yes'),
(383, 'cresta_social_shares_before_content', '', 'yes'),
(384, 'cresta_social_shares_after_content', '', 'yes'),
(385, 'cresta_social_shares_show_floatbutton', '', 'yes'),
(386, 'cresta_social_shares_show_credit', '', 'yes'),
(387, 'cresta_social_shares_enable_shadow', '', 'yes'),
(388, 'cresta_social_shares_enable_shadow_buttons', '', 'yes'),
(389, 'cresta_social_shares_z_index', '99', 'yes'),
(390, 'cresta_social_shares_button_hide_show', '', 'yes'),
(391, 'cresta_social_shares_custom_css', '', 'yes'),
(392, 'cresta_social_shares_twitter_shares', '', 'yes'),
(393, 'cresta_social_shares_twitter_shares_two', '', 'yes'),
(394, 'cresta_social_shares_facebook_appid', '', 'yes'),
(395, 'cresta_social_shares_facebook_appsecret', '', 'yes'),
(396, 'cresta_social_shares_pintmode', 'featimage', 'yes'),
(397, 'cresta_social_shares_linkedin_alternative_count', '0', 'yes'),
(398, 'cresta_social_shares_http_https_both', '', 'yes'),
(428, '_transient_orders-transient-version', '1538073166', 'yes'),
(491, 'woocommerce_meta_box_errors', 'a:0:{}', 'yes'),
(635, 'woocommerce_tracker_ua', 'a:3:{i:0;s:121:"mozilla/5.0 (macintosh; intel mac os x 10_13_2) applewebkit/537.36 (khtml, like gecko) chrome/66.0.3359.139 safari/537.36";i:1;s:121:"mozilla/5.0 (macintosh; intel mac os x 10_12_6) applewebkit/537.36 (khtml, like gecko) chrome/68.0.3440.106 safari/537.36";i:2;s:121:"mozilla/5.0 (macintosh; intel mac os x 10_12_6) applewebkit/537.36 (khtml, like gecko) chrome/69.0.3497.100 safari/537.36";}', 'yes'),
(711, 'br_filters_version', '1.2.6', 'yes'),
(712, 'berocket_admin_notices', 'a:1:{i:20;a:1:{i:0;a:1:{i:0;a:1:{s:9:"subscribe";a:15:{s:5:"start";i:0;s:3:"end";i:0;s:4:"name";s:9:"subscribe";s:4:"html";s:136:"Subscribe to get latest BeRocket news and updates, plugin recommendations and configuration help, promotional email with discount codes.";s:9:"righthtml";s:43:"<a class="berocket_no_thanks">No thanks</a>";s:10:"rightwidth";i:80;s:13:"nothankswidth";i:60;s:12:"contentwidth";i:400;s:9:"subscribe";b:1;s:6:"closed";i:2;s:8:"priority";i:20;s:6:"height";i:50;s:6:"repeat";b:0;s:11:"repeatcount";i:1;s:5:"image";a:4:{s:5:"local";s:115:"http://localhost/walldone-new/wp-content/plugins/woocommerce-ajax-filters/includes/../images/ad_white_on_orange.png";s:5:"width";i:239;s:6:"height";i:118;s:5:"scale";d:0.42372881355932202;}}}}}}', 'yes'),
(714, 'widget_berocket_aapf_widget', 'a:3:{i:2;a:16:{s:11:"widget_type";s:6:"filter";s:5:"title";s:18:"Filtrar por preço";s:9:"attribute";s:5:"price";s:4:"type";s:6:"slider";s:11:"product_cat";s:0:"";s:12:"scroll_theme";s:4:"dark";s:15:"cat_propagation";i:0;s:15:"select_multiple";i:0;s:9:"css_class";s:0:"";s:17:"text_before_price";s:0:"";s:16:"text_after_price";s:0:"";s:15:"order_values_by";s:0:"";s:17:"order_values_type";s:3:"asc";s:14:"is_hide_mobile";b:0;s:6:"height";s:4:"auto";s:8:"operator";s:3:"AND";}i:3;a:16:{s:11:"widget_type";s:6:"filter";s:5:"title";s:21:"Filtrar por atributos";s:9:"attribute";s:11:"pa_molduras";s:4:"type";s:8:"checkbox";s:11:"product_cat";s:0:"";s:12:"scroll_theme";s:4:"dark";s:15:"cat_propagation";i:0;s:15:"select_multiple";i:0;s:9:"css_class";s:0:"";s:17:"text_before_price";s:0:"";s:16:"text_after_price";s:0:"";s:15:"order_values_by";s:0:"";s:17:"order_values_type";s:3:"asc";s:14:"is_hide_mobile";b:0;s:6:"height";s:4:"auto";s:8:"operator";s:3:"AND";}s:12:"_multiwidget";i:1;}', 'yes'),
(715, 'berocket_current_displayed_notice', '', 'yes'),
(727, 'berocket_last_close_notices_time', '1535139650', 'yes'),
(728, '_transient_wc_attribute_taxonomies', 'a:1:{i:0;O:8:"stdClass":6:{s:12:"attribute_id";s:1:"1";s:14:"attribute_name";s:8:"molduras";s:15:"attribute_label";s:8:"Molduras";s:14:"attribute_type";s:6:"select";s:17:"attribute_orderby";s:10:"menu_order";s:16:"attribute_public";s:1:"0";}}', 'yes'),
(802, 'br_filters_options', 'a:13:{s:19:"no_products_message";s:50:"Não existem produtos para este tipo de critério.";s:17:"seo_friendly_urls";s:1:"1";s:10:"hide_value";a:1:{s:1:"o";s:1:"1";}s:18:"products_holder_id";s:11:"ul.products";s:30:"woocommerce_result_count_class";s:25:".woocommerce-result-count";s:26:"woocommerce_ordering_class";s:25:"form.woocommerce-ordering";s:28:"woocommerce_pagination_class";s:23:".woocommerce-pagination";s:17:"no_products_class";s:0:"";s:17:"ajax_request_load";s:1:"1";s:23:"ajax_request_load_style";s:6:"jquery";s:9:"user_func";a:3:{s:13:"before_update";s:0:"";s:9:"on_update";s:0:"";s:12:"after_update";s:0:"";}s:15:"user_custom_css";s:0:"";s:13:"br_opened_tab";s:7:"general";}', 'yes'),
(810, 'woocommerce_catalog_columns', '3', 'yes'),
(811, 'woocommerce_catalog_rows', '3', 'yes'),
(826, 'woocommerce_default_catalog_orderby', 'popularity', 'yes'),
(854, 'woocommerce_simple_registration_register_page', '8', 'yes'),
(855, 'woocommerce_simple_registration_name_fields', 'yes', 'yes'),
(856, 'woocommerce_simple_registration_name_fields_required', 'yes', 'yes'),
(908, 'acf_version', '4.4.12', 'yes'),
(913, 'duplicate_page_options', 'a:3:{s:21:"duplicate_post_status";s:5:"draft";s:23:"duplicate_post_redirect";s:7:"to_list";s:21:"duplicate_post_suffix";s:0:"";}', 'yes'),
(932, 'category_children', 'a:0:{}', 'yes'),
(1071, 'woocommerce_version', '3.4.5', 'yes'),
(1072, 'woocommerce_db_version', '3.4.5', 'yes'),
(1138, 'wordpress_file_upload_table_log_version', '4.0', 'yes'),
(1139, 'wordpress_file_upload_table_userdata_version', '1.0', 'yes'),
(1140, 'wordpress_file_upload_table_dbxqueue_version', '1.0', 'yes'),
(1141, 'widget_wordpress_file_upload_widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(1142, 'wordpress_file_upload_last_idlog', 'a:3:{s:3:"pre";N;s:4:"post";N;s:4:"time";i:1536162404;}', 'yes'),
(1143, 'wfu_params_index', 'Q1GRstnNk3LLbrG5||166||1||agencia904', 'yes'),
(1144, 'wfu_params_Q1GRstnNk3LLbrG5', '7b227769646765746964223a22222c2275706c6f61646964223a2231222c2273696e676c65627574746f6e223a2266616c7365222c2275706c6f616470617468223a2275706c6f616473222c226669746d6f6465223a226669786564222c22616c6c6f776e6f66696c65223a2266616c7365222c2272657365746d6f6465223a22616c77617973222c2275706c6f6164726f6c65223a22616c6c2c677565737473222c2275706c6f61647061747465726e73223a222a2e2a222c226d617873697a65223a223130222c2263726561746570617468223a2274727565222c22666f72636566696c656e616d65223a2266616c7365222c226163636573736d6574686f64223a226e6f726d616c222c22667470696e666f223a22222c22757365667470646f6d61696e223a2266616c7365222c22667470706173736976656d6f6465223a2266616c7365222c2266747066696c657065726d697373696f6e73223a22222c2273686f77746172676574666f6c646572223a2274727565222c2261736b666f72737562666f6c64657273223a2266616c7365222c22737562666f6c64657274726565223a22222c226475706c696361746573706f6c696379223a226d61696e7461696e20626f7468222c22756e697175657061747465726e223a22696e646578222c227265646972656374223a2266616c7365222c2272656469726563746c696e6b223a22222c2261646d696e6d65737361676573223a66616c73652c22666f726365636c6173736963223a2266616c7365222c22746573746d6f6465223a2266616c7365222c2264656275676d6f6465223a2266616c7365222c22706c6163656d656e7473223a227469746c655c2f66696c656e616d652b73656c656374627574746f6e2b75706c6f6164627574746f6e5c2f737562666f6c646572735c2f75736572646174615c2f6d657373616765222c2275706c6f61647469746c65223a22456e76696172206172717569766f73222c2273656c656374627574746f6e223a2253656c6563696f6e6172206172717569766f222c2275706c6f6164627574746f6e223a22456e76696172206172717569766f222c22746172676574666f6c6465726c6162656c223a224d6575204b6974222c22737562666f6c6465726c6162656c223a2253656c65637420537562666f6c646572222c22737563636573736d657373616765223a224172717569766f202566696c656e616d652520656e766961646f20636f6d207375636573736f21222c227761726e696e676d657373616765223a224172717569766f202566696c656e616d652520656e766961646f20636f6d207375636573736f206d617320636f6d20617669736f73222c226572726f726d657373616765223a224172717569766f202566696c656e616d6525206e5c75303065336f20666f6920656e766961646f222c22776169746d657373616765223a224172717569766f202566696c656e616d6525206573745c75303065312073656e646f20656e766961646f222c2275706c6f61646d65646961627574746f6e223a2255706c6f6164204d65646961222c22766964656f6e616d65223a22766964656f73747265616d222c22696d6167656e616d65223a2273637265656e73686f74222c2272657175697265646c6162656c223a22286f6272696761745c753030663372696f29222c226e6f74696679223a2266616c7365222c226e6f74696679726563697069656e7473223a22222c226e6f7469667968656164657273223a22222c226e6f746966797375626a656374223a224e6f7469666963615c75303065375c75303065336f20646520656e76696f206465206172717569766f222c226e6f746966796d657373616765223a224361726f2064657374696e61745c753030653172696f2c20256e25256e25202045737361205c753030653920756d61206d656e736167656d20656e7669616461206175746f6d61746963616d656e74652070617261206e6f7469666963612d6c6f2071756520756d206e6f766f206172717569766f20666f6920656e766961646f2e256e25256e254174656e63696f73616d656e7465222c2261747461636866696c65223a2266616c7365222c2261736b636f6e73656e74223a2266616c7365222c22706572736f6e616c646174617479706573223a227573657264617461222c22636f6e73656e74666f726d6174223a22636865636b626f78222c22636f6e73656e7470726573656c656374223a2266616c7365222c22636f6e73656e747175657374696f6e223a2242792061637469766174696e672074686973206f7074696f6e204920616772656520746f206c6574207468652077656273697465206b656570206d7920706572736f6e616c2064617461222c22636f6e73656e74646973636c61696d6572223a22222c22737563636573736d657373616765636f6c6f72223a226565222c22737563636573736d657373616765636f6c6f7273223a22233030363630302c234545464645452c23303036363636222c227761726e696e676d657373616765636f6c6f7273223a22234638383031372c234645463245372c23363333333039222c226661696c6d657373616765636f6c6f7273223a22233636303030302c234646454545452c23363636363030222c22776169746d657373616765636f6c6f7273223a22233636363636362c234545454545452c23333333333333222c22776964746873223a22706c7567696e3a313030252c207469746c653a313030252c2066696c656e616d653a313030252c2073656c656374627574746f6e3a313030252c2075706c6f6164627574746f6e3a313030252c2075706c6f6164666f6c6465725f6c6162656c3a313030252c20737562666f6c646572733a313030252c20737562666f6c646572735f6c6162656c3a313030252c20737562666f6c646572735f73656c6563743a313030252c2077656263616d3a313030252c2070726f67726573736261723a313030252c2075736572646174613a313030252c2075736572646174615f6c6162656c3a313030252c2075736572646174615f76616c75653a313030252c20636f6e73656e743a313030252c206d6573736167653a31303025222c2268656967687473223a22222c227573657264617461223a2266616c7365222c2275736572646174616c6162656c223a22537561206d656e736167656d7c743a746578747c733a6c6566747c723a307c613a307c703a696e6c696e657c643a222c2266696c65626173656c696e6b223a2266616c7365222c226d656469616c696e6b223a2266616c7365222c22706f73746c696e6b223a2266616c7365222c2277656263616d223a2266616c7365222c2277656263616d6d6f6465223a226361707475726520766964656f222c22617564696f63617074757265223a2266616c7365222c22766964656f7769647468223a22222c22766964656f686569676874223a22222c22766964656f617370656374726174696f223a22222c22766964656f6672616d6572617465223a22222c2263616d657261666163696e67223a22616e79222c226d61787265636f726474696d65223a223130222c22706167656964223a3136362c22626c6f676964223a312c227068705f656e76223a223634626974222c2261646d696e6572726f7273223a22222c2275736572646174615f6669656c6473223a5b5d2c22737562666f6c646572736172726179223a5b5d2c227375626469725f73656c656374696f6e5f696e646578223a222d31227d', 'yes'),
(1145, 'wordpress_file_upload_options', 'version=1.0;shortcode=;hashfiles=1;basedir=6d65752d6b6974;personaldata=0;postmethod=fopen;modsecurity=;userstatehandler=session;relaxcss=0;admindomain=siteurl;mediacustom=1;createthumbnails=;includeotherfiles=0;altserver=0;captcha_sitekey=;captcha_secretkey=;browser_permissions=', 'yes'),
(1221, 'frontend_uploader_settings', 'a:13:{s:25:"enable_akismet_protection";s:3:"off";s:27:"enable_recaptcha_protection";s:3:"off";s:18:"recaptcha_site_key";s:0:"";s:20:"recaptcha_secret_key";s:0:"";s:12:"notify_admin";s:3:"off";s:23:"admin_notification_text";s:132:"Someone uploaded a new UGC file, please moderate at: http://localhost/walldone-new/wp-admin/upload.php?page=manage_frontend_uploader";s:18:"notification_email";s:0:"";s:11:"show_author";s:3:"off";s:15:"wysiwyg_enabled";s:2:"on";s:23:"auto_approve_user_files";s:2:"on";s:22:"auto_approve_any_files";s:3:"off";s:19:"obfuscate_file_name";s:3:"off";s:23:"suppress_default_fields";s:3:"off";}', 'yes'),
(1226, 'usp_options', 'a:70:{s:11:"usp_version";i:20180822;s:13:"version_alert";i:0;s:15:"default_options";i:0;s:6:"author";s:1:"1";s:10:"categories";a:1:{i:0;s:1:"1";}s:15:"number-approved";i:-1;s:12:"redirect-url";s:0:"";s:13:"error-message";s:116:"There was an error. Please ensure that you have added a title, some content, and that you have uploaded only images.";s:10:"min-images";i:0;s:10:"max-images";i:1;s:16:"min-image-height";i:0;s:15:"min-image-width";i:0;s:16:"max-image-height";i:1500;s:15:"max-image-width";i:1500;s:8:"usp_name";s:4:"show";s:7:"usp_url";s:4:"show";s:9:"usp_email";s:4:"hide";s:9:"usp_title";s:4:"show";s:8:"usp_tags";s:4:"show";s:12:"usp_category";s:4:"show";s:10:"usp_images";s:4:"hide";s:14:"upload-message";s:38:"Please select your image(s) to upload.";s:12:"usp_question";s:7:"1 + 1 =";s:12:"usp_response";s:1:"2";s:10:"usp_casing";i:0;s:11:"usp_captcha";s:4:"show";s:11:"usp_content";s:4:"show";s:15:"success-message";s:39:"Success! Thank you for your submission.";s:16:"usp_form_version";s:7:"current";s:16:"usp_email_alerts";i:1;s:14:"usp_email_html";i:0;s:17:"usp_email_address";s:10:"web@904.ag";s:14:"usp_email_from";s:10:"web@904.ag";s:14:"usp_use_author";i:0;s:11:"usp_use_url";i:0;s:11:"usp_use_cat";i:0;s:14:"usp_use_cat_id";s:0:"";s:14:"usp_include_js";i:1;s:15:"usp_display_url";s:0:"";s:16:"usp_form_content";s:0:"";s:19:"usp_richtext_editor";i:0;s:19:"usp_featured_images";i:0;s:15:"usp_add_another";s:0:"";s:16:"disable_required";i:0;s:13:"titles_unique";i:0;s:17:"enable_shortcodes";i:0;s:19:"disable_ip_tracking";i:0;s:19:"email_alert_subject";s:0:"";s:19:"email_alert_message";s:0:"";s:19:"auto_display_images";s:7:"disable";s:18:"auto_display_email";s:7:"disable";s:16:"auto_display_url";s:7:"disable";s:17:"auto_image_markup";s:130:"<a href="%%full%%"><img src="%%thumb%%" width="%%width%%" height="%%height%%" alt="%%title%%" style="display:inline-block" /></a> ";s:17:"auto_email_markup";s:43:"<p><a href="mailto:%%email%%">Email</a></p>";s:15:"auto_url_markup";s:32:"<p><a href="%%url%%">URL</a></p>";s:15:"logged_in_users";i:0;s:14:"disable_author";i:0;s:16:"recaptcha_public";s:0:"";s:17:"recaptcha_private";s:0:"";s:13:"usp_recaptcha";s:4:"hide";s:13:"usp_post_type";s:4:"post";s:12:"custom_field";s:4:"hide";s:11:"custom_name";s:16:"usp_custom_field";s:12:"custom_label";s:12:"Custom Field";s:19:"auto_display_custom";s:7:"disable";s:18:"auto_custom_markup";s:60:"<p>%%custom_label%% : %%custom_name%% : %%custom_value%%</p>";s:15:"custom_checkbox";i:0;s:20:"custom_checkbox_name";s:19:"usp_custom_checkbox";s:20:"custom_checkbox_text";s:25:"I agree the to the terms.";s:19:"custom_checkbox_err";s:24:"Custom checkbox required";}', 'yes'),
(1269, '_transient_timeout_wc_shipping_method_count_0_1535033341', '1539196929', 'no'),
(1270, '_transient_wc_shipping_method_count_0_1535033341', '2', 'no'),
(1354, '_transient_timeout_wc_product_loopd3991537405113', '1539997966', 'no'),
(1355, '_transient_wc_product_loopd3991537405113', 'O:8:"stdClass":5:{s:3:"ids";a:1:{i:0;i:242;}s:5:"total";i:1;s:11:"total_pages";i:1;s:8:"per_page";i:1;s:12:"current_page";i:1;}', 'no'),
(1377, 'yit_recently_activated', 'a:1:{i:0;s:40:"yith-woocommerce-featured-video/init.php";}', 'yes'),
(1405, 'yith_wfbt_enable_integration', 'no', 'yes'),
(1412, 'yit_plugin_fw_panel_wc_default_options_set', 'a:2:{s:15:"yith_wcwl_panel";b:1;s:28:"yith_wc_featured_audio_video";b:1;}', 'yes'),
(1462, 'tinvwl_utm_source', 'wordpress_org', 'yes'),
(1463, 'tinvwl_utm_medium', 'organic', 'yes'),
(1464, 'tinvwl_utm_campaign', 'organic', 'yes'),
(1465, 'widget_widget_top_wishlist', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(1473, '_transient_timeout_wc_product_loopd3991537454934', '1540047685', 'no'),
(1474, '_transient_wc_product_loopd3991537454934', 'O:8:"stdClass":5:{s:3:"ids";a:1:{i:0;i:242;}s:5:"total";i:1;s:11:"total_pages";i:1;s:8:"per_page";i:1;s:12:"current_page";i:1;}', 'no'),
(1482, 'woosw_disable_unauthenticated', 'no', 'yes'),
(1483, 'woosw_page_id', '248', 'yes'),
(1484, 'woosw_page_share', 'no', 'yes'),
(1485, 'woosw_button_type', 'button', 'yes'),
(1486, 'woosw_button_text', 'Adicionar ao Kit', 'yes'),
(1487, 'woosw_button_text_added', 'Alterar no Kit', 'yes'),
(1488, 'woosw_button_class', '', 'yes'),
(1489, 'woosw_button_position_archive', 'after_add_to_cart', 'yes'),
(1490, 'woosw_button_position_single', '31', 'yes'),
(1491, 'woosw_color', '#5fbd74', 'yes'),
(1492, 'woosw_menus', '', 'yes'),
(1493, 'woosw_menu_action', 'open_page', 'yes'),
(1496, 'woosw_list_P5372Q', 'a:4:{i:90;i:1537997812;i:93;i:1537997295;i:96;i:1537551050;i:129;i:1537456974;}', 'yes'),
(1520, '_transient_timeout_wc_product_loopd3991537460225', '1540053591', 'no'),
(1521, '_transient_wc_product_loopd3991537460225', 'O:8:"stdClass":5:{s:3:"ids";a:1:{i:0;i:242;}s:5:"total";i:1;s:11:"total_pages";i:1;s:8:"per_page";i:1;s:12:"current_page";i:1;}', 'no'),
(1540, 'wccpf_options', 'a:20:{s:16:"show_custom_data";s:3:"yes";s:14:"field_location";s:37:"woocommerce_before_add_to_cart_button";s:17:"product_tab_title";s:0:"";s:20:"product_tab_priority";s:2:"30";s:14:"fields_cloning";s:2:"no";s:18:"fields_group_title";s:0:"";s:18:"group_meta_on_cart";s:2:"no";s:20:"group_fields_on_cart";s:2:"no";s:16:"show_group_title";s:2:"no";s:22:"client_side_validation";s:2:"no";s:27:"client_side_validation_type";s:6:"submit";s:20:"show_login_user_only";s:2:"no";s:26:"edit_field_value_cart_page";s:2:"no";s:19:"enable_multilingual";s:2:"no";s:25:"enable_ajax_pricing_rules";s:7:"disable";s:34:"ajax_pricing_rules_price_container";s:7:"default";s:28:"ajax_price_replace_container";s:0:"";s:21:"pricing_rules_details";s:4:"show";s:24:"ajax_pricing_rules_title";s:4:"hide";s:31:"ajax_pricing_rules_title_header";s:0:"";}', 'yes'),
(1594, '_transient_timeout_wc_shipping_method_count_1_1535033341', '1540222683', 'no'),
(1595, '_transient_wc_shipping_method_count_1_1535033341', '2', 'no'),
(1708, '_transient_timeout_wc_product_children_93', '1540579077', 'no'),
(1709, '_transient_wc_product_children_93', 'a:2:{s:3:"all";a:0:{}s:7:"visible";a:0:{}}', 'no'),
(1710, '_transient_timeout_wc_product_children_90', '1540579077', 'no'),
(1711, '_transient_wc_product_children_90', 'a:2:{s:3:"all";a:0:{}s:7:"visible";a:0:{}}', 'no'),
(1712, '_transient_timeout_wc_product_children_50', '1540579077', 'no'),
(1713, '_transient_wc_product_children_50', 'a:2:{s:3:"all";a:0:{}s:7:"visible";a:0:{}}', 'no'),
(1714, '_transient_timeout_wc_product_children_38', '1540579077', 'no'),
(1715, '_transient_wc_product_children_38', 'a:2:{s:3:"all";a:0:{}s:7:"visible";a:0:{}}', 'no'),
(1717, '_transient_timeout_external_ip_address_::1', '1538593378', 'no'),
(1718, '_transient_external_ip_address_::1', '177.35.211.135', 'no'),
(1720, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:"auto_add";a:0:{}}', 'yes'),
(1750, '_site_transient_update_core', 'O:8:"stdClass":4:{s:7:"updates";a:1:{i:0;O:8:"stdClass":10:{s:8:"response";s:6:"latest";s:8:"download";s:65:"https://downloads.wordpress.org/release/pt_BR/wordpress-4.9.8.zip";s:6:"locale";s:5:"pt_BR";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:65:"https://downloads.wordpress.org/release/pt_BR/wordpress-4.9.8.zip";s:10:"no_content";b:0;s:11:"new_bundled";b:0;s:7:"partial";b:0;s:8:"rollback";b:0;}s:7:"current";s:5:"4.9.8";s:7:"version";s:5:"4.9.8";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"4.7";s:15:"partial_version";s:0:"";}}s:12:"last_checked";i:1538532362;s:15:"version_checked";s:5:"4.9.8";s:12:"translations";a:0:{}}', 'no'),
(1751, '_site_transient_update_themes', 'O:8:"stdClass":4:{s:12:"last_checked";i:1538532365;s:7:"checked";a:1:{s:8:"walldone";s:3:"3.0";}s:8:"response";a:0:{}s:12:"translations";a:0:{}}', 'no'),
(1761, 'woocommerce_bacs_accounts', 'a:1:{i:0;a:6:{s:12:"account_name";s:5:"Teste";s:14:"account_number";s:5:"12345";s:9:"bank_name";s:13:"Banco da Loja";s:9:"sort_code";s:5:"54312";s:4:"iban";s:0:"";s:3:"bic";s:0:"";}}', 'yes'),
(1764, '_transient_timeout_wc_child_has_weight_50', '1540664526', 'no'),
(1765, '_transient_wc_child_has_weight_50', '', 'no'),
(1766, '_transient_timeout_wc_child_has_dimensions_50', '1540664526', 'no'),
(1767, '_transient_wc_child_has_dimensions_50', '', 'no'),
(1780, '_transient_timeout_wc_child_has_weight_93', '1540667858', 'no'),
(1781, '_transient_wc_child_has_weight_93', '', 'no'),
(1782, '_transient_timeout_wc_child_has_dimensions_93', '1540667858', 'no'),
(1783, '_transient_wc_child_has_dimensions_93', '', 'no'),
(1845, 'woo_variation_gallery_thumbnails_columns', '4', 'yes'),
(1846, 'woo_variation_gallery_thumbnails_gap', '0', 'yes'),
(1847, 'woo_variation_gallery_width', '100', 'yes'),
(1848, 'woo_variation_gallery_margin', '30', 'yes'),
(1849, 'woo_variation_gallery_reset_on_variation_change', 'yes', 'yes'),
(1883, 'redux_version_upgraded_from', '3.6.11', 'yes'),
(1885, 'woosvi_options', 'a:8:{s:8:"last_tab";s:0:"";s:7:"default";s:1:"1";s:8:"lightbox";s:0:"";s:4:"lens";s:0:"";s:7:"columns";s:1:"4";s:11:"hide_thumbs";s:0:"";s:12:"imagesloaded";s:0:"";s:11:"svi204_info";b:0;}', 'yes'),
(1886, 'woosvi_options-transients', 'a:2:{s:14:"changed_values";a:1:{s:7:"default";b:0;}s:9:"last_save";i:1538134005;}', 'yes'),
(1975, 'ywcfav_aspectratio', '16_9', 'yes'),
(1994, '_transient_wc_count_comments', 'O:8:"stdClass":7:{s:14:"total_comments";i:4;s:3:"all";i:4;s:8:"approved";s:1:"4";s:9:"moderated";i:0;s:4:"spam";i:0;s:5:"trash";i:0;s:12:"post-trashed";i:0;}', 'yes'),
(1995, '_transient_timeout_wc_term_counts', '1540987372', 'no'),
(1996, '_transient_wc_term_counts', 'a:22:{i:15;s:1:"7";i:37;s:1:"1";i:20;s:0:"";i:29;s:0:"";i:38;s:0:"";i:36;s:1:"1";i:30;s:0:"";i:21;s:0:"";i:22;s:0:"";i:23;s:0:"";i:31;s:0:"";i:32;s:0:"";i:33;s:0:"";i:34;s:0:"";i:39;s:0:"";i:35;s:0:"";i:24;s:0:"";i:25;s:0:"";i:26;s:0:"";i:27;s:0:"";i:28;s:0:"";i:40;s:0:"";}', 'no'),
(1997, '_transient_timeout_wc_product_children_96', '1540733632', 'no'),
(1998, '_transient_wc_product_children_96', 'a:2:{s:3:"all";a:9:{i:0;i:107;i:1;i:106;i:2;i:108;i:3;i:109;i:4;i:110;i:5;i:111;i:6;i:112;i:7;i:113;i:8;i:114;}s:7:"visible";a:9:{i:0;i:107;i:1;i:106;i:2;i:108;i:3;i:109;i:4;i:110;i:5;i:111;i:6;i:112;i:7;i:113;i:8;i:114;}}', 'no'),
(1999, '_transient_timeout_wc_var_prices_96', '1540733632', 'no'),
(2000, '_transient_wc_var_prices_96', '{"version":"1538141630","676dce7cb3a82cb252d26c1518287bc9":{"price":{"107":"29.90","106":"22.00","109":"20.00","110":"20.00","111":"20.00","112":"20.00","113":"20.00","114":"20.00"},"regular_price":{"107":"45.90","106":"40.00","109":"40.00","110":"40.00","111":"40.00","112":"40.00","113":"40.00","114":"40.00"},"sale_price":{"107":"29.90","106":"22.00","109":"20.00","110":"20.00","111":"20.00","112":"20.00","113":"20.00","114":"20.00"}}}', 'no'),
(2034, '_transient_timeout_wc_child_has_weight_38', '1540987233', 'no'),
(2035, '_transient_wc_child_has_weight_38', '', 'no'),
(2036, '_transient_timeout_wc_child_has_dimensions_38', '1540987233', 'no'),
(2037, '_transient_wc_child_has_dimensions_38', '', 'no'),
(2071, '_transient_timeout_plugin_slugs', '1538491603', 'no'),
(2072, '_transient_plugin_slugs', 'a:18:{i:0;s:48:"woocommerce-ajax-filters/woocommerce-filters.php";i:1;s:35:"advanced-cf7-db/advanced-cf7-db.php";i:2;s:30:"advanced-custom-fields/acf.php";i:3;s:59:"banner-management-for-woocommerce/woo-banner-management.php";i:4;s:37:"breadcrumb-trail/breadcrumb-trail.php";i:5;s:36:"contact-form-7/wp-contact-form-7.php";i:6;s:59:"cresta-social-share-counter/cresta-social-share-counter.php";i:7;s:32:"duplicate-page/duplicatepage.php";i:8;s:59:"product-import-export-for-woo/product-csv-import-export.php";i:9;s:67:"woocommerce-simple-registration/woocommerce-simple-registration.php";i:10;s:45:"user-submitted-posts/user-submitted-posts.php";i:11;s:26:"wc-fields-factory/wcff.php";i:12;s:27:"woocommerce/woocommerce.php";i:13;s:37:"woocommerce-ajax-cart/wooajaxcart.php";i:14;s:45:"woocommerce-correios/woocommerce-correios.php";i:15;s:51:"woocommerce-product-image-flipper/image-flipper.php";i:16;s:28:"woo-smart-wishlist/index.php";i:17;s:20:"wp-dropzone/init.php";}', 'no'),
(2074, 'product_cat_15_imagem', '261', 'no'),
(2075, '_product_cat_15_imagem', 'field_5bb23368f5bd9', 'no'),
(2076, 'product_cat_15_link', 'http://walldone/categoria/produtos/', 'no'),
(2077, '_product_cat_15_link', 'field_5bb23376f5bda', 'no'),
(2089, 'product_cat_15_titulo_descricao', 'Lorem ipsum dolor sit amet, consectetur adipiscing', 'no'),
(2090, '_product_cat_15_titulo_descricao', 'field_5bb270375a8ed', 'no'),
(2091, 'product_cat_15_conteudo_descricao', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut.', 'no'),
(2092, '_product_cat_15_conteudo_descricao', 'field_5bb270755a8ee', 'no'),
(2093, 'product_cat_children', 'a:4:{i:15;a:3:{i:0;i:20;i:1;i:28;i:2;i:36;}i:20;a:7:{i:0;i:21;i:1;i:22;i:2;i:23;i:3;i:24;i:4;i:25;i:5;i:26;i:6;i:27;}i:28;a:7:{i:0;i:29;i:1;i:30;i:2;i:31;i:3;i:32;i:4;i:33;i:5;i:34;i:6;i:35;}i:36;a:4:{i:0;i:37;i:1;i:38;i:2;i:39;i:3;i:40;}}', 'yes'),
(2138, '_transient_timeout_wc_related_96', '1538596361', 'no'),
(2139, '_transient_wc_related_96', 'a:1:{s:50:"limit=4&exclude_ids%5B0%5D=0&exclude_ids%5B1%5D=96";a:6:{i:0;s:2:"38";i:1;s:2:"50";i:2;s:2:"90";i:3;s:2:"93";i:4;s:2:"99";i:5;s:3:"129";}}', 'no'),
(2143, '_transient_timeout_wc_related_129', '1538615219', 'no'),
(2144, '_transient_wc_related_129', 'a:1:{s:51:"limit=4&exclude_ids%5B0%5D=0&exclude_ids%5B1%5D=129";a:6:{i:0;s:2:"38";i:1;s:2:"50";i:2;s:2:"90";i:3;s:2:"93";i:4;s:2:"96";i:5;s:2:"99";}}', 'no'),
(2148, '_site_transient_timeout_theme_roots', '1538534164', 'no'),
(2149, '_site_transient_theme_roots', 'a:1:{s:8:"walldone";s:7:"/themes";}', 'no'),
(2150, '_site_transient_update_plugins', 'O:8:"stdClass":5:{s:12:"last_checked";i:1538532366;s:7:"checked";a:18:{s:48:"woocommerce-ajax-filters/woocommerce-filters.php";s:5:"1.2.6";s:35:"advanced-cf7-db/advanced-cf7-db.php";s:5:"1.4.4";s:30:"advanced-custom-fields/acf.php";s:6:"4.4.12";s:59:"banner-management-for-woocommerce/woo-banner-management.php";s:5:"1.1.3";s:37:"breadcrumb-trail/breadcrumb-trail.php";s:5:"1.1.0";s:36:"contact-form-7/wp-contact-form-7.php";s:5:"5.0.4";s:59:"cresta-social-share-counter/cresta-social-share-counter.php";s:5:"2.7.9";s:32:"duplicate-page/duplicatepage.php";s:3:"2.7";s:59:"product-import-export-for-woo/product-csv-import-export.php";s:5:"1.4.6";s:67:"woocommerce-simple-registration/woocommerce-simple-registration.php";s:5:"1.5.2";s:45:"user-submitted-posts/user-submitted-posts.php";s:8:"20180822";s:26:"wc-fields-factory/wcff.php";s:5:"3.0.2";s:27:"woocommerce/woocommerce.php";s:5:"3.4.5";s:37:"woocommerce-ajax-cart/wooajaxcart.php";s:5:"1.2.6";s:45:"woocommerce-correios/woocommerce-correios.php";s:5:"3.7.1";s:51:"woocommerce-product-image-flipper/image-flipper.php";s:5:"0.4.1";s:28:"woo-smart-wishlist/index.php";s:9:"99999.2.5";s:20:"wp-dropzone/init.php";s:10:"9999999999";}s:8:"response";a:4:{s:48:"woocommerce-ajax-filters/woocommerce-filters.php";O:8:"stdClass":12:{s:2:"id";s:38:"w.org/plugins/woocommerce-ajax-filters";s:4:"slug";s:24:"woocommerce-ajax-filters";s:6:"plugin";s:48:"woocommerce-ajax-filters/woocommerce-filters.php";s:11:"new_version";s:5:"1.2.7";s:3:"url";s:55:"https://wordpress.org/plugins/woocommerce-ajax-filters/";s:7:"package";s:73:"https://downloads.wordpress.org/plugin/woocommerce-ajax-filters.1.2.7.zip";s:5:"icons";a:2:{s:2:"2x";s:77:"https://ps.w.org/woocommerce-ajax-filters/assets/icon-256x256.png?rev=1720711";s:2:"1x";s:77:"https://ps.w.org/woocommerce-ajax-filters/assets/icon-128x128.png?rev=1720711";}s:7:"banners";a:1:{s:2:"1x";s:79:"https://ps.w.org/woocommerce-ajax-filters/assets/banner-772x250.png?rev=1720711";}s:11:"banners_rtl";a:0:{}s:6:"tested";s:5:"4.9.8";s:12:"requires_php";b:0;s:13:"compatibility";O:8:"stdClass":0:{}}s:30:"advanced-custom-fields/acf.php";O:8:"stdClass":12:{s:2:"id";s:36:"w.org/plugins/advanced-custom-fields";s:4:"slug";s:22:"advanced-custom-fields";s:6:"plugin";s:30:"advanced-custom-fields/acf.php";s:11:"new_version";s:5:"5.7.6";s:3:"url";s:53:"https://wordpress.org/plugins/advanced-custom-fields/";s:7:"package";s:71:"https://downloads.wordpress.org/plugin/advanced-custom-fields.5.7.6.zip";s:5:"icons";a:2:{s:2:"2x";s:75:"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png?rev=1082746";s:2:"1x";s:75:"https://ps.w.org/advanced-custom-fields/assets/icon-128x128.png?rev=1082746";}s:7:"banners";a:2:{s:2:"2x";s:78:"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099";s:2:"1x";s:77:"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102";}s:11:"banners_rtl";a:0:{}s:6:"tested";s:5:"4.9.9";s:12:"requires_php";b:0;s:13:"compatibility";O:8:"stdClass":0:{}}s:59:"cresta-social-share-counter/cresta-social-share-counter.php";O:8:"stdClass":12:{s:2:"id";s:41:"w.org/plugins/cresta-social-share-counter";s:4:"slug";s:27:"cresta-social-share-counter";s:6:"plugin";s:59:"cresta-social-share-counter/cresta-social-share-counter.php";s:11:"new_version";s:5:"2.8.0";s:3:"url";s:58:"https://wordpress.org/plugins/cresta-social-share-counter/";s:7:"package";s:76:"https://downloads.wordpress.org/plugin/cresta-social-share-counter.2.8.0.zip";s:5:"icons";a:2:{s:2:"2x";s:79:"https://ps.w.org/cresta-social-share-counter/assets/icon-256x256.png?rev=980073";s:2:"1x";s:79:"https://ps.w.org/cresta-social-share-counter/assets/icon-128x128.png?rev=980072";}s:7:"banners";a:2:{s:2:"2x";s:82:"https://ps.w.org/cresta-social-share-counter/assets/banner-1544x500.png?rev=961888";s:2:"1x";s:81:"https://ps.w.org/cresta-social-share-counter/assets/banner-772x250.png?rev=961888";}s:11:"banners_rtl";a:0:{}s:6:"tested";s:3:"5.0";s:12:"requires_php";b:0;s:13:"compatibility";O:8:"stdClass":0:{}}s:59:"product-import-export-for-woo/product-csv-import-export.php";O:8:"stdClass":13:{s:2:"id";s:43:"w.org/plugins/product-import-export-for-woo";s:4:"slug";s:29:"product-import-export-for-woo";s:6:"plugin";s:59:"product-import-export-for-woo/product-csv-import-export.php";s:11:"new_version";s:5:"1.4.7";s:3:"url";s:60:"https://wordpress.org/plugins/product-import-export-for-woo/";s:7:"package";s:78:"https://downloads.wordpress.org/plugin/product-import-export-for-woo.1.4.7.zip";s:5:"icons";a:2:{s:2:"2x";s:82:"https://ps.w.org/product-import-export-for-woo/assets/icon-256x256.jpg?rev=1591128";s:2:"1x";s:82:"https://ps.w.org/product-import-export-for-woo/assets/icon-128x128.jpg?rev=1906359";}s:7:"banners";a:2:{s:2:"2x";s:85:"https://ps.w.org/product-import-export-for-woo/assets/banner-1544x500.jpg?rev=1689130";s:2:"1x";s:84:"https://ps.w.org/product-import-export-for-woo/assets/banner-772x250.jpg?rev=1906359";}s:11:"banners_rtl";a:0:{}s:14:"upgrade_notice";s:99:"<ul>\n<li>Tested OK with WC 3.4.5</li>\n<li>Review link added in footer.</li>\n<li>Bug fix.</li>\n</ul>";s:6:"tested";s:5:"4.9.8";s:12:"requires_php";b:0;s:13:"compatibility";O:8:"stdClass":0:{}}}s:12:"translations";a:0:{}s:9:"no_update";a:14:{s:35:"advanced-cf7-db/advanced-cf7-db.php";O:8:"stdClass":9:{s:2:"id";s:29:"w.org/plugins/advanced-cf7-db";s:4:"slug";s:15:"advanced-cf7-db";s:6:"plugin";s:35:"advanced-cf7-db/advanced-cf7-db.php";s:11:"new_version";s:5:"1.4.4";s:3:"url";s:46:"https://wordpress.org/plugins/advanced-cf7-db/";s:7:"package";s:64:"https://downloads.wordpress.org/plugin/advanced-cf7-db.1.4.4.zip";s:5:"icons";a:2:{s:2:"2x";s:68:"https://ps.w.org/advanced-cf7-db/assets/icon-256x256.jpg?rev=1696186";s:2:"1x";s:68:"https://ps.w.org/advanced-cf7-db/assets/icon-128x128.jpg?rev=1696186";}s:7:"banners";a:2:{s:2:"2x";s:71:"https://ps.w.org/advanced-cf7-db/assets/banner-1544x500.jpg?rev=1696186";s:2:"1x";s:70:"https://ps.w.org/advanced-cf7-db/assets/banner-772x250.jpg?rev=1696186";}s:11:"banners_rtl";a:0:{}}s:59:"banner-management-for-woocommerce/woo-banner-management.php";O:8:"stdClass":9:{s:2:"id";s:47:"w.org/plugins/banner-management-for-woocommerce";s:4:"slug";s:33:"banner-management-for-woocommerce";s:6:"plugin";s:59:"banner-management-for-woocommerce/woo-banner-management.php";s:11:"new_version";s:5:"1.1.3";s:3:"url";s:64:"https://wordpress.org/plugins/banner-management-for-woocommerce/";s:7:"package";s:82:"https://downloads.wordpress.org/plugin/banner-management-for-woocommerce.1.1.3.zip";s:5:"icons";a:2:{s:2:"2x";s:86:"https://ps.w.org/banner-management-for-woocommerce/assets/icon-256x256.png?rev=1743733";s:2:"1x";s:86:"https://ps.w.org/banner-management-for-woocommerce/assets/icon-128x128.png?rev=1743733";}s:7:"banners";a:1:{s:2:"1x";s:88:"https://ps.w.org/banner-management-for-woocommerce/assets/banner-772x250.jpg?rev=1517364";}s:11:"banners_rtl";a:0:{}}s:37:"breadcrumb-trail/breadcrumb-trail.php";O:8:"stdClass":9:{s:2:"id";s:30:"w.org/plugins/breadcrumb-trail";s:4:"slug";s:16:"breadcrumb-trail";s:6:"plugin";s:37:"breadcrumb-trail/breadcrumb-trail.php";s:11:"new_version";s:5:"1.1.0";s:3:"url";s:47:"https://wordpress.org/plugins/breadcrumb-trail/";s:7:"package";s:65:"https://downloads.wordpress.org/plugin/breadcrumb-trail.1.1.0.zip";s:5:"icons";a:2:{s:2:"2x";s:69:"https://ps.w.org/breadcrumb-trail/assets/icon-256x256.png?rev=1256005";s:2:"1x";s:69:"https://ps.w.org/breadcrumb-trail/assets/icon-128x128.png?rev=1256005";}s:7:"banners";a:2:{s:2:"2x";s:72:"https://ps.w.org/breadcrumb-trail/assets/banner-1544x500.png?rev=1256005";s:2:"1x";s:71:"https://ps.w.org/breadcrumb-trail/assets/banner-772x250.png?rev=1256005";}s:11:"banners_rtl";a:0:{}}s:36:"contact-form-7/wp-contact-form-7.php";O:8:"stdClass":9:{s:2:"id";s:28:"w.org/plugins/contact-form-7";s:4:"slug";s:14:"contact-form-7";s:6:"plugin";s:36:"contact-form-7/wp-contact-form-7.php";s:11:"new_version";s:5:"5.0.4";s:3:"url";s:45:"https://wordpress.org/plugins/contact-form-7/";s:7:"package";s:63:"https://downloads.wordpress.org/plugin/contact-form-7.5.0.4.zip";s:5:"icons";a:2:{s:2:"2x";s:66:"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=984007";s:2:"1x";s:66:"https://ps.w.org/contact-form-7/assets/icon-128x128.png?rev=984007";}s:7:"banners";a:2:{s:2:"2x";s:69:"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901";s:2:"1x";s:68:"https://ps.w.org/contact-form-7/assets/banner-772x250.png?rev=880427";}s:11:"banners_rtl";a:0:{}}s:32:"duplicate-page/duplicatepage.php";O:8:"stdClass":9:{s:2:"id";s:28:"w.org/plugins/duplicate-page";s:4:"slug";s:14:"duplicate-page";s:6:"plugin";s:32:"duplicate-page/duplicatepage.php";s:11:"new_version";s:3:"2.7";s:3:"url";s:45:"https://wordpress.org/plugins/duplicate-page/";s:7:"package";s:57:"https://downloads.wordpress.org/plugin/duplicate-page.zip";s:5:"icons";a:1:{s:2:"1x";s:67:"https://ps.w.org/duplicate-page/assets/icon-128x128.jpg?rev=1412874";}s:7:"banners";a:1:{s:2:"1x";s:69:"https://ps.w.org/duplicate-page/assets/banner-772x250.jpg?rev=1410328";}s:11:"banners_rtl";a:0:{}}s:67:"woocommerce-simple-registration/woocommerce-simple-registration.php";O:8:"stdClass":9:{s:2:"id";s:45:"w.org/plugins/woocommerce-simple-registration";s:4:"slug";s:31:"woocommerce-simple-registration";s:6:"plugin";s:67:"woocommerce-simple-registration/woocommerce-simple-registration.php";s:11:"new_version";s:5:"1.5.2";s:3:"url";s:62:"https://wordpress.org/plugins/woocommerce-simple-registration/";s:7:"package";s:80:"https://downloads.wordpress.org/plugin/woocommerce-simple-registration.1.5.2.zip";s:5:"icons";a:2:{s:2:"2x";s:84:"https://ps.w.org/woocommerce-simple-registration/assets/icon-256x256.png?rev=1488276";s:2:"1x";s:84:"https://ps.w.org/woocommerce-simple-registration/assets/icon-256x256.png?rev=1488276";}s:7:"banners";a:2:{s:2:"2x";s:87:"https://ps.w.org/woocommerce-simple-registration/assets/banner-1544x500.png?rev=1488276";s:2:"1x";s:86:"https://ps.w.org/woocommerce-simple-registration/assets/banner-772x250.png?rev=1488276";}s:11:"banners_rtl";a:0:{}}s:45:"user-submitted-posts/user-submitted-posts.php";O:8:"stdClass":9:{s:2:"id";s:34:"w.org/plugins/user-submitted-posts";s:4:"slug";s:20:"user-submitted-posts";s:6:"plugin";s:45:"user-submitted-posts/user-submitted-posts.php";s:11:"new_version";s:8:"20180822";s:3:"url";s:51:"https://wordpress.org/plugins/user-submitted-posts/";s:7:"package";s:72:"https://downloads.wordpress.org/plugin/user-submitted-posts.20180822.zip";s:5:"icons";a:2:{s:2:"2x";s:73:"https://ps.w.org/user-submitted-posts/assets/icon-256x256.png?rev=1475972";s:2:"1x";s:73:"https://ps.w.org/user-submitted-posts/assets/icon-128x128.png?rev=1475972";}s:7:"banners";a:2:{s:2:"2x";s:76:"https://ps.w.org/user-submitted-posts/assets/banner-1544x500.jpg?rev=1392443";s:2:"1x";s:75:"https://ps.w.org/user-submitted-posts/assets/banner-772x250.jpg?rev=1392443";}s:11:"banners_rtl";a:0:{}}s:26:"wc-fields-factory/wcff.php";O:8:"stdClass":9:{s:2:"id";s:31:"w.org/plugins/wc-fields-factory";s:4:"slug";s:17:"wc-fields-factory";s:6:"plugin";s:26:"wc-fields-factory/wcff.php";s:11:"new_version";s:5:"3.0.2";s:3:"url";s:48:"https://wordpress.org/plugins/wc-fields-factory/";s:7:"package";s:66:"https://downloads.wordpress.org/plugin/wc-fields-factory.3.0.2.zip";s:5:"icons";a:1:{s:2:"1x";s:70:"https://ps.w.org/wc-fields-factory/assets/icon-128x128.jpg?rev=1928921";}s:7:"banners";a:1:{s:2:"1x";s:72:"https://ps.w.org/wc-fields-factory/assets/banner-772x250.jpg?rev=1928919";}s:11:"banners_rtl";a:0:{}}s:27:"woocommerce/woocommerce.php";O:8:"stdClass":9:{s:2:"id";s:25:"w.org/plugins/woocommerce";s:4:"slug";s:11:"woocommerce";s:6:"plugin";s:27:"woocommerce/woocommerce.php";s:11:"new_version";s:5:"3.4.5";s:3:"url";s:42:"https://wordpress.org/plugins/woocommerce/";s:7:"package";s:60:"https://downloads.wordpress.org/plugin/woocommerce.3.4.5.zip";s:5:"icons";a:2:{s:2:"2x";s:64:"https://ps.w.org/woocommerce/assets/icon-256x256.png?rev=1440831";s:2:"1x";s:64:"https://ps.w.org/woocommerce/assets/icon-128x128.png?rev=1440831";}s:7:"banners";a:2:{s:2:"2x";s:67:"https://ps.w.org/woocommerce/assets/banner-1544x500.png?rev=1629184";s:2:"1x";s:66:"https://ps.w.org/woocommerce/assets/banner-772x250.png?rev=1629184";}s:11:"banners_rtl";a:0:{}}s:37:"woocommerce-ajax-cart/wooajaxcart.php";O:8:"stdClass":9:{s:2:"id";s:35:"w.org/plugins/woocommerce-ajax-cart";s:4:"slug";s:21:"woocommerce-ajax-cart";s:6:"plugin";s:37:"woocommerce-ajax-cart/wooajaxcart.php";s:11:"new_version";s:5:"1.2.6";s:3:"url";s:52:"https://wordpress.org/plugins/woocommerce-ajax-cart/";s:7:"package";s:70:"https://downloads.wordpress.org/plugin/woocommerce-ajax-cart.1.2.6.zip";s:5:"icons";a:1:{s:2:"1x";s:74:"https://ps.w.org/woocommerce-ajax-cart/assets/icon-128x128.png?rev=1186993";}s:7:"banners";a:0:{}s:11:"banners_rtl";a:0:{}}s:45:"woocommerce-correios/woocommerce-correios.php";O:8:"stdClass":9:{s:2:"id";s:34:"w.org/plugins/woocommerce-correios";s:4:"slug";s:20:"woocommerce-correios";s:6:"plugin";s:45:"woocommerce-correios/woocommerce-correios.php";s:11:"new_version";s:5:"3.7.1";s:3:"url";s:51:"https://wordpress.org/plugins/woocommerce-correios/";s:7:"package";s:69:"https://downloads.wordpress.org/plugin/woocommerce-correios.3.7.1.zip";s:5:"icons";a:2:{s:2:"2x";s:73:"https://ps.w.org/woocommerce-correios/assets/icon-256x256.png?rev=1356952";s:2:"1x";s:73:"https://ps.w.org/woocommerce-correios/assets/icon-128x128.png?rev=1356952";}s:7:"banners";a:2:{s:2:"2x";s:76:"https://ps.w.org/woocommerce-correios/assets/banner-1544x500.png?rev=1356952";s:2:"1x";s:75:"https://ps.w.org/woocommerce-correios/assets/banner-772x250.png?rev=1356952";}s:11:"banners_rtl";a:0:{}}s:51:"woocommerce-product-image-flipper/image-flipper.php";O:8:"stdClass":9:{s:2:"id";s:47:"w.org/plugins/woocommerce-product-image-flipper";s:4:"slug";s:33:"woocommerce-product-image-flipper";s:6:"plugin";s:51:"woocommerce-product-image-flipper/image-flipper.php";s:11:"new_version";s:5:"0.4.1";s:3:"url";s:64:"https://wordpress.org/plugins/woocommerce-product-image-flipper/";s:7:"package";s:82:"https://downloads.wordpress.org/plugin/woocommerce-product-image-flipper.0.4.1.zip";s:5:"icons";a:1:{s:7:"default";s:84:"https://s.w.org/plugins/geopattern-icon/woocommerce-product-image-flipper_595a63.svg";}s:7:"banners";a:2:{s:2:"2x";s:88:"https://ps.w.org/woocommerce-product-image-flipper/assets/banner-1544x500.png?rev=791235";s:2:"1x";s:87:"https://ps.w.org/woocommerce-product-image-flipper/assets/banner-772x250.png?rev=791235";}s:11:"banners_rtl";a:0:{}}s:28:"woo-smart-wishlist/index.php";O:8:"stdClass":9:{s:2:"id";s:32:"w.org/plugins/woo-smart-wishlist";s:4:"slug";s:18:"woo-smart-wishlist";s:6:"plugin";s:28:"woo-smart-wishlist/index.php";s:11:"new_version";s:5:"1.3.0";s:3:"url";s:49:"https://wordpress.org/plugins/woo-smart-wishlist/";s:7:"package";s:61:"https://downloads.wordpress.org/plugin/woo-smart-wishlist.zip";s:5:"icons";a:1:{s:2:"1x";s:71:"https://ps.w.org/woo-smart-wishlist/assets/icon-128x128.png?rev=1857805";}s:7:"banners";a:1:{s:2:"1x";s:73:"https://ps.w.org/woo-smart-wishlist/assets/banner-772x250.png?rev=1797324";}s:11:"banners_rtl";a:0:{}}s:20:"wp-dropzone/init.php";O:8:"stdClass":9:{s:2:"id";s:25:"w.org/plugins/wp-dropzone";s:4:"slug";s:11:"wp-dropzone";s:6:"plugin";s:20:"wp-dropzone/init.php";s:11:"new_version";s:5:"1.0.5";s:3:"url";s:42:"https://wordpress.org/plugins/wp-dropzone/";s:7:"package";s:60:"https://downloads.wordpress.org/plugin/wp-dropzone.1.0.5.zip";s:5:"icons";a:1:{s:7:"default";s:55:"https://s.w.org/plugins/geopattern-icon/wp-dropzone.svg";}s:7:"banners";a:0:{}s:11:"banners_rtl";a:0:{}}}}', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `wd_postmeta`
--

CREATE TABLE `wd_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB AUTO_INCREMENT=2143 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wd_postmeta`
--

INSERT INTO `wd_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(5, 38, '_sku', ''),
(6, 38, '_downloadable', 'no'),
(7, 38, '_virtual', 'no'),
(8, 38, '_price', '109.00'),
(9, 38, '_visibility', 'visible'),
(10, 38, '_stock', ''),
(11, 38, '_stock_status', 'instock'),
(12, 38, '_backorders', 'no'),
(13, 38, '_manage_stock', 'no'),
(14, 38, '_sale_price', ''),
(15, 38, '_regular_price', '109.00'),
(16, 38, '_weight', ''),
(17, 38, '_length', ''),
(18, 38, '_width', ''),
(19, 38, '_height', ''),
(20, 38, '_tax_status', 'taxable'),
(21, 38, '_tax_class', ''),
(22, 38, '_upsell_ids', 'a:0:{}'),
(23, 38, '_crosssell_ids', 'a:0:{}'),
(24, 38, '_upsell_skus', 'a:0:{}'),
(25, 38, '_crosssell_skus', 'a:0:{}'),
(26, 38, '_sale_price_dates_from', ''),
(27, 38, '_sale_price_dates_to', ''),
(28, 38, '_min_variation_price', ''),
(29, 38, '_max_variation_price', ''),
(30, 38, '_min_variation_regular_price', ''),
(31, 38, '_max_variation_regular_price', ''),
(32, 38, '_min_variation_sale_price', ''),
(33, 38, '_max_variation_sale_price', ''),
(34, 38, '_featured', 'no'),
(35, 38, '_file_path', ''),
(36, 38, '_file_paths', ''),
(37, 38, '_download_limit', '-1'),
(38, 38, '_download_expiry', '-1'),
(39, 38, '_product_url', ''),
(40, 38, '_button_text', ''),
(41, 38, '_default_attributes', NULL),
(42, 39, '_wp_attached_file', '2018/08/vaso-melancia-walldone.jpg'),
(43, 39, '_wp_attachment_image_alt', 'Vaso Melancia'),
(44, 38, '_thumbnail_id', '39'),
(45, 39, '_woocommerce_exclude_image', '0'),
(46, 40, '_wp_attached_file', '2018/08/vaso-melancia-hover-walldone.jpg'),
(47, 40, '_wp_attachment_image_alt', 'Vaso Melancia'),
(48, 40, '_woocommerce_exclude_image', '0'),
(49, 38, '_product_image_gallery', '40'),
(50, 38, '_product_attributes', 'a:2:{s:8:"molduras";a:6:{s:4:"name";s:8:"Molduras";s:5:"value";s:43:"Moldura Verde | Moldura Preta | Sem Moldura";s:11:"is_taxonomy";i:0;s:8:"position";i:0;s:10:"is_visible";i:1;s:12:"is_variation";i:1;}s:5:"cores";a:6:{s:4:"name";s:5:"Cores";s:5:"value";s:23:"Verde | Preto | Sem cor";s:11:"is_taxonomy";i:0;s:8:"position";i:1;s:10:"is_visible";i:1;s:12:"is_variation";i:1;}}'),
(51, 38, 'total_sales', '0'),
(52, 50, '_sku', ''),
(53, 50, '_downloadable', 'no'),
(54, 50, '_virtual', 'no'),
(55, 50, '_price', '25.00'),
(56, 50, '_visibility', 'visible'),
(57, 50, '_stock', ''),
(58, 50, '_stock_status', 'instock'),
(59, 50, '_backorders', 'no'),
(60, 50, '_manage_stock', 'no'),
(61, 50, '_sale_price', ''),
(62, 50, '_regular_price', '25.00'),
(63, 50, '_weight', ''),
(64, 50, '_length', ''),
(65, 50, '_width', ''),
(66, 50, '_height', ''),
(67, 50, '_tax_status', 'taxable'),
(68, 50, '_tax_class', ''),
(69, 50, '_upsell_ids', 'a:0:{}'),
(70, 50, '_crosssell_ids', 'a:0:{}'),
(71, 50, '_upsell_skus', 'a:0:{}'),
(72, 50, '_crosssell_skus', 'a:0:{}'),
(73, 50, '_sale_price_dates_from', ''),
(74, 50, '_sale_price_dates_to', ''),
(75, 50, '_min_variation_price', ''),
(76, 50, '_max_variation_price', ''),
(77, 50, '_min_variation_regular_price', ''),
(78, 50, '_max_variation_regular_price', ''),
(79, 50, '_min_variation_sale_price', ''),
(80, 50, '_max_variation_sale_price', ''),
(81, 50, '_featured', 'no'),
(82, 50, '_file_path', ''),
(83, 50, '_file_paths', ''),
(84, 50, '_download_limit', '-1'),
(85, 50, '_download_expiry', '-1'),
(86, 50, '_product_url', ''),
(87, 50, '_button_text', ''),
(88, 50, '_default_attributes', NULL),
(89, 51, '_wp_attached_file', '2018/08/adesivo-parede-flamingo-walldone.jpg'),
(90, 51, '_wp_attachment_image_alt', 'Adesivo para parede Flamingo'),
(91, 50, '_thumbnail_id', '51'),
(92, 51, '_woocommerce_exclude_image', '0'),
(93, 52, '_wp_attached_file', '2018/08/adesivo-parede-flamingo-hover-walldone.jpg'),
(94, 52, '_wp_attachment_image_alt', 'Adesivo para parede Flamingo'),
(95, 52, '_woocommerce_exclude_image', '0'),
(96, 53, '_wp_attached_file', '2018/08/quadro-gato-frida-aquarela-walldone.jpg'),
(97, 53, '_wp_attachment_image_alt', 'Adesivo para parede Flamingo'),
(98, 53, '_woocommerce_exclude_image', '0'),
(99, 54, '_wp_attached_file', '2018/08/adesivo-bolas-irregulares-hover-walldone.png'),
(100, 54, '_wp_attachment_image_alt', 'Adesivo para parede Flamingo'),
(101, 54, '_woocommerce_exclude_image', '0'),
(102, 55, '_wp_attached_file', '2018/08/quadro-nosso-lar-hover-walldone.jpg'),
(103, 55, '_wp_attachment_image_alt', 'Adesivo para parede Flamingo'),
(104, 55, '_woocommerce_exclude_image', '0'),
(105, 56, '_wp_attached_file', '2018/08/ela-e-minha-menina-hover-walldone.jpg'),
(106, 56, '_wp_attachment_image_alt', 'Adesivo para parede Flamingo'),
(107, 56, '_woocommerce_exclude_image', '0'),
(108, 57, '_wp_attached_file', '2018/08/ela-e-minha-menina-walldone.jpg'),
(109, 57, '_wp_attachment_image_alt', 'Adesivo para parede Flamingo'),
(110, 57, '_woocommerce_exclude_image', '0'),
(111, 58, '_wp_attached_file', '2018/08/video.jpg'),
(112, 58, '_wp_attachment_image_alt', 'Adesivo para parede Flamingo'),
(113, 58, '_woocommerce_exclude_image', '0'),
(114, 50, '_product_image_gallery', '52,53,54,55,56,57,58'),
(115, 50, '_product_attributes', 'a:4:{s:8:"molduras";a:6:{s:4:"name";s:8:"Molduras";s:5:"value";s:43:"Moldura Verde | Moldura Preta | Sem Moldura";s:11:"is_taxonomy";i:0;s:8:"position";i:0;s:10:"is_visible";i:1;s:12:"is_variation";i:1;}s:5:"cores";a:6:{s:4:"name";s:5:"Cores";s:5:"value";s:23:"Verde | Preto | Sem cor";s:11:"is_taxonomy";i:0;s:8:"position";i:1;s:10:"is_visible";i:1;s:12:"is_variation";i:1;}s:7:"tamanho";a:6:{s:4:"name";s:7:"Tamanho";s:5:"value";s:37:"Pequeno 4x4 | Médio 6x6 | Grande 8x8";s:11:"is_taxonomy";i:0;s:8:"position";i:2;s:10:"is_visible";i:1;s:12:"is_variation";i:1;}s:6:"cantos";a:6:{s:4:"name";s:6:"Cantos";s:5:"value";s:32:"Arredondados | Quadrado | Normal";s:11:"is_taxonomy";i:0;s:8:"position";i:3;s:10:"is_visible";i:1;s:12:"is_variation";i:1;}}'),
(116, 50, 'total_sales', '0'),
(117, 90, '_sku', ''),
(118, 90, '_downloadable', 'no'),
(119, 90, '_virtual', 'no'),
(120, 90, '_price', '25.00'),
(121, 90, '_visibility', 'visible'),
(122, 90, '_stock', ''),
(123, 90, '_stock_status', 'instock'),
(124, 90, '_backorders', 'no'),
(125, 90, '_manage_stock', 'no'),
(126, 90, '_sale_price', ''),
(127, 90, '_regular_price', '25.00'),
(128, 90, '_weight', ''),
(129, 90, '_length', ''),
(130, 90, '_width', ''),
(131, 90, '_height', ''),
(132, 90, '_tax_status', 'taxable'),
(133, 90, '_tax_class', ''),
(134, 90, '_upsell_ids', 'a:0:{}'),
(135, 90, '_crosssell_ids', 'a:0:{}'),
(136, 90, '_upsell_skus', 'a:0:{}'),
(137, 90, '_crosssell_skus', 'a:0:{}'),
(138, 90, '_sale_price_dates_from', ''),
(139, 90, '_sale_price_dates_to', ''),
(140, 90, '_min_variation_price', ''),
(141, 90, '_max_variation_price', ''),
(142, 90, '_min_variation_regular_price', ''),
(143, 90, '_max_variation_regular_price', ''),
(144, 90, '_min_variation_sale_price', ''),
(145, 90, '_max_variation_sale_price', ''),
(146, 90, '_featured', 'no'),
(147, 90, '_file_path', ''),
(148, 90, '_file_paths', ''),
(149, 90, '_download_limit', '-1'),
(150, 90, '_download_expiry', '-1'),
(151, 90, '_product_url', ''),
(152, 90, '_button_text', ''),
(153, 90, '_default_attributes', NULL),
(154, 91, '_wp_attached_file', '2018/08/ela-e-minha-menina-walldone-1.jpg'),
(155, 91, '_wp_attachment_image_alt', 'Quarto de Casal Menina e Menino'),
(156, 90, '_thumbnail_id', '91'),
(157, 91, '_woocommerce_exclude_image', '0'),
(158, 92, '_wp_attached_file', '2018/08/ela-e-minha-menina-hover-walldone-1.jpg'),
(159, 92, '_wp_attachment_image_alt', 'Quarto de Casal Menina e Menino'),
(160, 92, '_woocommerce_exclude_image', '0'),
(161, 90, '_product_image_gallery', '92'),
(162, 90, '_product_attributes', 'a:2:{s:8:"molduras";a:6:{s:4:"name";s:8:"Molduras";s:5:"value";s:43:"Moldura Verde | Moldura Preta | Sem Moldura";s:11:"is_taxonomy";i:0;s:8:"position";i:0;s:10:"is_visible";i:1;s:12:"is_variation";i:1;}s:5:"cores";a:6:{s:4:"name";s:5:"Cores";s:5:"value";s:23:"Verde | Preto | Sem cor";s:11:"is_taxonomy";i:0;s:8:"position";i:1;s:10:"is_visible";i:1;s:12:"is_variation";i:1;}}'),
(163, 90, 'total_sales', '0'),
(164, 93, '_sku', ''),
(165, 93, '_downloadable', 'no'),
(166, 93, '_virtual', 'no'),
(167, 93, '_price', '109.00'),
(168, 93, '_visibility', 'visible'),
(169, 93, '_stock', ''),
(170, 93, '_stock_status', 'instock'),
(171, 93, '_backorders', 'no'),
(172, 93, '_manage_stock', 'no'),
(173, 93, '_sale_price', ''),
(174, 93, '_regular_price', '109.00'),
(175, 93, '_weight', ''),
(176, 93, '_length', ''),
(177, 93, '_width', ''),
(178, 93, '_height', ''),
(179, 93, '_tax_status', 'taxable'),
(180, 93, '_tax_class', ''),
(181, 93, '_upsell_ids', 'a:0:{}'),
(182, 93, '_crosssell_ids', 'a:0:{}'),
(183, 93, '_upsell_skus', 'a:0:{}'),
(184, 93, '_crosssell_skus', 'a:0:{}'),
(185, 93, '_sale_price_dates_from', ''),
(186, 93, '_sale_price_dates_to', ''),
(187, 93, '_min_variation_price', ''),
(188, 93, '_max_variation_price', ''),
(189, 93, '_min_variation_regular_price', ''),
(190, 93, '_max_variation_regular_price', ''),
(191, 93, '_min_variation_sale_price', ''),
(192, 93, '_max_variation_sale_price', ''),
(193, 93, '_featured', 'no'),
(194, 93, '_file_path', ''),
(195, 93, '_file_paths', ''),
(196, 93, '_download_limit', '-1'),
(197, 93, '_download_expiry', '-1'),
(198, 93, '_product_url', ''),
(199, 93, '_button_text', ''),
(200, 93, '_default_attributes', NULL),
(201, 94, '_wp_attached_file', '2018/08/quadro-nosso-lar-walldone.jpg'),
(202, 94, '_wp_attachment_image_alt', 'Quadro - Nosso Lar'),
(203, 93, '_thumbnail_id', '94'),
(204, 94, '_woocommerce_exclude_image', '0'),
(205, 95, '_wp_attached_file', '2018/08/quadro-nosso-lar-hover-walldone-1.jpg'),
(206, 95, '_wp_attachment_image_alt', 'Quadro - Nosso Lar'),
(207, 95, '_woocommerce_exclude_image', '0'),
(208, 93, '_product_image_gallery', '95'),
(209, 93, '_product_attributes', 'a:2:{s:8:"molduras";a:6:{s:4:"name";s:8:"Molduras";s:5:"value";s:43:"Moldura Verde | Moldura Preta | Sem Moldura";s:11:"is_taxonomy";i:0;s:8:"position";i:0;s:10:"is_visible";i:1;s:12:"is_variation";i:1;}s:5:"cores";a:6:{s:4:"name";s:5:"Cores";s:5:"value";s:23:"Verde | Preto | Sem cor";s:11:"is_taxonomy";i:0;s:8:"position";i:1;s:10:"is_visible";i:1;s:12:"is_variation";i:1;}}'),
(210, 93, 'total_sales', '0'),
(211, 96, '_sku', '98749832794823'),
(212, 96, '_downloadable', 'no'),
(213, 96, '_virtual', 'no'),
(215, 96, '_visibility', 'visible'),
(216, 96, '_stock', NULL),
(217, 96, '_stock_status', 'instock'),
(218, 96, '_backorders', 'no'),
(219, 96, '_manage_stock', 'no'),
(222, 96, '_weight', '1.5'),
(223, 96, '_length', '1'),
(224, 96, '_width', '1'),
(225, 96, '_height', '1'),
(226, 96, '_tax_status', 'taxable'),
(227, 96, '_tax_class', ''),
(228, 96, '_upsell_ids', 'a:0:{}'),
(229, 96, '_crosssell_ids', 'a:0:{}'),
(230, 96, '_upsell_skus', 'a:0:{}'),
(231, 96, '_crosssell_skus', 'a:0:{}'),
(232, 96, '_sale_price_dates_from', ''),
(233, 96, '_sale_price_dates_to', ''),
(234, 96, '_min_variation_price', ''),
(235, 96, '_max_variation_price', ''),
(236, 96, '_min_variation_regular_price', ''),
(237, 96, '_max_variation_regular_price', ''),
(238, 96, '_min_variation_sale_price', ''),
(239, 96, '_max_variation_sale_price', ''),
(240, 96, '_featured', 'no'),
(241, 96, '_file_path', ''),
(242, 96, '_file_paths', ''),
(243, 96, '_download_limit', '-1'),
(244, 96, '_download_expiry', '-1'),
(245, 96, '_product_url', ''),
(246, 96, '_button_text', ''),
(247, 96, '_default_attributes', NULL),
(248, 97, '_wp_attached_file', '2018/08/adesivo-bolas-irregulares-walldone.jpg'),
(249, 97, '_wp_attachment_image_alt', 'Adesivo para Parede - Bolas Irregulares'),
(250, 96, '_thumbnail_id', '97'),
(251, 97, '_woocommerce_exclude_image', '0'),
(252, 98, '_wp_attached_file', '2018/08/adesivo-bolas-irregulares-hover-walldone-1.png'),
(253, 98, '_wp_attachment_image_alt', 'Adesivo para Parede - Bolas Irregulares'),
(254, 98, '_woocommerce_exclude_image', '0'),
(255, 96, '_product_image_gallery', '98,97'),
(256, 96, '_product_attributes', 'a:2:{s:8:"molduras";a:6:{s:4:"name";s:8:"Molduras";s:5:"value";s:43:"Moldura Verde | Moldura Preta | Sem Moldura";s:8:"position";i:0;s:10:"is_visible";i:1;s:12:"is_variation";i:1;s:11:"is_taxonomy";i:0;}s:5:"cores";a:6:{s:4:"name";s:5:"Cores";s:5:"value";s:23:"Verde | Preto | Sem cor";s:8:"position";i:1;s:10:"is_visible";i:1;s:12:"is_variation";i:1;s:11:"is_taxonomy";i:0;}}'),
(257, 96, 'total_sales', '0'),
(258, 99, '_sku', '98749832794827'),
(259, 99, '_downloadable', 'no'),
(260, 99, '_virtual', 'no'),
(261, 99, '_price', '20.00'),
(262, 99, '_visibility', 'visible'),
(263, 99, '_stock', NULL),
(264, 99, '_stock_status', 'instock'),
(265, 99, '_backorders', 'no'),
(266, 99, '_manage_stock', 'no'),
(267, 99, '_sale_price', '20.00'),
(268, 99, '_regular_price', '25.00'),
(269, 99, '_weight', '0.8'),
(270, 99, '_length', '29'),
(271, 99, '_width', '11'),
(272, 99, '_height', '42'),
(273, 99, '_tax_status', 'taxable'),
(274, 99, '_tax_class', ''),
(275, 99, '_upsell_ids', 'a:0:{}'),
(276, 99, '_crosssell_ids', 'a:0:{}'),
(277, 99, '_upsell_skus', 'a:0:{}'),
(278, 99, '_crosssell_skus', 'a:0:{}'),
(279, 99, '_sale_price_dates_from', ''),
(280, 99, '_sale_price_dates_to', ''),
(281, 99, '_min_variation_price', ''),
(282, 99, '_max_variation_price', ''),
(283, 99, '_min_variation_regular_price', ''),
(284, 99, '_max_variation_regular_price', ''),
(285, 99, '_min_variation_sale_price', ''),
(286, 99, '_max_variation_sale_price', ''),
(287, 99, '_featured', 'no'),
(288, 99, '_file_path', ''),
(289, 99, '_file_paths', ''),
(290, 99, '_download_limit', '-1'),
(291, 99, '_download_expiry', '-1'),
(292, 99, '_product_url', ''),
(293, 99, '_button_text', ''),
(294, 99, '_default_attributes', NULL),
(295, 100, '_wp_attached_file', '2018/08/quadro-gato-frida-aquarela-walldone-1.jpg'),
(296, 100, '_wp_attachment_image_alt', 'Quadro Gato Frida Aquarela'),
(297, 99, '_thumbnail_id', '100'),
(298, 100, '_woocommerce_exclude_image', '0'),
(299, 101, '_wp_attached_file', '2018/08/quadro-gato-frida-aquarela-hover-walldone.jpg'),
(300, 101, '_wp_attachment_image_alt', 'Quadro Gato Frida Aquarela'),
(301, 101, '_woocommerce_exclude_image', '0'),
(302, 99, '_product_image_gallery', '101,100,116'),
(303, 99, 'total_sales', '0'),
(304, 39, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:250;s:6:"height";i:250;s:4:"file";s:34:"2018/08/vaso-melancia-walldone.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:34:"vaso-melancia-walldone-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:34:"vaso-melancia-walldone-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:34:"vaso-melancia-walldone-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(305, 51, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:250;s:6:"height";i:250;s:4:"file";s:44:"2018/08/adesivo-parede-flamingo-walldone.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:44:"adesivo-parede-flamingo-walldone-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:44:"adesivo-parede-flamingo-walldone-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:44:"adesivo-parede-flamingo-walldone-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(306, 53, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:400;s:6:"height";i:400;s:4:"file";s:47:"2018/08/quadro-gato-frida-aquarela-walldone.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:47:"quadro-gato-frida-aquarela-walldone-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:47:"quadro-gato-frida-aquarela-walldone-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:47:"quadro-gato-frida-aquarela-walldone-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:47:"quadro-gato-frida-aquarela-walldone-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:47:"quadro-gato-frida-aquarela-walldone-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:47:"quadro-gato-frida-aquarela-walldone-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(307, 52, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:400;s:6:"height";i:400;s:4:"file";s:50:"2018/08/adesivo-parede-flamingo-hover-walldone.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:50:"adesivo-parede-flamingo-hover-walldone-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:50:"adesivo-parede-flamingo-hover-walldone-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:50:"adesivo-parede-flamingo-hover-walldone-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:50:"adesivo-parede-flamingo-hover-walldone-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:50:"adesivo-parede-flamingo-hover-walldone-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:50:"adesivo-parede-flamingo-hover-walldone-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(308, 40, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:400;s:6:"height";i:399;s:4:"file";s:40:"2018/08/vaso-melancia-hover-walldone.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:40:"vaso-melancia-hover-walldone-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:40:"vaso-melancia-hover-walldone-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:40:"vaso-melancia-hover-walldone-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:40:"vaso-melancia-hover-walldone-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:40:"vaso-melancia-hover-walldone-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:40:"vaso-melancia-hover-walldone-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(309, 54, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:398;s:6:"height";i:400;s:4:"file";s:52:"2018/08/adesivo-bolas-irregulares-hover-walldone.png";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:52:"adesivo-bolas-irregulares-hover-walldone-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:52:"adesivo-bolas-irregulares-hover-walldone-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:52:"adesivo-bolas-irregulares-hover-walldone-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";s:9:"uncropped";b:1;}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:52:"adesivo-bolas-irregulares-hover-walldone-100x100.png";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:9:"image/png";}s:12:"shop_catalog";a:4:{s:4:"file";s:52:"adesivo-bolas-irregulares-hover-walldone-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:52:"adesivo-bolas-irregulares-hover-walldone-100x100.png";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(310, 55, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:400;s:6:"height";i:400;s:4:"file";s:43:"2018/08/quadro-nosso-lar-hover-walldone.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:43:"quadro-nosso-lar-hover-walldone-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:43:"quadro-nosso-lar-hover-walldone-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:43:"quadro-nosso-lar-hover-walldone-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:43:"quadro-nosso-lar-hover-walldone-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:43:"quadro-nosso-lar-hover-walldone-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:43:"quadro-nosso-lar-hover-walldone-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(311, 56, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:400;s:6:"height";i:400;s:4:"file";s:45:"2018/08/ela-e-minha-menina-hover-walldone.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:45:"ela-e-minha-menina-hover-walldone-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:45:"ela-e-minha-menina-hover-walldone-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:45:"ela-e-minha-menina-hover-walldone-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:45:"ela-e-minha-menina-hover-walldone-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:45:"ela-e-minha-menina-hover-walldone-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:45:"ela-e-minha-menina-hover-walldone-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(312, 57, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:400;s:6:"height";i:400;s:4:"file";s:39:"2018/08/ela-e-minha-menina-walldone.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:39:"ela-e-minha-menina-walldone-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:39:"ela-e-minha-menina-walldone-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:39:"ela-e-minha-menina-walldone-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:39:"ela-e-minha-menina-walldone-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:39:"ela-e-minha-menina-walldone-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:39:"ela-e-minha-menina-walldone-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(313, 91, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:400;s:6:"height";i:400;s:4:"file";s:41:"2018/08/ela-e-minha-menina-walldone-1.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:41:"ela-e-minha-menina-walldone-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:41:"ela-e-minha-menina-walldone-1-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:41:"ela-e-minha-menina-walldone-1-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:41:"ela-e-minha-menina-walldone-1-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:41:"ela-e-minha-menina-walldone-1-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:41:"ela-e-minha-menina-walldone-1-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(314, 58, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:679;s:6:"height";i:380;s:4:"file";s:17:"2018/08/video.jpg";s:5:"sizes";a:8:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"video-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:17:"video-300x168.jpg";s:5:"width";i:300;s:6:"height";i:168;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:17:"video-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:18:"woocommerce_single";a:4:{s:4:"file";s:17:"video-600x336.jpg";s:5:"width";i:600;s:6:"height";i:336;s:9:"mime-type";s:10:"image/jpeg";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:17:"video-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:17:"video-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:17:"video-600x336.jpg";s:5:"width";i:600;s:6:"height";i:336;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:17:"video-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(315, 92, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:400;s:6:"height";i:400;s:4:"file";s:47:"2018/08/ela-e-minha-menina-hover-walldone-1.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:47:"ela-e-minha-menina-hover-walldone-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:47:"ela-e-minha-menina-hover-walldone-1-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:47:"ela-e-minha-menina-hover-walldone-1-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:47:"ela-e-minha-menina-hover-walldone-1-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:47:"ela-e-minha-menina-hover-walldone-1-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:47:"ela-e-minha-menina-hover-walldone-1-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(316, 94, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:400;s:6:"height";i:400;s:4:"file";s:37:"2018/08/quadro-nosso-lar-walldone.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:37:"quadro-nosso-lar-walldone-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:37:"quadro-nosso-lar-walldone-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:37:"quadro-nosso-lar-walldone-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:37:"quadro-nosso-lar-walldone-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:37:"quadro-nosso-lar-walldone-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:37:"quadro-nosso-lar-walldone-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(317, 95, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:400;s:6:"height";i:400;s:4:"file";s:45:"2018/08/quadro-nosso-lar-hover-walldone-1.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:45:"quadro-nosso-lar-hover-walldone-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:45:"quadro-nosso-lar-hover-walldone-1-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:45:"quadro-nosso-lar-hover-walldone-1-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:45:"quadro-nosso-lar-hover-walldone-1-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:45:"quadro-nosso-lar-hover-walldone-1-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:45:"quadro-nosso-lar-hover-walldone-1-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(318, 97, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:400;s:6:"height";i:400;s:4:"file";s:46:"2018/08/adesivo-bolas-irregulares-walldone.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:46:"adesivo-bolas-irregulares-walldone-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:46:"adesivo-bolas-irregulares-walldone-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:46:"adesivo-bolas-irregulares-walldone-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:46:"adesivo-bolas-irregulares-walldone-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:46:"adesivo-bolas-irregulares-walldone-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:46:"adesivo-bolas-irregulares-walldone-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(319, 100, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:400;s:6:"height";i:400;s:4:"file";s:49:"2018/08/quadro-gato-frida-aquarela-walldone-1.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:49:"quadro-gato-frida-aquarela-walldone-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:49:"quadro-gato-frida-aquarela-walldone-1-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:49:"quadro-gato-frida-aquarela-walldone-1-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:49:"quadro-gato-frida-aquarela-walldone-1-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:49:"quadro-gato-frida-aquarela-walldone-1-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:49:"quadro-gato-frida-aquarela-walldone-1-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(320, 101, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:400;s:6:"height";i:400;s:4:"file";s:53:"2018/08/quadro-gato-frida-aquarela-hover-walldone.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:53:"quadro-gato-frida-aquarela-hover-walldone-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:53:"quadro-gato-frida-aquarela-hover-walldone-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:53:"quadro-gato-frida-aquarela-hover-walldone-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:53:"quadro-gato-frida-aquarela-hover-walldone-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:53:"quadro-gato-frida-aquarela-hover-walldone-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:53:"quadro-gato-frida-aquarela-hover-walldone-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(321, 98, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:398;s:6:"height";i:400;s:4:"file";s:54:"2018/08/adesivo-bolas-irregulares-hover-walldone-1.png";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:54:"adesivo-bolas-irregulares-hover-walldone-1-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:54:"adesivo-bolas-irregulares-hover-walldone-1-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:54:"adesivo-bolas-irregulares-hover-walldone-1-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";s:9:"uncropped";b:1;}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:54:"adesivo-bolas-irregulares-hover-walldone-1-100x100.png";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:9:"image/png";}s:12:"shop_catalog";a:4:{s:4:"file";s:54:"adesivo-bolas-irregulares-hover-walldone-1-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:54:"adesivo-bolas-irregulares-hover-walldone-1-100x100.png";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(322, 99, '_wc_review_count', '2'),
(323, 99, '_wc_rating_count', 'a:2:{i:4;i:1;i:5;i:1;}'),
(324, 99, '_wc_average_rating', '4.50'),
(325, 96, '_wc_review_count', '1'),
(326, 96, '_wc_rating_count', 'a:1:{i:3;i:1;}'),
(327, 96, '_wc_average_rating', '3.00'),
(328, 93, '_wc_review_count', '0'),
(329, 93, '_wc_rating_count', 'a:0:{}'),
(330, 93, '_wc_average_rating', '0'),
(331, 90, '_wc_review_count', '0'),
(332, 90, '_wc_rating_count', 'a:0:{}'),
(333, 90, '_wc_average_rating', '0'),
(334, 50, '_wc_review_count', '0'),
(335, 50, '_wc_rating_count', 'a:0:{}'),
(336, 50, '_wc_average_rating', '0'),
(337, 38, '_wc_review_count', '0'),
(338, 38, '_wc_rating_count', 'a:0:{}'),
(339, 38, '_wc_average_rating', '0'),
(340, 102, '_form', '<div class="form-group">\n    [text Nome class:form-control placeholder "Nome"]\n</div>\n\n<div class="form-group">\n    [email Email class:form-control placeholder "E-mail"]\n</div>\n\n<div class="form-group submit">\n   [submit class:btn-clean "Quero Super Novidades"]\n</div>'),
(341, 102, '_mail', 'a:9:{s:6:"active";b:1;s:7:"subject";s:26:"Wall Done "[your-subject]"";s:6:"sender";s:24:"[your-name] <web@904.ag>";s:9:"recipient";s:10:"web@904.ag";s:4:"body";s:192:"De: [your-name] <[your-email]>\nAssunto: [your-subject]\n\nCorpo da mensagem:\n[your-message]\n\n-- \nEste e-mail foi enviado de um formulário de contato em Wall Done (http://localhost/walldone-new)";s:18:"additional_headers";s:22:"Reply-To: [your-email]";s:11:"attachments";s:0:"";s:8:"use_html";b:0;s:13:"exclude_blank";b:0;}'),
(342, 102, '_mail_2', 'a:9:{s:6:"active";b:0;s:7:"subject";s:26:"Wall Done "[your-subject]"";s:6:"sender";s:22:"Wall Done <web@904.ag>";s:9:"recipient";s:12:"[your-email]";s:4:"body";s:136:"Corpo da mensagem:\n[your-message]\n\n-- \nEste e-mail foi enviado de um formulário de contato em Wall Done (http://localhost/walldone-new)";s:18:"additional_headers";s:20:"Reply-To: web@904.ag";s:11:"attachments";s:0:"";s:8:"use_html";b:0;s:13:"exclude_blank";b:0;}'),
(343, 102, '_messages', 'a:23:{s:12:"mail_sent_ok";s:27:"Agradecemos a sua mensagem.";s:12:"mail_sent_ng";s:74:"Ocorreu um erro ao tentar enviar sua mensagem. Tente novamente mais tarde.";s:16:"validation_error";s:63:"Um ou mais campos possuem um erro. Verifique e tente novamente.";s:4:"spam";s:74:"Ocorreu um erro ao tentar enviar sua mensagem. Tente novamente mais tarde.";s:12:"accept_terms";s:72:"Você deve aceitar os termos e condições antes de enviar sua mensagem.";s:16:"invalid_required";s:24:"O campo é obrigatório.";s:16:"invalid_too_long";s:23:"O campo é muito longo.";s:17:"invalid_too_short";s:23:"O campo é muito curto.";s:12:"invalid_date";s:34:"O formato de data está incorreto.";s:14:"date_too_early";s:44:"A data é anterior à mais antiga permitida.";s:13:"date_too_late";s:44:"A data é posterior à maior data permitida.";s:13:"upload_failed";s:49:"Ocorreu um erro desconhecido ao enviar o arquivo.";s:24:"upload_file_type_invalid";s:59:"Você não tem permissão para enviar esse tipo de arquivo.";s:21:"upload_file_too_large";s:26:"O arquivo é muito grande.";s:23:"upload_failed_php_error";s:36:"Ocorreu um erro ao enviar o arquivo.";s:14:"invalid_number";s:34:"O formato de número é inválido.";s:16:"number_too_small";s:46:"O número é menor do que o mínimo permitido.";s:16:"number_too_large";s:46:"O número é maior do que o máximo permitido.";s:23:"quiz_answer_not_correct";s:39:"A resposta para o quiz está incorreta.";s:17:"captcha_not_match";s:35:"O código digitado está incorreto.";s:13:"invalid_email";s:45:"O endereço de e-mail informado é inválido.";s:11:"invalid_url";s:19:"A URL é inválida.";s:11:"invalid_tel";s:35:"O número de telefone é inválido.";}'),
(344, 102, '_additional_settings', ''),
(345, 102, '_locale', 'pt_BR'),
(346, 103, '_edit_last', '1'),
(347, 103, '_edit_lock', '1538444315:1'),
(348, 103, 'slider_link', 'teste.html'),
(349, 103, 'slider_link_title', 'QUERO CRIAR COM A LARISSA'),
(350, 104, '_wp_attached_file', '2018/08/slider-top-home-walldone.jpg'),
(351, 104, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1600;s:6:"height";i:800;s:4:"file";s:36:"2018/08/slider-top-home-walldone.jpg";s:5:"sizes";a:10:{s:9:"thumbnail";a:4:{s:4:"file";s:36:"slider-top-home-walldone-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:36:"slider-top-home-walldone-300x150.jpg";s:5:"width";i:300;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:36:"slider-top-home-walldone-768x384.jpg";s:5:"width";i:768;s:6:"height";i:384;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:37:"slider-top-home-walldone-1024x512.jpg";s:5:"width";i:1024;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:36:"slider-top-home-walldone-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:18:"woocommerce_single";a:4:{s:4:"file";s:36:"slider-top-home-walldone-600x300.jpg";s:5:"width";i:600;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:36:"slider-top-home-walldone-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:36:"slider-top-home-walldone-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:36:"slider-top-home-walldone-600x300.jpg";s:5:"width";i:600;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:36:"slider-top-home-walldone-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(352, 103, '_thumbnail_id', '104'),
(353, 8, '_edit_lock', '1535034930:1'),
(354, 8, '_edit_last', '1'),
(355, 8, '_wp_page_template', 'minha-conta.php'),
(356, 99, '_edit_lock', '1535152403:1'),
(357, 99, '_edit_last', '1'),
(358, 99, '_single_product_quote', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc non enim vehicula metus egestas dignissim at et ante. Proin pellentesque neque nec sollicitudin porta. Donec magna sapien, pretium sit amet laoreet nec, tincidunt in diam.'),
(359, 99, '_sold_individually', 'no'),
(360, 99, '_purchase_note', ''),
(361, 99, '_product_version', '3.4.4'),
(362, 96, '_edit_lock', '1538157121:1'),
(363, 96, '_sold_individually', 'no'),
(364, 96, '_purchase_note', ''),
(365, 96, '_product_version', '3.4.5'),
(366, 96, '_edit_last', '1'),
(367, 106, '_variation_description', ''),
(368, 106, '_sku', ''),
(369, 106, '_regular_price', '40.00'),
(370, 106, '_sale_price', '22.00'),
(371, 106, '_sale_price_dates_from', ''),
(372, 106, '_sale_price_dates_to', ''),
(373, 106, 'total_sales', '0'),
(374, 106, '_tax_status', 'taxable'),
(375, 106, '_tax_class', 'parent'),
(376, 106, '_manage_stock', 'no'),
(377, 106, '_backorders', 'no'),
(378, 106, '_sold_individually', 'no'),
(379, 106, '_weight', ''),
(380, 106, '_length', ''),
(381, 106, '_width', ''),
(382, 106, '_height', ''),
(383, 106, '_upsell_ids', 'a:0:{}'),
(384, 106, '_crosssell_ids', 'a:0:{}'),
(385, 106, '_purchase_note', ''),
(386, 106, '_default_attributes', 'a:0:{}'),
(387, 106, '_virtual', 'no'),
(388, 106, '_downloadable', 'no'),
(389, 106, '_product_image_gallery', ''),
(390, 106, '_download_limit', '-1'),
(391, 106, '_download_expiry', '-1'),
(392, 106, '_stock', NULL),
(393, 106, '_stock_status', 'instock'),
(394, 106, '_wc_average_rating', '0'),
(395, 106, '_wc_rating_count', 'a:0:{}'),
(396, 106, '_wc_review_count', '0'),
(397, 106, '_downloadable_files', 'a:0:{}'),
(398, 106, 'attribute_cores', 'Verde'),
(399, 106, 'attribute_molduras', 'Moldura Verde'),
(400, 106, '_price', '22.00'),
(401, 106, '_product_version', '3.4.5'),
(402, 107, '_variation_description', ''),
(403, 107, '_sku', ''),
(404, 107, '_regular_price', '45.90'),
(405, 107, '_sale_price', '29.90'),
(406, 107, '_sale_price_dates_from', ''),
(407, 107, '_sale_price_dates_to', ''),
(408, 107, 'total_sales', '0'),
(409, 107, '_tax_status', 'taxable'),
(410, 107, '_tax_class', 'parent'),
(411, 107, '_manage_stock', 'no'),
(412, 107, '_backorders', 'no'),
(413, 107, '_sold_individually', 'no'),
(414, 107, '_weight', ''),
(415, 107, '_length', ''),
(416, 107, '_width', ''),
(417, 107, '_height', ''),
(418, 107, '_upsell_ids', 'a:0:{}'),
(419, 107, '_crosssell_ids', 'a:0:{}'),
(420, 107, '_purchase_note', ''),
(421, 107, '_default_attributes', 'a:0:{}'),
(422, 107, '_virtual', 'no'),
(423, 107, '_downloadable', 'no'),
(424, 107, '_product_image_gallery', ''),
(425, 107, '_download_limit', '-1'),
(426, 107, '_download_expiry', '-1'),
(427, 107, '_stock', NULL),
(428, 107, '_stock_status', 'instock'),
(429, 107, '_wc_average_rating', '0'),
(430, 107, '_wc_rating_count', 'a:0:{}'),
(431, 107, '_wc_review_count', '0'),
(432, 107, '_downloadable_files', 'a:0:{}'),
(433, 107, 'attribute_cores', 'Preto'),
(434, 107, 'attribute_molduras', 'Moldura Verde'),
(435, 107, '_price', '29.90'),
(436, 107, '_product_version', '3.4.5'),
(437, 108, '_variation_description', ''),
(438, 108, '_sku', ''),
(439, 108, '_regular_price', ''),
(440, 108, '_sale_price', ''),
(441, 108, '_sale_price_dates_from', ''),
(442, 108, '_sale_price_dates_to', ''),
(443, 108, 'total_sales', '0'),
(444, 108, '_tax_status', 'taxable'),
(445, 108, '_tax_class', 'parent'),
(446, 108, '_manage_stock', 'no'),
(447, 108, '_backorders', 'no'),
(448, 108, '_sold_individually', 'no'),
(449, 108, '_weight', ''),
(450, 108, '_length', ''),
(451, 108, '_width', ''),
(452, 108, '_height', ''),
(453, 108, '_upsell_ids', 'a:0:{}'),
(454, 108, '_crosssell_ids', 'a:0:{}'),
(455, 108, '_purchase_note', ''),
(456, 108, '_default_attributes', 'a:0:{}'),
(457, 108, '_virtual', 'no'),
(458, 108, '_downloadable', 'no'),
(459, 108, '_product_image_gallery', ''),
(460, 108, '_download_limit', '-1'),
(461, 108, '_download_expiry', '-1'),
(462, 108, '_stock', NULL),
(463, 108, '_stock_status', 'instock'),
(464, 108, '_wc_average_rating', '0'),
(465, 108, '_wc_rating_count', 'a:0:{}'),
(466, 108, '_wc_review_count', '0'),
(467, 108, '_downloadable_files', 'a:0:{}'),
(468, 108, 'attribute_cores', 'Sem cor'),
(469, 108, 'attribute_molduras', 'Moldura Verde'),
(470, 108, '_price', ''),
(471, 108, '_product_version', '3.4.4'),
(472, 109, '_variation_description', ''),
(473, 109, '_sku', ''),
(474, 109, '_regular_price', '40.00'),
(475, 109, '_sale_price', '20.00'),
(476, 109, '_sale_price_dates_from', ''),
(477, 109, '_sale_price_dates_to', ''),
(478, 109, 'total_sales', '0'),
(479, 109, '_tax_status', 'taxable');
INSERT INTO `wd_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(480, 109, '_tax_class', 'parent'),
(481, 109, '_manage_stock', 'no'),
(482, 109, '_backorders', 'no'),
(483, 109, '_sold_individually', 'no'),
(484, 109, '_weight', ''),
(485, 109, '_length', ''),
(486, 109, '_width', ''),
(487, 109, '_height', ''),
(488, 109, '_upsell_ids', 'a:0:{}'),
(489, 109, '_crosssell_ids', 'a:0:{}'),
(490, 109, '_purchase_note', ''),
(491, 109, '_default_attributes', 'a:0:{}'),
(492, 109, '_virtual', 'no'),
(493, 109, '_downloadable', 'no'),
(494, 109, '_product_image_gallery', ''),
(495, 109, '_download_limit', '-1'),
(496, 109, '_download_expiry', '-1'),
(497, 109, '_stock', NULL),
(498, 109, '_stock_status', 'instock'),
(499, 109, '_wc_average_rating', '0'),
(500, 109, '_wc_rating_count', 'a:0:{}'),
(501, 109, '_wc_review_count', '0'),
(502, 109, '_downloadable_files', 'a:0:{}'),
(503, 109, 'attribute_cores', 'Verde'),
(504, 109, 'attribute_molduras', 'Moldura Preta'),
(505, 109, '_price', '20.00'),
(506, 109, '_product_version', '3.4.4'),
(507, 110, '_variation_description', ''),
(508, 110, '_sku', ''),
(509, 110, '_regular_price', '40.00'),
(510, 110, '_sale_price', '20.00'),
(511, 110, '_sale_price_dates_from', ''),
(512, 110, '_sale_price_dates_to', ''),
(513, 110, 'total_sales', '0'),
(514, 110, '_tax_status', 'taxable'),
(515, 110, '_tax_class', 'parent'),
(516, 110, '_manage_stock', 'no'),
(517, 110, '_backorders', 'no'),
(518, 110, '_sold_individually', 'no'),
(519, 110, '_weight', ''),
(520, 110, '_length', ''),
(521, 110, '_width', ''),
(522, 110, '_height', ''),
(523, 110, '_upsell_ids', 'a:0:{}'),
(524, 110, '_crosssell_ids', 'a:0:{}'),
(525, 110, '_purchase_note', ''),
(526, 110, '_default_attributes', 'a:0:{}'),
(527, 110, '_virtual', 'no'),
(528, 110, '_downloadable', 'no'),
(529, 110, '_product_image_gallery', ''),
(530, 110, '_download_limit', '-1'),
(531, 110, '_download_expiry', '-1'),
(532, 110, '_stock', NULL),
(533, 110, '_stock_status', 'instock'),
(534, 110, '_wc_average_rating', '0'),
(535, 110, '_wc_rating_count', 'a:0:{}'),
(536, 110, '_wc_review_count', '0'),
(537, 110, '_downloadable_files', 'a:0:{}'),
(538, 110, 'attribute_cores', 'Preto'),
(539, 110, 'attribute_molduras', 'Moldura Preta'),
(540, 110, '_price', '20.00'),
(541, 110, '_product_version', '3.4.4'),
(542, 111, '_variation_description', ''),
(543, 111, '_sku', ''),
(544, 111, '_regular_price', '40.00'),
(545, 111, '_sale_price', '20.00'),
(546, 111, '_sale_price_dates_from', ''),
(547, 111, '_sale_price_dates_to', ''),
(548, 111, 'total_sales', '0'),
(549, 111, '_tax_status', 'taxable'),
(550, 111, '_tax_class', 'parent'),
(551, 111, '_manage_stock', 'no'),
(552, 111, '_backorders', 'no'),
(553, 111, '_sold_individually', 'no'),
(554, 111, '_weight', ''),
(555, 111, '_length', ''),
(556, 111, '_width', ''),
(557, 111, '_height', ''),
(558, 111, '_upsell_ids', 'a:0:{}'),
(559, 111, '_crosssell_ids', 'a:0:{}'),
(560, 111, '_purchase_note', ''),
(561, 111, '_default_attributes', 'a:0:{}'),
(562, 111, '_virtual', 'no'),
(563, 111, '_downloadable', 'no'),
(564, 111, '_product_image_gallery', ''),
(565, 111, '_download_limit', '-1'),
(566, 111, '_download_expiry', '-1'),
(567, 111, '_stock', NULL),
(568, 111, '_stock_status', 'instock'),
(569, 111, '_wc_average_rating', '0'),
(570, 111, '_wc_rating_count', 'a:0:{}'),
(571, 111, '_wc_review_count', '0'),
(572, 111, '_downloadable_files', 'a:0:{}'),
(573, 111, 'attribute_cores', 'Sem cor'),
(574, 111, 'attribute_molduras', 'Moldura Preta'),
(575, 111, '_price', '20.00'),
(576, 111, '_product_version', '3.4.4'),
(577, 112, '_variation_description', ''),
(578, 112, '_sku', ''),
(579, 112, '_regular_price', '40.00'),
(580, 112, '_sale_price', '20.00'),
(581, 112, '_sale_price_dates_from', ''),
(582, 112, '_sale_price_dates_to', ''),
(583, 112, 'total_sales', '0'),
(584, 112, '_tax_status', 'taxable'),
(585, 112, '_tax_class', 'parent'),
(586, 112, '_manage_stock', 'no'),
(587, 112, '_backorders', 'no'),
(588, 112, '_sold_individually', 'no'),
(589, 112, '_weight', ''),
(590, 112, '_length', ''),
(591, 112, '_width', ''),
(592, 112, '_height', ''),
(593, 112, '_upsell_ids', 'a:0:{}'),
(594, 112, '_crosssell_ids', 'a:0:{}'),
(595, 112, '_purchase_note', ''),
(596, 112, '_default_attributes', 'a:0:{}'),
(597, 112, '_virtual', 'no'),
(598, 112, '_downloadable', 'no'),
(599, 112, '_product_image_gallery', ''),
(600, 112, '_download_limit', '-1'),
(601, 112, '_download_expiry', '-1'),
(602, 112, '_stock', NULL),
(603, 112, '_stock_status', 'instock'),
(604, 112, '_wc_average_rating', '0'),
(605, 112, '_wc_rating_count', 'a:0:{}'),
(606, 112, '_wc_review_count', '0'),
(607, 112, '_downloadable_files', 'a:0:{}'),
(608, 112, 'attribute_cores', 'Verde'),
(609, 112, 'attribute_molduras', 'Sem Moldura'),
(610, 112, '_price', '20.00'),
(611, 112, '_product_version', '3.4.4'),
(612, 113, '_variation_description', ''),
(613, 113, '_sku', ''),
(614, 113, '_regular_price', '40.00'),
(615, 113, '_sale_price', '20.00'),
(616, 113, '_sale_price_dates_from', ''),
(617, 113, '_sale_price_dates_to', ''),
(618, 113, 'total_sales', '0'),
(619, 113, '_tax_status', 'taxable'),
(620, 113, '_tax_class', 'parent'),
(621, 113, '_manage_stock', 'no'),
(622, 113, '_backorders', 'no'),
(623, 113, '_sold_individually', 'no'),
(624, 113, '_weight', ''),
(625, 113, '_length', ''),
(626, 113, '_width', ''),
(627, 113, '_height', ''),
(628, 113, '_upsell_ids', 'a:0:{}'),
(629, 113, '_crosssell_ids', 'a:0:{}'),
(630, 113, '_purchase_note', ''),
(631, 113, '_default_attributes', 'a:0:{}'),
(632, 113, '_virtual', 'no'),
(633, 113, '_downloadable', 'no'),
(634, 113, '_product_image_gallery', ''),
(635, 113, '_download_limit', '-1'),
(636, 113, '_download_expiry', '-1'),
(637, 113, '_stock', NULL),
(638, 113, '_stock_status', 'instock'),
(639, 113, '_wc_average_rating', '0'),
(640, 113, '_wc_rating_count', 'a:0:{}'),
(641, 113, '_wc_review_count', '0'),
(642, 113, '_downloadable_files', 'a:0:{}'),
(643, 113, 'attribute_cores', 'Preto'),
(644, 113, 'attribute_molduras', 'Sem Moldura'),
(645, 113, '_price', '20.00'),
(646, 113, '_product_version', '3.4.4'),
(647, 114, '_variation_description', ''),
(648, 114, '_sku', ''),
(649, 114, '_regular_price', '40.00'),
(650, 114, '_sale_price', '20.00'),
(651, 114, '_sale_price_dates_from', ''),
(652, 114, '_sale_price_dates_to', ''),
(653, 114, 'total_sales', '0'),
(654, 114, '_tax_status', 'taxable'),
(655, 114, '_tax_class', 'parent'),
(656, 114, '_manage_stock', 'no'),
(657, 114, '_backorders', 'no'),
(658, 114, '_sold_individually', 'no'),
(659, 114, '_weight', ''),
(660, 114, '_length', ''),
(661, 114, '_width', ''),
(662, 114, '_height', ''),
(663, 114, '_upsell_ids', 'a:0:{}'),
(664, 114, '_crosssell_ids', 'a:0:{}'),
(665, 114, '_purchase_note', ''),
(666, 114, '_default_attributes', 'a:0:{}'),
(667, 114, '_virtual', 'no'),
(668, 114, '_downloadable', 'no'),
(669, 114, '_product_image_gallery', ''),
(670, 114, '_download_limit', '-1'),
(671, 114, '_download_expiry', '-1'),
(672, 114, '_stock', NULL),
(673, 114, '_stock_status', 'instock'),
(674, 114, '_wc_average_rating', '0'),
(675, 114, '_wc_rating_count', 'a:0:{}'),
(676, 114, '_wc_review_count', '0'),
(677, 114, '_downloadable_files', 'a:0:{}'),
(678, 114, 'attribute_cores', 'Sem cor'),
(679, 114, 'attribute_molduras', 'Sem Moldura'),
(680, 114, '_price', '20.00'),
(681, 114, '_product_version', '3.4.4'),
(704, 96, '_single_product_quote', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc non enim vehicula metus egestas dignissim at et ante. Proin pellentesque neque nec sollicitudin porta. Donec magna sapien, pretium sit amet laoreet nec, tincidunt in diam.'),
(705, 116, '_wp_attached_file', '2018/06/teste-quadro-gato-frida-aquarela-walldone-1.jpg'),
(706, 116, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1200;s:6:"height";i:1200;s:4:"file";s:55:"2018/06/teste-quadro-gato-frida-aquarela-walldone-1.jpg";s:5:"sizes";a:11:{s:9:"thumbnail";a:4:{s:4:"file";s:55:"teste-quadro-gato-frida-aquarela-walldone-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:55:"teste-quadro-gato-frida-aquarela-walldone-1-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:55:"teste-quadro-gato-frida-aquarela-walldone-1-768x768.jpg";s:5:"width";i:768;s:6:"height";i:768;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:57:"teste-quadro-gato-frida-aquarela-walldone-1-1024x1024.jpg";s:5:"width";i:1024;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:56:"teste-quadro-gato-frida-aquarela-walldone-1-1200x800.jpg";s:5:"width";i:1200;s:6:"height";i:800;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:55:"teste-quadro-gato-frida-aquarela-walldone-1-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:18:"woocommerce_single";a:4:{s:4:"file";s:55:"teste-quadro-gato-frida-aquarela-walldone-1-600x600.jpg";s:5:"width";i:600;s:6:"height";i:600;s:9:"mime-type";s:10:"image/jpeg";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:55:"teste-quadro-gato-frida-aquarela-walldone-1-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:55:"teste-quadro-gato-frida-aquarela-walldone-1-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:55:"teste-quadro-gato-frida-aquarela-walldone-1-600x600.jpg";s:5:"width";i:600;s:6:"height";i:600;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:55:"teste-quadro-gato-frida-aquarela-walldone-1-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(707, 99, '_product_attributes', 'a:1:{s:11:"pa_molduras";a:6:{s:4:"name";s:11:"pa_molduras";s:5:"value";s:0:"";s:8:"position";i:2;s:10:"is_visible";i:1;s:12:"is_variation";i:0;s:11:"is_taxonomy";i:1;}}'),
(711, 118, '_edit_last', '1'),
(712, 118, '_edit_lock', '1538443670:1'),
(713, 118, '_wp_page_template', 'promocoes.php'),
(734, 129, '_sku', '98749832794827-1'),
(735, 129, '_regular_price', '25.00'),
(736, 129, '_sale_price', '20.00'),
(737, 129, '_sale_price_dates_from', ''),
(738, 129, '_sale_price_dates_to', ''),
(739, 129, 'total_sales', '1'),
(740, 129, '_tax_status', 'taxable'),
(741, 129, '_tax_class', ''),
(742, 129, '_manage_stock', 'no'),
(743, 129, '_backorders', 'no'),
(744, 129, '_sold_individually', 'no'),
(745, 129, '_weight', '0.8'),
(746, 129, '_length', '29'),
(747, 129, '_width', '11'),
(748, 129, '_height', '42'),
(749, 129, '_upsell_ids', 'a:0:{}'),
(750, 129, '_crosssell_ids', 'a:0:{}'),
(751, 129, '_purchase_note', ''),
(752, 129, '_default_attributes', 'a:0:{}'),
(753, 129, '_virtual', 'no'),
(754, 129, '_downloadable', 'no'),
(755, 129, '_product_image_gallery', '101,100,116'),
(756, 129, '_download_limit', '-1'),
(757, 129, '_download_expiry', '-1'),
(758, 129, '_thumbnail_id', '100'),
(759, 129, '_stock', NULL),
(760, 129, '_stock_status', 'instock'),
(761, 129, '_wc_average_rating', '0'),
(762, 129, '_wc_rating_count', 'a:0:{}'),
(763, 129, '_wc_review_count', '0'),
(764, 129, '_downloadable_files', 'a:0:{}'),
(765, 129, '_product_attributes', 'a:1:{s:11:"pa_molduras";a:6:{s:4:"name";s:11:"pa_molduras";s:5:"value";s:0:"";s:8:"position";i:2;s:10:"is_visible";i:1;s:12:"is_variation";i:0;s:11:"is_taxonomy";i:1;}}'),
(766, 129, '_product_version', '3.4.5'),
(767, 129, '_price', '20.00'),
(768, 129, '_upsell_skus', 'a:0:{}'),
(769, 129, '_crosssell_skus', 'a:0:{}'),
(770, 129, '_min_variation_price', ''),
(771, 129, '_max_variation_price', ''),
(772, 129, '_min_variation_regular_price', ''),
(773, 129, '_max_variation_regular_price', ''),
(774, 129, '_min_variation_sale_price', ''),
(775, 129, '_max_variation_sale_price', ''),
(776, 129, '_file_path', ''),
(777, 129, '_product_url', ''),
(778, 129, '_button_text', ''),
(779, 129, '_single_product_quote', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc non enim vehicula metus egestas dignissim at et ante. Proin pellentesque neque nec sollicitudin porta. Donec magna sapien, pretium sit amet laoreet nec, tincidunt in diam.'),
(780, 129, '_edit_lock', '1538074565:1'),
(781, 129, '_edit_last', '1'),
(782, 132, '_edit_lock', '1536066716:1'),
(783, 132, '_edit_last', '1'),
(784, 132, '_wp_page_template', 'blog.php'),
(785, 1, '_edit_lock', '1535379429:1'),
(786, 1, '_edit_last', '1'),
(803, 136, '_edit_lock', '1535334620:1'),
(804, 136, '_edit_last', '1'),
(805, 136, 'slider_link', 'teste.html'),
(806, 136, 'slider_link_title', 'Clique e confira!'),
(807, 137, '_wp_attached_file', '2018/08/teste-slider-blog-walldone.jpg'),
(808, 137, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1200;s:6:"height";i:600;s:4:"file";s:38:"2018/08/teste-slider-blog-walldone.jpg";s:5:"sizes";a:10:{s:9:"thumbnail";a:4:{s:4:"file";s:38:"teste-slider-blog-walldone-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:38:"teste-slider-blog-walldone-300x150.jpg";s:5:"width";i:300;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:38:"teste-slider-blog-walldone-768x384.jpg";s:5:"width";i:768;s:6:"height";i:384;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:39:"teste-slider-blog-walldone-1024x512.jpg";s:5:"width";i:1024;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:38:"teste-slider-blog-walldone-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:18:"woocommerce_single";a:4:{s:4:"file";s:38:"teste-slider-blog-walldone-600x300.jpg";s:5:"width";i:600;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:38:"teste-slider-blog-walldone-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:38:"teste-slider-blog-walldone-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:38:"teste-slider-blog-walldone-600x300.jpg";s:5:"width";i:600;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:38:"teste-slider-blog-walldone-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:22:"Maura Mello Fotografia";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(809, 138, '_wp_attached_file', '2018/08/teste2-slider-blog-walldone.jpg'),
(810, 138, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1200;s:6:"height";i:600;s:4:"file";s:39:"2018/08/teste2-slider-blog-walldone.jpg";s:5:"sizes";a:10:{s:9:"thumbnail";a:4:{s:4:"file";s:39:"teste2-slider-blog-walldone-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:39:"teste2-slider-blog-walldone-300x150.jpg";s:5:"width";i:300;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:39:"teste2-slider-blog-walldone-768x384.jpg";s:5:"width";i:768;s:6:"height";i:384;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:40:"teste2-slider-blog-walldone-1024x512.jpg";s:5:"width";i:1024;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:39:"teste2-slider-blog-walldone-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:18:"woocommerce_single";a:4:{s:4:"file";s:39:"teste2-slider-blog-walldone-600x300.jpg";s:5:"width";i:600;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:39:"teste2-slider-blog-walldone-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:39:"teste2-slider-blog-walldone-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:39:"teste2-slider-blog-walldone-600x300.jpg";s:5:"width";i:600;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:39:"teste2-slider-blog-walldone-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(811, 136, '_thumbnail_id', '138'),
(813, 140, '_edit_lock', '1535335891:1'),
(814, 140, '_edit_last', '1'),
(815, 140, 'slider_link', 'teste.html'),
(816, 140, 'slider_link_title', 'Clique e confira!'),
(817, 140, '_thumbnail_id', '137'),
(820, 140, '_wp_old_slug', 'lorem-ipsum-dolor-sit'),
(821, 141, '_edit_lock', '1535394899:1'),
(822, 141, '_edit_last', '1'),
(824, 142, '_edit_lock', '1535379471:1'),
(825, 142, '_edit_last', '1'),
(827, 143, '_edit_lock', '1535381613:1'),
(828, 143, '_edit_last', '1'),
(830, 144, '_edit_lock', '1535379534:1'),
(831, 144, '_edit_last', '1'),
(833, 145, '_edit_lock', '1535393382:1'),
(834, 145, '_edit_last', '1'),
(836, 146, '_wp_attached_file', '2018/08/teste-1-image-blog-walldone.jpg'),
(837, 146, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:870;s:6:"height";i:550;s:4:"file";s:39:"2018/08/teste-1-image-blog-walldone.jpg";s:5:"sizes";a:9:{s:9:"thumbnail";a:4:{s:4:"file";s:39:"teste-1-image-blog-walldone-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:39:"teste-1-image-blog-walldone-300x190.jpg";s:5:"width";i:300;s:6:"height";i:190;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:39:"teste-1-image-blog-walldone-768x486.jpg";s:5:"width";i:768;s:6:"height";i:486;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:39:"teste-1-image-blog-walldone-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:18:"woocommerce_single";a:4:{s:4:"file";s:39:"teste-1-image-blog-walldone-600x379.jpg";s:5:"width";i:600;s:6:"height";i:379;s:9:"mime-type";s:10:"image/jpeg";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:39:"teste-1-image-blog-walldone-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:39:"teste-1-image-blog-walldone-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:39:"teste-1-image-blog-walldone-600x379.jpg";s:5:"width";i:600;s:6:"height";i:379;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:39:"teste-1-image-blog-walldone-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(838, 147, '_wp_attached_file', '2018/08/teste-2-image-blog-walldone.jpg'),
(839, 147, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:870;s:6:"height";i:550;s:4:"file";s:39:"2018/08/teste-2-image-blog-walldone.jpg";s:5:"sizes";a:9:{s:9:"thumbnail";a:4:{s:4:"file";s:39:"teste-2-image-blog-walldone-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:39:"teste-2-image-blog-walldone-300x190.jpg";s:5:"width";i:300;s:6:"height";i:190;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:39:"teste-2-image-blog-walldone-768x486.jpg";s:5:"width";i:768;s:6:"height";i:486;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:39:"teste-2-image-blog-walldone-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:18:"woocommerce_single";a:4:{s:4:"file";s:39:"teste-2-image-blog-walldone-600x379.jpg";s:5:"width";i:600;s:6:"height";i:379;s:9:"mime-type";s:10:"image/jpeg";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:39:"teste-2-image-blog-walldone-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:39:"teste-2-image-blog-walldone-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:39:"teste-2-image-blog-walldone-600x379.jpg";s:5:"width";i:600;s:6:"height";i:379;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:39:"teste-2-image-blog-walldone-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(840, 148, '_wp_attached_file', '2018/08/teste-3-image-blog-walldone.jpg'),
(841, 148, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:870;s:6:"height";i:550;s:4:"file";s:39:"2018/08/teste-3-image-blog-walldone.jpg";s:5:"sizes";a:9:{s:9:"thumbnail";a:4:{s:4:"file";s:39:"teste-3-image-blog-walldone-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:39:"teste-3-image-blog-walldone-300x190.jpg";s:5:"width";i:300;s:6:"height";i:190;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:39:"teste-3-image-blog-walldone-768x486.jpg";s:5:"width";i:768;s:6:"height";i:486;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:39:"teste-3-image-blog-walldone-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:18:"woocommerce_single";a:4:{s:4:"file";s:39:"teste-3-image-blog-walldone-600x379.jpg";s:5:"width";i:600;s:6:"height";i:379;s:9:"mime-type";s:10:"image/jpeg";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:39:"teste-3-image-blog-walldone-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:39:"teste-3-image-blog-walldone-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:39:"teste-3-image-blog-walldone-600x379.jpg";s:5:"width";i:600;s:6:"height";i:379;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:39:"teste-3-image-blog-walldone-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(842, 149, '_wp_attached_file', '2018/08/teste-4-image-blog-walldone.jpg'),
(843, 149, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:870;s:6:"height";i:550;s:4:"file";s:39:"2018/08/teste-4-image-blog-walldone.jpg";s:5:"sizes";a:9:{s:9:"thumbnail";a:4:{s:4:"file";s:39:"teste-4-image-blog-walldone-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:39:"teste-4-image-blog-walldone-300x190.jpg";s:5:"width";i:300;s:6:"height";i:190;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:39:"teste-4-image-blog-walldone-768x486.jpg";s:5:"width";i:768;s:6:"height";i:486;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:39:"teste-4-image-blog-walldone-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:18:"woocommerce_single";a:4:{s:4:"file";s:39:"teste-4-image-blog-walldone-600x379.jpg";s:5:"width";i:600;s:6:"height";i:379;s:9:"mime-type";s:10:"image/jpeg";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:39:"teste-4-image-blog-walldone-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:39:"teste-4-image-blog-walldone-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:39:"teste-4-image-blog-walldone-600x379.jpg";s:5:"width";i:600;s:6:"height";i:379;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:39:"teste-4-image-blog-walldone-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(846, 151, '_wp_attached_file', '2018/08/teste-6-image-blog-walldone.jpg'),
(847, 151, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:870;s:6:"height";i:550;s:4:"file";s:39:"2018/08/teste-6-image-blog-walldone.jpg";s:5:"sizes";a:9:{s:9:"thumbnail";a:4:{s:4:"file";s:39:"teste-6-image-blog-walldone-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:39:"teste-6-image-blog-walldone-300x190.jpg";s:5:"width";i:300;s:6:"height";i:190;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:39:"teste-6-image-blog-walldone-768x486.jpg";s:5:"width";i:768;s:6:"height";i:486;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:39:"teste-6-image-blog-walldone-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:18:"woocommerce_single";a:4:{s:4:"file";s:39:"teste-6-image-blog-walldone-600x379.jpg";s:5:"width";i:600;s:6:"height";i:379;s:9:"mime-type";s:10:"image/jpeg";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:39:"teste-6-image-blog-walldone-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:39:"teste-6-image-blog-walldone-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:39:"teste-6-image-blog-walldone-600x379.jpg";s:5:"width";i:600;s:6:"height";i:379;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:39:"teste-6-image-blog-walldone-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(848, 1, '_thumbnail_id', '146'),
(851, 1, '_wp_old_slug', 'ola-mundo'),
(852, 141, '_thumbnail_id', '147'),
(855, 141, '_wp_old_slug', 'ola-mundo'),
(856, 142, '_thumbnail_id', '148'),
(859, 142, '_wp_old_slug', 'ola-mundo'),
(860, 143, '_thumbnail_id', '149'),
(863, 143, '_wp_old_slug', 'ola-mundo'),
(867, 144, '_wp_old_slug', 'ola-mundo'),
(868, 145, '_thumbnail_id', '151'),
(871, 145, '_wp_old_slug', 'ola-mundo'),
(872, 102, '_config_errors', 'a:1:{s:23:"mail.additional_headers";a:1:{i:0;a:2:{s:4:"code";i:102;s:4:"args";a:3:{s:7:"message";s:64:"A sintaxe de caixa de e-mail usada no campo %name% é inválida.";s:6:"params";a:1:{s:4:"name";s:8:"Reply-To";}s:4:"link";s:68:"https://contactform7.com/configuration-errors/invalid-mailbox-syntax";}}}}'),
(873, 161, '_edit_lock', '1536066430:1'),
(874, 161, '_edit_last', '1'),
(875, 161, '_wp_page_template', 'default'),
(879, 161, '_wp_trash_meta_status', 'publish'),
(880, 161, '_wp_trash_meta_time', '1536066841'),
(881, 161, '_wp_desired_post_slug', 'pagina-inicial'),
(882, 163, '_wp_trash_meta_status', 'publish'),
(883, 163, '_wp_trash_meta_time', '1536067341'),
(884, 164, '_edit_last', '1'),
(885, 164, '_wp_page_template', 'meu-kit.php'),
(886, 164, '_edit_lock', '1536853445:1'),
(887, 166, '_edit_lock', '1536161862:1'),
(888, 166, '_edit_last', '1'),
(889, 166, '_wp_page_template', 'meu-kit-briefing.php'),
(927, 6, '_edit_lock', '1538504832:1'),
(928, 6, '_edit_last', '1'),
(929, 6, '_wp_page_template', 'carrinho-de-compras.php'),
(984, 217, '_wp_attached_file', '2018/09/3-9-2018-904-1.png'),
(985, 217, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:720;s:6:"height";i:720;s:4:"file";s:26:"2018/09/3-9-2018-904-1.png";s:5:"sizes";a:9:{s:9:"thumbnail";a:4:{s:4:"file";s:26:"3-9-2018-904-1-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:26:"3-9-2018-904-1-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:14:"post-thumbnail";a:4:{s:4:"file";s:26:"3-9-2018-904-1-720x600.png";s:5:"width";i:720;s:6:"height";i:600;s:9:"mime-type";s:9:"image/png";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:26:"3-9-2018-904-1-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";s:9:"uncropped";b:1;}s:18:"woocommerce_single";a:4:{s:4:"file";s:26:"3-9-2018-904-1-600x600.png";s:5:"width";i:600;s:6:"height";i:600;s:9:"mime-type";s:9:"image/png";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:26:"3-9-2018-904-1-100x100.png";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:9:"image/png";}s:12:"shop_catalog";a:4:{s:4:"file";s:26:"3-9-2018-904-1-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:11:"shop_single";a:4:{s:4:"file";s:26:"3-9-2018-904-1-600x600.png";s:5:"width";i:600;s:6:"height";i:600;s:9:"mime-type";s:9:"image/png";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:26:"3-9-2018-904-1-100x100.png";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(986, 218, '_wp_attached_file', '2018/09/Captura-de-Tela-2018-09-05-às-10.00.12.png'),
(987, 218, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:777;s:6:"height";i:820;s:4:"file";s:52:"2018/09/Captura-de-Tela-2018-09-05-às-10.00.12.png";s:5:"sizes";a:10:{s:9:"thumbnail";a:4:{s:4:"file";s:52:"Captura-de-Tela-2018-09-05-às-10.00.12-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:52:"Captura-de-Tela-2018-09-05-às-10.00.12-284x300.png";s:5:"width";i:284;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:12:"medium_large";a:4:{s:4:"file";s:52:"Captura-de-Tela-2018-09-05-às-10.00.12-768x811.png";s:5:"width";i:768;s:6:"height";i:811;s:9:"mime-type";s:9:"image/png";}s:14:"post-thumbnail";a:4:{s:4:"file";s:52:"Captura-de-Tela-2018-09-05-às-10.00.12-777x600.png";s:5:"width";i:777;s:6:"height";i:600;s:9:"mime-type";s:9:"image/png";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:52:"Captura-de-Tela-2018-09-05-às-10.00.12-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";s:9:"uncropped";b:1;}s:18:"woocommerce_single";a:4:{s:4:"file";s:52:"Captura-de-Tela-2018-09-05-às-10.00.12-600x633.png";s:5:"width";i:600;s:6:"height";i:633;s:9:"mime-type";s:9:"image/png";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:52:"Captura-de-Tela-2018-09-05-às-10.00.12-100x100.png";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:9:"image/png";}s:12:"shop_catalog";a:4:{s:4:"file";s:52:"Captura-de-Tela-2018-09-05-às-10.00.12-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:11:"shop_single";a:4:{s:4:"file";s:52:"Captura-de-Tela-2018-09-05-às-10.00.12-600x633.png";s:5:"width";i:600;s:6:"height";i:633;s:9:"mime-type";s:9:"image/png";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:52:"Captura-de-Tela-2018-09-05-às-10.00.12-100x100.png";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(988, 219, '_wp_attached_file', '2018/09/Captura-de-Tela-2018-09-05-às-10.00.12-1.png'),
(989, 220, '_wp_attached_file', '2018/09/3-9-2018-904-2.png'),
(990, 219, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:777;s:6:"height";i:820;s:4:"file";s:54:"2018/09/Captura-de-Tela-2018-09-05-às-10.00.12-1.png";s:5:"sizes";a:10:{s:9:"thumbnail";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-1-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-1-284x300.png";s:5:"width";i:284;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:12:"medium_large";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-1-768x811.png";s:5:"width";i:768;s:6:"height";i:811;s:9:"mime-type";s:9:"image/png";}s:14:"post-thumbnail";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-1-777x600.png";s:5:"width";i:777;s:6:"height";i:600;s:9:"mime-type";s:9:"image/png";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-1-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";s:9:"uncropped";b:1;}s:18:"woocommerce_single";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-1-600x633.png";s:5:"width";i:600;s:6:"height";i:633;s:9:"mime-type";s:9:"image/png";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-1-100x100.png";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:9:"image/png";}s:12:"shop_catalog";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-1-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:11:"shop_single";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-1-600x633.png";s:5:"width";i:600;s:6:"height";i:633;s:9:"mime-type";s:9:"image/png";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-1-100x100.png";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(991, 220, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:720;s:6:"height";i:720;s:4:"file";s:26:"2018/09/3-9-2018-904-2.png";s:5:"sizes";a:9:{s:9:"thumbnail";a:4:{s:4:"file";s:26:"3-9-2018-904-2-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:26:"3-9-2018-904-2-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:14:"post-thumbnail";a:4:{s:4:"file";s:26:"3-9-2018-904-2-720x600.png";s:5:"width";i:720;s:6:"height";i:600;s:9:"mime-type";s:9:"image/png";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:26:"3-9-2018-904-2-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";s:9:"uncropped";b:1;}s:18:"woocommerce_single";a:4:{s:4:"file";s:26:"3-9-2018-904-2-600x600.png";s:5:"width";i:600;s:6:"height";i:600;s:9:"mime-type";s:9:"image/png";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:26:"3-9-2018-904-2-100x100.png";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:9:"image/png";}s:12:"shop_catalog";a:4:{s:4:"file";s:26:"3-9-2018-904-2-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:11:"shop_single";a:4:{s:4:"file";s:26:"3-9-2018-904-2-600x600.png";s:5:"width";i:600;s:6:"height";i:600;s:9:"mime-type";s:9:"image/png";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:26:"3-9-2018-904-2-100x100.png";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(992, 221, '_wp_attached_file', '2018/09/3-9-2018-904-3.png'),
(993, 221, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:720;s:6:"height";i:720;s:4:"file";s:26:"2018/09/3-9-2018-904-3.png";s:5:"sizes";a:9:{s:9:"thumbnail";a:4:{s:4:"file";s:26:"3-9-2018-904-3-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:26:"3-9-2018-904-3-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:14:"post-thumbnail";a:4:{s:4:"file";s:26:"3-9-2018-904-3-720x600.png";s:5:"width";i:720;s:6:"height";i:600;s:9:"mime-type";s:9:"image/png";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:26:"3-9-2018-904-3-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";s:9:"uncropped";b:1;}s:18:"woocommerce_single";a:4:{s:4:"file";s:26:"3-9-2018-904-3-600x600.png";s:5:"width";i:600;s:6:"height";i:600;s:9:"mime-type";s:9:"image/png";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:26:"3-9-2018-904-3-100x100.png";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:9:"image/png";}s:12:"shop_catalog";a:4:{s:4:"file";s:26:"3-9-2018-904-3-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:11:"shop_single";a:4:{s:4:"file";s:26:"3-9-2018-904-3-600x600.png";s:5:"width";i:600;s:6:"height";i:600;s:9:"mime-type";s:9:"image/png";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:26:"3-9-2018-904-3-100x100.png";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(994, 222, 'wpfm_file_name', '3_9_2018_904_1.png'),
(995, 222, 'wpfm_node_type', 'file'),
(996, 222, 'wpfm_title', '3_9_2018_904_1.png'),
(997, 222, 'wpfm_discription', ''),
(998, 222, 'wpfm_file_parent', '0'),
(999, 222, 'wpfm_dir_path', '/Users/agencia904/Dropbox/dev/htdocs/walldone-new/wp-content/uploads/user_uploads/agencia904/3_9_2018_904_1.png'),
(1000, 222, 'wpfm_file_url', 'http://localhost/walldone-new/wp-content/uploads/user_uploads/agencia904/3_9_2018_904_1.png'),
(1001, 222, 'wpfm_date_created', '5 de setembro de 2018'),
(1002, 222, 'wpfm_is_image', 'yes'),
(1003, 222, 'wpfm_file_thumb_url', 'http://localhost/walldone-new/wp-content/uploads/user_uploads/agencia904/thumbs/3_9_2018_904_1.png'),
(1004, 222, 'wpfm_file_size', '513 KB'),
(1005, 222, 'wpfm_total_downloads', '0'),
(1006, 223, '_wp_attached_file', 'http://localhost/walldone-new/wp-content/uploads/user_uploads/agencia904/3_9_2018_904_1.png'),
(1007, 223, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:720;s:6:"height";i:720;s:4:"file";s:42:"user_uploads/agencia904/3_9_2018_904_1.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1008, 222, 'wpfm_file_location', 'local'),
(1009, 224, 'wpfm_file_name', 'undefined'),
(1010, 224, 'wpfm_node_type', 'file'),
(1011, 224, 'wpfm_title', 'undefined'),
(1012, 224, 'wpfm_discription', ''),
(1013, 224, 'wpfm_file_parent', '0'),
(1014, 224, 'wpfm_dir_path', '/Users/agencia904/Dropbox/dev/htdocs/walldone-new/wp-content/uploads/user_uploads/agencia904/undefined'),
(1015, 224, 'wpfm_file_url', 'http://localhost/walldone-new/wp-content/uploads/user_uploads/agencia904/undefined'),
(1016, 224, 'wpfm_date_created', '5 de setembro de 2018'),
(1017, 224, 'wpfm_is_image', 'no'),
(1018, 224, 'wpfm_file_thumb_url', 'http://localhost/walldone-new/wp-content/uploads/user_uploads/agencia904/thumbs/undefined'),
(1019, 224, 'wpfm_file_size', '--'),
(1020, 224, 'wpfm_total_downloads', '0'),
(1021, 224, 'wpfm_file_location', 'local'),
(1022, 225, '_wp_attached_file', '2018/09/Captura-de-Tela-2018-09-05-às-10.00.12-2.png'),
(1023, 226, '_wp_attached_file', '2018/09/3-9-2018-904-4.png'),
(1024, 225, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:777;s:6:"height";i:820;s:4:"file";s:54:"2018/09/Captura-de-Tela-2018-09-05-às-10.00.12-2.png";s:5:"sizes";a:10:{s:9:"thumbnail";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-2-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-2-284x300.png";s:5:"width";i:284;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:12:"medium_large";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-2-768x811.png";s:5:"width";i:768;s:6:"height";i:811;s:9:"mime-type";s:9:"image/png";}s:14:"post-thumbnail";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-2-777x600.png";s:5:"width";i:777;s:6:"height";i:600;s:9:"mime-type";s:9:"image/png";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-2-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";s:9:"uncropped";b:1;}s:18:"woocommerce_single";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-2-600x633.png";s:5:"width";i:600;s:6:"height";i:633;s:9:"mime-type";s:9:"image/png";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-2-100x100.png";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:9:"image/png";}s:12:"shop_catalog";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-2-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:11:"shop_single";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-2-600x633.png";s:5:"width";i:600;s:6:"height";i:633;s:9:"mime-type";s:9:"image/png";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-2-100x100.png";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1025, 226, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:720;s:6:"height";i:720;s:4:"file";s:26:"2018/09/3-9-2018-904-4.png";s:5:"sizes";a:9:{s:9:"thumbnail";a:4:{s:4:"file";s:26:"3-9-2018-904-4-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:26:"3-9-2018-904-4-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:14:"post-thumbnail";a:4:{s:4:"file";s:26:"3-9-2018-904-4-720x600.png";s:5:"width";i:720;s:6:"height";i:600;s:9:"mime-type";s:9:"image/png";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:26:"3-9-2018-904-4-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";s:9:"uncropped";b:1;}s:18:"woocommerce_single";a:4:{s:4:"file";s:26:"3-9-2018-904-4-600x600.png";s:5:"width";i:600;s:6:"height";i:600;s:9:"mime-type";s:9:"image/png";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:26:"3-9-2018-904-4-100x100.png";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:9:"image/png";}s:12:"shop_catalog";a:4:{s:4:"file";s:26:"3-9-2018-904-4-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:11:"shop_single";a:4:{s:4:"file";s:26:"3-9-2018-904-4-600x600.png";s:5:"width";i:600;s:6:"height";i:600;s:9:"mime-type";s:9:"image/png";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:26:"3-9-2018-904-4-100x100.png";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1026, 227, '_wp_attached_file', '2018/09/Captura-de-Tela-2018-09-05-às-10.00.12-3.png'),
(1027, 228, '_wp_attached_file', '2018/09/3-9-2018-904-5.png'),
(1028, 227, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:777;s:6:"height";i:820;s:4:"file";s:54:"2018/09/Captura-de-Tela-2018-09-05-às-10.00.12-3.png";s:5:"sizes";a:10:{s:9:"thumbnail";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-3-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-3-284x300.png";s:5:"width";i:284;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:12:"medium_large";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-3-768x811.png";s:5:"width";i:768;s:6:"height";i:811;s:9:"mime-type";s:9:"image/png";}s:14:"post-thumbnail";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-3-777x600.png";s:5:"width";i:777;s:6:"height";i:600;s:9:"mime-type";s:9:"image/png";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-3-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";s:9:"uncropped";b:1;}s:18:"woocommerce_single";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-3-600x633.png";s:5:"width";i:600;s:6:"height";i:633;s:9:"mime-type";s:9:"image/png";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-3-100x100.png";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:9:"image/png";}s:12:"shop_catalog";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-3-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:11:"shop_single";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-3-600x633.png";s:5:"width";i:600;s:6:"height";i:633;s:9:"mime-type";s:9:"image/png";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-3-100x100.png";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1029, 228, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:720;s:6:"height";i:720;s:4:"file";s:26:"2018/09/3-9-2018-904-5.png";s:5:"sizes";a:9:{s:9:"thumbnail";a:4:{s:4:"file";s:26:"3-9-2018-904-5-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:26:"3-9-2018-904-5-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:14:"post-thumbnail";a:4:{s:4:"file";s:26:"3-9-2018-904-5-720x600.png";s:5:"width";i:720;s:6:"height";i:600;s:9:"mime-type";s:9:"image/png";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:26:"3-9-2018-904-5-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";s:9:"uncropped";b:1;}s:18:"woocommerce_single";a:4:{s:4:"file";s:26:"3-9-2018-904-5-600x600.png";s:5:"width";i:600;s:6:"height";i:600;s:9:"mime-type";s:9:"image/png";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:26:"3-9-2018-904-5-100x100.png";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:9:"image/png";}s:12:"shop_catalog";a:4:{s:4:"file";s:26:"3-9-2018-904-5-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:11:"shop_single";a:4:{s:4:"file";s:26:"3-9-2018-904-5-600x600.png";s:5:"width";i:600;s:6:"height";i:600;s:9:"mime-type";s:9:"image/png";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:26:"3-9-2018-904-5-100x100.png";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1030, 229, '_wp_attached_file', '2018/09/Captura-de-Tela-2018-09-05-às-10.00.12-4.png'),
(1031, 230, '_wp_attached_file', '2018/09/3-9-2018-904-6.png');
INSERT INTO `wd_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1032, 229, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:777;s:6:"height";i:820;s:4:"file";s:54:"2018/09/Captura-de-Tela-2018-09-05-às-10.00.12-4.png";s:5:"sizes";a:10:{s:9:"thumbnail";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-4-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-4-284x300.png";s:5:"width";i:284;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:12:"medium_large";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-4-768x811.png";s:5:"width";i:768;s:6:"height";i:811;s:9:"mime-type";s:9:"image/png";}s:14:"post-thumbnail";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-4-777x600.png";s:5:"width";i:777;s:6:"height";i:600;s:9:"mime-type";s:9:"image/png";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-4-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";s:9:"uncropped";b:1;}s:18:"woocommerce_single";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-4-600x633.png";s:5:"width";i:600;s:6:"height";i:633;s:9:"mime-type";s:9:"image/png";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-4-100x100.png";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:9:"image/png";}s:12:"shop_catalog";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-4-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:11:"shop_single";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-4-600x633.png";s:5:"width";i:600;s:6:"height";i:633;s:9:"mime-type";s:9:"image/png";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-4-100x100.png";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1033, 230, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:720;s:6:"height";i:720;s:4:"file";s:26:"2018/09/3-9-2018-904-6.png";s:5:"sizes";a:9:{s:9:"thumbnail";a:4:{s:4:"file";s:26:"3-9-2018-904-6-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:26:"3-9-2018-904-6-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:14:"post-thumbnail";a:4:{s:4:"file";s:26:"3-9-2018-904-6-720x600.png";s:5:"width";i:720;s:6:"height";i:600;s:9:"mime-type";s:9:"image/png";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:26:"3-9-2018-904-6-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";s:9:"uncropped";b:1;}s:18:"woocommerce_single";a:4:{s:4:"file";s:26:"3-9-2018-904-6-600x600.png";s:5:"width";i:600;s:6:"height";i:600;s:9:"mime-type";s:9:"image/png";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:26:"3-9-2018-904-6-100x100.png";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:9:"image/png";}s:12:"shop_catalog";a:4:{s:4:"file";s:26:"3-9-2018-904-6-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:11:"shop_single";a:4:{s:4:"file";s:26:"3-9-2018-904-6-600x600.png";s:5:"width";i:600;s:6:"height";i:600;s:9:"mime-type";s:9:"image/png";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:26:"3-9-2018-904-6-100x100.png";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1034, 231, '_wp_attached_file', '2018/09/3-9-2018-904-7.png'),
(1035, 232, '_wp_attached_file', '2018/09/Captura-de-Tela-2018-09-05-às-10.00.12-5.png'),
(1036, 232, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:777;s:6:"height";i:820;s:4:"file";s:54:"2018/09/Captura-de-Tela-2018-09-05-às-10.00.12-5.png";s:5:"sizes";a:10:{s:9:"thumbnail";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-5-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-5-284x300.png";s:5:"width";i:284;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:12:"medium_large";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-5-768x811.png";s:5:"width";i:768;s:6:"height";i:811;s:9:"mime-type";s:9:"image/png";}s:14:"post-thumbnail";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-5-777x600.png";s:5:"width";i:777;s:6:"height";i:600;s:9:"mime-type";s:9:"image/png";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-5-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";s:9:"uncropped";b:1;}s:18:"woocommerce_single";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-5-600x633.png";s:5:"width";i:600;s:6:"height";i:633;s:9:"mime-type";s:9:"image/png";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-5-100x100.png";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:9:"image/png";}s:12:"shop_catalog";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-5-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:11:"shop_single";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-5-600x633.png";s:5:"width";i:600;s:6:"height";i:633;s:9:"mime-type";s:9:"image/png";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-5-100x100.png";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1037, 231, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:720;s:6:"height";i:720;s:4:"file";s:26:"2018/09/3-9-2018-904-7.png";s:5:"sizes";a:9:{s:9:"thumbnail";a:4:{s:4:"file";s:26:"3-9-2018-904-7-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:26:"3-9-2018-904-7-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:14:"post-thumbnail";a:4:{s:4:"file";s:26:"3-9-2018-904-7-720x600.png";s:5:"width";i:720;s:6:"height";i:600;s:9:"mime-type";s:9:"image/png";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:26:"3-9-2018-904-7-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";s:9:"uncropped";b:1;}s:18:"woocommerce_single";a:4:{s:4:"file";s:26:"3-9-2018-904-7-600x600.png";s:5:"width";i:600;s:6:"height";i:600;s:9:"mime-type";s:9:"image/png";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:26:"3-9-2018-904-7-100x100.png";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:9:"image/png";}s:12:"shop_catalog";a:4:{s:4:"file";s:26:"3-9-2018-904-7-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:11:"shop_single";a:4:{s:4:"file";s:26:"3-9-2018-904-7-600x600.png";s:5:"width";i:600;s:6:"height";i:600;s:9:"mime-type";s:9:"image/png";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:26:"3-9-2018-904-7-100x100.png";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1038, 233, '_wp_attached_file', '2018/09/3-9-2018-904-8.png'),
(1039, 233, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:720;s:6:"height";i:720;s:4:"file";s:26:"2018/09/3-9-2018-904-8.png";s:5:"sizes";a:9:{s:9:"thumbnail";a:4:{s:4:"file";s:26:"3-9-2018-904-8-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:26:"3-9-2018-904-8-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:14:"post-thumbnail";a:4:{s:4:"file";s:26:"3-9-2018-904-8-720x600.png";s:5:"width";i:720;s:6:"height";i:600;s:9:"mime-type";s:9:"image/png";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:26:"3-9-2018-904-8-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";s:9:"uncropped";b:1;}s:18:"woocommerce_single";a:4:{s:4:"file";s:26:"3-9-2018-904-8-600x600.png";s:5:"width";i:600;s:6:"height";i:600;s:9:"mime-type";s:9:"image/png";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:26:"3-9-2018-904-8-100x100.png";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:9:"image/png";}s:12:"shop_catalog";a:4:{s:4:"file";s:26:"3-9-2018-904-8-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:11:"shop_single";a:4:{s:4:"file";s:26:"3-9-2018-904-8-600x600.png";s:5:"width";i:600;s:6:"height";i:600;s:9:"mime-type";s:9:"image/png";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:26:"3-9-2018-904-8-100x100.png";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1040, 234, '_wp_attached_file', '2018/09/3-9-2018-904-9.png'),
(1041, 235, '_wp_attached_file', '2018/09/Captura-de-Tela-2018-09-05-às-10.00.12-6.png'),
(1042, 235, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:777;s:6:"height";i:820;s:4:"file";s:54:"2018/09/Captura-de-Tela-2018-09-05-às-10.00.12-6.png";s:5:"sizes";a:10:{s:9:"thumbnail";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-6-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-6-284x300.png";s:5:"width";i:284;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:12:"medium_large";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-6-768x811.png";s:5:"width";i:768;s:6:"height";i:811;s:9:"mime-type";s:9:"image/png";}s:14:"post-thumbnail";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-6-777x600.png";s:5:"width";i:777;s:6:"height";i:600;s:9:"mime-type";s:9:"image/png";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-6-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";s:9:"uncropped";b:1;}s:18:"woocommerce_single";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-6-600x633.png";s:5:"width";i:600;s:6:"height";i:633;s:9:"mime-type";s:9:"image/png";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-6-100x100.png";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:9:"image/png";}s:12:"shop_catalog";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-6-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:11:"shop_single";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-6-600x633.png";s:5:"width";i:600;s:6:"height";i:633;s:9:"mime-type";s:9:"image/png";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-6-100x100.png";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1043, 234, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:720;s:6:"height";i:720;s:4:"file";s:26:"2018/09/3-9-2018-904-9.png";s:5:"sizes";a:9:{s:9:"thumbnail";a:4:{s:4:"file";s:26:"3-9-2018-904-9-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:26:"3-9-2018-904-9-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:14:"post-thumbnail";a:4:{s:4:"file";s:26:"3-9-2018-904-9-720x600.png";s:5:"width";i:720;s:6:"height";i:600;s:9:"mime-type";s:9:"image/png";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:26:"3-9-2018-904-9-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";s:9:"uncropped";b:1;}s:18:"woocommerce_single";a:4:{s:4:"file";s:26:"3-9-2018-904-9-600x600.png";s:5:"width";i:600;s:6:"height";i:600;s:9:"mime-type";s:9:"image/png";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:26:"3-9-2018-904-9-100x100.png";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:9:"image/png";}s:12:"shop_catalog";a:4:{s:4:"file";s:26:"3-9-2018-904-9-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:11:"shop_single";a:4:{s:4:"file";s:26:"3-9-2018-904-9-600x600.png";s:5:"width";i:600;s:6:"height";i:600;s:9:"mime-type";s:9:"image/png";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:26:"3-9-2018-904-9-100x100.png";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1044, 236, '_wp_attached_file', '2018/09/3-9-2018-904-10.png'),
(1045, 237, '_wp_attached_file', '2018/09/Captura-de-Tela-2018-09-05-às-10.00.12-7.png'),
(1046, 237, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:777;s:6:"height";i:820;s:4:"file";s:54:"2018/09/Captura-de-Tela-2018-09-05-às-10.00.12-7.png";s:5:"sizes";a:10:{s:9:"thumbnail";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-7-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-7-284x300.png";s:5:"width";i:284;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:12:"medium_large";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-7-768x811.png";s:5:"width";i:768;s:6:"height";i:811;s:9:"mime-type";s:9:"image/png";}s:14:"post-thumbnail";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-7-777x600.png";s:5:"width";i:777;s:6:"height";i:600;s:9:"mime-type";s:9:"image/png";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-7-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";s:9:"uncropped";b:1;}s:18:"woocommerce_single";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-7-600x633.png";s:5:"width";i:600;s:6:"height";i:633;s:9:"mime-type";s:9:"image/png";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-7-100x100.png";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:9:"image/png";}s:12:"shop_catalog";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-7-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:11:"shop_single";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-7-600x633.png";s:5:"width";i:600;s:6:"height";i:633;s:9:"mime-type";s:9:"image/png";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-7-100x100.png";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1047, 236, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:720;s:6:"height";i:720;s:4:"file";s:27:"2018/09/3-9-2018-904-10.png";s:5:"sizes";a:9:{s:9:"thumbnail";a:4:{s:4:"file";s:27:"3-9-2018-904-10-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:27:"3-9-2018-904-10-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:14:"post-thumbnail";a:4:{s:4:"file";s:27:"3-9-2018-904-10-720x600.png";s:5:"width";i:720;s:6:"height";i:600;s:9:"mime-type";s:9:"image/png";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:27:"3-9-2018-904-10-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";s:9:"uncropped";b:1;}s:18:"woocommerce_single";a:4:{s:4:"file";s:27:"3-9-2018-904-10-600x600.png";s:5:"width";i:600;s:6:"height";i:600;s:9:"mime-type";s:9:"image/png";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:27:"3-9-2018-904-10-100x100.png";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:9:"image/png";}s:12:"shop_catalog";a:4:{s:4:"file";s:27:"3-9-2018-904-10-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:11:"shop_single";a:4:{s:4:"file";s:27:"3-9-2018-904-10-600x600.png";s:5:"width";i:600;s:6:"height";i:600;s:9:"mime-type";s:9:"image/png";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:27:"3-9-2018-904-10-100x100.png";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1048, 238, '_wp_attached_file', '2018/09/Captura-de-Tela-2018-09-05-às-10.00.12-8.png'),
(1049, 239, '_wp_attached_file', '2018/09/3-9-2018-904-11.png'),
(1050, 238, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:777;s:6:"height";i:820;s:4:"file";s:54:"2018/09/Captura-de-Tela-2018-09-05-às-10.00.12-8.png";s:5:"sizes";a:10:{s:9:"thumbnail";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-8-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-8-284x300.png";s:5:"width";i:284;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:12:"medium_large";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-8-768x811.png";s:5:"width";i:768;s:6:"height";i:811;s:9:"mime-type";s:9:"image/png";}s:14:"post-thumbnail";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-8-777x600.png";s:5:"width";i:777;s:6:"height";i:600;s:9:"mime-type";s:9:"image/png";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-8-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";s:9:"uncropped";b:1;}s:18:"woocommerce_single";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-8-600x633.png";s:5:"width";i:600;s:6:"height";i:633;s:9:"mime-type";s:9:"image/png";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-8-100x100.png";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:9:"image/png";}s:12:"shop_catalog";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-8-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:11:"shop_single";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-8-600x633.png";s:5:"width";i:600;s:6:"height";i:633;s:9:"mime-type";s:9:"image/png";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:54:"Captura-de-Tela-2018-09-05-às-10.00.12-8-100x100.png";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1051, 239, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:720;s:6:"height";i:720;s:4:"file";s:27:"2018/09/3-9-2018-904-11.png";s:5:"sizes";a:9:{s:9:"thumbnail";a:4:{s:4:"file";s:27:"3-9-2018-904-11-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:27:"3-9-2018-904-11-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:14:"post-thumbnail";a:4:{s:4:"file";s:27:"3-9-2018-904-11-720x600.png";s:5:"width";i:720;s:6:"height";i:600;s:9:"mime-type";s:9:"image/png";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:27:"3-9-2018-904-11-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";s:9:"uncropped";b:1;}s:18:"woocommerce_single";a:4:{s:4:"file";s:27:"3-9-2018-904-11-600x600.png";s:5:"width";i:600;s:6:"height";i:600;s:9:"mime-type";s:9:"image/png";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:27:"3-9-2018-904-11-100x100.png";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:9:"image/png";}s:12:"shop_catalog";a:4:{s:4:"file";s:27:"3-9-2018-904-11-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:11:"shop_single";a:4:{s:4:"file";s:27:"3-9-2018-904-11-600x600.png";s:5:"width";i:600;s:6:"height";i:600;s:9:"mime-type";s:9:"image/png";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:27:"3-9-2018-904-11-100x100.png";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1052, 166, 'fu_form:7473e7ea4d69440f69abfd3cd04ee525', 'a:4:{s:8:"internal";a:4:{i:0;s:7:"post_ID";i:1;s:0:"";i:2;s:6:"action";i:3;s:11:"form_layout";}s:5:"title";a:1:{i:0;s:10:"post_title";}s:7:"content";a:1:{i:0;s:12:"post_content";}s:4:"meta";a:5:{i:0;s:10:"post_title";i:1;s:12:"post_content";i:2;s:5:"photo";i:3;s:0:"";i:4;s:5:"files";}}'),
(1053, 166, 'fu_form:92b6cbfa6120e13ff1654e28cef2a271', 'a:4:{s:8:"internal";a:4:{i:0;s:7:"post_ID";i:1;s:0:"";i:2;s:6:"action";i:3;s:11:"form_layout";}s:5:"title";a:1:{i:0;s:10:"post_title";}s:7:"content";a:1:{i:0;s:12:"post_content";}s:4:"meta";a:1:{i:0;s:5:"files";}}'),
(1054, 166, 'fu_form:14e303854237ea91c16b573841b63536', 'a:4:{s:8:"internal";a:4:{i:0;s:7:"post_ID";i:1;s:0:"";i:2;s:6:"action";i:3;s:11:"form_layout";}s:5:"title";a:1:{i:0;s:10:"post_title";}s:7:"content";a:1:{i:0;s:12:"post_content";}s:4:"meta";a:2:{i:0;s:5:"photo";i:1;s:5:"files";}}'),
(1056, 242, '_wc_review_count', '0'),
(1057, 242, '_wc_rating_count', 'a:0:{}'),
(1058, 242, '_wc_average_rating', '0'),
(1059, 242, '_edit_lock', '1538071550:1'),
(1060, 242, '_edit_last', '1'),
(1061, 242, '_sku', 'produto-kit'),
(1062, 242, '_regular_price', '100'),
(1063, 242, '_sale_price', ''),
(1064, 242, '_sale_price_dates_from', ''),
(1065, 242, '_sale_price_dates_to', ''),
(1066, 242, 'total_sales', '0'),
(1067, 242, '_tax_status', 'taxable'),
(1068, 242, '_tax_class', ''),
(1069, 242, '_manage_stock', 'no'),
(1070, 242, '_backorders', 'no'),
(1071, 242, '_sold_individually', 'no'),
(1072, 242, '_weight', ''),
(1073, 242, '_length', ''),
(1074, 242, '_width', ''),
(1075, 242, '_height', ''),
(1076, 242, '_upsell_ids', 'a:0:{}'),
(1077, 242, '_crosssell_ids', 'a:0:{}'),
(1078, 242, '_purchase_note', ''),
(1079, 242, '_default_attributes', 'a:0:{}'),
(1080, 242, '_virtual', 'no'),
(1081, 242, '_downloadable', 'no'),
(1082, 242, '_product_image_gallery', ''),
(1083, 242, '_download_limit', '-1'),
(1084, 242, '_download_expiry', '-1'),
(1085, 242, '_stock', NULL),
(1086, 242, '_stock_status', 'instock'),
(1087, 242, '_product_version', '3.4.5'),
(1088, 242, '_price', '100'),
(1089, 243, '_edit_lock', '1537555318:1'),
(1090, 243, '_edit_last', '1'),
(1091, 243, '_wp_page_template', 'meu-kit-briefing.php'),
(1092, 246, '_wp_attached_file', '2018/09/imagem_teste2.jpg'),
(1093, 245, '_wp_attached_file', '2018/09/imagem_teste3.jpg'),
(1094, 245, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:393;s:6:"height";i:393;s:4:"file";s:25:"2018/09/imagem_teste3.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:25:"imagem_teste3-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:25:"imagem_teste3-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:25:"imagem_teste3-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:25:"imagem_teste3-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:25:"imagem_teste3-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:25:"imagem_teste3-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1095, 246, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:393;s:6:"height";i:393;s:4:"file";s:25:"2018/09/imagem_teste2.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:25:"imagem_teste2-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:25:"imagem_teste2-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:25:"imagem_teste2-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:25:"imagem_teste2-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:25:"imagem_teste2-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:25:"imagem_teste2-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1096, 247, '_wp_attached_file', '2018/09/imagem_teste4.jpg'),
(1097, 247, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:393;s:6:"height";i:394;s:4:"file";s:25:"2018/09/imagem_teste4.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:25:"imagem_teste4-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:25:"imagem_teste4-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:25:"imagem_teste4-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:25:"imagem_teste4-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:25:"imagem_teste4-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:25:"imagem_teste4-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1098, 248, '_edit_lock', '1537474936:1'),
(1099, 248, '_edit_last', '1'),
(1100, 248, '_wp_page_template', 'page.php'),
(1101, 129, 'woosw_count', '1'),
(1102, 129, 'woosw_add', '1537456974'),
(1105, 242, 'custom_text_field_title', 'Produtos do Kit'),
(1106, 252, '_edit_lock', '1537477274:1'),
(1107, 252, '_edit_last', '1'),
(1108, 252, 'field_5ba405910928f', 'a:11:{s:3:"key";s:19:"field_5ba405910928f";s:5:"label";s:6:"Imagem";s:4:"name";s:6:"imagem";s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";s:1:"0";s:11:"save_format";s:2:"id";s:12:"preview_size";s:6:"medium";s:7:"library";s:3:"all";s:17:"conditional_logic";a:3:{s:6:"status";s:1:"0";s:5:"rules";a:1:{i:0;a:2:{s:5:"field";s:4:"null";s:8:"operator";s:2:"==";}}s:8:"allorany";s:3:"all";}s:8:"order_no";i:0;}'),
(1110, 252, 'position', 'normal'),
(1111, 252, 'layout', 'no_box'),
(1112, 252, 'hide_on_screen', ''),
(1113, 252, 'rule', 'a:5:{s:5:"param";s:4:"post";s:8:"operator";s:2:"==";s:5:"value";s:3:"242";s:8:"order_no";i:0;s:8:"group_no";i:0;}'),
(1114, 252, '_wp_trash_meta_status', 'publish'),
(1115, 252, '_wp_trash_meta_time', '1537477416'),
(1116, 252, '_wp_desired_post_slug', 'acf_upload-images'),
(1117, 253, '_wp_attached_file', '2018/09/imagem_teste2-1.jpg'),
(1118, 254, '_wp_attached_file', '2018/09/imagem_teste.jpg'),
(1119, 253, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:393;s:6:"height";i:393;s:4:"file";s:27:"2018/09/imagem_teste2-1.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:27:"imagem_teste2-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:27:"imagem_teste2-1-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:27:"imagem_teste2-1-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:27:"imagem_teste2-1-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:27:"imagem_teste2-1-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:27:"imagem_teste2-1-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1120, 254, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:393;s:6:"height";i:393;s:4:"file";s:24:"2018/09/imagem_teste.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"imagem_teste-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:24:"imagem_teste-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:24:"imagem_teste-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:24:"imagem_teste-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:24:"imagem_teste-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:24:"imagem_teste-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1121, 255, '_wp_attached_file', '2018/09/imagem_teste3-1.jpg'),
(1122, 256, '_wp_attached_file', '2018/09/imagem_teste4-1.jpg'),
(1123, 255, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:393;s:6:"height";i:393;s:4:"file";s:27:"2018/09/imagem_teste3-1.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:27:"imagem_teste3-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:27:"imagem_teste3-1-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:27:"imagem_teste3-1-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:27:"imagem_teste3-1-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:27:"imagem_teste3-1-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:27:"imagem_teste3-1-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1124, 256, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:393;s:6:"height";i:394;s:4:"file";s:27:"2018/09/imagem_teste4-1.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:27:"imagem_teste4-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:27:"imagem_teste4-1-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:27:"imagem_teste4-1-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:27:"imagem_teste4-1-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:27:"imagem_teste4-1-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:27:"imagem_teste4-1-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1125, 257, '_wp_attached_file', '2018/09/imagem_full.jpg'),
(1126, 258, '_wp_attached_file', '2018/09/imagem_teste-1.jpg'),
(1127, 258, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:393;s:6:"height";i:393;s:4:"file";s:26:"2018/09/imagem_teste-1.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:26:"imagem_teste-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:26:"imagem_teste-1-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:26:"imagem_teste-1-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:26:"imagem_teste-1-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:26:"imagem_teste-1-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:26:"imagem_teste-1-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1128, 257, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1280;s:6:"height";i:720;s:4:"file";s:23:"2018/09/imagem_full.jpg";s:5:"sizes";a:11:{s:9:"thumbnail";a:4:{s:4:"file";s:23:"imagem_full-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:23:"imagem_full-300x169.jpg";s:5:"width";i:300;s:6:"height";i:169;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:23:"imagem_full-768x432.jpg";s:5:"width";i:768;s:6:"height";i:432;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:24:"imagem_full-1024x576.jpg";s:5:"width";i:1024;s:6:"height";i:576;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:24:"imagem_full-1200x600.jpg";s:5:"width";i:1200;s:6:"height";i:600;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:23:"imagem_full-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:18:"woocommerce_single";a:4:{s:4:"file";s:23:"imagem_full-600x338.jpg";s:5:"width";i:600;s:6:"height";i:338;s:9:"mime-type";s:10:"image/jpeg";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:23:"imagem_full-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:23:"imagem_full-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:23:"imagem_full-600x338.jpg";s:5:"width";i:600;s:6:"height";i:338;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:23:"imagem_full-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1129, 259, '_wp_attached_file', '2018/09/imagem_teste2-2.jpg'),
(1130, 259, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:393;s:6:"height";i:393;s:4:"file";s:27:"2018/09/imagem_teste2-2.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:27:"imagem_teste2-2-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:27:"imagem_teste2-2-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:27:"imagem_teste2-2-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:27:"imagem_teste2-2-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:27:"imagem_teste2-2-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:27:"imagem_teste2-2-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1131, 260, '_wp_attached_file', '2018/09/imagem_teste3-2.jpg'),
(1132, 260, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:393;s:6:"height";i:393;s:4:"file";s:27:"2018/09/imagem_teste3-2.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:27:"imagem_teste3-2-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:27:"imagem_teste3-2-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:27:"imagem_teste3-2-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:27:"imagem_teste3-2-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:27:"imagem_teste3-2-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:27:"imagem_teste3-2-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1133, 261, '_wp_attached_file', '2018/09/imagem_teste4-2.jpg'),
(1134, 261, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:393;s:6:"height";i:394;s:4:"file";s:27:"2018/09/imagem_teste4-2.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:27:"imagem_teste4-2-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:27:"imagem_teste4-2-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:27:"imagem_teste4-2-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:27:"imagem_teste4-2-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:27:"imagem_teste4-2-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:27:"imagem_teste4-2-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1135, 262, 'wcccf_shipping_first_name', '{"type":"text","label":"Nome","is_unremovable":true,"is_enable":true,"name":"shipping_first_name","order":10,"placeholder":"","default_value":"","maxlength":"","required":"yes","message":"","visibility":"yes","order_meta":"yes","login_user_field":"no","show_for_roles":[],"cart_editable":"no","cloneable":"no","field_class":"","initial_show":"yes","locale":[],"key":"wcccf_shipping_first_name","use_this_label":false,"priority":110}'),
(1136, 262, 'wcccf_shipping_last_name', '{"type":"text","label":"Sobrenome","is_unremovable":true,"is_enable":true,"name":"shipping_last_name","order":20,"placeholder":"","default_value":"","maxlength":"","required":"yes","message":"","visibility":"yes","order_meta":"yes","login_user_field":"no","show_for_roles":[],"cart_editable":"no","cloneable":"no","field_class":"","initial_show":"yes","locale":[],"key":"wcccf_shipping_last_name","use_this_label":false,"priority":110}'),
(1137, 262, 'wcccf_shipping_company', '{"type":"text","label":"Nome da empresa","is_unremovable":true,"is_enable":true,"name":"shipping_company","order":30,"placeholder":"","default_value":"","maxlength":"","required":"no","message":"","visibility":"yes","order_meta":"yes","login_user_field":"no","show_for_roles":[],"cart_editable":"no","cloneable":"no","field_class":"","initial_show":"yes","locale":[],"key":"wcccf_shipping_company","use_this_label":false,"priority":110}'),
(1138, 262, 'wcccf_shipping_country', '{"type":"select","label":"Pa\\u00eds","is_unremovable":true,"is_enable":true,"name":"shipping_country","order":40,"placeholder":"","default_value":"","maxlength":"","required":"yes","message":"","visibility":"yes","order_meta":"yes","login_user_field":"no","show_for_roles":[],"cart_editable":"no","cloneable":"no","field_class":"","initial_show":"yes","locale":[],"key":"wcccf_shipping_country","use_this_label":false,"priority":110,"choices":"Don''t modify option|Don''t modify option"}'),
(1139, 262, 'wcccf_shipping_address_1', '{"type":"text","label":"Endere\\u00e7o","is_unremovable":true,"is_enable":true,"name":"shipping_address_1","order":50,"placeholder":"Nome da rua e n\\u00famero da casa","default_value":"","maxlength":"","required":"yes","message":"","visibility":"yes","order_meta":"yes","login_user_field":"no","show_for_roles":[],"cart_editable":"no","cloneable":"no","field_class":"","initial_show":"yes","locale":[],"key":"wcccf_shipping_address_1","use_this_label":false,"priority":110}'),
(1140, 262, 'wcccf_shipping_address_2', '{"type":"text","label":"Address 2 (Blank)","is_unremovable":true,"is_enable":true,"name":"shipping_address_2","order":60,"placeholder":"Apartamento, sala, condom\\u00ednio, etc. (opcional)","default_value":"","maxlength":"","required":"no","message":"","visibility":"yes","order_meta":"yes","login_user_field":"no","show_for_roles":[],"cart_editable":"no","cloneable":"no","field_class":"","initial_show":"yes","locale":[],"key":"wcccf_shipping_address_2","use_this_label":false,"priority":110}'),
(1141, 262, 'wcccf_shipping_city', '{"type":"text","label":"Cidade","is_unremovable":true,"is_enable":true,"name":"shipping_city","order":70,"placeholder":"","default_value":"","maxlength":"","required":"yes","message":"","visibility":"yes","order_meta":"yes","login_user_field":"no","show_for_roles":[],"cart_editable":"no","cloneable":"no","field_class":"","initial_show":"yes","locale":[],"key":"wcccf_shipping_city","use_this_label":false,"priority":110}'),
(1142, 262, 'wcccf_shipping_state', '{"type":"select","label":"Estado","is_unremovable":true,"is_enable":true,"name":"shipping_state","order":80,"placeholder":"","default_value":"","maxlength":"","required":"yes","message":"","visibility":"yes","order_meta":"yes","login_user_field":"no","show_for_roles":[],"cart_editable":"no","cloneable":"no","field_class":"","initial_show":"yes","locale":[],"key":"wcccf_shipping_state","use_this_label":false,"priority":110,"choices":"Don''t modify option|Don''t modify option"}'),
(1143, 262, 'wcccf_shipping_postcode', '{"type":"text","label":"CEP","is_unremovable":true,"is_enable":true,"name":"shipping_postcode","order":90,"placeholder":"","default_value":"","maxlength":"","required":"yes","message":"","visibility":"yes","order_meta":"yes","login_user_field":"no","show_for_roles":[],"cart_editable":"no","cloneable":"no","field_class":"","initial_show":"yes","locale":[],"key":"wcccf_shipping_postcode","use_this_label":false,"priority":110}'),
(1144, 263, 'wcccf_billing_first_name', '{"type":"text","label":"Nome","is_unremovable":true,"is_enable":true,"name":"billing_first_name","order":10,"placeholder":"","default_value":"","maxlength":"","required":"yes","message":"","visibility":"yes","order_meta":"yes","login_user_field":"no","show_for_roles":[],"cart_editable":"no","cloneable":"no","field_class":"","initial_show":"yes","locale":[],"key":"wcccf_billing_first_name","use_this_label":false,"priority":110}'),
(1145, 263, 'wcccf_billing_last_name', '{"type":"text","label":"Sobrenome","is_unremovable":true,"is_enable":true,"name":"billing_last_name","order":20,"placeholder":"","default_value":"","maxlength":"","required":"yes","message":"","visibility":"yes","order_meta":"yes","login_user_field":"no","show_for_roles":[],"cart_editable":"no","cloneable":"no","field_class":"","initial_show":"yes","locale":[],"key":"wcccf_billing_last_name","use_this_label":false,"priority":110}'),
(1146, 263, 'wcccf_billing_company', '{"type":"text","label":"Nome da empresa","is_unremovable":true,"is_enable":true,"name":"billing_company","order":30,"placeholder":"","default_value":"","maxlength":"","required":"no","message":"","visibility":"yes","order_meta":"yes","login_user_field":"no","show_for_roles":[],"cart_editable":"no","cloneable":"no","field_class":"","initial_show":"yes","locale":[],"key":"wcccf_billing_company","use_this_label":false,"priority":110}'),
(1147, 263, 'wcccf_billing_country', '{"type":"select","label":"Pa\\u00eds","is_unremovable":true,"is_enable":true,"name":"billing_country","order":40,"placeholder":"","default_value":"","maxlength":"","required":"yes","message":"","visibility":"yes","order_meta":"yes","login_user_field":"no","show_for_roles":[],"cart_editable":"no","cloneable":"no","field_class":"","initial_show":"yes","locale":[],"key":"wcccf_billing_country","use_this_label":false,"priority":110,"choices":"Don''t modify option|Don''t modify option"}');
INSERT INTO `wd_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1148, 263, 'wcccf_billing_address_1', '{"type":"text","label":"Endere\\u00e7o","is_unremovable":true,"is_enable":true,"name":"billing_address_1","order":50,"placeholder":"Nome da rua e n\\u00famero da casa","default_value":"","maxlength":"","required":"yes","message":"","visibility":"yes","order_meta":"yes","login_user_field":"no","show_for_roles":[],"cart_editable":"no","cloneable":"no","field_class":"","initial_show":"yes","locale":[],"key":"wcccf_billing_address_1","use_this_label":false,"priority":110}'),
(1149, 263, 'wcccf_billing_address_2', '{"type":"text","label":"Address 2 (Blank)","is_unremovable":true,"is_enable":true,"name":"billing_address_2","order":60,"placeholder":"Apartamento, sala, condom\\u00ednio, etc. (opcional)","default_value":"","maxlength":"","required":"no","message":"","visibility":"yes","order_meta":"yes","login_user_field":"no","show_for_roles":[],"cart_editable":"no","cloneable":"no","field_class":"","initial_show":"yes","locale":[],"key":"wcccf_billing_address_2","use_this_label":false,"priority":110}'),
(1150, 263, 'wcccf_billing_city', '{"type":"text","label":"Cidade","is_unremovable":true,"is_enable":true,"name":"billing_city","order":70,"placeholder":"","default_value":"","maxlength":"","required":"yes","message":"","visibility":"yes","order_meta":"yes","login_user_field":"no","show_for_roles":[],"cart_editable":"no","cloneable":"no","field_class":"","initial_show":"yes","locale":[],"key":"wcccf_billing_city","use_this_label":false,"priority":110}'),
(1151, 263, 'wcccf_billing_state', '{"type":"select","label":"Estado","is_unremovable":true,"is_enable":true,"name":"billing_state","order":80,"placeholder":"","default_value":"","maxlength":"","required":"yes","message":"","visibility":"yes","order_meta":"yes","login_user_field":"no","show_for_roles":[],"cart_editable":"no","cloneable":"no","field_class":"","initial_show":"yes","locale":[],"key":"wcccf_billing_state","use_this_label":false,"priority":110,"choices":"Don''t modify option|Don''t modify option"}'),
(1152, 263, 'wcccf_billing_postcode', '{"type":"text","label":"CEP","is_unremovable":true,"is_enable":true,"name":"billing_postcode","order":90,"placeholder":"","default_value":"","maxlength":"","required":"yes","message":"","visibility":"yes","order_meta":"yes","login_user_field":"no","show_for_roles":[],"cart_editable":"no","cloneable":"no","field_class":"","initial_show":"yes","locale":[],"key":"wcccf_billing_postcode","use_this_label":false,"priority":110}'),
(1153, 263, 'wcccf_billing_phone', '{"type":"text","label":"Telefone","is_unremovable":true,"is_enable":true,"name":"billing_phone","order":100,"placeholder":"","default_value":"","maxlength":"","required":"yes","message":"","visibility":"yes","order_meta":"yes","login_user_field":"no","show_for_roles":[],"cart_editable":"no","cloneable":"no","field_class":"","initial_show":"yes","locale":[],"key":"wcccf_billing_phone","use_this_label":false,"priority":110}'),
(1154, 263, 'wcccf_billing_email', '{"type":"text","label":"Endere\\u00e7o de e-mail","is_unremovable":true,"is_enable":true,"name":"billing_email","order":110,"placeholder":"","default_value":"","maxlength":"","required":"yes","message":"","visibility":"yes","order_meta":"yes","login_user_field":"no","show_for_roles":[],"cart_editable":"no","cloneable":"no","field_class":"","initial_show":"yes","locale":[],"key":"wcccf_billing_email","use_this_label":false,"priority":110}'),
(1155, 265, '_edit_lock', '1538533300:1'),
(1157, 265, '_edit_last', '1'),
(1239, 96, 'woosw_count', '1'),
(1240, 96, 'woosw_add', '1537551050'),
(1241, 268, '_menu_item_type', 'custom'),
(1242, 268, '_menu_item_menu_item_parent', '0'),
(1243, 268, '_menu_item_object_id', '268'),
(1244, 268, '_menu_item_object', 'custom'),
(1245, 268, '_menu_item_target', ''),
(1246, 268, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1247, 268, '_menu_item_xfn', ''),
(1248, 268, '_menu_item_url', 'http://walldone/'),
(1249, 268, '_menu_item_orphaned', '1537987328'),
(1250, 269, '_menu_item_type', 'post_type'),
(1251, 269, '_menu_item_menu_item_parent', '0'),
(1252, 269, '_menu_item_object_id', '132'),
(1253, 269, '_menu_item_object', 'page'),
(1254, 269, '_menu_item_target', ''),
(1255, 269, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1256, 269, '_menu_item_xfn', ''),
(1257, 269, '_menu_item_url', ''),
(1258, 269, '_menu_item_orphaned', '1537987328'),
(1259, 270, '_menu_item_type', 'post_type'),
(1260, 270, '_menu_item_menu_item_parent', '0'),
(1261, 270, '_menu_item_object_id', '243'),
(1262, 270, '_menu_item_object', 'page'),
(1263, 270, '_menu_item_target', ''),
(1264, 270, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1265, 270, '_menu_item_xfn', ''),
(1266, 270, '_menu_item_url', ''),
(1267, 270, '_menu_item_orphaned', '1537987328'),
(1268, 271, '_menu_item_type', 'post_type'),
(1269, 271, '_menu_item_menu_item_parent', '0'),
(1270, 271, '_menu_item_object_id', '6'),
(1271, 271, '_menu_item_object', 'page'),
(1272, 271, '_menu_item_target', ''),
(1273, 271, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1274, 271, '_menu_item_xfn', ''),
(1275, 271, '_menu_item_url', ''),
(1276, 271, '_menu_item_orphaned', '1537987328'),
(1277, 272, '_menu_item_type', 'post_type'),
(1278, 272, '_menu_item_menu_item_parent', '0'),
(1279, 272, '_menu_item_object_id', '7'),
(1280, 272, '_menu_item_object', 'page'),
(1281, 272, '_menu_item_target', ''),
(1282, 272, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1283, 272, '_menu_item_xfn', ''),
(1284, 272, '_menu_item_url', ''),
(1285, 272, '_menu_item_orphaned', '1537987328'),
(1286, 273, '_menu_item_type', 'post_type'),
(1287, 273, '_menu_item_menu_item_parent', '0'),
(1288, 273, '_menu_item_object_id', '250'),
(1289, 273, '_menu_item_object', 'page'),
(1290, 273, '_menu_item_target', ''),
(1291, 273, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1292, 273, '_menu_item_xfn', ''),
(1293, 273, '_menu_item_url', ''),
(1294, 273, '_menu_item_orphaned', '1537987328'),
(1295, 274, '_menu_item_type', 'post_type'),
(1296, 274, '_menu_item_menu_item_parent', '0'),
(1297, 274, '_menu_item_object_id', '5'),
(1298, 274, '_menu_item_object', 'page'),
(1299, 274, '_menu_item_target', ''),
(1300, 274, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1301, 274, '_menu_item_xfn', ''),
(1302, 274, '_menu_item_url', ''),
(1303, 274, '_menu_item_orphaned', '1537987328'),
(1304, 275, '_menu_item_type', 'post_type'),
(1305, 275, '_menu_item_menu_item_parent', '0'),
(1306, 275, '_menu_item_object_id', '164'),
(1307, 275, '_menu_item_object', 'page'),
(1308, 275, '_menu_item_target', ''),
(1309, 275, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1310, 275, '_menu_item_xfn', ''),
(1311, 275, '_menu_item_url', ''),
(1312, 275, '_menu_item_orphaned', '1537987328'),
(1313, 276, '_menu_item_type', 'post_type'),
(1314, 276, '_menu_item_menu_item_parent', '0'),
(1315, 276, '_menu_item_object_id', '166'),
(1316, 276, '_menu_item_object', 'page'),
(1317, 276, '_menu_item_target', ''),
(1318, 276, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1319, 276, '_menu_item_xfn', ''),
(1320, 276, '_menu_item_url', ''),
(1321, 276, '_menu_item_orphaned', '1537987328'),
(1322, 277, '_menu_item_type', 'post_type'),
(1323, 277, '_menu_item_menu_item_parent', '0'),
(1324, 277, '_menu_item_object_id', '8'),
(1325, 277, '_menu_item_object', 'page'),
(1326, 277, '_menu_item_target', ''),
(1327, 277, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1328, 277, '_menu_item_xfn', ''),
(1329, 277, '_menu_item_url', ''),
(1330, 277, '_menu_item_orphaned', '1537987328'),
(1331, 278, '_menu_item_type', 'post_type'),
(1332, 278, '_menu_item_menu_item_parent', '0'),
(1333, 278, '_menu_item_object_id', '2'),
(1334, 278, '_menu_item_object', 'page'),
(1335, 278, '_menu_item_target', ''),
(1336, 278, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1337, 278, '_menu_item_xfn', ''),
(1338, 278, '_menu_item_url', ''),
(1339, 278, '_menu_item_orphaned', '1537987328'),
(1340, 279, '_menu_item_type', 'post_type'),
(1341, 279, '_menu_item_menu_item_parent', '0'),
(1342, 279, '_menu_item_object_id', '248'),
(1343, 279, '_menu_item_object', 'page'),
(1344, 279, '_menu_item_target', ''),
(1345, 279, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1346, 279, '_menu_item_xfn', ''),
(1347, 279, '_menu_item_url', ''),
(1348, 279, '_menu_item_orphaned', '1537987328'),
(1349, 280, '_menu_item_type', 'post_type'),
(1350, 280, '_menu_item_menu_item_parent', '0'),
(1351, 280, '_menu_item_object_id', '118'),
(1352, 280, '_menu_item_object', 'page'),
(1353, 280, '_menu_item_target', ''),
(1354, 280, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1355, 280, '_menu_item_xfn', ''),
(1356, 280, '_menu_item_url', ''),
(1357, 280, '_menu_item_orphaned', '1537987328'),
(1358, 281, '_menu_item_type', 'taxonomy'),
(1359, 281, '_menu_item_menu_item_parent', '0'),
(1360, 281, '_menu_item_object_id', '20'),
(1361, 281, '_menu_item_object', 'product_cat'),
(1362, 281, '_menu_item_target', ''),
(1363, 281, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1364, 281, '_menu_item_xfn', ''),
(1365, 281, '_menu_item_url', ''),
(1366, 281, '_menu_item_orphaned', '1537988702'),
(1367, 282, '_menu_item_type', 'taxonomy'),
(1368, 282, '_menu_item_menu_item_parent', '0'),
(1369, 282, '_menu_item_object_id', '20'),
(1370, 282, '_menu_item_object', 'product_cat'),
(1371, 282, '_menu_item_target', ''),
(1372, 282, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1373, 282, '_menu_item_xfn', ''),
(1374, 282, '_menu_item_url', ''),
(1376, 283, '_menu_item_type', 'taxonomy'),
(1377, 283, '_menu_item_menu_item_parent', '282'),
(1378, 283, '_menu_item_object_id', '21'),
(1379, 283, '_menu_item_object', 'product_cat'),
(1380, 283, '_menu_item_target', ''),
(1381, 283, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1382, 283, '_menu_item_xfn', ''),
(1383, 283, '_menu_item_url', ''),
(1385, 284, '_menu_item_type', 'taxonomy'),
(1386, 284, '_menu_item_menu_item_parent', '282'),
(1387, 284, '_menu_item_object_id', '22'),
(1388, 284, '_menu_item_object', 'product_cat'),
(1389, 284, '_menu_item_target', ''),
(1390, 284, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1391, 284, '_menu_item_xfn', ''),
(1392, 284, '_menu_item_url', ''),
(1394, 285, '_menu_item_type', 'taxonomy'),
(1395, 285, '_menu_item_menu_item_parent', '282'),
(1396, 285, '_menu_item_object_id', '23'),
(1397, 285, '_menu_item_object', 'product_cat'),
(1398, 285, '_menu_item_target', ''),
(1399, 285, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1400, 285, '_menu_item_xfn', ''),
(1401, 285, '_menu_item_url', ''),
(1403, 286, '_menu_item_type', 'taxonomy'),
(1404, 286, '_menu_item_menu_item_parent', '282'),
(1405, 286, '_menu_item_object_id', '24'),
(1406, 286, '_menu_item_object', 'product_cat'),
(1407, 286, '_menu_item_target', ''),
(1408, 286, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1409, 286, '_menu_item_xfn', ''),
(1410, 286, '_menu_item_url', ''),
(1412, 287, '_menu_item_type', 'taxonomy'),
(1413, 287, '_menu_item_menu_item_parent', '282'),
(1414, 287, '_menu_item_object_id', '25'),
(1415, 287, '_menu_item_object', 'product_cat'),
(1416, 287, '_menu_item_target', ''),
(1417, 287, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1418, 287, '_menu_item_xfn', ''),
(1419, 287, '_menu_item_url', ''),
(1421, 288, '_menu_item_type', 'taxonomy'),
(1422, 288, '_menu_item_menu_item_parent', '282'),
(1423, 288, '_menu_item_object_id', '26'),
(1424, 288, '_menu_item_object', 'product_cat'),
(1425, 288, '_menu_item_target', ''),
(1426, 288, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1427, 288, '_menu_item_xfn', ''),
(1428, 288, '_menu_item_url', ''),
(1430, 289, '_menu_item_type', 'taxonomy'),
(1431, 289, '_menu_item_menu_item_parent', '282'),
(1432, 289, '_menu_item_object_id', '27'),
(1433, 289, '_menu_item_object', 'product_cat'),
(1434, 289, '_menu_item_target', ''),
(1435, 289, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1436, 289, '_menu_item_xfn', ''),
(1437, 289, '_menu_item_url', ''),
(1439, 93, 'woosw_count', '1'),
(1440, 93, 'woosw_add', '1537997295'),
(1441, 90, 'woosw_count', '1'),
(1442, 90, 'woosw_add', '1537997812'),
(1443, 290, '_menu_item_type', 'taxonomy'),
(1444, 290, '_menu_item_menu_item_parent', '0'),
(1445, 290, '_menu_item_object_id', '28'),
(1446, 290, '_menu_item_object', 'product_cat'),
(1447, 290, '_menu_item_target', ''),
(1448, 290, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1449, 290, '_menu_item_xfn', ''),
(1450, 290, '_menu_item_url', ''),
(1452, 291, '_menu_item_type', 'taxonomy'),
(1453, 291, '_menu_item_menu_item_parent', '290'),
(1454, 291, '_menu_item_object_id', '29'),
(1455, 291, '_menu_item_object', 'product_cat'),
(1456, 291, '_menu_item_target', ''),
(1457, 291, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1458, 291, '_menu_item_xfn', ''),
(1459, 291, '_menu_item_url', ''),
(1461, 292, '_menu_item_type', 'taxonomy'),
(1462, 292, '_menu_item_menu_item_parent', '290'),
(1463, 292, '_menu_item_object_id', '30'),
(1464, 292, '_menu_item_object', 'product_cat'),
(1465, 292, '_menu_item_target', ''),
(1466, 292, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1467, 292, '_menu_item_xfn', ''),
(1468, 292, '_menu_item_url', ''),
(1470, 293, '_menu_item_type', 'taxonomy'),
(1471, 293, '_menu_item_menu_item_parent', '290'),
(1472, 293, '_menu_item_object_id', '31'),
(1473, 293, '_menu_item_object', 'product_cat'),
(1474, 293, '_menu_item_target', ''),
(1475, 293, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1476, 293, '_menu_item_xfn', ''),
(1477, 293, '_menu_item_url', ''),
(1479, 294, '_menu_item_type', 'taxonomy'),
(1480, 294, '_menu_item_menu_item_parent', '290'),
(1481, 294, '_menu_item_object_id', '32'),
(1482, 294, '_menu_item_object', 'product_cat'),
(1483, 294, '_menu_item_target', ''),
(1484, 294, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1485, 294, '_menu_item_xfn', ''),
(1486, 294, '_menu_item_url', ''),
(1488, 295, '_menu_item_type', 'taxonomy'),
(1489, 295, '_menu_item_menu_item_parent', '290'),
(1490, 295, '_menu_item_object_id', '33'),
(1491, 295, '_menu_item_object', 'product_cat'),
(1492, 295, '_menu_item_target', ''),
(1493, 295, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1494, 295, '_menu_item_xfn', ''),
(1495, 295, '_menu_item_url', ''),
(1497, 296, '_menu_item_type', 'taxonomy'),
(1498, 296, '_menu_item_menu_item_parent', '290'),
(1499, 296, '_menu_item_object_id', '34'),
(1500, 296, '_menu_item_object', 'product_cat'),
(1501, 296, '_menu_item_target', ''),
(1502, 296, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1503, 296, '_menu_item_xfn', ''),
(1504, 296, '_menu_item_url', ''),
(1506, 297, '_menu_item_type', 'taxonomy'),
(1507, 297, '_menu_item_menu_item_parent', '290'),
(1508, 297, '_menu_item_object_id', '35'),
(1509, 297, '_menu_item_object', 'product_cat'),
(1510, 297, '_menu_item_target', ''),
(1511, 297, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1512, 297, '_menu_item_xfn', ''),
(1513, 297, '_menu_item_url', ''),
(1515, 298, '_menu_item_type', 'taxonomy'),
(1516, 298, '_menu_item_menu_item_parent', '0'),
(1517, 298, '_menu_item_object_id', '36'),
(1518, 298, '_menu_item_object', 'product_cat'),
(1519, 298, '_menu_item_target', ''),
(1520, 298, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1521, 298, '_menu_item_xfn', ''),
(1522, 298, '_menu_item_url', ''),
(1524, 299, '_menu_item_type', 'taxonomy'),
(1525, 299, '_menu_item_menu_item_parent', '298'),
(1526, 299, '_menu_item_object_id', '37'),
(1527, 299, '_menu_item_object', 'product_cat'),
(1528, 299, '_menu_item_target', ''),
(1529, 299, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1530, 299, '_menu_item_xfn', ''),
(1531, 299, '_menu_item_url', ''),
(1533, 300, '_menu_item_type', 'taxonomy'),
(1534, 300, '_menu_item_menu_item_parent', '298'),
(1535, 300, '_menu_item_object_id', '38'),
(1536, 300, '_menu_item_object', 'product_cat'),
(1537, 300, '_menu_item_target', ''),
(1538, 300, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1539, 300, '_menu_item_xfn', ''),
(1540, 300, '_menu_item_url', ''),
(1542, 301, '_menu_item_type', 'taxonomy'),
(1543, 301, '_menu_item_menu_item_parent', '298'),
(1544, 301, '_menu_item_object_id', '39'),
(1545, 301, '_menu_item_object', 'product_cat'),
(1546, 301, '_menu_item_target', ''),
(1547, 301, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1548, 301, '_menu_item_xfn', ''),
(1549, 301, '_menu_item_url', ''),
(1551, 302, '_menu_item_type', 'taxonomy'),
(1552, 302, '_menu_item_menu_item_parent', '298'),
(1553, 302, '_menu_item_object_id', '40'),
(1554, 302, '_menu_item_object', 'product_cat'),
(1555, 302, '_menu_item_target', ''),
(1556, 302, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1557, 302, '_menu_item_xfn', ''),
(1558, 302, '_menu_item_url', ''),
(1560, 303, '_menu_item_type', 'taxonomy'),
(1561, 303, '_menu_item_menu_item_parent', '0'),
(1562, 303, '_menu_item_object_id', '15'),
(1563, 303, '_menu_item_object', 'product_cat'),
(1564, 303, '_menu_item_target', ''),
(1565, 303, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1566, 303, '_menu_item_xfn', ''),
(1567, 303, '_menu_item_url', ''),
(1569, 304, '_menu_item_type', 'custom'),
(1570, 304, '_menu_item_menu_item_parent', '303'),
(1571, 304, '_menu_item_object_id', '304'),
(1572, 304, '_menu_item_object', 'custom'),
(1573, 304, '_menu_item_target', ''),
(1574, 304, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1575, 304, '_menu_item_xfn', ''),
(1576, 304, '_menu_item_url', 'http://#'),
(1578, 305, '_menu_item_type', 'custom'),
(1579, 305, '_menu_item_menu_item_parent', '303'),
(1580, 305, '_menu_item_object_id', '305'),
(1581, 305, '_menu_item_object', 'custom'),
(1582, 305, '_menu_item_target', ''),
(1583, 305, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1584, 305, '_menu_item_xfn', ''),
(1585, 305, '_menu_item_url', 'http://#'),
(1587, 306, '_menu_item_type', 'custom'),
(1588, 306, '_menu_item_menu_item_parent', '303'),
(1589, 306, '_menu_item_object_id', '306'),
(1590, 306, '_menu_item_object', 'custom'),
(1591, 306, '_menu_item_target', ''),
(1592, 306, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1593, 306, '_menu_item_xfn', ''),
(1594, 306, '_menu_item_url', 'http://#'),
(1596, 307, '_menu_item_type', 'custom'),
(1597, 307, '_menu_item_menu_item_parent', '303'),
(1598, 307, '_menu_item_object_id', '307'),
(1599, 307, '_menu_item_object', 'custom'),
(1600, 307, '_menu_item_target', ''),
(1601, 307, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1602, 307, '_menu_item_xfn', ''),
(1603, 307, '_menu_item_url', 'http://#'),
(1605, 308, '_menu_item_type', 'custom'),
(1606, 308, '_menu_item_menu_item_parent', '303'),
(1607, 308, '_menu_item_object_id', '308'),
(1608, 308, '_menu_item_object', 'custom'),
(1609, 308, '_menu_item_target', ''),
(1610, 308, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1611, 308, '_menu_item_xfn', ''),
(1612, 308, '_menu_item_url', 'http://#'),
(1614, 309, '_menu_item_type', 'custom'),
(1615, 309, '_menu_item_menu_item_parent', '303'),
(1616, 309, '_menu_item_object_id', '309'),
(1617, 309, '_menu_item_object', 'custom'),
(1618, 309, '_menu_item_target', ''),
(1619, 309, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1620, 309, '_menu_item_xfn', ''),
(1621, 309, '_menu_item_url', 'http://#'),
(1623, 310, '_menu_item_type', 'custom'),
(1624, 310, '_menu_item_menu_item_parent', '303'),
(1625, 310, '_menu_item_object_id', '310'),
(1626, 310, '_menu_item_object', 'custom'),
(1627, 310, '_menu_item_target', ''),
(1628, 310, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1629, 310, '_menu_item_xfn', ''),
(1630, 310, '_menu_item_url', 'http://#'),
(1632, 311, '_menu_item_type', 'custom'),
(1633, 311, '_menu_item_menu_item_parent', '303'),
(1634, 311, '_menu_item_object_id', '311'),
(1635, 311, '_menu_item_object', 'custom'),
(1636, 311, '_menu_item_target', ''),
(1637, 311, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1638, 311, '_menu_item_xfn', ''),
(1639, 311, '_menu_item_url', 'http://#'),
(1641, 312, '_menu_item_type', 'custom'),
(1642, 312, '_menu_item_menu_item_parent', '303'),
(1643, 312, '_menu_item_object_id', '312'),
(1644, 312, '_menu_item_object', 'custom'),
(1645, 312, '_menu_item_target', ''),
(1646, 312, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1647, 312, '_menu_item_xfn', ''),
(1648, 312, '_menu_item_url', 'http://#'),
(1650, 313, '_menu_item_type', 'custom'),
(1651, 313, '_menu_item_menu_item_parent', '303'),
(1652, 313, '_menu_item_object_id', '313'),
(1653, 313, '_menu_item_object', 'custom'),
(1654, 313, '_menu_item_target', ''),
(1655, 313, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1656, 313, '_menu_item_xfn', ''),
(1657, 313, '_menu_item_url', 'http://#'),
(1659, 314, '_menu_item_type', 'custom'),
(1660, 314, '_menu_item_menu_item_parent', '303'),
(1661, 314, '_menu_item_object_id', '314'),
(1662, 314, '_menu_item_object', 'custom'),
(1663, 314, '_menu_item_target', ''),
(1664, 314, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1665, 314, '_menu_item_xfn', ''),
(1666, 314, '_menu_item_url', 'http://#'),
(1668, 315, '_menu_item_type', 'custom'),
(1669, 315, '_menu_item_menu_item_parent', '303'),
(1670, 315, '_menu_item_object_id', '315'),
(1671, 315, '_menu_item_object', 'custom'),
(1672, 315, '_menu_item_target', ''),
(1673, 315, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1674, 315, '_menu_item_xfn', ''),
(1675, 315, '_menu_item_url', 'http://#'),
(1677, 316, '_menu_item_type', 'custom'),
(1678, 316, '_menu_item_menu_item_parent', '0'),
(1679, 316, '_menu_item_object_id', '316'),
(1680, 316, '_menu_item_object', 'custom'),
(1681, 316, '_menu_item_target', ''),
(1682, 316, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1683, 316, '_menu_item_xfn', ''),
(1684, 316, '_menu_item_url', '/blog'),
(1686, 317, '_menu_item_type', 'taxonomy'),
(1687, 317, '_menu_item_menu_item_parent', '0'),
(1688, 317, '_menu_item_object_id', '20'),
(1689, 317, '_menu_item_object', 'product_cat'),
(1690, 317, '_menu_item_target', ''),
(1691, 317, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1692, 317, '_menu_item_xfn', ''),
(1693, 317, '_menu_item_url', ''),
(1695, 318, '_menu_item_type', 'taxonomy'),
(1696, 318, '_menu_item_menu_item_parent', '317'),
(1697, 318, '_menu_item_object_id', '21'),
(1698, 318, '_menu_item_object', 'product_cat'),
(1699, 318, '_menu_item_target', ''),
(1700, 318, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1701, 318, '_menu_item_xfn', ''),
(1702, 318, '_menu_item_url', ''),
(1704, 319, '_menu_item_type', 'taxonomy'),
(1705, 319, '_menu_item_menu_item_parent', '317'),
(1706, 319, '_menu_item_object_id', '22'),
(1707, 319, '_menu_item_object', 'product_cat'),
(1708, 319, '_menu_item_target', ''),
(1709, 319, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1710, 319, '_menu_item_xfn', ''),
(1711, 319, '_menu_item_url', ''),
(1713, 320, '_menu_item_type', 'taxonomy'),
(1714, 320, '_menu_item_menu_item_parent', '317'),
(1715, 320, '_menu_item_object_id', '23'),
(1716, 320, '_menu_item_object', 'product_cat'),
(1717, 320, '_menu_item_target', ''),
(1718, 320, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1719, 320, '_menu_item_xfn', ''),
(1720, 320, '_menu_item_url', ''),
(1722, 321, '_menu_item_type', 'taxonomy'),
(1723, 321, '_menu_item_menu_item_parent', '317'),
(1724, 321, '_menu_item_object_id', '24'),
(1725, 321, '_menu_item_object', 'product_cat'),
(1726, 321, '_menu_item_target', ''),
(1727, 321, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1728, 321, '_menu_item_xfn', ''),
(1729, 321, '_menu_item_url', ''),
(1731, 322, '_menu_item_type', 'taxonomy'),
(1732, 322, '_menu_item_menu_item_parent', '317'),
(1733, 322, '_menu_item_object_id', '25'),
(1734, 322, '_menu_item_object', 'product_cat'),
(1735, 322, '_menu_item_target', ''),
(1736, 322, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1737, 322, '_menu_item_xfn', ''),
(1738, 322, '_menu_item_url', ''),
(1740, 323, '_menu_item_type', 'taxonomy'),
(1741, 323, '_menu_item_menu_item_parent', '317'),
(1742, 323, '_menu_item_object_id', '26'),
(1743, 323, '_menu_item_object', 'product_cat'),
(1744, 323, '_menu_item_target', ''),
(1745, 323, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1746, 323, '_menu_item_xfn', ''),
(1747, 323, '_menu_item_url', ''),
(1749, 324, '_menu_item_type', 'taxonomy'),
(1750, 324, '_menu_item_menu_item_parent', '317'),
(1751, 324, '_menu_item_object_id', '27'),
(1752, 324, '_menu_item_object', 'product_cat'),
(1753, 324, '_menu_item_target', ''),
(1754, 324, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1755, 324, '_menu_item_xfn', ''),
(1756, 324, '_menu_item_url', ''),
(1758, 325, '_menu_item_type', 'taxonomy'),
(1759, 325, '_menu_item_menu_item_parent', '0'),
(1760, 325, '_menu_item_object_id', '28'),
(1761, 325, '_menu_item_object', 'product_cat'),
(1762, 325, '_menu_item_target', ''),
(1763, 325, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1764, 325, '_menu_item_xfn', ''),
(1765, 325, '_menu_item_url', ''),
(1767, 326, '_menu_item_type', 'taxonomy'),
(1768, 326, '_menu_item_menu_item_parent', '325'),
(1769, 326, '_menu_item_object_id', '29'),
(1770, 326, '_menu_item_object', 'product_cat'),
(1771, 326, '_menu_item_target', ''),
(1772, 326, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1773, 326, '_menu_item_xfn', ''),
(1774, 326, '_menu_item_url', ''),
(1776, 327, '_menu_item_type', 'taxonomy'),
(1777, 327, '_menu_item_menu_item_parent', '325'),
(1778, 327, '_menu_item_object_id', '30'),
(1779, 327, '_menu_item_object', 'product_cat'),
(1780, 327, '_menu_item_target', ''),
(1781, 327, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1782, 327, '_menu_item_xfn', ''),
(1783, 327, '_menu_item_url', ''),
(1785, 328, '_menu_item_type', 'taxonomy'),
(1786, 328, '_menu_item_menu_item_parent', '325'),
(1787, 328, '_menu_item_object_id', '31'),
(1788, 328, '_menu_item_object', 'product_cat'),
(1789, 328, '_menu_item_target', ''),
(1790, 328, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1791, 328, '_menu_item_xfn', ''),
(1792, 328, '_menu_item_url', ''),
(1794, 329, '_menu_item_type', 'taxonomy'),
(1795, 329, '_menu_item_menu_item_parent', '325'),
(1796, 329, '_menu_item_object_id', '32'),
(1797, 329, '_menu_item_object', 'product_cat'),
(1798, 329, '_menu_item_target', ''),
(1799, 329, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1800, 329, '_menu_item_xfn', ''),
(1801, 329, '_menu_item_url', ''),
(1803, 330, '_menu_item_type', 'taxonomy'),
(1804, 330, '_menu_item_menu_item_parent', '325'),
(1805, 330, '_menu_item_object_id', '33'),
(1806, 330, '_menu_item_object', 'product_cat'),
(1807, 330, '_menu_item_target', ''),
(1808, 330, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1809, 330, '_menu_item_xfn', ''),
(1810, 330, '_menu_item_url', ''),
(1812, 331, '_menu_item_type', 'taxonomy'),
(1813, 331, '_menu_item_menu_item_parent', '325'),
(1814, 331, '_menu_item_object_id', '34'),
(1815, 331, '_menu_item_object', 'product_cat'),
(1816, 331, '_menu_item_target', ''),
(1817, 331, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1818, 331, '_menu_item_xfn', ''),
(1819, 331, '_menu_item_url', ''),
(1821, 332, '_menu_item_type', 'taxonomy'),
(1822, 332, '_menu_item_menu_item_parent', '325'),
(1823, 332, '_menu_item_object_id', '35'),
(1824, 332, '_menu_item_object', 'product_cat'),
(1825, 332, '_menu_item_target', ''),
(1826, 332, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1827, 332, '_menu_item_xfn', ''),
(1828, 332, '_menu_item_url', ''),
(1830, 333, '_menu_item_type', 'taxonomy'),
(1831, 333, '_menu_item_menu_item_parent', '0'),
(1832, 333, '_menu_item_object_id', '36'),
(1833, 333, '_menu_item_object', 'product_cat'),
(1834, 333, '_menu_item_target', ''),
(1835, 333, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1836, 333, '_menu_item_xfn', ''),
(1837, 333, '_menu_item_url', ''),
(1839, 334, '_menu_item_type', 'taxonomy'),
(1840, 334, '_menu_item_menu_item_parent', '333'),
(1841, 334, '_menu_item_object_id', '37'),
(1842, 334, '_menu_item_object', 'product_cat'),
(1843, 334, '_menu_item_target', ''),
(1844, 334, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1845, 334, '_menu_item_xfn', ''),
(1846, 334, '_menu_item_url', ''),
(1848, 335, '_menu_item_type', 'taxonomy'),
(1849, 335, '_menu_item_menu_item_parent', '333'),
(1850, 335, '_menu_item_object_id', '38'),
(1851, 335, '_menu_item_object', 'product_cat'),
(1852, 335, '_menu_item_target', ''),
(1853, 335, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1854, 335, '_menu_item_xfn', ''),
(1855, 335, '_menu_item_url', ''),
(1857, 336, '_menu_item_type', 'taxonomy'),
(1858, 336, '_menu_item_menu_item_parent', '333'),
(1859, 336, '_menu_item_object_id', '39'),
(1860, 336, '_menu_item_object', 'product_cat'),
(1861, 336, '_menu_item_target', ''),
(1862, 336, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1863, 336, '_menu_item_xfn', ''),
(1864, 336, '_menu_item_url', ''),
(1866, 337, '_menu_item_type', 'taxonomy'),
(1867, 337, '_menu_item_menu_item_parent', '333'),
(1868, 337, '_menu_item_object_id', '40'),
(1869, 337, '_menu_item_object', 'product_cat'),
(1870, 337, '_menu_item_target', ''),
(1871, 337, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1872, 337, '_menu_item_xfn', ''),
(1873, 337, '_menu_item_url', ''),
(1875, 338, '_menu_item_type', 'taxonomy'),
(1876, 338, '_menu_item_menu_item_parent', '0'),
(1877, 338, '_menu_item_object_id', '15'),
(1878, 338, '_menu_item_object', 'product_cat'),
(1879, 338, '_menu_item_target', ''),
(1880, 338, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1881, 338, '_menu_item_xfn', ''),
(1882, 338, '_menu_item_url', ''),
(1884, 339, '_menu_item_type', 'custom'),
(1885, 339, '_menu_item_menu_item_parent', '338'),
(1886, 339, '_menu_item_object_id', '339'),
(1887, 339, '_menu_item_object', 'custom'),
(1888, 339, '_menu_item_target', ''),
(1889, 339, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1890, 339, '_menu_item_xfn', ''),
(1891, 339, '_menu_item_url', 'http://#'),
(1893, 340, '_menu_item_type', 'custom'),
(1894, 340, '_menu_item_menu_item_parent', '338'),
(1895, 340, '_menu_item_object_id', '340'),
(1896, 340, '_menu_item_object', 'custom'),
(1897, 340, '_menu_item_target', ''),
(1898, 340, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1899, 340, '_menu_item_xfn', ''),
(1900, 340, '_menu_item_url', 'http://#'),
(1902, 341, '_menu_item_type', 'custom'),
(1903, 341, '_menu_item_menu_item_parent', '338'),
(1904, 341, '_menu_item_object_id', '341'),
(1905, 341, '_menu_item_object', 'custom'),
(1906, 341, '_menu_item_target', ''),
(1907, 341, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1908, 341, '_menu_item_xfn', ''),
(1909, 341, '_menu_item_url', 'http://#'),
(1911, 342, '_menu_item_type', 'custom'),
(1912, 342, '_menu_item_menu_item_parent', '338'),
(1913, 342, '_menu_item_object_id', '342'),
(1914, 342, '_menu_item_object', 'custom'),
(1915, 342, '_menu_item_target', ''),
(1916, 342, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1917, 342, '_menu_item_xfn', ''),
(1918, 342, '_menu_item_url', 'http://#'),
(1920, 343, '_menu_item_type', 'custom'),
(1921, 343, '_menu_item_menu_item_parent', '338'),
(1922, 343, '_menu_item_object_id', '343'),
(1923, 343, '_menu_item_object', 'custom'),
(1924, 343, '_menu_item_target', ''),
(1925, 343, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1926, 343, '_menu_item_xfn', ''),
(1927, 343, '_menu_item_url', 'http://#'),
(1929, 344, '_menu_item_type', 'custom'),
(1930, 344, '_menu_item_menu_item_parent', '338'),
(1931, 344, '_menu_item_object_id', '344'),
(1932, 344, '_menu_item_object', 'custom'),
(1933, 344, '_menu_item_target', ''),
(1934, 344, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1935, 344, '_menu_item_xfn', ''),
(1936, 344, '_menu_item_url', 'http://#'),
(1938, 345, '_menu_item_type', 'custom'),
(1939, 345, '_menu_item_menu_item_parent', '338'),
(1940, 345, '_menu_item_object_id', '345'),
(1941, 345, '_menu_item_object', 'custom'),
(1942, 345, '_menu_item_target', ''),
(1943, 345, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1944, 345, '_menu_item_xfn', ''),
(1945, 345, '_menu_item_url', 'http://#'),
(1947, 346, '_menu_item_type', 'custom'),
(1948, 346, '_menu_item_menu_item_parent', '338'),
(1949, 346, '_menu_item_object_id', '346'),
(1950, 346, '_menu_item_object', 'custom'),
(1951, 346, '_menu_item_target', ''),
(1952, 346, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1953, 346, '_menu_item_xfn', ''),
(1954, 346, '_menu_item_url', 'http://#'),
(1956, 347, '_menu_item_type', 'custom'),
(1957, 347, '_menu_item_menu_item_parent', '338'),
(1958, 347, '_menu_item_object_id', '347'),
(1959, 347, '_menu_item_object', 'custom'),
(1960, 347, '_menu_item_target', ''),
(1961, 347, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1962, 347, '_menu_item_xfn', ''),
(1963, 347, '_menu_item_url', 'http://#'),
(1965, 348, '_menu_item_type', 'custom'),
(1966, 348, '_menu_item_menu_item_parent', '338'),
(1967, 348, '_menu_item_object_id', '348'),
(1968, 348, '_menu_item_object', 'custom'),
(1969, 348, '_menu_item_target', ''),
(1970, 348, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1971, 348, '_menu_item_xfn', ''),
(1972, 348, '_menu_item_url', 'http://#'),
(1974, 349, '_menu_item_type', 'custom'),
(1975, 349, '_menu_item_menu_item_parent', '338'),
(1976, 349, '_menu_item_object_id', '349'),
(1977, 349, '_menu_item_object', 'custom'),
(1978, 349, '_menu_item_target', ''),
(1979, 349, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1980, 349, '_menu_item_xfn', ''),
(1981, 349, '_menu_item_url', 'http://#'),
(1983, 350, '_menu_item_type', 'custom'),
(1984, 350, '_menu_item_menu_item_parent', '338'),
(1985, 350, '_menu_item_object_id', '350'),
(1986, 350, '_menu_item_object', 'custom'),
(1987, 350, '_menu_item_target', ''),
(1988, 350, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1989, 350, '_menu_item_xfn', ''),
(1990, 350, '_menu_item_url', 'http://#'),
(1992, 351, '_menu_item_type', 'custom'),
(1993, 351, '_menu_item_menu_item_parent', '0'),
(1994, 351, '_menu_item_object_id', '351'),
(1995, 351, '_menu_item_object', 'custom'),
(1996, 351, '_menu_item_target', ''),
(1997, 351, '_menu_item_classes', 'a:1:{i:0;s:7:"meu-kit";}'),
(1998, 351, '_menu_item_xfn', ''),
(1999, 351, '_menu_item_url', '/meu-kit'),
(2001, 352, '_edit_lock', '1538075730:1'),
(2002, 352, '_edit_last', '1'),
(2003, 352, 'field_5bad1cc4ea2ac', 'a:10:{s:3:"key";s:19:"field_5bad1cc4ea2ac";s:5:"label";s:11:"Lançamento";s:4:"name";s:10:"lancamento";s:4:"type";s:10:"true_false";s:12:"instructions";s:0:"";s:8:"required";s:1:"0";s:7:"message";s:0:"";s:13:"default_value";s:1:"0";s:17:"conditional_logic";a:3:{s:6:"status";s:1:"0";s:5:"rules";a:1:{i:0;a:3:{s:5:"field";s:4:"null";s:8:"operator";s:2:"==";s:5:"value";s:0:"";}}s:8:"allorany";s:3:"all";}s:8:"order_no";i:0;}'),
(2005, 352, 'position', 'acf_after_title'),
(2006, 352, 'layout', 'default'),
(2007, 352, 'hide_on_screen', ''),
(2008, 129, 'lancamento', '1'),
(2009, 129, '_lancamento', 'field_5bad1cc4ea2ac'),
(2010, 353, '_order_key', 'wc_order_5bad224e48a8a'),
(2011, 353, '_customer_user', '1'),
(2012, 353, '_payment_method', 'bacs'),
(2013, 353, '_payment_method_title', 'Transferência bancária direta'),
(2014, 353, '_transaction_id', ''),
(2015, 353, '_customer_ip_address', '::1'),
(2016, 353, '_customer_user_agent', 'mozilla/5.0 (macintosh; intel mac os x 10_12_6) applewebkit/537.36 (khtml, like gecko) chrome/69.0.3497.100 safari/537.36'),
(2017, 353, '_created_via', 'checkout'),
(2018, 353, '_date_completed', ''),
(2019, 353, '_completed_date', ''),
(2020, 353, '_date_paid', ''),
(2021, 353, '_paid_date', ''),
(2022, 353, '_cart_hash', '5afabce95d56531db5323fec7145a5bd'),
(2023, 353, '_billing_first_name', 'Vanssiler'),
(2024, 353, '_billing_last_name', 'Gonçalves'),
(2025, 353, '_billing_company', ''),
(2026, 353, '_billing_address_1', 'Rua Nove de Julho 533'),
(2027, 353, '_billing_address_2', ''),
(2028, 353, '_billing_city', 'Marília'),
(2029, 353, '_billing_state', 'SP'),
(2030, 353, '_billing_postcode', '17509-110'),
(2031, 353, '_billing_country', 'BR'),
(2032, 353, '_billing_email', 'vanssiler.goncalves@gmail.com'),
(2033, 353, '_billing_phone', '997669642'),
(2034, 353, '_shipping_first_name', 'Vanssiler'),
(2035, 353, '_shipping_last_name', 'Gonçalves'),
(2036, 353, '_shipping_company', ''),
(2037, 353, '_shipping_address_1', 'Rua Nove de Julho 533'),
(2038, 353, '_shipping_address_2', ''),
(2039, 353, '_shipping_city', 'Marília'),
(2040, 353, '_shipping_state', 'SP'),
(2041, 353, '_shipping_postcode', '17509110'),
(2042, 353, '_shipping_country', 'BR'),
(2043, 353, '_order_currency', 'BRL'),
(2044, 353, '_cart_discount', '0'),
(2045, 353, '_cart_discount_tax', '0'),
(2046, 353, '_order_shipping', '0.00'),
(2047, 353, '_order_shipping_tax', '0'),
(2048, 353, '_order_tax', '0'),
(2049, 353, '_order_total', '20.00'),
(2050, 353, '_order_version', '3.4.5'),
(2051, 353, '_prices_include_tax', 'no'),
(2052, 353, '_billing_address_index', 'Vanssiler Gonçalves  Rua Nove de Julho 533  Marília SP 17509-110 BR vanssiler.goncalves@gmail.com 997669642'),
(2053, 353, '_shipping_address_index', 'Vanssiler Gonçalves  Rua Nove de Julho 533  Marília SP 17509110 BR'),
(2054, 353, '_recorded_sales', 'yes'),
(2055, 353, '_recorded_coupon_usage_counts', 'yes'),
(2056, 353, '_order_stock_reduced', 'yes'),
(2057, 352, 'field_5bad22ee57464', 'a:10:{s:3:"key";s:19:"field_5bad22ee57464";s:5:"label";s:13:"Mais vendidos";s:4:"name";s:13:"mais_vendidos";s:4:"type";s:10:"true_false";s:12:"instructions";s:0:"";s:8:"required";s:1:"0";s:7:"message";s:0:"";s:13:"default_value";s:1:"0";s:17:"conditional_logic";a:3:{s:6:"status";s:1:"0";s:5:"rules";a:1:{i:0;a:3:{s:5:"field";s:19:"field_5bad1cc4ea2ac";s:8:"operator";s:2:"==";s:5:"value";s:1:"1";}}s:8:"allorany";s:3:"all";}s:8:"order_no";i:1;}'),
(2058, 352, 'rule', 'a:5:{s:5:"param";s:9:"post_type";s:8:"operator";s:2:"==";s:5:"value";s:7:"product";s:8:"order_no";i:0;s:8:"group_no";i:0;}'),
(2059, 129, 'mais_vendidos', '1'),
(2060, 129, '_mais_vendidos', 'field_5bad22ee57464'),
(2063, 355, '_wp_attached_file', '2018/09/home-depoimentos-larissa-walldone.jpg'),
(2064, 355, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:400;s:6:"height";i:400;s:4:"file";s:45:"2018/09/home-depoimentos-larissa-walldone.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:45:"home-depoimentos-larissa-walldone-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:45:"home-depoimentos-larissa-walldone-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:45:"home-depoimentos-larissa-walldone-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:45:"home-depoimentos-larissa-walldone-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:45:"home-depoimentos-larissa-walldone-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:45:"home-depoimentos-larissa-walldone-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(2066, 356, '_edit_lock', '1538075730:1'),
(2067, 356, '_edit_last', '1'),
(2068, 356, '_thumbnail_id', '355'),
(2072, 357, '_edit_lock', '1538075734:1'),
(2073, 357, '_edit_last', '1'),
(2074, 357, '_thumbnail_id', '355'),
(2075, 107, '_thumbnail_id', '259'),
(2081, 96, 'lancamento', '0'),
(2082, 96, '_lancamento', 'field_5bad1cc4ea2ac'),
(2083, 96, 'mais_vendidos', '0'),
(2084, 96, '_mais_vendidos', 'field_5bad22ee57464'),
(2085, 107, 'woo_variation_gallery_images', 'a:2:{i:0;s:3:"257";i:1;s:3:"255";}'),
(2091, 106, '_thumbnail_id', '261'),
(2092, 106, 'woo_variation_gallery_images', 'a:2:{i:0;s:3:"253";i:1;s:3:"254";}'),
(2093, 96, '_price', '20.00'),
(2094, 96, '_price', '22.00'),
(2095, 96, '_price', '29.90'),
(2096, 96, '_regular_price', ''),
(2097, 96, '_sale_price', ''),
(2098, 260, 'woosvi_slug', 'molduraverde'),
(2099, 259, 'woosvi_slug', 'verde'),
(2100, 257, 'woosvi_slug', 'molduraverde'),
(2101, 258, 'videolink_id', 'tm66BXwrYb4'),
(2102, 258, 'video_site', 'youtube'),
(2103, 358, '_wp_attached_file', '2018/06/video_play.jpg'),
(2104, 358, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:400;s:6:"height";i:400;s:4:"file";s:22:"2018/06/video_play.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:22:"video_play-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:22:"video_play-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:22:"video_play-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:22:"video_play-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:22:"video_play-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:22:"video_play-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(2105, 97, 'videolink_id', 'tm66BXwrYb4'),
(2106, 97, 'video_site', 'youtube'),
(2107, 96, '_video_url', 'https://www.youtube.com/watch?v=tm66BXwrYb4'),
(2108, 359, '_edit_lock', '1538435292:1'),
(2109, 359, '_edit_last', '1'),
(2110, 359, 'field_5bb23368f5bd9', 'a:11:{s:3:"key";s:19:"field_5bb23368f5bd9";s:5:"label";s:16:"Imagem do banner";s:4:"name";s:6:"imagem";s:4:"type";s:5:"image";s:12:"instructions";s:80:"Imagem do banner que será exibido no menu lateral, na listagem dessa categoria.";s:8:"required";s:1:"0";s:11:"save_format";s:3:"url";s:12:"preview_size";s:6:"medium";s:7:"library";s:3:"all";s:17:"conditional_logic";a:3:{s:6:"status";s:1:"0";s:5:"rules";a:1:{i:0;a:3:{s:5:"field";s:4:"null";s:8:"operator";s:2:"==";s:5:"value";s:0:"";}}s:8:"allorany";s:3:"all";}s:8:"order_no";i:2;}'),
(2111, 359, 'field_5bb23376f5bda', 'a:14:{s:3:"key";s:19:"field_5bb23376f5bda";s:5:"label";s:14:"Link do banner";s:4:"name";s:4:"link";s:4:"type";s:4:"text";s:12:"instructions";s:27:"Link do banner de categoria";s:8:"required";s:1:"0";s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:10:"formatting";s:4:"html";s:9:"maxlength";s:0:"";s:17:"conditional_logic";a:3:{s:6:"status";s:1:"0";s:5:"rules";a:1:{i:0;a:3:{s:5:"field";s:4:"null";s:8:"operator";s:2:"==";s:5:"value";s:0:"";}}s:8:"allorany";s:3:"all";}s:8:"order_no";i:3;}'),
(2113, 359, 'position', 'acf_after_title'),
(2114, 359, 'layout', 'default'),
(2115, 359, 'hide_on_screen', ''),
(2119, 359, 'field_5bb270375a8ed', 'a:14:{s:3:"key";s:19:"field_5bb270375a8ed";s:5:"label";s:19:"Título descrição";s:4:"name";s:16:"titulo_descricao";s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";s:1:"0";s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:10:"formatting";s:4:"html";s:9:"maxlength";s:0:"";s:17:"conditional_logic";a:3:{s:6:"status";s:1:"0";s:5:"rules";a:1:{i:0;a:2:{s:5:"field";s:4:"null";s:8:"operator";s:2:"==";}}s:8:"allorany";s:3:"all";}s:8:"order_no";i:0;}'),
(2120, 359, 'field_5bb270755a8ee', 'a:11:{s:3:"key";s:19:"field_5bb270755a8ee";s:5:"label";s:21:"Conteúdo Descrição";s:4:"name";s:18:"conteudo_descricao";s:4:"type";s:7:"wysiwyg";s:12:"instructions";s:0:"";s:8:"required";s:1:"0";s:13:"default_value";s:0:"";s:7:"toolbar";s:4:"full";s:12:"media_upload";s:3:"yes";s:17:"conditional_logic";a:3:{s:6:"status";s:1:"0";s:5:"rules";a:1:{i:0;a:2:{s:5:"field";s:4:"null";s:8:"operator";s:2:"==";}}s:8:"allorany";s:3:"all";}s:8:"order_no";i:1;}'),
(2121, 359, 'rule', 'a:5:{s:5:"param";s:11:"ef_taxonomy";s:8:"operator";s:2:"==";s:5:"value";s:11:"product_cat";s:8:"order_no";i:0;s:8:"group_no";i:0;}'),
(2122, 360, '_edit_lock', '1538444324:1'),
(2123, 360, '_edit_last', '1'),
(2124, 360, '_thumbnail_id', '104'),
(2125, 361, '_edit_lock', '1538445270:1'),
(2126, 361, '_edit_last', '1'),
(2127, 361, '_thumbnail_id', '104'),
(2128, 7, '_edit_lock', '1538508877:1'),
(2129, 7, '_edit_last', '1'),
(2130, 7, '_wp_page_template', 'checkout.php'),
(2131, 265, 'wccpf_imagens', '{"type":"file","key":"wccpf_imagens","label":"Imagens","name":"imagens","is_unremovable":false,"is_enable":false,"order":"0","filetypes":"","multi_file":"yes","img_is_prev":"yes","img_is_prev_width":"","upload_url":"","max_file_size":"5000","required":"no","message":"","visibility":"no","order_meta":"yes","login_user_field":"no","show_for_roles":[],"cart_editable":"no","cloneable":"no","field_class":"","initial_show":"yes","locale":[]}'),
(2132, 265, 'wccpf_largura', '{"type":"text","label":"Largura","name":"largura","is_unremovable":false,"is_enable":false,"order":"1","placeholder":"Largura","default_value":"","maxlength":"","required":"no","message":"","visibility":"yes","order_meta":"yes","login_user_field":"no","show_for_roles":[],"cart_editable":"no","cloneable":"no","field_class":"","initial_show":"yes","locale":[],"key":"wccpf_largura"}'),
(2133, 265, 'wccpf_altura', '{"type":"text","label":"Altura","name":"altura","is_unremovable":false,"is_enable":false,"order":"2","placeholder":"Altura","default_value":"","maxlength":"","required":"no","message":"","visibility":"yes","order_meta":"yes","login_user_field":"no","show_for_roles":[],"cart_editable":"no","cloneable":"no","field_class":"","initial_show":"yes","locale":[],"key":"wccpf_altura"}'),
(2134, 265, 'wccpf_informaes_adicionais', '{"type":"text","label":"Informa\\u00e7\\u00f5es adicionais","name":"informaes_adicionais","is_unremovable":false,"is_enable":false,"order":"3","placeholder":"Informa\\u00e7\\u00f5es Adicionais","default_value":"","maxlength":"","required":"no","message":"","visibility":"yes","order_meta":"yes","login_user_field":"no","show_for_roles":[],"cart_editable":"no","cloneable":"no","field_class":"","initial_show":"yes","locale":[],"key":"wccpf_informaes_adicionais"}'),
(2135, 265, 'wccpf_dos_produtos_que_voc_escolheu', '{"type":"radio","key":"wccpf_dos_produtos_que_voc_escolheu","label":"Dos produtos que voc\\u00ea escolheu","name":"dos_produtos_que_voc_escolheu","is_unremovable":false,"is_enable":false,"order":"4","choices":"Somente eles|Somente eles far\\u00e3o parte do Kit.;Parte do kit|Ser\\u00e3o parte do kit, gostaria que me ajudasse a escolher mais alguns para compor.;Nao escolhi|N\\u00e3o escolhi nenhum, preciso de ajuda come\\u00e7ando do zero.;","layout":"vertical","required":"no","message":"","visibility":"yes","order_meta":"yes","login_user_field":"no","show_for_roles":[],"cart_editable":"no","cloneable":"no","field_class":"","initial_show":"yes","locale":[]}'),
(2136, 265, 'wccpf_sobre_os_produtos_escolhidos', '{"type":"textarea","label":"Sobre os produtos escolhidos","name":"sobre_os_produtos_escolhidos","is_unremovable":false,"is_enable":false,"order":"5","placeholder":"Digite sua resposta aqui","default_value":"","maxlength":"","rows":"","required":"no","message":"","visibility":"yes","order_meta":"yes","login_user_field":"no","show_for_roles":[],"cart_editable":"no","cloneable":"no","field_class":"","initial_show":"yes","locale":[],"key":"wccpf_sobre_os_produtos_escolhidos"}'),
(2137, 265, 'wccpf_sobre_outro_produto', '{"type":"textarea","label":"Sobre outro produto","name":"sobre_outro_produto","is_unremovable":false,"is_enable":false,"order":"6","placeholder":"Digite sua resposta aqui","default_value":"","maxlength":"","rows":"","required":"no","message":"","visibility":"yes","order_meta":"yes","login_user_field":"no","show_for_roles":[],"cart_editable":"no","cloneable":"no","field_class":"","initial_show":"yes","locale":[],"key":"wccpf_sobre_outro_produto"}'),
(2138, 265, 'wccpf_mais_informaes', '{"type":"textarea","label":"Mais informa\\u00e7\\u00f5es","name":"mais_informaes","is_unremovable":false,"is_enable":false,"order":"7","placeholder":"Digite sua resposta aqui","default_value":"","maxlength":"","rows":"","required":"no","message":"","visibility":"yes","order_meta":"yes","login_user_field":"no","show_for_roles":[],"cart_editable":"no","cloneable":"no","field_class":"","initial_show":"yes","locale":[],"key":"wccpf_mais_informaes"}'),
(2139, 265, 'wccpf_produtos_do_kit', '{"type":"textarea","label":"Produtos do Kit","name":"produtos_do_kit","is_unremovable":false,"is_enable":false,"order":"8","placeholder":"","default_value":"","maxlength":"","rows":"","required":"no","message":"","visibility":"yes","order_meta":"yes","login_user_field":"no","show_for_roles":[],"cart_editable":"no","cloneable":"no","field_class":"","initial_show":"yes","locale":[],"key":"wccpf_produtos_do_kit"}'),
(2140, 265, 'wccpf_condition_rules', '[[{"context":"product","logic":"==","endpoint":"242"}]]'),
(2141, 265, 'wccpf_field_location_on_product', 'use_global_setting'),
(2142, 265, 'wccpf_field_location_on_archive', 'none');

-- --------------------------------------------------------

--
-- Table structure for table `wd_posts`
--

CREATE TABLE `wd_posts` (
  `ID` bigint(20) unsigned NOT NULL,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=366 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wd_posts`
--

INSERT INTO `wd_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2018-08-23 11:05:40', '2018-08-23 14:05:40', 'Bem-vindo ao WordPress. Esse é o seu primeiro post. Edite-o ou exclua-o, e então comece a escrever!', 'Lorem ipsum dolor sit!', '', 'publish', 'open', 'open', '', 'ola-mundo-2', '', '', '2018-08-27 11:19:22', '2018-08-27 14:19:22', '', 0, 'http://localhost/walldone-new/?p=1', 0, 'post', '', 1),
(2, 1, '2018-08-23 11:05:40', '2018-08-23 14:05:40', 'Este é o exemplo de uma página. É diferente de um post de blog porque é estática e pode aparecer em menus de navegação (na maioria dos temas). A maioria das pessoas começam com uma página ''Sobre'' que as apresenta aos potenciais visitantes do site. Você pode usar algo como:\n\n<blockquote>Oi! Sou um estudante de Biologia e gosto de esportes e natureza. Nos fins de semana pratico futebol com meus amigos no clube local. Eu moro em Valinhos e fiz este site para falar sobre minha cidade.</blockquote>\n\n...ou algo como:\n\n<blockquote>A empresa Logos foi fundada em 1980, e tem provido o comércio local com o que há de melhor em informatização. Localizada em Recife, nossa empresa tem se destacado como um das que também contribuem para o descarte correto de equipamentos eletrônicos substituídos.</blockquote>\n\nComo um novo usuário WordPress, vá ao seu <a href="http://localhost/walldone-new/wp-admin/">Painel</a> para excluir este conteúdo e criar o seu. Divirta-se!', 'Página de exemplo', '', 'publish', 'closed', 'open', '', 'pagina-exemplo', '', '', '2018-08-23 11:05:40', '2018-08-23 14:05:40', '', 0, 'http://localhost/walldone-new/?page_id=2', 0, 'page', '', 0),
(3, 1, '2018-08-23 11:05:40', '2018-08-23 14:05:40', '<h2>Quem somos</h2><p>O endereço do nosso site é: http://localhost/walldone-new.</p><h2>Quais dados pessoais coletamos e porque</h2><h3>Comentários</h3><p>Quando os visitantes deixam comentários no site, coletamos os dados mostrados no formulário de comentários, além do endereço de IP e de dados do navegador do visitante, para auxiliar na detecção de spam.</p><p>Uma sequência anonimizada de caracteres criada a partir do seu e-mail (também chamada de hash) poderá ser enviada para o Gravatar para verificar se você usa o serviço. A política de privacidade do Gravatar está disponível aqui: https://automattic.com/privacy/. Depois da aprovação do seu comentário, a foto do seu perfil fica visível publicamente junto de seu comentário.</p><h3>Mídia</h3><p>Se você envia imagens para o site, evite enviar as que contenham dados de localização incorporados (EXIF GPS). Visitantes podem baixar estas imagens do site e extrair delas seus dados de localização.</p><h3>Formulários de contato</h3><h3>Cookies</h3><p>Ao deixar um comentário no site, você poderá optar por salvar seu nome, e-mail e site nos cookies. Isso visa seu conforto, assim você não precisará preencher seus  dados novamente quando fizer outro comentário. Estes cookies duram um ano.</p><p>Se você tem uma conta e acessa este site, um cookie temporário será criado para determinar se seu navegador aceita cookies. Ele não contém nenhum dado pessoal e será descartado quando você fechar seu navegador.</p><p>Quando você acessa sua conta no site, também criamos vários cookies para salvar os dados da sua conta e suas escolhas de exibição de tela. Cookies de login são mantidos por dois dias e cookies de opções de tela por um ano. Se você selecionar &quot;Lembrar-me&quot;, seu acesso será mantido por duas semanas. Se você se desconectar da sua conta, os cookies de login serão removidos.</p><p>Se você editar ou publicar um artigo, um cookie adicional será salvo no seu navegador. Este cookie não inclui nenhum dado pessoal e simplesmente indica o ID do post referente ao artigo que você acabou de editar. Ele expira depois de 1 dia.</p><h3>Mídia incorporada de outros sites</h3><p>Artigos neste site podem incluir conteúdo incorporado como, por exemplo, vídeos, imagens, artigos, etc. Conteúdos incorporados de outros sites se comportam exatamente da mesma forma como se o visitante estivesse visitando o outro site.</p><p>Estes sites podem coletar dados sobre você, usar cookies, incorporar rastreamento adicional de terceiros e monitorar sua interação com este conteúdo incorporado, incluindo sua interação com o conteúdo incorporado se você tem uma conta e está conectado com o site.</p><h3>Análises</h3><h2>Com quem partilhamos seus dados</h2><h2>Por quanto tempo mantemos os seus dados</h2><p>Se você deixar um comentário, o comentário e os seus metadados são conservados indefinidamente. Fazemos isso para que seja possível reconhecer e aprovar automaticamente qualquer comentário posterior ao invés de retê-lo para moderação.</p><p>Para usuários que se registram no nosso site (se houver), também guardamos as informações pessoais que fornecem no seu perfil de usuário. Todos os usuários podem ver, editar ou excluir suas informações pessoais a qualquer momento (só não é possível alterar o seu username). Os administradores de sites também podem ver e editar estas informações.</p><h2>Quais os seus direitos sobre seus dados</h2><p>Se você tiver uma conta neste site ou se tiver deixado comentários, pode solicitar um arquivo exportado dos dados pessoais que mantemos sobre você, inclusive quaisquer dados que nos tenha fornecido. Também pode solicitar que removamos qualquer dado pessoal que mantemos sobre você. Isto não inclui nenhuns dados que somos obrigados a manter para propósitos administrativos, legais ou de segurança.</p><h2>Para onde enviamos seus dados</h2><p>Comentários de visitantes podem ser marcados por um serviço automático de detecção de spam.</p><h2>Suas informações de contato</h2><h2>Informações adicionais</h2><h3>Como protegemos seus dados</h3><h3>Quais são nossos procedimentos contra violação de dados</h3><h3>De quais terceiros nós recebemos dados</h3><h3>Quais tomadas de decisão ou análises de perfil automatizadas fazemos com os dados de usuários</h3><h3>Requisitos obrigatórios de divulgação para sua categoria profissional</h3>', 'Política de privacidade', '', 'draft', 'closed', 'open', '', 'politica-de-privacidade', '', '', '2018-08-23 11:05:40', '2018-08-23 14:05:40', '', 0, 'http://localhost/walldone-new/?page_id=3', 0, 'page', '', 0),
(5, 1, '2018-08-23 11:08:48', '2018-08-23 14:08:48', '', 'Loja', '', 'publish', 'closed', 'closed', '', 'loja', '', '', '2018-08-23 11:08:48', '2018-08-23 14:08:48', '', 0, 'http://localhost/walldone-new/loja/', 0, 'page', '', 0),
(6, 1, '2018-08-23 11:08:48', '2018-08-23 14:08:48', '[woocommerce_cart]', 'Carrinho', '', 'publish', 'closed', 'closed', '', 'carrinho', '', '', '2018-10-01 22:57:49', '2018-10-02 01:57:49', '', 0, 'http://localhost/walldone-new/carrinho/', 0, 'page', '', 0),
(7, 1, '2018-08-23 11:08:48', '2018-08-23 14:08:48', '[woocommerce_checkout]', 'Finalizar compra', '', 'publish', 'closed', 'closed', '', 'finalizar-compra', '', '', '2018-10-02 15:32:20', '2018-10-02 18:32:20', '', 0, 'http://localhost/walldone-new/finalizar-compra/', 0, 'page', '', 0),
(8, 1, '2018-08-23 11:08:48', '2018-08-23 14:08:48', '', 'Minha conta', '', 'publish', 'closed', 'closed', '', 'minha-conta', '', '', '2018-08-23 11:36:48', '2018-08-23 14:36:48', '', 0, 'http://localhost/walldone-new/minha-conta/', 0, 'page', '', 0),
(38, 1, '2018-05-25 16:14:25', '2018-05-25 19:14:25', '<span style="vertical-align: inherit;"><span style="vertical-align: inherit;">Nutrição moradores de futebol triste netus velho e feio et malesuada fome ea pobreza. </span><span style="vertical-align: inherit;">Vestibulum quam tortor, feugiat vitae, ultricies eget, tempor sit amet, ante. </span><span style="vertical-align: inherit;">Eu sempre quero futebol livre até as cenouras. </span><span style="vertical-align: inherit;">ultricies Aenean vitae mi est. </span><span style="vertical-align: inherit;">maior leão imobiliário do Japão.</span></span>', 'Vaso Melancia', 'ou 2x de R$ 54,50 sem juros', 'publish', 'open', 'closed', '', 'vaso-melancia', '', '', '2018-05-25 16:14:25', '2018-05-25 19:14:25', '', 0, 'http://localhost/walldone-new/produto/vaso-melancia/', 0, 'product', '', 0),
(39, 1, '2018-08-23 11:16:13', '2018-08-23 14:16:13', '', 'Vaso Melancia 1', '', 'inherit', 'open', 'closed', '', 'vaso-melancia-1', '', '', '2018-08-23 11:16:13', '2018-08-23 14:16:13', '', 38, 'http://localhost/walldone-new/wp-content/uploads/2018/08/vaso-melancia-walldone.jpg', 0, 'attachment', 'image/jpeg', 0),
(40, 1, '2018-08-23 11:16:13', '2018-08-23 14:16:13', '', 'Vaso Melancia 2', '', 'inherit', 'open', 'closed', '', 'vaso-melancia-2', '', '', '2018-08-23 11:16:13', '2018-08-23 14:16:13', '', 38, 'http://localhost/walldone-new/wp-content/uploads/2018/08/vaso-melancia-hover-walldone.jpg', 0, 'attachment', 'image/jpeg', 0),
(50, 1, '2018-06-07 12:05:22', '2018-06-07 15:05:22', '<span style="vertical-align: inherit;"><span style="vertical-align: inherit;">Lorem ipsum dolor sit amet, elit consectetur adipiscing, tempor sed e vitalidade, de modo que o trabalho e tristeza, algumas coisas importantes a fazer eiusmod. </span><span style="vertical-align: inherit;">Ao longo dos anos vir. </span><span style="vertical-align: inherit;">:)</span></span>', 'Adesivo para parede Flamingo', 'ou 2x de 12,50 sem juros', 'publish', 'open', 'closed', '', 'adesivo-para-parede-flamingo', '', '', '2018-06-07 12:05:22', '2018-06-07 15:05:22', '', 0, 'http://localhost/walldone-new/produto/adesivo-para-parede-flamingo/', 0, 'product', '', 0),
(51, 1, '2018-08-23 11:16:13', '2018-08-23 14:16:13', '', 'Adesivo para parede Flamingo 1', '', 'inherit', 'open', 'closed', '', 'adesivo-para-parede-flamingo-1', '', '', '2018-08-23 11:16:13', '2018-08-23 14:16:13', '', 50, 'http://localhost/walldone-new/wp-content/uploads/2018/08/adesivo-parede-flamingo-walldone.jpg', 0, 'attachment', 'image/jpeg', 0),
(52, 1, '2018-08-23 11:16:13', '2018-08-23 14:16:13', '', 'Adesivo para parede Flamingo 2', '', 'inherit', 'open', 'closed', '', 'adesivo-para-parede-flamingo-2', '', '', '2018-08-23 11:16:13', '2018-08-23 14:16:13', '', 50, 'http://localhost/walldone-new/wp-content/uploads/2018/08/adesivo-parede-flamingo-hover-walldone.jpg', 0, 'attachment', 'image/jpeg', 0),
(53, 1, '2018-08-23 11:16:13', '2018-08-23 14:16:13', '', 'Adesivo para parede Flamingo 3', '', 'inherit', 'open', 'closed', '', 'adesivo-para-parede-flamingo-3', '', '', '2018-08-23 11:16:13', '2018-08-23 14:16:13', '', 50, 'http://localhost/walldone-new/wp-content/uploads/2018/08/quadro-gato-frida-aquarela-walldone.jpg', 0, 'attachment', 'image/jpeg', 0),
(54, 1, '2018-08-23 11:16:13', '2018-08-23 14:16:13', '', 'Adesivo para parede Flamingo 4', '', 'inherit', 'open', 'closed', '', 'adesivo-para-parede-flamingo-4', '', '', '2018-08-23 11:16:13', '2018-08-23 14:16:13', '', 50, 'http://localhost/walldone-new/wp-content/uploads/2018/08/adesivo-bolas-irregulares-hover-walldone.png', 0, 'attachment', 'image/png', 0),
(55, 1, '2018-08-23 11:16:13', '2018-08-23 14:16:13', '', 'Adesivo para parede Flamingo 5', '', 'inherit', 'open', 'closed', '', 'adesivo-para-parede-flamingo-5', '', '', '2018-08-23 11:16:13', '2018-08-23 14:16:13', '', 50, 'http://localhost/walldone-new/wp-content/uploads/2018/08/quadro-nosso-lar-hover-walldone.jpg', 0, 'attachment', 'image/jpeg', 0),
(56, 1, '2018-08-23 11:16:13', '2018-08-23 14:16:13', '', 'Adesivo para parede Flamingo 6', '', 'inherit', 'open', 'closed', '', 'adesivo-para-parede-flamingo-6', '', '', '2018-08-23 11:16:13', '2018-08-23 14:16:13', '', 50, 'http://localhost/walldone-new/wp-content/uploads/2018/08/ela-e-minha-menina-hover-walldone.jpg', 0, 'attachment', 'image/jpeg', 0),
(57, 1, '2018-08-23 11:16:13', '2018-08-23 14:16:13', '', 'Adesivo para parede Flamingo 7', '', 'inherit', 'open', 'closed', '', 'adesivo-para-parede-flamingo-7', '', '', '2018-08-23 11:16:13', '2018-08-23 14:16:13', '', 50, 'http://localhost/walldone-new/wp-content/uploads/2018/08/ela-e-minha-menina-walldone.jpg', 0, 'attachment', 'image/jpeg', 0),
(58, 1, '2018-08-23 11:16:13', '2018-08-23 14:16:13', '', 'Adesivo para parede Flamingo 8', '', 'inherit', 'open', 'closed', '', 'adesivo-para-parede-flamingo-8', '', '', '2018-08-23 11:16:13', '2018-08-23 14:16:13', '', 50, 'http://localhost/walldone-new/wp-content/uploads/2018/08/video.jpg', 0, 'attachment', 'image/jpeg', 0),
(90, 1, '2018-06-27 16:39:59', '2018-06-27 19:39:59', 'Quarto de Casal Menina e Menino', 'Quarto de Casal Menina e Menino', 'a partir de', 'publish', 'open', 'closed', '', 'quarto-de-casal-menina-e-menino', '', '', '2018-06-27 16:39:59', '2018-06-27 19:39:59', '', 0, 'http://localhost/walldone-new/produto/quarto-de-casal-menina-e-menino/', 0, 'product', '', 0),
(91, 1, '2018-08-23 11:16:13', '2018-08-23 14:16:13', '', 'Quarto de Casal Menina e Menino 1', '', 'inherit', 'open', 'closed', '', 'quarto-de-casal-menina-e-menino-1', '', '', '2018-08-23 11:16:13', '2018-08-23 14:16:13', '', 90, 'http://localhost/walldone-new/wp-content/uploads/2018/08/ela-e-minha-menina-walldone-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(92, 1, '2018-08-23 11:16:13', '2018-08-23 14:16:13', '', 'Quarto de Casal Menina e Menino 2', '', 'inherit', 'open', 'closed', '', 'quarto-de-casal-menina-e-menino-2', '', '', '2018-08-23 11:16:13', '2018-08-23 14:16:13', '', 90, 'http://localhost/walldone-new/wp-content/uploads/2018/08/ela-e-minha-menina-hover-walldone-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(93, 1, '2018-06-27 17:19:22', '2018-06-27 20:19:22', 'Quadro - Nosso Lar', 'Quadro - Nosso Lar', 'ou 2x de R$ 54,50 sem juros', 'publish', 'open', 'closed', '', 'quadro-nosso-lar', '', '', '2018-06-27 17:19:22', '2018-06-27 20:19:22', '', 0, 'http://localhost/walldone-new/produto/quadro-nosso-lar/', 0, 'product', '', 0),
(94, 1, '2018-08-23 11:16:13', '2018-08-23 14:16:13', '', 'Quadro - Nosso Lar 1', '', 'inherit', 'open', 'closed', '', 'quadro-nosso-lar-1', '', '', '2018-08-23 11:16:13', '2018-08-23 14:16:13', '', 93, 'http://localhost/walldone-new/wp-content/uploads/2018/08/quadro-nosso-lar-walldone.jpg', 0, 'attachment', 'image/jpeg', 0),
(95, 1, '2018-08-23 11:16:13', '2018-08-23 14:16:13', '', 'Quadro - Nosso Lar 2', '', 'inherit', 'open', 'closed', '', 'quadro-nosso-lar-2', '', '', '2018-08-23 11:16:13', '2018-08-23 14:16:13', '', 93, 'http://localhost/walldone-new/wp-content/uploads/2018/08/quadro-nosso-lar-hover-walldone-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(96, 1, '2018-06-27 17:21:19', '2018-06-27 20:21:19', 'Adesivo para Parede - Bolas Irregulares\r\n\r\n<iframe width="560" height="315" src="https://www.youtube.com/embed/TF0Z2lKxJoo?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>', 'Adesivo para Parede - Bolas Irregulares', '', 'publish', 'open', 'closed', '', 'adesivo-para-parede-bolas-irregulares', '', '', '2018-09-28 10:33:49', '2018-09-28 13:33:49', '', 0, 'http://localhost/walldone-new/produto/adesivo-para-parede-bolas-irregulares/', 0, 'product', '', 1),
(97, 1, '2018-08-23 11:16:13', '2018-08-23 14:16:13', '', 'Adesivo para Parede - Bolas Irregulares 1', '', 'inherit', 'open', 'closed', '', 'adesivo-para-parede-bolas-irregulares-1', '', '', '2018-09-28 08:58:57', '2018-09-28 11:58:57', '', 96, 'http://localhost/walldone-new/wp-content/uploads/2018/08/adesivo-bolas-irregulares-walldone.jpg', 0, 'attachment', 'image/jpeg', 0),
(98, 1, '2018-08-23 11:16:13', '2018-08-23 14:16:13', '', 'Adesivo para Parede - Bolas Irregulares 2', '', 'inherit', 'open', 'closed', '', 'adesivo-para-parede-bolas-irregulares-2', '', '', '2018-08-23 11:16:13', '2018-08-23 14:16:13', '', 96, 'http://localhost/walldone-new/wp-content/uploads/2018/08/adesivo-bolas-irregulares-hover-walldone-1.png', 0, 'attachment', 'image/png', 0),
(99, 1, '2018-06-27 17:22:35', '2018-06-27 20:22:35', 'Quadro Gato Frida Aquarela, tempor sed e vitalidade, de modo que o trabalho e tristeza, algumas coisas importantes a fazer eiusmod. Ao longo dos anos vir. :)', 'Quadro Gato Frida Aquarela', '', 'publish', 'open', 'closed', '', 'quadro-gato-frida-aquarela', '', '', '2018-08-24 20:13:23', '2018-08-24 23:13:23', '', 0, 'http://localhost/walldone-new/produto/quadro-gato-frida-aquarela/', 0, 'product', '', 2),
(100, 1, '2018-08-23 11:16:13', '2018-08-23 14:16:13', '', 'Quadro Gato Frida Aquarela 1', '', 'inherit', 'open', 'closed', '', 'quadro-gato-frida-aquarela-1', '', '', '2018-08-23 11:16:13', '2018-08-23 14:16:13', '', 99, 'http://localhost/walldone-new/wp-content/uploads/2018/08/quadro-gato-frida-aquarela-walldone-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(101, 1, '2018-08-23 11:16:13', '2018-08-23 14:16:13', '', 'Quadro Gato Frida Aquarela 2', '', 'inherit', 'open', 'closed', '', 'quadro-gato-frida-aquarela-2', '', '', '2018-08-23 11:16:13', '2018-08-23 14:16:13', '', 99, 'http://localhost/walldone-new/wp-content/uploads/2018/08/quadro-gato-frida-aquarela-hover-walldone.jpg', 0, 'attachment', 'image/jpeg', 0),
(102, 1, '2018-08-23 11:19:30', '2018-08-23 14:19:30', '<div class="form-group">\r\n    [text Nome class:form-control placeholder "Nome"]\r\n</div>\r\n\r\n<div class="form-group">\r\n    [email Email class:form-control placeholder "E-mail"]\r\n</div>\r\n\r\n<div class="form-group submit">\r\n   [submit class:btn-clean "Quero Super Novidades"]\r\n</div>\n1\nWall Done "[your-subject]"\n[your-name] <web@904.ag>\nweb@904.ag\nDe: [your-name] <[your-email]>\r\nAssunto: [your-subject]\r\n\r\nCorpo da mensagem:\r\n[your-message]\r\n\r\n-- \r\nEste e-mail foi enviado de um formulário de contato em Wall Done (http://localhost/walldone-new)\nReply-To: [your-email]\n\n\n\n\nWall Done "[your-subject]"\nWall Done <web@904.ag>\n[your-email]\nCorpo da mensagem:\r\n[your-message]\r\n\r\n-- \r\nEste e-mail foi enviado de um formulário de contato em Wall Done (http://localhost/walldone-new)\nReply-To: web@904.ag\n\n\n\nAgradecemos a sua mensagem.\nOcorreu um erro ao tentar enviar sua mensagem. Tente novamente mais tarde.\nUm ou mais campos possuem um erro. Verifique e tente novamente.\nOcorreu um erro ao tentar enviar sua mensagem. Tente novamente mais tarde.\nVocê deve aceitar os termos e condições antes de enviar sua mensagem.\nO campo é obrigatório.\nO campo é muito longo.\nO campo é muito curto.\nO formato de data está incorreto.\nA data é anterior à mais antiga permitida.\nA data é posterior à maior data permitida.\nOcorreu um erro desconhecido ao enviar o arquivo.\nVocê não tem permissão para enviar esse tipo de arquivo.\nO arquivo é muito grande.\nOcorreu um erro ao enviar o arquivo.\nO formato de número é inválido.\nO número é menor do que o mínimo permitido.\nO número é maior do que o máximo permitido.\nA resposta para o quiz está incorreta.\nO código digitado está incorreto.\nO endereço de e-mail informado é inválido.\nA URL é inválida.\nO número de telefone é inválido.', 'Newsletter', '', 'publish', 'closed', 'closed', '', 'formulario-de-contato-1', '', '', '2018-08-27 14:39:50', '2018-08-27 17:39:50', '', 0, 'http://localhost/walldone-new/?post_type=wpcf7_contact_form&#038;p=102', 0, 'wpcf7_contact_form', '', 0),
(103, 1, '2018-08-23 11:21:55', '2018-08-23 14:21:55', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.', 'Com amor, Larissa Rothen!', '', 'publish', 'closed', 'closed', '', 'com-amor-larissa-rothen', '', '', '2018-08-23 11:21:55', '2018-08-23 14:21:55', '', 0, 'http://localhost/walldone-new/?post_type=slider&#038;p=103', 0, 'slider', '', 0),
(104, 1, '2018-08-23 11:21:38', '2018-08-23 14:21:38', '', 'slider-top-home-walldone', '', 'inherit', 'open', 'closed', '', 'slider-top-home-walldone', '', '', '2018-08-23 11:21:38', '2018-08-23 14:21:38', '', 103, 'http://localhost/walldone-new/wp-content/uploads/2018/08/slider-top-home-walldone.jpg', 0, 'attachment', 'image/jpeg', 0),
(105, 1, '2018-08-23 11:36:42', '2018-08-23 14:36:42', '', 'Minha conta', '', 'inherit', 'closed', 'closed', '', '8-revision-v1', '', '', '2018-08-23 11:36:42', '2018-08-23 14:36:42', '', 8, 'http://localhost/walldone-new/2018/08/23/8-revision-v1/', 0, 'revision', '', 0),
(106, 1, '2018-08-23 15:54:19', '2018-08-23 18:54:19', '', 'Adesivo para Parede - Bolas Irregulares - Moldura Verde, Verde', '', 'publish', 'closed', 'closed', '', 'adesivo-para-parede-bolas-irregulares-verde-moldura-verde', '', '', '2018-09-28 08:19:55', '2018-09-28 11:19:55', '', 96, 'http://localhost/walldone-new/produto/adesivo-para-parede-bolas-irregulares/', 2, 'product_variation', '', 0),
(107, 1, '2018-08-23 15:54:19', '2018-08-23 18:54:19', '', 'Adesivo para Parede - Bolas Irregulares - Moldura Verde, Preto', '', 'publish', 'closed', 'closed', '', 'adesivo-para-parede-bolas-irregulares-preto-moldura-verde', '', '', '2018-09-28 08:14:06', '2018-09-28 11:14:06', '', 96, 'http://localhost/walldone-new/produto/adesivo-para-parede-bolas-irregulares/', 1, 'product_variation', '', 0),
(108, 1, '2018-08-23 15:54:19', '2018-08-23 18:54:19', '', 'Adesivo para Parede - Bolas Irregulares - Moldura Verde, Sem cor', '', 'publish', 'closed', 'closed', '', 'adesivo-para-parede-bolas-irregulares-sem-cor-moldura-verde', '', '', '2018-08-23 16:09:46', '2018-08-23 19:09:46', '', 96, 'http://localhost/walldone-new/produto/adesivo-para-parede-bolas-irregulares/', 3, 'product_variation', '', 0),
(109, 1, '2018-08-23 15:54:19', '2018-08-23 18:54:19', '', 'Adesivo para Parede - Bolas Irregulares - Moldura Preta, Verde', '', 'publish', 'closed', 'closed', '', 'adesivo-para-parede-bolas-irregulares-verde-moldura-preta', '', '', '2018-08-23 16:09:46', '2018-08-23 19:09:46', '', 96, 'http://localhost/walldone-new/produto/adesivo-para-parede-bolas-irregulares/', 4, 'product_variation', '', 0),
(110, 1, '2018-08-23 15:54:19', '2018-08-23 18:54:19', '', 'Adesivo para Parede - Bolas Irregulares - Moldura Preta, Preto', '', 'publish', 'closed', 'closed', '', 'adesivo-para-parede-bolas-irregulares-preto-moldura-preta', '', '', '2018-08-23 16:09:47', '2018-08-23 19:09:47', '', 96, 'http://localhost/walldone-new/produto/adesivo-para-parede-bolas-irregulares/', 5, 'product_variation', '', 0),
(111, 1, '2018-08-23 15:54:19', '2018-08-23 18:54:19', '', 'Adesivo para Parede - Bolas Irregulares - Moldura Preta, Sem cor', '', 'publish', 'closed', 'closed', '', 'adesivo-para-parede-bolas-irregulares-sem-cor-moldura-preta', '', '', '2018-08-23 16:09:47', '2018-08-23 19:09:47', '', 96, 'http://localhost/walldone-new/produto/adesivo-para-parede-bolas-irregulares/', 6, 'product_variation', '', 0),
(112, 1, '2018-08-23 15:54:19', '2018-08-23 18:54:19', '', 'Adesivo para Parede - Bolas Irregulares - Sem Moldura, Verde', '', 'publish', 'closed', 'closed', '', 'adesivo-para-parede-bolas-irregulares-verde-sem-moldura', '', '', '2018-08-23 16:09:47', '2018-08-23 19:09:47', '', 96, 'http://localhost/walldone-new/produto/adesivo-para-parede-bolas-irregulares/', 7, 'product_variation', '', 0),
(113, 1, '2018-08-23 15:54:20', '2018-08-23 18:54:20', '', 'Adesivo para Parede - Bolas Irregulares - Sem Moldura, Preto', '', 'publish', 'closed', 'closed', '', 'adesivo-para-parede-bolas-irregulares-preto-sem-moldura', '', '', '2018-08-23 16:09:47', '2018-08-23 19:09:47', '', 96, 'http://localhost/walldone-new/produto/adesivo-para-parede-bolas-irregulares/', 8, 'product_variation', '', 0),
(114, 1, '2018-08-23 15:54:20', '2018-08-23 18:54:20', '', 'Adesivo para Parede - Bolas Irregulares - Sem Moldura, Sem cor', '', 'publish', 'closed', 'closed', '', 'adesivo-para-parede-bolas-irregulares-sem-cor-sem-moldura', '', '', '2018-08-23 16:09:47', '2018-08-23 19:09:47', '', 96, 'http://localhost/walldone-new/produto/adesivo-para-parede-bolas-irregulares/', 9, 'product_variation', '', 0),
(115, 1, '2018-08-23 16:47:45', '2018-08-23 19:47:45', 'Quadro Gato Frida Aquarela, tempor sed e vitalidade, de modo que o trabalho e tristeza, algumas coisas importantes a fazer eiusmod. Ao longo dos anos vir. :)', 'Quadro Gato Frida Aquarela', '<p><br data-mce-bogus="1"></p>', 'inherit', 'closed', 'closed', '', '99-autosave-v1', '', '', '2018-08-23 16:47:45', '2018-08-23 19:47:45', '', 99, 'http://localhost/walldone-new/2018/08/23/99-autosave-v1/', 0, 'revision', '', 0),
(116, 1, '2018-08-23 17:54:38', '2018-08-23 20:54:38', '', 'teste-quadro-gato-frida-aquarela-walldone-1', '', 'inherit', 'open', 'closed', '', 'teste-quadro-gato-frida-aquarela-walldone-1', '', '', '2018-08-23 17:54:38', '2018-08-23 20:54:38', '', 99, 'http://localhost/walldone-new/wp-content/uploads/2018/06/teste-quadro-gato-frida-aquarela-walldone-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(118, 1, '2018-08-24 20:18:43', '2018-08-24 23:18:43', '', 'Produtos em Promoção', '', 'publish', 'closed', 'closed', '', 'promocoes', '', '', '2018-08-26 21:51:45', '2018-08-27 00:51:45', '', 0, 'http://localhost/walldone-new/?page_id=118', 0, 'page', '', 0),
(119, 1, '2018-08-24 20:18:43', '2018-08-24 23:18:43', '', 'Promoção', '', 'inherit', 'closed', 'closed', '', '118-revision-v1', '', '', '2018-08-24 20:18:43', '2018-08-24 23:18:43', '', 118, 'http://localhost/walldone-new/2018/08/24/118-revision-v1/', 0, 'revision', '', 0),
(129, 1, '2018-08-25 19:59:45', '2018-08-25 22:59:45', 'Quadro Gato Frida Aquarela, tempor sed e vitalidade, de modo que o trabalho e tristeza, algumas coisas importantes a fazer eiusmod. Ao longo dos anos vir. :)', 'Quadro Gato Frida Aquarela (cópia)', '', 'publish', 'open', 'closed', '', 'quadro-gato-frida-aquarela-copia', '', '', '2018-09-27 15:36:41', '2018-09-27 18:36:41', '', 0, 'http://localhost/walldone-new/?post_type=product&#038;p=129', 0, 'product', '', 0),
(130, 1, '2018-08-25 20:29:45', '2018-08-25 23:29:45', '', 'Promoções', '', 'inherit', 'closed', 'closed', '', '118-revision-v1', '', '', '2018-08-25 20:29:45', '2018-08-25 23:29:45', '', 118, 'http://localhost/walldone-new/2018/08/25/118-revision-v1/', 0, 'revision', '', 0),
(131, 1, '2018-08-25 20:30:02', '2018-08-25 23:30:02', '', 'Produtos em Promoção', '', 'inherit', 'closed', 'closed', '', '118-revision-v1', '', '', '2018-08-25 20:30:02', '2018-08-25 23:30:02', '', 118, 'http://localhost/walldone-new/2018/08/25/118-revision-v1/', 0, 'revision', '', 0),
(132, 1, '2018-08-26 21:53:59', '2018-08-27 00:53:59', '', 'Blog da Larissa', '', 'publish', 'closed', 'closed', '', 'blog-da-larissa', '', '', '2018-09-04 10:14:10', '2018-09-04 13:14:10', '', 0, 'http://localhost/walldone-new/?page_id=132', 0, 'page', '', 0),
(133, 1, '2018-08-26 21:53:59', '2018-08-27 00:53:59', '', 'Blog da Larissa', '', 'inherit', 'closed', 'closed', '', '132-revision-v1', '', '', '2018-08-26 21:53:59', '2018-08-27 00:53:59', '', 132, 'http://localhost/walldone-new/2018/08/26/132-revision-v1/', 0, 'revision', '', 0),
(134, 1, '2018-08-26 22:24:20', '2018-08-27 01:24:20', 'Bem-vindo ao WordPress. Esse é o seu primeiro post. Edite-o ou exclua-o, e então comece a escrever!', 'Lorem ipsum dolor sit!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2018-08-26 22:24:20', '2018-08-27 01:24:20', '', 1, 'http://localhost/walldone-new/2018/08/26/1-revision-v1/', 0, 'revision', '', 0),
(136, 1, '2018-08-26 22:41:43', '2018-08-27 01:41:43', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.', 'Lorem ipsum dolor sit!', '', 'publish', 'closed', 'closed', '', 'lorem-ipsum-dolor-sit', '', '', '2018-08-26 22:41:43', '2018-08-27 01:41:43', '', 0, 'http://localhost/walldone-new/?post_type=slider_blog&#038;p=136', 0, 'slider_blog', '', 0),
(137, 1, '2018-08-26 22:41:35', '2018-08-27 01:41:35', '', 'teste-slider-blog-walldone', '', 'inherit', 'open', 'closed', '', 'teste-slider-blog-walldone', '', '', '2018-08-26 22:41:35', '2018-08-27 01:41:35', '', 136, 'http://localhost/walldone-new/wp-content/uploads/2018/08/teste-slider-blog-walldone.jpg', 0, 'attachment', 'image/jpeg', 0),
(138, 1, '2018-08-26 22:41:36', '2018-08-27 01:41:36', '', 'teste2-slider-blog-walldone', '', 'inherit', 'open', 'closed', '', 'teste2-slider-blog-walldone', '', '', '2018-08-26 22:41:36', '2018-08-27 01:41:36', '', 136, 'http://localhost/walldone-new/wp-content/uploads/2018/08/teste2-slider-blog-walldone.jpg', 0, 'attachment', 'image/jpeg', 0),
(140, 1, '2018-08-26 22:53:09', '2018-08-27 01:53:09', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.', 'Lorem ipsum dolor sit!', '', 'publish', 'closed', 'closed', '', 'lorem-ipsum-dolor-sit-2', '', '', '2018-08-26 22:53:09', '2018-08-27 01:53:09', '', 0, 'http://localhost/walldone-new/?post_type=slider_blog&#038;p=140', 0, 'slider_blog', '', 0),
(141, 1, '2018-08-27 11:19:49', '2018-08-27 14:19:49', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reptrehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\r\n\r\nUt enim ad minim veniam, quis nostrud exercitation ullamco.Laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.\r\n\r\n[gallery columns="4" link="none" size="medium" ids="40,97,95,98,100,101,52,55"]\r\n\r\nDuis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.\r\n\r\nExcepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Lorem ipsum dolor sit!', '', 'publish', 'open', 'open', '', 'teste2', '', '', '2018-08-27 15:31:58', '2018-08-27 18:31:58', '', 0, 'http://localhost/walldone-new/?p=141', 0, 'post', '', 0),
(142, 1, '2018-08-27 11:20:11', '2018-08-27 14:20:11', 'Bem-vindo ao WordPress. Esse é o seu primeiro post. Edite-o ou exclua-o, e então comece a escrever!', 'Lorem ipsum dolor sit!', '', 'publish', 'open', 'open', '', 'teste3', '', '', '2018-08-27 11:20:11', '2018-08-27 14:20:11', '', 0, 'http://localhost/walldone-new/?p=142', 0, 'post', '', 0),
(143, 1, '2018-08-27 11:20:36', '2018-08-27 14:20:36', 'Bem-vindo ao WordPress. Esse é o seu primeiro post. Edite-o ou exclua-o, e então comece a escrever!', 'Lorem ipsum dolor sit!', '', 'publish', 'open', 'open', '', 'teste4', '', '', '2018-08-27 11:20:36', '2018-08-27 14:20:36', '', 0, 'http://localhost/walldone-new/?p=143', 0, 'post', '', 0),
(144, 1, '2018-08-27 11:20:56', '2018-08-27 14:20:56', 'Bem-vindo ao WordPress. Esse é o seu primeiro post. Edite-o ou exclua-o, e então comece a escrever!', 'Lorem ipsum dolor sit!', '', 'publish', 'open', 'open', '', 'teste5', '', '', '2018-08-27 11:20:56', '2018-08-27 14:20:56', '', 0, 'http://localhost/walldone-new/?p=144', 0, 'post', '', 0),
(145, 1, '2018-08-27 11:21:15', '2018-08-27 14:21:15', 'Bem-vindo ao WordPress. Esse é o seu primeiro post. Edite-o ou exclua-o, e então comece a escrever!', 'Lorem ipsum dolor sit!', '', 'publish', 'open', 'open', '', 'teste6', '', '', '2018-08-27 11:21:15', '2018-08-27 14:21:15', '', 0, 'http://localhost/walldone-new/?p=145', 0, 'post', '', 0),
(146, 1, '2018-08-27 11:19:07', '2018-08-27 14:19:07', '', 'teste-1-image-blog-walldone', '', 'inherit', 'open', 'closed', '', 'teste-1-image-blog-walldone', '', '', '2018-08-27 11:19:07', '2018-08-27 14:19:07', '', 1, 'http://localhost/walldone-new/wp-content/uploads/2018/08/teste-1-image-blog-walldone.jpg', 0, 'attachment', 'image/jpeg', 0),
(147, 1, '2018-08-27 11:19:08', '2018-08-27 14:19:08', '', 'teste-2-image-blog-walldone', '', 'inherit', 'open', 'closed', '', 'teste-2-image-blog-walldone', '', '', '2018-08-27 11:19:08', '2018-08-27 14:19:08', '', 1, 'http://localhost/walldone-new/wp-content/uploads/2018/08/teste-2-image-blog-walldone.jpg', 0, 'attachment', 'image/jpeg', 0),
(148, 1, '2018-08-27 11:19:09', '2018-08-27 14:19:09', '', 'teste-3-image-blog-walldone', '', 'inherit', 'open', 'closed', '', 'teste-3-image-blog-walldone', '', '', '2018-08-27 11:19:09', '2018-08-27 14:19:09', '', 1, 'http://localhost/walldone-new/wp-content/uploads/2018/08/teste-3-image-blog-walldone.jpg', 0, 'attachment', 'image/jpeg', 0),
(149, 1, '2018-08-27 11:19:10', '2018-08-27 14:19:10', '', 'teste-4-image-blog-walldone', '', 'inherit', 'open', 'closed', '', 'teste-4-image-blog-walldone', '', '', '2018-08-27 11:19:10', '2018-08-27 14:19:10', '', 1, 'http://localhost/walldone-new/wp-content/uploads/2018/08/teste-4-image-blog-walldone.jpg', 0, 'attachment', 'image/jpeg', 0),
(151, 1, '2018-08-27 11:19:12', '2018-08-27 14:19:12', '', 'teste-6-image-blog-walldone', '', 'inherit', 'open', 'closed', '', 'teste-6-image-blog-walldone', '', '', '2018-08-27 11:19:12', '2018-08-27 14:19:12', '', 1, 'http://localhost/walldone-new/wp-content/uploads/2018/08/teste-6-image-blog-walldone.jpg', 0, 'attachment', 'image/jpeg', 0),
(152, 1, '2018-08-27 11:19:49', '2018-08-27 14:19:49', 'Bem-vindo ao WordPress. Esse é o seu primeiro post. Edite-o ou exclua-o, e então comece a escrever!', 'Lorem ipsum dolor sit!', '', 'inherit', 'closed', 'closed', '', '141-revision-v1', '', '', '2018-08-27 11:19:49', '2018-08-27 14:19:49', '', 141, 'http://localhost/walldone-new/141-revision-v1/', 0, 'revision', '', 0),
(153, 1, '2018-08-27 11:20:11', '2018-08-27 14:20:11', 'Bem-vindo ao WordPress. Esse é o seu primeiro post. Edite-o ou exclua-o, e então comece a escrever!', 'Lorem ipsum dolor sit!', '', 'inherit', 'closed', 'closed', '', '142-revision-v1', '', '', '2018-08-27 11:20:11', '2018-08-27 14:20:11', '', 142, 'http://localhost/walldone-new/142-revision-v1/', 0, 'revision', '', 0),
(154, 1, '2018-08-27 11:20:36', '2018-08-27 14:20:36', 'Bem-vindo ao WordPress. Esse é o seu primeiro post. Edite-o ou exclua-o, e então comece a escrever!', 'Lorem ipsum dolor sit!', '', 'inherit', 'closed', 'closed', '', '143-revision-v1', '', '', '2018-08-27 11:20:36', '2018-08-27 14:20:36', '', 143, 'http://localhost/walldone-new/143-revision-v1/', 0, 'revision', '', 0),
(155, 1, '2018-08-27 11:20:56', '2018-08-27 14:20:56', 'Bem-vindo ao WordPress. Esse é o seu primeiro post. Edite-o ou exclua-o, e então comece a escrever!', 'Lorem ipsum dolor sit!', '', 'inherit', 'closed', 'closed', '', '144-revision-v1', '', '', '2018-08-27 11:20:56', '2018-08-27 14:20:56', '', 144, 'http://localhost/walldone-new/144-revision-v1/', 0, 'revision', '', 0),
(156, 1, '2018-08-27 11:21:15', '2018-08-27 14:21:15', 'Bem-vindo ao WordPress. Esse é o seu primeiro post. Edite-o ou exclua-o, e então comece a escrever!', 'Lorem ipsum dolor sit!', '', 'inherit', 'closed', 'closed', '', '145-revision-v1', '', '', '2018-08-27 11:21:15', '2018-08-27 14:21:15', '', 145, 'http://localhost/walldone-new/145-revision-v1/', 0, 'revision', '', 0),
(157, 1, '2018-08-27 15:30:44', '2018-08-27 18:30:44', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reptrehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\r\n\r\nUt enim ad minim veniam, quis nostrud exercitation ullamco.Laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.\r\n\r\nDuis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.\r\n\r\nExcepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Lorem ipsum dolor sit!', '', 'inherit', 'closed', 'closed', '', '141-revision-v1', '', '', '2018-08-27 15:30:44', '2018-08-27 18:30:44', '', 141, 'http://localhost/walldone-new/141-revision-v1/', 0, 'revision', '', 0),
(158, 1, '2018-08-27 15:31:47', '2018-08-27 18:31:47', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reptrehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\n\nUt enim ad minim veniam, quis nostrud exercitation ullamco.Laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.\n\n[gallery columns="4" link="none" size="medium" ids="51,40,97,95,98,100,101,52"]\n\nDuis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.\n\nExcepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Lorem ipsum dolor sit!', '', 'inherit', 'closed', 'closed', '', '141-autosave-v1', '', '', '2018-08-27 15:31:47', '2018-08-27 18:31:47', '', 141, 'http://localhost/walldone-new/141-autosave-v1/', 0, 'revision', '', 0),
(159, 1, '2018-08-27 15:31:58', '2018-08-27 18:31:58', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reptrehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\r\n\r\nUt enim ad minim veniam, quis nostrud exercitation ullamco.Laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.\r\n\r\n[gallery columns="4" link="none" size="medium" ids="40,97,95,98,100,101,52,55"]\r\n\r\nDuis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.\r\n\r\nExcepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Lorem ipsum dolor sit!', '', 'inherit', 'closed', 'closed', '', '141-revision-v1', '', '', '2018-08-27 15:31:58', '2018-08-27 18:31:58', '', 141, 'http://localhost/walldone-new/141-revision-v1/', 0, 'revision', '', 0),
(161, 1, '2018-09-03 19:28:21', '2018-09-03 22:28:21', '', 'Página inicial', '', 'trash', 'closed', 'closed', '', 'pagina-inicial__trashed', '', '', '2018-09-04 10:14:01', '2018-09-04 13:14:01', '', 0, 'http://localhost/walldone-new/?page_id=161', 0, 'page', '', 0),
(162, 1, '2018-09-03 19:28:21', '2018-09-03 22:28:21', '', 'Página inicial', '', 'inherit', 'closed', 'closed', '', '161-revision-v1', '', '', '2018-09-03 19:28:21', '2018-09-03 22:28:21', '', 161, 'http://localhost/walldone-new/161-revision-v1/', 0, 'revision', '', 0),
(163, 1, '2018-09-04 10:22:21', '2018-09-04 13:22:21', '{\n    "woocommerce_catalog_columns": {\n        "value": "3",\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-09-04 13:22:21"\n    },\n    "woocommerce_catalog_rows": {\n        "value": "3",\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-09-04 13:22:21"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '00f6dea7-f523-42f0-902b-c1424b38c41f', '', '', '2018-09-04 10:22:21', '2018-09-04 13:22:21', '', 0, 'http://localhost/walldone-new/00f6dea7-f523-42f0-902b-c1424b38c41f/', 0, 'customize_changeset', '', 0),
(164, 1, '2018-09-04 10:35:47', '2018-09-04 13:35:47', '', 'Meu Kit', '', 'publish', 'closed', 'closed', '', 'meu-kit', '', '', '2018-09-04 10:35:51', '2018-09-04 13:35:51', '', 0, 'http://localhost/walldone-new/?page_id=164', 0, 'page', '', 0),
(165, 1, '2018-09-04 10:35:47', '2018-09-04 13:35:47', '', 'Meu Kit', '', 'inherit', 'closed', 'closed', '', '164-revision-v1', '', '', '2018-09-04 10:35:47', '2018-09-04 13:35:47', '', 164, 'http://localhost/walldone-new/164-revision-v1/', 0, 'revision', '', 0),
(166, 1, '2018-09-05 11:38:16', '2018-09-05 14:38:16', '[wordpress_file_upload maxsize="10" createpath="true" showtargetfolder="true" duplicatespolicy="maintain both" targetfolderlabel="Meu Kit" widths="plugin:100%, title:100%, filename:100%, selectbutton:100%, uploadbutton:100%, uploadfolder_label:100%, subfolders:100%, subfolders_label:100%, subfolders_select:100%, webcam:100%, progressbar:100%, userdata:100%, userdata_label:100%, userdata_value:100%, consent:100%, message:100%"]', 'Monte seu Kit com a Larissa', '', 'publish', 'closed', 'closed', '', 'briefing', '', '', '2018-09-05 12:54:56', '2018-09-05 15:54:56', '', 164, 'http://localhost/walldone-new/?page_id=166', 0, 'page', '', 0),
(167, 1, '2018-09-05 11:38:16', '2018-09-05 14:38:16', '', 'Monte seu Kit com a Larissa', '', 'inherit', 'closed', 'closed', '', '166-revision-v1', '', '', '2018-09-05 11:38:16', '2018-09-05 14:38:16', '', 166, 'http://localhost/walldone-new/166-revision-v1/', 0, 'revision', '', 0),
(168, 1, '2018-09-05 12:50:20', '2018-09-05 15:50:20', '[wordpress_file_upload]', 'Monte seu Kit com a Larissa', '', 'inherit', 'closed', 'closed', '', '166-revision-v1', '', '', '2018-09-05 12:50:20', '2018-09-05 15:50:20', '', 166, 'http://localhost/walldone-new/166-revision-v1/', 0, 'revision', '', 0),
(169, 1, '2018-09-05 12:53:44', '2018-09-05 15:53:44', '[wordpress_file_upload maxsize="10" createpath="true" showtargetfolder="true" duplicatespolicy="maintain both" targetfolderlabel="Meu Kit"]', 'Monte seu Kit com a Larissa', '', 'inherit', 'closed', 'closed', '', '166-revision-v1', '', '', '2018-09-05 12:53:44', '2018-09-05 15:53:44', '', 166, 'http://localhost/walldone-new/166-revision-v1/', 0, 'revision', '', 0),
(170, 1, '2018-09-05 12:54:56', '2018-09-05 15:54:56', '[wordpress_file_upload maxsize="10" createpath="true" showtargetfolder="true" duplicatespolicy="maintain both" targetfolderlabel="Meu Kit" widths="plugin:100%, title:100%, filename:100%, selectbutton:100%, uploadbutton:100%, uploadfolder_label:100%, subfolders:100%, subfolders_label:100%, subfolders_select:100%, webcam:100%, progressbar:100%, userdata:100%, userdata_label:100%, userdata_value:100%, consent:100%, message:100%"]', 'Monte seu Kit com a Larissa', '', 'inherit', 'closed', 'closed', '', '166-revision-v1', '', '', '2018-09-05 12:54:56', '2018-09-05 15:54:56', '', 166, 'http://localhost/walldone-new/166-revision-v1/', 0, 'revision', '', 0),
(189, 1, '2018-09-05 17:22:25', '2018-09-05 20:22:25', '[woocommerce_cart]', 'Carrinho', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2018-09-05 17:22:25', '2018-09-05 20:22:25', '', 6, 'http://localhost/walldone-new/6-revision-v1/', 0, 'revision', '', 0),
(217, 1, '2018-09-05 18:47:47', '2018-09-05 21:47:47', '', '3-9-2018-904-1', '', 'inherit', 'open', 'closed', '', '3-9-2018-904-1', '', '', '2018-09-05 18:47:47', '2018-09-05 21:47:47', '', 0, 'http://localhost/walldone-new/wp-content/uploads/2018/09/3-9-2018-904-1.png', 0, 'attachment', 'image/png', 0),
(218, 1, '2018-09-05 18:48:02', '2018-09-05 21:48:02', '', 'Captura-de-Tela-2018-09-05-às-10.00.12', '', 'inherit', 'open', 'closed', '', 'captura-de-tela-2018-09-05-as-10-00-12', '', '', '2018-09-05 18:48:02', '2018-09-05 21:48:02', '', 0, 'http://localhost/walldone-new/wp-content/uploads/2018/09/Captura-de-Tela-2018-09-05-às-10.00.12.png', 0, 'attachment', 'image/png', 0),
(219, 1, '2018-09-05 18:48:33', '2018-09-05 21:48:33', '', 'Captura-de-Tela-2018-09-05-às-10.00.12-1', '', 'inherit', 'open', 'closed', '', 'captura-de-tela-2018-09-05-as-10-00-12-1', '', '', '2018-09-05 18:48:33', '2018-09-05 21:48:33', '', 0, 'http://localhost/walldone-new/wp-content/uploads/2018/09/Captura-de-Tela-2018-09-05-às-10.00.12-1.png', 0, 'attachment', 'image/png', 0),
(220, 1, '2018-09-05 18:48:33', '2018-09-05 21:48:33', '', '3-9-2018-904-2', '', 'inherit', 'open', 'closed', '', '3-9-2018-904-2', '', '', '2018-09-05 18:48:33', '2018-09-05 21:48:33', '', 0, 'http://localhost/walldone-new/wp-content/uploads/2018/09/3-9-2018-904-2.png', 0, 'attachment', 'image/png', 0),
(221, 1, '2018-09-05 18:48:44', '2018-09-05 21:48:44', '', '3-9-2018-904-3', '', 'inherit', 'open', 'closed', '', '3-9-2018-904-3', '', '', '2018-09-05 18:48:44', '2018-09-05 21:48:44', '', 0, 'http://localhost/walldone-new/wp-content/uploads/2018/09/3-9-2018-904-3.png', 0, 'attachment', 'image/png', 0),
(222, 1, '2018-09-05 18:57:31', '2018-09-05 21:57:31', '', '3_9_2018_904_1.png', '', 'publish', 'closed', 'closed', '', '3_9_2018_904_1-png', '', '', '2018-09-05 18:57:31', '2018-09-05 21:57:31', '', 0, 'http://localhost/walldone-new/wpfm-files/3_9_2018_904_1-png/', 0, 'wpfm-files', '', 0),
(223, 1, '2018-09-05 18:57:31', '2018-09-05 21:57:31', '', '3_9_2018_904_1.png', '', 'inherit', 'open', 'closed', '', '3_9_2018_904_1-png-2', '', '', '2018-09-05 18:57:31', '2018-09-05 21:57:31', '', 222, 'http://localhost/walldone-new/wp-content/uploads/user_uploads/agencia904/3_9_2018_904_1.png', 0, 'attachment', 'image/png', 0),
(224, 1, '2018-09-05 18:57:31', '2018-09-05 21:57:31', '', 'undefined', '', 'publish', 'closed', 'closed', '', 'undefined', '', '', '2018-09-05 18:57:31', '2018-09-05 21:57:31', '', 0, 'http://localhost/walldone-new/wpfm-files/undefined/', 0, 'wpfm-files', '', 0),
(225, 1, '2018-09-05 19:00:24', '2018-09-05 22:00:24', '', 'Captura-de-Tela-2018-09-05-às-10.00.12-2', '', 'inherit', 'open', 'closed', '', 'captura-de-tela-2018-09-05-as-10-00-12-2', '', '', '2018-09-05 19:00:24', '2018-09-05 22:00:24', '', 0, 'http://localhost/walldone-new/wp-content/uploads/2018/09/Captura-de-Tela-2018-09-05-às-10.00.12-2.png', 0, 'attachment', 'image/png', 0),
(226, 1, '2018-09-05 19:00:24', '2018-09-05 22:00:24', '', '3-9-2018-904-4', '', 'inherit', 'open', 'closed', '', '3-9-2018-904-4', '', '', '2018-09-05 19:00:24', '2018-09-05 22:00:24', '', 0, 'http://localhost/walldone-new/wp-content/uploads/2018/09/3-9-2018-904-4.png', 0, 'attachment', 'image/png', 0);
INSERT INTO `wd_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(227, 1, '2018-09-05 19:01:00', '2018-09-05 22:01:00', '', 'Captura-de-Tela-2018-09-05-às-10.00.12-3', '', 'inherit', 'open', 'closed', '', 'captura-de-tela-2018-09-05-as-10-00-12-3', '', '', '2018-09-05 19:01:00', '2018-09-05 22:01:00', '', 0, 'http://localhost/walldone-new/wp-content/uploads/2018/09/Captura-de-Tela-2018-09-05-às-10.00.12-3.png', 0, 'attachment', 'image/png', 0),
(228, 1, '2018-09-05 19:01:00', '2018-09-05 22:01:00', '', '3-9-2018-904-5', '', 'inherit', 'open', 'closed', '', '3-9-2018-904-5', '', '', '2018-09-05 19:01:00', '2018-09-05 22:01:00', '', 0, 'http://localhost/walldone-new/wp-content/uploads/2018/09/3-9-2018-904-5.png', 0, 'attachment', 'image/png', 0),
(229, 1, '2018-09-05 19:01:11', '2018-09-05 22:01:11', '', 'Captura-de-Tela-2018-09-05-às-10.00.12-4', '', 'inherit', 'open', 'closed', '', 'captura-de-tela-2018-09-05-as-10-00-12-4', '', '', '2018-09-05 19:01:11', '2018-09-05 22:01:11', '', 0, 'http://localhost/walldone-new/wp-content/uploads/2018/09/Captura-de-Tela-2018-09-05-às-10.00.12-4.png', 0, 'attachment', 'image/png', 0),
(230, 1, '2018-09-05 19:01:11', '2018-09-05 22:01:11', '', '3-9-2018-904-6', '', 'inherit', 'open', 'closed', '', '3-9-2018-904-6', '', '', '2018-09-05 19:01:11', '2018-09-05 22:01:11', '', 0, 'http://localhost/walldone-new/wp-content/uploads/2018/09/3-9-2018-904-6.png', 0, 'attachment', 'image/png', 0),
(231, 1, '2018-09-05 19:02:24', '2018-09-05 22:02:24', '', '3-9-2018-904-7', '', 'inherit', 'open', 'closed', '', '3-9-2018-904-7', '', '', '2018-09-05 19:02:24', '2018-09-05 22:02:24', '', 0, 'http://localhost/walldone-new/wp-content/uploads/2018/09/3-9-2018-904-7.png', 0, 'attachment', 'image/png', 0),
(232, 1, '2018-09-05 19:02:24', '2018-09-05 22:02:24', '', 'Captura-de-Tela-2018-09-05-às-10.00.12-5', '', 'inherit', 'open', 'closed', '', 'captura-de-tela-2018-09-05-as-10-00-12-5', '', '', '2018-09-05 19:02:24', '2018-09-05 22:02:24', '', 0, 'http://localhost/walldone-new/wp-content/uploads/2018/09/Captura-de-Tela-2018-09-05-às-10.00.12-5.png', 0, 'attachment', 'image/png', 0),
(233, 1, '2018-09-05 19:02:47', '2018-09-05 22:02:47', '', '3-9-2018-904-8', '', 'inherit', 'open', 'closed', '', '3-9-2018-904-8', '', '', '2018-09-05 19:02:47', '2018-09-05 22:02:47', '', 0, 'http://localhost/walldone-new/wp-content/uploads/2018/09/3-9-2018-904-8.png', 0, 'attachment', 'image/png', 0),
(234, 1, '2018-09-05 19:06:04', '2018-09-05 22:06:04', '', '3-9-2018-904-9', '', 'inherit', 'open', 'closed', '', '3-9-2018-904-9', '', '', '2018-09-05 19:06:04', '2018-09-05 22:06:04', '', 0, 'http://localhost/walldone-new/wp-content/uploads/2018/09/3-9-2018-904-9.png', 0, 'attachment', 'image/png', 0),
(235, 1, '2018-09-05 19:06:04', '2018-09-05 22:06:04', '', 'Captura-de-Tela-2018-09-05-às-10.00.12-6', '', 'inherit', 'open', 'closed', '', 'captura-de-tela-2018-09-05-as-10-00-12-6', '', '', '2018-09-05 19:06:04', '2018-09-05 22:06:04', '', 0, 'http://localhost/walldone-new/wp-content/uploads/2018/09/Captura-de-Tela-2018-09-05-às-10.00.12-6.png', 0, 'attachment', 'image/png', 0),
(236, 1, '2018-09-05 19:07:52', '2018-09-05 22:07:52', '', '3-9-2018-904-10', '', 'inherit', 'open', 'closed', '', '3-9-2018-904-10', '', '', '2018-09-05 19:07:52', '2018-09-05 22:07:52', '', 0, 'http://localhost/walldone-new/wp-content/uploads/2018/09/3-9-2018-904-10.png', 0, 'attachment', 'image/png', 0),
(237, 1, '2018-09-05 19:07:52', '2018-09-05 22:07:52', '', 'Captura-de-Tela-2018-09-05-às-10.00.12-7', '', 'inherit', 'open', 'closed', '', 'captura-de-tela-2018-09-05-as-10-00-12-7', '', '', '2018-09-05 19:07:52', '2018-09-05 22:07:52', '', 0, 'http://localhost/walldone-new/wp-content/uploads/2018/09/Captura-de-Tela-2018-09-05-às-10.00.12-7.png', 0, 'attachment', 'image/png', 0),
(238, 1, '2018-09-05 19:08:50', '2018-09-05 22:08:50', '', 'Captura-de-Tela-2018-09-05-às-10.00.12-8', '', 'inherit', 'open', 'closed', '', 'captura-de-tela-2018-09-05-as-10-00-12-8', '', '', '2018-09-05 19:08:50', '2018-09-05 22:08:50', '', 0, 'http://localhost/walldone-new/wp-content/uploads/2018/09/Captura-de-Tela-2018-09-05-às-10.00.12-8.png', 0, 'attachment', 'image/png', 0),
(239, 1, '2018-09-05 19:08:50', '2018-09-05 22:08:50', '', '3-9-2018-904-11', '', 'inherit', 'open', 'closed', '', '3-9-2018-904-11', '', '', '2018-09-05 19:08:50', '2018-09-05 22:08:50', '', 0, 'http://localhost/walldone-new/wp-content/uploads/2018/09/3-9-2018-904-11.png', 0, 'attachment', 'image/png', 0),
(242, 1, '2018-09-19 16:05:42', '2018-09-19 19:05:42', '', 'Kit', '', 'publish', 'open', 'closed', '', 'kit', '', '', '2018-09-26 15:37:53', '2018-09-26 18:37:53', '', 0, 'http://walldone/?post_type=product&#038;p=242', 0, 'product', '', 0),
(243, 1, '2018-09-19 21:59:06', '2018-09-20 00:59:06', '[product sku="produto-kit"]', 'briefing do projeto', '', 'publish', 'closed', 'closed', '', 'briefing-do-projeto', '', '', '2018-09-21 14:23:09', '2018-09-21 17:23:09', '', 0, 'http://walldone/?page_id=243', 0, 'page', '', 0),
(244, 1, '2018-09-19 21:59:06', '2018-09-20 00:59:06', '[product sku="produto-kit"]', 'briefing do projeto', '', 'inherit', 'closed', 'closed', '', '243-revision-v1', '', '', '2018-09-19 21:59:06', '2018-09-20 00:59:06', '', 243, 'http://walldone/243-revision-v1/', 0, 'revision', '', 0),
(245, 1, '2018-09-19 22:19:49', '2018-09-20 01:19:49', '', 'imagem_teste3', '', 'inherit', 'open', 'closed', '', 'imagem_teste3', '', '', '2018-09-19 22:19:49', '2018-09-20 01:19:49', '', 0, 'http://walldone/wp-content/uploads/2018/09/imagem_teste3.jpg', 0, 'attachment', 'image/jpeg', 0),
(246, 1, '2018-09-19 22:19:49', '2018-09-20 01:19:49', '', 'imagem_teste2', '', 'inherit', 'open', 'closed', '', 'imagem_teste2', '', '', '2018-09-19 22:19:49', '2018-09-20 01:19:49', '', 0, 'http://walldone/wp-content/uploads/2018/09/imagem_teste2.jpg', 0, 'attachment', 'image/jpeg', 0),
(247, 1, '2018-09-19 22:19:51', '2018-09-20 01:19:51', '', 'imagem_teste4', '', 'inherit', 'open', 'closed', '', 'imagem_teste4', '', '', '2018-09-19 22:19:51', '2018-09-20 01:19:51', '', 0, 'http://walldone/wp-content/uploads/2018/09/imagem_teste4.jpg', 0, 'attachment', 'image/jpeg', 0),
(248, 1, '2018-09-20 11:42:52', '2018-09-20 14:42:52', '', 'Produtos do kit', '', 'publish', 'closed', 'closed', '', 'produtos-do-kit', '', '', '2018-09-20 12:24:53', '2018-09-20 15:24:53', '', 0, 'http://walldone/?page_id=248', 0, 'page', '', 0),
(249, 1, '2018-09-20 11:42:52', '2018-09-20 14:42:52', '', 'Produtos do kit', '', 'inherit', 'closed', 'closed', '', '248-revision-v1', '', '', '2018-09-20 11:42:52', '2018-09-20 14:42:52', '', 248, 'http://walldone/248-revision-v1/', 0, 'revision', '', 0),
(250, 1, '2018-09-20 11:53:25', '2018-09-20 14:53:25', '[ti_wishlistsview]', 'Lista de Desejos', '', 'publish', 'closed', 'closed', '', 'wishlist', '', '', '2018-09-20 11:53:25', '2018-09-20 14:53:25', '', 0, 'http://walldone/?page_id=250', 0, 'page', '', 0),
(251, 1, '2018-09-20 11:53:25', '2018-09-20 14:53:25', '[ti_wishlistsview]', 'Lista de Desejos', '', 'inherit', 'closed', 'closed', '', '250-revision-v1', '', '', '2018-09-20 11:53:25', '2018-09-20 14:53:25', '', 250, 'http://walldone/250-revision-v1/', 0, 'revision', '', 0),
(252, 1, '2018-09-20 17:40:32', '2018-09-20 20:40:32', '', 'Upload images', '', 'trash', 'closed', 'closed', '', 'acf_upload-images__trashed', '', '', '2018-09-20 18:03:36', '2018-09-20 21:03:36', '', 0, 'http://walldone/?post_type=acf&#038;p=252', 0, 'acf', '', 0),
(253, 1, '2018-09-20 18:05:32', '2018-09-20 21:05:32', '', 'imagem_teste2-1', '', 'inherit', 'open', 'closed', '', 'imagem_teste2-1', '', '', '2018-09-20 18:05:32', '2018-09-20 21:05:32', '', 0, 'http://walldone/wp-content/uploads/2018/09/imagem_teste2-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(254, 1, '2018-09-20 18:05:32', '2018-09-20 21:05:32', '', 'imagem_teste', '', 'inherit', 'open', 'closed', '', 'imagem_teste', '', '', '2018-09-20 18:05:32', '2018-09-20 21:05:32', '', 0, 'http://walldone/wp-content/uploads/2018/09/imagem_teste.jpg', 0, 'attachment', 'image/jpeg', 0),
(255, 1, '2018-09-20 18:05:33', '2018-09-20 21:05:33', '', 'imagem_teste3-1', '', 'inherit', 'open', 'closed', '', 'imagem_teste3-1', '', '', '2018-09-20 18:05:33', '2018-09-20 21:05:33', '', 0, 'http://walldone/wp-content/uploads/2018/09/imagem_teste3-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(256, 1, '2018-09-20 18:05:33', '2018-09-20 21:05:33', '', 'imagem_teste4-1', '', 'inherit', 'open', 'closed', '', 'imagem_teste4-1', '', '', '2018-09-20 18:05:33', '2018-09-20 21:05:33', '', 0, 'http://walldone/wp-content/uploads/2018/09/imagem_teste4-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(257, 1, '2018-09-20 18:33:49', '2018-09-20 21:33:49', '', 'imagem_full', '', 'inherit', 'open', 'closed', '', 'imagem_full', '', '', '2018-09-28 08:29:09', '2018-09-28 11:29:09', '', 0, 'http://walldone/wp-content/uploads/2018/09/imagem_full.jpg', 0, 'attachment', 'image/jpeg', 0),
(258, 1, '2018-09-20 18:33:49', '2018-09-20 21:33:49', '', 'imagem_teste-1', '', 'inherit', 'open', 'closed', '', 'imagem_teste-1', '', '', '2018-09-28 08:54:51', '2018-09-28 11:54:51', '', 0, 'http://walldone/wp-content/uploads/2018/09/imagem_teste-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(259, 1, '2018-09-20 18:33:51', '2018-09-20 21:33:51', '', 'imagem_teste2-2', '', 'inherit', 'open', 'closed', '', 'imagem_teste2-2', '', '', '2018-09-28 08:28:11', '2018-09-28 11:28:11', '', 0, 'http://walldone/wp-content/uploads/2018/09/imagem_teste2-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(260, 1, '2018-09-20 18:33:52', '2018-09-20 21:33:52', '', 'imagem_teste3-2', '', 'inherit', 'open', 'closed', '', 'imagem_teste3-2', '', '', '2018-09-28 08:27:51', '2018-09-28 11:27:51', '', 0, 'http://walldone/wp-content/uploads/2018/09/imagem_teste3-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(261, 1, '2018-09-20 18:33:53', '2018-09-20 21:33:53', '', 'imagem_teste4-2', '', 'inherit', 'open', 'closed', '', 'imagem_teste4-2', '', '', '2018-09-20 18:33:53', '2018-09-20 21:33:53', '', 0, 'http://walldone/wp-content/uploads/2018/09/imagem_teste4-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(262, 1, '2018-09-20 18:38:06', '2018-09-20 21:38:06', '', 'shipping-fields', '', 'publish', 'closed', 'closed', '', 'shipping-fields', '', '', '2018-09-20 18:38:06', '2018-09-20 21:38:06', '', 0, 'http://walldone/?wcccf=shipping-fields', 0, 'wcccf', '', 0),
(263, 1, '2018-09-20 18:38:06', '2018-09-20 21:38:06', '', 'billing-fields', '', 'publish', 'closed', 'closed', '', 'billing-fields', '', '', '2018-09-20 18:38:06', '2018-09-20 21:38:06', '', 0, 'http://walldone/?wcccf=billing-fields', 0, 'wcccf', '', 0),
(264, 1, '2018-09-20 18:38:06', '2018-09-20 21:38:06', '', 'custom-fields', '', 'publish', 'closed', 'closed', '', 'custom-fields', '', '', '2018-09-20 18:38:06', '2018-09-20 21:38:06', '', 0, 'http://walldone/?wcccf=custom-fields', 0, 'wcccf', '', 0),
(265, 1, '2018-09-20 18:42:14', '2018-09-20 21:42:14', '', 'Produto KIT', '', 'publish', 'closed', 'closed', '', '265-2', '', '', '2018-10-02 16:37:26', '2018-10-02 19:37:26', '', 0, 'http://walldone/?post_type=wccpf&#038;p=265', 0, 'wccpf', '', 0),
(267, 1, '2018-09-21 13:12:21', '2018-09-21 16:12:21', '', 'Produto KIT', '', 'inherit', 'closed', 'closed', '', '265-autosave-v1', '', '', '2018-09-21 13:12:21', '2018-09-21 16:12:21', '', 265, 'http://walldone/265-autosave-v1/', 0, 'revision', '', 0),
(268, 1, '2018-09-26 15:42:08', '0000-00-00 00:00:00', '', 'Início', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-09-26 15:42:08', '0000-00-00 00:00:00', '', 0, 'http://walldone/?p=268', 1, 'nav_menu_item', '', 0),
(269, 1, '2018-09-26 15:42:08', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-09-26 15:42:08', '0000-00-00 00:00:00', '', 0, 'http://walldone/?p=269', 1, 'nav_menu_item', '', 0),
(270, 1, '2018-09-26 15:42:08', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-09-26 15:42:08', '0000-00-00 00:00:00', '', 0, 'http://walldone/?p=270', 1, 'nav_menu_item', '', 0),
(271, 1, '2018-09-26 15:42:08', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-09-26 15:42:08', '0000-00-00 00:00:00', '', 0, 'http://walldone/?p=271', 1, 'nav_menu_item', '', 0),
(272, 1, '2018-09-26 15:42:08', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-09-26 15:42:08', '0000-00-00 00:00:00', '', 0, 'http://walldone/?p=272', 1, 'nav_menu_item', '', 0),
(273, 1, '2018-09-26 15:42:08', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-09-26 15:42:08', '0000-00-00 00:00:00', '', 0, 'http://walldone/?p=273', 1, 'nav_menu_item', '', 0),
(274, 1, '2018-09-26 15:42:08', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-09-26 15:42:08', '0000-00-00 00:00:00', '', 0, 'http://walldone/?p=274', 1, 'nav_menu_item', '', 0),
(275, 1, '2018-09-26 15:42:08', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-09-26 15:42:08', '0000-00-00 00:00:00', '', 0, 'http://walldone/?p=275', 1, 'nav_menu_item', '', 0),
(276, 1, '2018-09-26 15:42:08', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-09-26 15:42:08', '0000-00-00 00:00:00', '', 164, 'http://walldone/?p=276', 1, 'nav_menu_item', '', 0),
(277, 1, '2018-09-26 15:42:08', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-09-26 15:42:08', '0000-00-00 00:00:00', '', 0, 'http://walldone/?p=277', 1, 'nav_menu_item', '', 0),
(278, 1, '2018-09-26 15:42:08', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-09-26 15:42:08', '0000-00-00 00:00:00', '', 0, 'http://walldone/?p=278', 1, 'nav_menu_item', '', 0),
(279, 1, '2018-09-26 15:42:08', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-09-26 15:42:08', '0000-00-00 00:00:00', '', 0, 'http://walldone/?p=279', 1, 'nav_menu_item', '', 0),
(280, 1, '2018-09-26 15:42:08', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-09-26 15:42:08', '0000-00-00 00:00:00', '', 0, 'http://walldone/?p=280', 1, 'nav_menu_item', '', 0),
(281, 1, '2018-09-26 16:05:02', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-09-26 16:05:02', '0000-00-00 00:00:00', '', 15, 'http://walldone/?p=281', 1, 'nav_menu_item', '', 0),
(282, 1, '2018-09-26 16:05:55', '2018-09-26 19:05:55', ' ', '', '', 'publish', 'closed', 'closed', '', '282', '', '', '2018-09-27 10:18:27', '2018-09-27 13:18:27', '', 15, 'http://walldone/?p=282', 1, 'nav_menu_item', '', 0),
(283, 1, '2018-09-26 16:05:55', '2018-09-26 19:05:55', ' ', '', '', 'publish', 'closed', 'closed', '', '283', '', '', '2018-09-27 10:18:27', '2018-09-27 13:18:27', '', 20, 'http://walldone/?p=283', 2, 'nav_menu_item', '', 0),
(284, 1, '2018-09-26 16:05:55', '2018-09-26 19:05:55', ' ', '', '', 'publish', 'closed', 'closed', '', '284', '', '', '2018-09-27 10:18:27', '2018-09-27 13:18:27', '', 20, 'http://walldone/?p=284', 3, 'nav_menu_item', '', 0),
(285, 1, '2018-09-26 16:05:55', '2018-09-26 19:05:55', ' ', '', '', 'publish', 'closed', 'closed', '', '285', '', '', '2018-09-27 10:18:27', '2018-09-27 13:18:27', '', 20, 'http://walldone/?p=285', 4, 'nav_menu_item', '', 0),
(286, 1, '2018-09-26 16:05:55', '2018-09-26 19:05:55', ' ', '', '', 'publish', 'closed', 'closed', '', '286', '', '', '2018-09-27 10:18:27', '2018-09-27 13:18:27', '', 20, 'http://walldone/?p=286', 5, 'nav_menu_item', '', 0),
(287, 1, '2018-09-26 16:05:55', '2018-09-26 19:05:55', ' ', '', '', 'publish', 'closed', 'closed', '', '287', '', '', '2018-09-27 10:18:27', '2018-09-27 13:18:27', '', 20, 'http://walldone/?p=287', 6, 'nav_menu_item', '', 0),
(288, 1, '2018-09-26 16:05:55', '2018-09-26 19:05:55', ' ', '', '', 'publish', 'closed', 'closed', '', '288', '', '', '2018-09-27 10:18:27', '2018-09-27 13:18:27', '', 20, 'http://walldone/?p=288', 7, 'nav_menu_item', '', 0),
(289, 1, '2018-09-26 16:05:55', '2018-09-26 19:05:55', ' ', '', '', 'publish', 'closed', 'closed', '', '289', '', '', '2018-09-27 10:18:27', '2018-09-27 13:18:27', '', 20, 'http://walldone/?p=289', 8, 'nav_menu_item', '', 0),
(290, 1, '2018-09-27 10:06:30', '2018-09-27 13:06:30', ' ', '', '', 'publish', 'closed', 'closed', '', '290', '', '', '2018-09-27 10:18:28', '2018-09-27 13:18:28', '', 15, 'http://walldone/?p=290', 9, 'nav_menu_item', '', 0),
(291, 1, '2018-09-27 10:06:30', '2018-09-27 13:06:30', ' ', '', '', 'publish', 'closed', 'closed', '', '291', '', '', '2018-09-27 10:18:28', '2018-09-27 13:18:28', '', 28, 'http://walldone/?p=291', 10, 'nav_menu_item', '', 0),
(292, 1, '2018-09-27 10:06:30', '2018-09-27 13:06:30', ' ', '', '', 'publish', 'closed', 'closed', '', '292', '', '', '2018-09-27 10:18:28', '2018-09-27 13:18:28', '', 28, 'http://walldone/?p=292', 11, 'nav_menu_item', '', 0),
(293, 1, '2018-09-27 10:06:30', '2018-09-27 13:06:30', ' ', '', '', 'publish', 'closed', 'closed', '', '293', '', '', '2018-09-27 10:18:28', '2018-09-27 13:18:28', '', 28, 'http://walldone/?p=293', 12, 'nav_menu_item', '', 0),
(294, 1, '2018-09-27 10:06:30', '2018-09-27 13:06:30', ' ', '', '', 'publish', 'closed', 'closed', '', '294', '', '', '2018-09-27 10:18:28', '2018-09-27 13:18:28', '', 28, 'http://walldone/?p=294', 13, 'nav_menu_item', '', 0),
(295, 1, '2018-09-27 10:06:30', '2018-09-27 13:06:30', ' ', '', '', 'publish', 'closed', 'closed', '', '295', '', '', '2018-09-27 10:18:28', '2018-09-27 13:18:28', '', 28, 'http://walldone/?p=295', 14, 'nav_menu_item', '', 0),
(296, 1, '2018-09-27 10:06:30', '2018-09-27 13:06:30', ' ', '', '', 'publish', 'closed', 'closed', '', '296', '', '', '2018-09-27 10:18:28', '2018-09-27 13:18:28', '', 28, 'http://walldone/?p=296', 15, 'nav_menu_item', '', 0),
(297, 1, '2018-09-27 10:06:30', '2018-09-27 13:06:30', ' ', '', '', 'publish', 'closed', 'closed', '', '297', '', '', '2018-09-27 10:18:28', '2018-09-27 13:18:28', '', 28, 'http://walldone/?p=297', 16, 'nav_menu_item', '', 0),
(298, 1, '2018-09-27 10:09:23', '2018-09-27 13:09:23', ' ', '', '', 'publish', 'closed', 'closed', '', '298', '', '', '2018-09-27 10:18:28', '2018-09-27 13:18:28', '', 15, 'http://walldone/?p=298', 17, 'nav_menu_item', '', 0),
(299, 1, '2018-09-27 10:09:23', '2018-09-27 13:09:23', ' ', '', '', 'publish', 'closed', 'closed', '', '299', '', '', '2018-09-27 10:18:28', '2018-09-27 13:18:28', '', 36, 'http://walldone/?p=299', 18, 'nav_menu_item', '', 0),
(300, 1, '2018-09-27 10:09:23', '2018-09-27 13:09:23', ' ', '', '', 'publish', 'closed', 'closed', '', '300', '', '', '2018-09-27 10:18:28', '2018-09-27 13:18:28', '', 36, 'http://walldone/?p=300', 19, 'nav_menu_item', '', 0),
(301, 1, '2018-09-27 10:09:23', '2018-09-27 13:09:23', ' ', '', '', 'publish', 'closed', 'closed', '', '301', '', '', '2018-09-27 10:18:28', '2018-09-27 13:18:28', '', 36, 'http://walldone/?p=301', 20, 'nav_menu_item', '', 0),
(302, 1, '2018-09-27 10:09:23', '2018-09-27 13:09:23', ' ', '', '', 'publish', 'closed', 'closed', '', '302', '', '', '2018-09-27 10:18:28', '2018-09-27 13:18:28', '', 36, 'http://walldone/?p=302', 21, 'nav_menu_item', '', 0),
(303, 1, '2018-09-27 10:18:28', '2018-09-27 13:18:28', 'Lorem ipsum dolor sit amet, consectetur adipiscing Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut.', '', '', 'publish', 'closed', 'closed', '', '303', '', '', '2018-09-27 10:18:28', '2018-09-27 13:18:28', '', 0, 'http://walldone/?p=303', 22, 'nav_menu_item', '', 0),
(304, 1, '2018-09-27 10:18:28', '2018-09-27 13:18:28', '', 'Adesivos', '', 'publish', 'closed', 'closed', '', 'adesivos', '', '', '2018-09-27 10:18:28', '2018-09-27 13:18:28', '', 0, 'http://walldone/?p=304', 23, 'nav_menu_item', '', 0),
(305, 1, '2018-09-27 10:18:28', '2018-09-27 13:18:28', '', 'Aromatizador de Ambientes', '', 'publish', 'closed', 'closed', '', 'aromatizador-de-ambientes', '', '', '2018-09-27 10:18:28', '2018-09-27 13:18:28', '', 0, 'http://walldone/?p=305', 24, 'nav_menu_item', '', 0),
(306, 1, '2018-09-27 10:18:28', '2018-09-27 13:18:28', '', 'Espelhos', '', 'publish', 'closed', 'closed', '', 'espelhos', '', '', '2018-09-27 10:18:28', '2018-09-27 13:18:28', '', 0, 'http://walldone/?p=306', 25, 'nav_menu_item', '', 0),
(307, 1, '2018-09-27 10:18:28', '2018-09-27 13:18:28', '', 'Flâmulas', '', 'publish', 'closed', 'closed', '', 'flamulas', '', '', '2018-09-27 10:18:28', '2018-09-27 13:18:28', '', 0, 'http://walldone/?p=307', 26, 'nav_menu_item', '', 0),
(308, 1, '2018-09-27 10:18:28', '2018-09-27 13:18:28', '', 'Letreiros', '', 'publish', 'closed', 'closed', '', 'letreiros', '', '', '2018-09-27 10:18:28', '2018-09-27 13:18:28', '', 0, 'http://walldone/?p=308', 27, 'nav_menu_item', '', 0),
(309, 1, '2018-09-27 10:18:28', '2018-09-27 13:18:28', '', 'Penduradores', '', 'publish', 'closed', 'closed', '', 'penduradores', '', '', '2018-09-27 10:18:28', '2018-09-27 13:18:28', '', 0, 'http://walldone/?p=309', 28, 'nav_menu_item', '', 0),
(310, 1, '2018-09-27 10:18:28', '2018-09-27 13:18:28', '', 'Plaquinhas', '', 'publish', 'closed', 'closed', '', 'plaquinhas', '', '', '2018-09-27 10:18:28', '2018-09-27 13:18:28', '', 0, 'http://walldone/?p=310', 29, 'nav_menu_item', '', 0),
(311, 1, '2018-09-27 10:18:28', '2018-09-27 13:18:28', '', 'Prateleiras', '', 'publish', 'closed', 'closed', '', 'prateleiras', '', '', '2018-09-27 10:18:28', '2018-09-27 13:18:28', '', 0, 'http://walldone/?p=311', 30, 'nav_menu_item', '', 0),
(312, 1, '2018-09-27 10:18:28', '2018-09-27 13:18:28', '', 'Quadros', '', 'publish', 'closed', 'closed', '', 'quadros', '', '', '2018-09-27 10:18:28', '2018-09-27 13:18:28', '', 0, 'http://walldone/?p=312', 31, 'nav_menu_item', '', 0),
(313, 1, '2018-09-27 10:18:28', '2018-09-27 13:18:28', '', 'Quadros Luminosos', '', 'publish', 'closed', 'closed', '', 'quadros-luminosos', '', '', '2018-09-27 10:18:28', '2018-09-27 13:18:28', '', 0, 'http://walldone/?p=313', 32, 'nav_menu_item', '', 0),
(314, 1, '2018-09-27 10:18:28', '2018-09-27 13:18:28', '', 'Relógios', '', 'publish', 'closed', 'closed', '', 'relogios', '', '', '2018-09-27 10:18:28', '2018-09-27 13:18:28', '', 0, 'http://walldone/?p=314', 33, 'nav_menu_item', '', 0),
(315, 1, '2018-09-27 10:18:28', '2018-09-27 13:18:28', '', 'Vasinhos', '', 'publish', 'closed', 'closed', '', 'vasinhos', '', '', '2018-09-27 10:18:28', '2018-09-27 13:18:28', '', 0, 'http://walldone/?p=315', 34, 'nav_menu_item', '', 0),
(316, 1, '2018-09-27 12:12:54', '2018-09-27 15:12:54', '', '<i class="fal fa-comment-alt-lines"></i> Blog da Larissa', '', 'publish', 'closed', 'closed', '', 'blog-da-larissa', '', '', '2018-09-27 14:40:53', '2018-09-27 17:40:53', '', 0, 'http://walldone/?p=316', 1, 'nav_menu_item', '', 0),
(317, 1, '2018-09-27 12:13:40', '2018-09-27 15:13:40', ' ', '', '', 'publish', 'closed', 'closed', '', '317', '', '', '2018-09-27 14:40:53', '2018-09-27 17:40:53', '', 15, 'http://walldone/?p=317', 2, 'nav_menu_item', '', 0),
(318, 1, '2018-09-27 12:13:40', '2018-09-27 15:13:40', ' ', '', '', 'publish', 'closed', 'closed', '', '318', '', '', '2018-09-27 14:40:53', '2018-09-27 17:40:53', '', 20, 'http://walldone/?p=318', 3, 'nav_menu_item', '', 0),
(319, 1, '2018-09-27 12:13:40', '2018-09-27 15:13:40', ' ', '', '', 'publish', 'closed', 'closed', '', '319', '', '', '2018-09-27 14:40:53', '2018-09-27 17:40:53', '', 20, 'http://walldone/?p=319', 4, 'nav_menu_item', '', 0),
(320, 1, '2018-09-27 12:13:40', '2018-09-27 15:13:40', ' ', '', '', 'publish', 'closed', 'closed', '', '320', '', '', '2018-09-27 14:40:53', '2018-09-27 17:40:53', '', 20, 'http://walldone/?p=320', 5, 'nav_menu_item', '', 0),
(321, 1, '2018-09-27 12:13:40', '2018-09-27 15:13:40', ' ', '', '', 'publish', 'closed', 'closed', '', '321', '', '', '2018-09-27 14:40:53', '2018-09-27 17:40:53', '', 20, 'http://walldone/?p=321', 6, 'nav_menu_item', '', 0),
(322, 1, '2018-09-27 12:13:40', '2018-09-27 15:13:40', ' ', '', '', 'publish', 'closed', 'closed', '', '322', '', '', '2018-09-27 14:40:53', '2018-09-27 17:40:53', '', 20, 'http://walldone/?p=322', 7, 'nav_menu_item', '', 0),
(323, 1, '2018-09-27 12:13:40', '2018-09-27 15:13:40', ' ', '', '', 'publish', 'closed', 'closed', '', '323', '', '', '2018-09-27 14:40:53', '2018-09-27 17:40:53', '', 20, 'http://walldone/?p=323', 8, 'nav_menu_item', '', 0),
(324, 1, '2018-09-27 12:13:40', '2018-09-27 15:13:40', ' ', '', '', 'publish', 'closed', 'closed', '', '324', '', '', '2018-09-27 14:40:53', '2018-09-27 17:40:53', '', 20, 'http://walldone/?p=324', 9, 'nav_menu_item', '', 0),
(325, 1, '2018-09-27 14:34:20', '2018-09-27 17:34:20', ' ', '', '', 'publish', 'closed', 'closed', '', '325', '', '', '2018-09-27 14:40:53', '2018-09-27 17:40:53', '', 15, 'http://walldone/?p=325', 10, 'nav_menu_item', '', 0),
(326, 1, '2018-09-27 14:34:20', '2018-09-27 17:34:20', ' ', '', '', 'publish', 'closed', 'closed', '', '326', '', '', '2018-09-27 14:40:53', '2018-09-27 17:40:53', '', 28, 'http://walldone/?p=326', 11, 'nav_menu_item', '', 0),
(327, 1, '2018-09-27 14:34:20', '2018-09-27 17:34:20', ' ', '', '', 'publish', 'closed', 'closed', '', '327', '', '', '2018-09-27 14:40:53', '2018-09-27 17:40:53', '', 28, 'http://walldone/?p=327', 12, 'nav_menu_item', '', 0),
(328, 1, '2018-09-27 14:34:20', '2018-09-27 17:34:20', ' ', '', '', 'publish', 'closed', 'closed', '', '328', '', '', '2018-09-27 14:40:53', '2018-09-27 17:40:53', '', 28, 'http://walldone/?p=328', 13, 'nav_menu_item', '', 0),
(329, 1, '2018-09-27 14:34:20', '2018-09-27 17:34:20', ' ', '', '', 'publish', 'closed', 'closed', '', '329', '', '', '2018-09-27 14:40:53', '2018-09-27 17:40:53', '', 28, 'http://walldone/?p=329', 14, 'nav_menu_item', '', 0),
(330, 1, '2018-09-27 14:34:20', '2018-09-27 17:34:20', ' ', '', '', 'publish', 'closed', 'closed', '', '330', '', '', '2018-09-27 14:40:53', '2018-09-27 17:40:53', '', 28, 'http://walldone/?p=330', 15, 'nav_menu_item', '', 0),
(331, 1, '2018-09-27 14:34:20', '2018-09-27 17:34:20', ' ', '', '', 'publish', 'closed', 'closed', '', '331', '', '', '2018-09-27 14:40:53', '2018-09-27 17:40:53', '', 28, 'http://walldone/?p=331', 16, 'nav_menu_item', '', 0),
(332, 1, '2018-09-27 14:34:21', '2018-09-27 17:34:21', ' ', '', '', 'publish', 'closed', 'closed', '', '332', '', '', '2018-09-27 14:40:53', '2018-09-27 17:40:53', '', 28, 'http://walldone/?p=332', 17, 'nav_menu_item', '', 0),
(333, 1, '2018-09-27 14:34:21', '2018-09-27 17:34:21', ' ', '', '', 'publish', 'closed', 'closed', '', '333', '', '', '2018-09-27 14:40:53', '2018-09-27 17:40:53', '', 15, 'http://walldone/?p=333', 18, 'nav_menu_item', '', 0),
(334, 1, '2018-09-27 14:34:21', '2018-09-27 17:34:21', ' ', '', '', 'publish', 'closed', 'closed', '', '334', '', '', '2018-09-27 14:40:53', '2018-09-27 17:40:53', '', 36, 'http://walldone/?p=334', 19, 'nav_menu_item', '', 0),
(335, 1, '2018-09-27 14:34:21', '2018-09-27 17:34:21', ' ', '', '', 'publish', 'closed', 'closed', '', '335', '', '', '2018-09-27 14:40:53', '2018-09-27 17:40:53', '', 36, 'http://walldone/?p=335', 20, 'nav_menu_item', '', 0),
(336, 1, '2018-09-27 14:34:21', '2018-09-27 17:34:21', ' ', '', '', 'publish', 'closed', 'closed', '', '336', '', '', '2018-09-27 14:40:53', '2018-09-27 17:40:53', '', 36, 'http://walldone/?p=336', 21, 'nav_menu_item', '', 0),
(337, 1, '2018-09-27 14:34:21', '2018-09-27 17:34:21', ' ', '', '', 'publish', 'closed', 'closed', '', '337', '', '', '2018-09-27 14:40:53', '2018-09-27 17:40:53', '', 36, 'http://walldone/?p=337', 22, 'nav_menu_item', '', 0),
(338, 1, '2018-09-27 14:34:21', '2018-09-27 17:34:21', 'Lorem ipsum dolor sit amet, consectetur adipiscing Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut.', '', '', 'publish', 'closed', 'closed', '', '338', '', '', '2018-09-27 14:40:53', '2018-09-27 17:40:53', '', 0, 'http://walldone/?p=338', 23, 'nav_menu_item', '', 0),
(339, 1, '2018-09-27 14:34:21', '2018-09-27 17:34:21', '', 'Adesivos', '', 'publish', 'closed', 'closed', '', 'adesivos-2', '', '', '2018-09-27 14:40:53', '2018-09-27 17:40:53', '', 0, 'http://walldone/?p=339', 24, 'nav_menu_item', '', 0),
(340, 1, '2018-09-27 14:34:21', '2018-09-27 17:34:21', '', 'Aromatizador de Ambientes', '', 'publish', 'closed', 'closed', '', 'aromatizador-de-ambientes-2', '', '', '2018-09-27 14:40:53', '2018-09-27 17:40:53', '', 0, 'http://walldone/?p=340', 25, 'nav_menu_item', '', 0),
(341, 1, '2018-09-27 14:34:21', '2018-09-27 17:34:21', '', 'Espelhos', '', 'publish', 'closed', 'closed', '', 'espelhos-2', '', '', '2018-09-27 14:40:53', '2018-09-27 17:40:53', '', 0, 'http://walldone/?p=341', 26, 'nav_menu_item', '', 0),
(342, 1, '2018-09-27 14:34:21', '2018-09-27 17:34:21', '', 'Flâmulas', '', 'publish', 'closed', 'closed', '', 'flamulas-2', '', '', '2018-09-27 14:40:53', '2018-09-27 17:40:53', '', 0, 'http://walldone/?p=342', 27, 'nav_menu_item', '', 0),
(343, 1, '2018-09-27 14:34:21', '2018-09-27 17:34:21', '', 'Letreiros', '', 'publish', 'closed', 'closed', '', 'letreiros-2', '', '', '2018-09-27 14:40:53', '2018-09-27 17:40:53', '', 0, 'http://walldone/?p=343', 28, 'nav_menu_item', '', 0),
(344, 1, '2018-09-27 14:34:21', '2018-09-27 17:34:21', '', 'Penduradores', '', 'publish', 'closed', 'closed', '', 'penduradores-2', '', '', '2018-09-27 14:40:53', '2018-09-27 17:40:53', '', 0, 'http://walldone/?p=344', 29, 'nav_menu_item', '', 0),
(345, 1, '2018-09-27 14:34:21', '2018-09-27 17:34:21', '', 'Plaquinhas', '', 'publish', 'closed', 'closed', '', 'plaquinhas-2', '', '', '2018-09-27 14:40:53', '2018-09-27 17:40:53', '', 0, 'http://walldone/?p=345', 30, 'nav_menu_item', '', 0),
(346, 1, '2018-09-27 14:34:21', '2018-09-27 17:34:21', '', 'Prateleiras', '', 'publish', 'closed', 'closed', '', 'prateleiras-2', '', '', '2018-09-27 14:40:53', '2018-09-27 17:40:53', '', 0, 'http://walldone/?p=346', 31, 'nav_menu_item', '', 0),
(347, 1, '2018-09-27 14:34:21', '2018-09-27 17:34:21', '', 'Quadros', '', 'publish', 'closed', 'closed', '', 'quadros-2', '', '', '2018-09-27 14:40:53', '2018-09-27 17:40:53', '', 0, 'http://walldone/?p=347', 32, 'nav_menu_item', '', 0),
(348, 1, '2018-09-27 14:34:21', '2018-09-27 17:34:21', '', 'Quadros Luminosos', '', 'publish', 'closed', 'closed', '', 'quadros-luminosos-2', '', '', '2018-09-27 14:40:54', '2018-09-27 17:40:54', '', 0, 'http://walldone/?p=348', 33, 'nav_menu_item', '', 0),
(349, 1, '2018-09-27 14:34:21', '2018-09-27 17:34:21', '', 'Relógios', '', 'publish', 'closed', 'closed', '', 'relogios-2', '', '', '2018-09-27 14:40:54', '2018-09-27 17:40:54', '', 0, 'http://walldone/?p=349', 34, 'nav_menu_item', '', 0),
(350, 1, '2018-09-27 14:34:21', '2018-09-27 17:34:21', '', 'Vasinhos', '', 'publish', 'closed', 'closed', '', 'vasinhos-2', '', '', '2018-09-27 14:40:54', '2018-09-27 17:40:54', '', 0, 'http://walldone/?p=350', 35, 'nav_menu_item', '', 0),
(351, 1, '2018-09-27 14:39:38', '2018-09-27 17:39:38', '', '<span><i class="fal fa-box-heart"></i> Meu Kit</span>', '', 'publish', 'closed', 'closed', '', 'meu-kit', '', '', '2018-09-27 14:40:54', '2018-09-27 17:40:54', '', 0, 'http://walldone/?p=351', 36, 'nav_menu_item', '', 0),
(352, 1, '2018-09-27 15:09:50', '2018-09-27 18:09:50', '', 'Campos de Produtos', '', 'publish', 'closed', 'closed', '', 'acf_campos-de-produtos', '', '', '2018-09-27 15:35:41', '2018-09-27 18:35:41', '', 0, 'http://walldone/?post_type=acf&#038;p=352', 0, 'acf', '', 0),
(353, 1, '2018-09-27 15:32:46', '2018-09-27 18:32:46', '', 'Order &ndash; setembro 27, 2018 @ 03:32 PM', '', 'wc-on-hold', 'open', 'closed', 'order_5bad224e48d46', 'pedido-27-de-sep-de-2018-as-183232', '', '', '2018-09-27 15:32:46', '2018-09-27 18:32:46', '', 0, 'http://walldone/?post_type=shop_order&#038;p=353', 0, 'shop_order', '', 1),
(355, 1, '2018-09-27 16:04:40', '2018-09-27 19:04:40', '', 'home-depoimentos-larissa-walldone', '', 'inherit', 'open', 'closed', '', 'home-depoimentos-larissa-walldone', '', '', '2018-09-27 16:04:40', '2018-09-27 19:04:40', '', 0, 'http://walldone/wp-content/uploads/2018/09/home-depoimentos-larissa-walldone.jpg', 0, 'attachment', 'image/jpeg', 0),
(356, 1, '2018-09-27 16:09:19', '2018-09-27 19:09:19', '"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus dolorum, laboriosam provident nihil, aliquid consequuntur quae! Vel dolor architecto deleniti, reiciendis, quaerat molestias veritatis voluptate, iste eos quas totam cum."', 'Lorem ipsum dolor sit amet', '', 'publish', 'open', 'closed', '', 'lorem-ipsum-dolor-sit-amet', '', '', '2018-09-27 16:10:59', '2018-09-27 19:10:59', '', 0, 'http://walldone/?post_type=depoimento&#038;p=356', 0, 'depoimento', '', 0),
(357, 1, '2018-09-27 16:17:02', '2018-09-27 19:17:02', '"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus dolorum, laboriosam provident nihil, aliquid consequuntur quae! Vel dolor architecto deleniti, reiciendis, quaerat molestias veritatis voluptate, iste eos quas totam cum."', 'Lorem ipsum dolor sit amet', '', 'publish', 'open', 'closed', '', 'lorem-ipsum-dolor-sit-amet-2', '', '', '2018-09-27 16:17:02', '2018-09-27 19:17:02', '', 0, 'http://walldone/?post_type=depoimento&#038;p=357', 0, 'depoimento', '', 0),
(358, 1, '2018-09-28 08:58:14', '2018-09-28 11:58:14', '', 'video_play', '', 'inherit', 'open', 'closed', '', 'video_play', '', '', '2018-09-28 08:58:14', '2018-09-28 11:58:14', '', 96, 'http://walldone/wp-content/uploads/2018/06/video_play.jpg', 0, 'attachment', 'image/jpeg', 0),
(359, 1, '2018-10-01 11:48:00', '2018-10-01 14:48:00', '', 'Categorias de produtos', '', 'publish', 'closed', 'closed', '', 'acf_categorias-de-produtos', '', '', '2018-10-01 16:08:24', '2018-10-01 19:08:24', '', 0, 'http://walldone/?post_type=acf&#038;p=359', 0, 'acf', '', 0),
(360, 1, '2018-10-01 22:30:29', '2018-10-02 01:30:29', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.', 'Teste', '', 'publish', 'closed', 'closed', '', 'teste', '', '', '2018-10-01 22:34:27', '2018-10-02 01:34:27', '', 0, 'http://walldone/?post_type=slider_promo&#038;p=360', 0, 'slider_promo', '', 0),
(361, 1, '2018-10-01 22:41:28', '2018-10-02 01:41:28', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.', 'Teste 2', '', 'publish', 'closed', 'closed', '', 'teste-2', '', '', '2018-10-01 22:41:28', '2018-10-02 01:41:28', '', 0, 'http://walldone/?post_type=slider_promo&#038;p=361', 1, 'slider_promo', '', 0),
(362, 1, '2018-10-01 22:57:08', '2018-10-02 01:57:08', 'Lorem\r\n[woocommerce_cart]', 'Carrinho', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2018-10-01 22:57:08', '2018-10-02 01:57:08', '', 6, 'http://walldone/6-revision-v1/', 0, 'revision', '', 0),
(363, 1, '2018-10-01 22:57:43', '2018-10-02 01:57:43', '[woocommerce_cart]', 'Carrinho', '', 'inherit', 'closed', 'closed', '', '6-autosave-v1', '', '', '2018-10-01 22:57:43', '2018-10-02 01:57:43', '', 6, 'http://walldone/6-autosave-v1/', 0, 'revision', '', 0),
(364, 1, '2018-10-01 22:57:49', '2018-10-02 01:57:49', '[woocommerce_cart]', 'Carrinho', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2018-10-01 22:57:49', '2018-10-02 01:57:49', '', 6, 'http://walldone/6-revision-v1/', 0, 'revision', '', 0),
(365, 1, '2018-10-02 15:32:20', '2018-10-02 18:32:20', '[woocommerce_checkout]', 'Finalizar compra', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2018-10-02 15:32:20', '2018-10-02 18:32:20', '', 7, 'http://walldone/7-revision-v1/', 0, 'revision', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wd_termmeta`
--

CREATE TABLE `wd_termmeta` (
  `meta_id` bigint(20) unsigned NOT NULL,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wd_termmeta`
--

INSERT INTO `wd_termmeta` (`meta_id`, `term_id`, `meta_key`, `meta_value`) VALUES
(9, 15, 'product_count_product_cat', '7'),
(10, 15, 'display_type', ''),
(11, 15, 'thumbnail_id', '0'),
(12, 20, 'order', '0'),
(13, 20, 'display_type', ''),
(14, 20, 'thumbnail_id', '0'),
(15, 21, 'order', '0'),
(16, 21, 'display_type', ''),
(17, 21, 'thumbnail_id', '0'),
(18, 22, 'order', '0'),
(19, 22, 'display_type', ''),
(20, 22, 'thumbnail_id', '0'),
(21, 23, 'order', '0'),
(22, 23, 'display_type', ''),
(23, 23, 'thumbnail_id', '0'),
(24, 24, 'order', '0'),
(25, 24, 'display_type', ''),
(26, 24, 'thumbnail_id', '0'),
(27, 25, 'order', '0'),
(28, 25, 'display_type', ''),
(29, 25, 'thumbnail_id', '0'),
(30, 26, 'order', '0'),
(31, 26, 'display_type', ''),
(32, 26, 'thumbnail_id', '0'),
(33, 27, 'order', '0'),
(34, 27, 'display_type', ''),
(35, 27, 'thumbnail_id', '0'),
(36, 28, 'order', '0'),
(37, 28, 'display_type', ''),
(38, 28, 'thumbnail_id', '0'),
(39, 29, 'order', '0'),
(40, 29, 'display_type', ''),
(41, 29, 'thumbnail_id', '0'),
(42, 30, 'order', '0'),
(43, 30, 'display_type', ''),
(44, 30, 'thumbnail_id', '0'),
(45, 31, 'order', '0'),
(46, 31, 'display_type', ''),
(47, 31, 'thumbnail_id', '0'),
(48, 32, 'order', '0'),
(49, 32, 'display_type', ''),
(50, 32, 'thumbnail_id', '0'),
(51, 33, 'order', '0'),
(52, 33, 'display_type', ''),
(53, 33, 'thumbnail_id', '0'),
(54, 34, 'order', '0'),
(55, 34, 'display_type', ''),
(56, 34, 'thumbnail_id', '0'),
(57, 35, 'order', '0'),
(58, 35, 'display_type', ''),
(59, 35, 'thumbnail_id', '0'),
(60, 36, 'order', '0'),
(61, 36, 'display_type', ''),
(62, 36, 'thumbnail_id', '0'),
(63, 37, 'order', '0'),
(64, 37, 'display_type', ''),
(65, 37, 'thumbnail_id', '0'),
(66, 38, 'order', '0'),
(67, 38, 'display_type', ''),
(68, 38, 'thumbnail_id', '0'),
(69, 39, 'order', '0'),
(70, 39, 'display_type', ''),
(71, 39, 'thumbnail_id', '0'),
(72, 40, 'order', '0'),
(73, 40, 'display_type', ''),
(74, 40, 'thumbnail_id', '0'),
(75, 41, 'order_pa_molduras', '0'),
(76, 42, 'order_pa_molduras', '0'),
(77, 43, 'order_pa_molduras', '0'),
(78, 37, 'product_count_product_cat', '1'),
(79, 36, 'product_count_product_cat', '1');

-- --------------------------------------------------------

--
-- Table structure for table `wd_terms`
--

CREATE TABLE `wd_terms` (
  `term_id` bigint(20) unsigned NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wd_terms`
--

INSERT INTO `wd_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Sem categoria', 'sem-categoria', 0),
(2, 'simple', 'simple', 0),
(3, 'grouped', 'grouped', 0),
(4, 'variable', 'variable', 0),
(5, 'external', 'external', 0),
(6, 'exclude-from-search', 'exclude-from-search', 0),
(7, 'exclude-from-catalog', 'exclude-from-catalog', 0),
(8, 'featured', 'featured', 0),
(9, 'outofstock', 'outofstock', 0),
(10, 'rated-1', 'rated-1', 0),
(11, 'rated-2', 'rated-2', 0),
(12, 'rated-3', 'rated-3', 0),
(13, 'rated-4', 'rated-4', 0),
(14, 'rated-5', 'rated-5', 0),
(15, 'Produtos', 'produtos', 0),
(20, 'Ambientes', 'ambientes', 0),
(21, 'Corredores e Halls de Entrada', 'corredores-e-halls-de-entrada', 0),
(22, 'Cozinhas', 'cozinhas', 0),
(23, 'Escritórios', 'escritorios', 0),
(24, 'Quartos de Casal', 'quartos-de-casal', 0),
(25, 'Quartos de Solteiro', 'quartos-de-solteiro', 0),
(26, 'Quartos Infantis', 'quartos-infantis', 0),
(27, 'Sacadas e Churrasqueiras', 'sacadas-e-churrasqueiras', 0),
(28, 'Temas', 'temas', 0),
(29, 'Amor', 'amor', 0),
(30, 'Comes e Bebes', 'comes-e-bebes', 0),
(31, 'Fé', 'fe', 0),
(32, 'Good Vibes', 'good-vibes', 0),
(33, 'Infantil', 'infantil', 0),
(34, 'Lar Doce Lar', 'lar-doce-lar', 0),
(35, 'Pet', 'pet', 0),
(36, 'Coleções', 'colecoes', 0),
(37, 'Aquarela - Mariana Terleski', 'aquarela-mariana-terleski', 0),
(38, 'Bença - Borgodó', 'benca-borogodo', 0),
(39, 'Mãos ao Alto - Marcos Bernardes', 'maos-ao-alto-marcos-bernardes', 0),
(40, 'Tô de Olho', 'to-de-olho', 0),
(41, 'Verde', 'moldura-verde', 0),
(42, 'Vermelha', 'moldura-vermelha', 0),
(43, 'Azul', 'moldura-azul', 0),
(44, 'Categoria 1', 'categoria-1', 0),
(45, 'Categoria 2', 'categoria-2', 0),
(46, 'Categoria 3', 'categoria-3', 0),
(47, 'Menu Principal', 'menu-principal', 0),
(48, 'Menu Mobile', 'menu-mobile', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wd_term_relationships`
--

CREATE TABLE `wd_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wd_term_relationships`
--

INSERT INTO `wd_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 44, 0),
(38, 4, 0),
(38, 15, 0),
(50, 4, 0),
(50, 15, 0),
(90, 4, 0),
(90, 15, 0),
(93, 4, 0),
(93, 15, 0),
(96, 4, 0),
(96, 12, 0),
(96, 15, 0),
(96, 37, 0),
(99, 2, 0),
(99, 14, 0),
(99, 15, 0),
(99, 41, 0),
(99, 42, 0),
(99, 43, 0),
(129, 2, 0),
(129, 15, 0),
(129, 41, 0),
(129, 42, 0),
(129, 43, 0),
(141, 44, 0),
(142, 45, 0),
(143, 45, 0),
(144, 46, 0),
(145, 46, 0),
(242, 2, 0),
(242, 6, 0),
(242, 7, 0),
(242, 15, 0),
(282, 47, 0),
(283, 47, 0),
(284, 47, 0),
(285, 47, 0),
(286, 47, 0),
(287, 47, 0),
(288, 47, 0),
(289, 47, 0),
(290, 47, 0),
(291, 47, 0),
(292, 47, 0),
(293, 47, 0),
(294, 47, 0),
(295, 47, 0),
(296, 47, 0),
(297, 47, 0),
(298, 47, 0),
(299, 47, 0),
(300, 47, 0),
(301, 47, 0),
(302, 47, 0),
(303, 47, 0),
(304, 47, 0),
(305, 47, 0),
(306, 47, 0),
(307, 47, 0),
(308, 47, 0),
(309, 47, 0),
(310, 47, 0),
(311, 47, 0),
(312, 47, 0),
(313, 47, 0),
(314, 47, 0),
(315, 47, 0),
(316, 48, 0),
(317, 48, 0),
(318, 48, 0),
(319, 48, 0),
(320, 48, 0),
(321, 48, 0),
(322, 48, 0),
(323, 48, 0),
(324, 48, 0),
(325, 48, 0),
(326, 48, 0),
(327, 48, 0),
(328, 48, 0),
(329, 48, 0),
(330, 48, 0),
(331, 48, 0),
(332, 48, 0),
(333, 48, 0),
(334, 48, 0),
(335, 48, 0),
(336, 48, 0),
(337, 48, 0),
(338, 48, 0),
(339, 48, 0),
(340, 48, 0),
(341, 48, 0),
(342, 48, 0),
(343, 48, 0),
(344, 48, 0),
(345, 48, 0),
(346, 48, 0),
(347, 48, 0),
(348, 48, 0),
(349, 48, 0),
(350, 48, 0),
(351, 48, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wd_term_taxonomy`
--

CREATE TABLE `wd_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wd_term_taxonomy`
--

INSERT INTO `wd_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 0),
(2, 2, 'product_type', '', 0, 3),
(3, 3, 'product_type', '', 0, 0),
(4, 4, 'product_type', '', 0, 5),
(5, 5, 'product_type', '', 0, 0),
(6, 6, 'product_visibility', '', 0, 1),
(7, 7, 'product_visibility', '', 0, 1),
(8, 8, 'product_visibility', '', 0, 0),
(9, 9, 'product_visibility', '', 0, 0),
(10, 10, 'product_visibility', '', 0, 0),
(11, 11, 'product_visibility', '', 0, 0),
(12, 12, 'product_visibility', '', 0, 1),
(13, 13, 'product_visibility', '', 0, 0),
(14, 14, 'product_visibility', '', 0, 1),
(15, 15, 'product_cat', '', 0, 8),
(20, 20, 'product_cat', '', 15, 0),
(21, 21, 'product_cat', '', 20, 0),
(22, 22, 'product_cat', '', 20, 0),
(23, 23, 'product_cat', '', 20, 0),
(24, 24, 'product_cat', '', 20, 0),
(25, 25, 'product_cat', '', 20, 0),
(26, 26, 'product_cat', '', 20, 0),
(27, 27, 'product_cat', '', 20, 0),
(28, 28, 'product_cat', '', 15, 0),
(29, 29, 'product_cat', '', 28, 0),
(30, 30, 'product_cat', '', 28, 0),
(31, 31, 'product_cat', '', 28, 0),
(32, 32, 'product_cat', '', 28, 0),
(33, 33, 'product_cat', '', 28, 0),
(34, 34, 'product_cat', '', 28, 0),
(35, 35, 'product_cat', '', 28, 0),
(36, 36, 'product_cat', '', 15, 0),
(37, 37, 'product_cat', '', 36, 1),
(38, 38, 'product_cat', '', 36, 0),
(39, 39, 'product_cat', '', 36, 0),
(40, 40, 'product_cat', '', 36, 0),
(41, 41, 'pa_molduras', '', 0, 2),
(42, 42, 'pa_molduras', '', 0, 2),
(43, 43, 'pa_molduras', '', 0, 2),
(44, 44, 'category', '', 0, 2),
(45, 45, 'category', '', 0, 2),
(46, 46, 'category', '', 0, 2),
(47, 47, 'nav_menu', '', 0, 34),
(48, 48, 'nav_menu', '', 0, 36);

-- --------------------------------------------------------

--
-- Table structure for table `wd_usermeta`
--

CREATE TABLE `wd_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wd_usermeta`
--

INSERT INTO `wd_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'agencia904'),
(2, 1, 'first_name', 'Vanssiler'),
(3, 1, 'last_name', 'Gonçalves'),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wd_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(13, 1, 'wd_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'wp496_privacy,yith_wcwl_panel'),
(15, 1, 'show_welcome_panel', '1'),
(16, 1, 'session_tokens', 'a:1:{s:64:"fa2f75db546811adf53113ee963c33f8cb2975d9adabac70b7a1d859094c2e22";a:4:{s:10:"expiration";i:1538567518;s:2:"ip";s:3:"::1";s:2:"ua";s:121:"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36";s:5:"login";i:1538394718;}}'),
(17, 1, 'wd_dashboard_quick_press_last_post_id', '266'),
(19, 1, 'wc_last_active', '1538524800'),
(20, 1, 'dismissed_no_secure_connection_notice', '1'),
(21, 1, 'wd_user-settings', 'libraryContent=browse&editor=html'),
(22, 1, 'wd_user-settings-time', '1538135939'),
(23, 1, 'closedpostboxes_product', 'a:0:{}'),
(24, 1, 'metaboxhidden_product', 'a:2:{i:0;s:10:"postcustom";i:1;s:7:"slugdiv";}'),
(25, 2, 'nickname', 'felipe'),
(26, 2, 'first_name', 'Felipe'),
(27, 2, 'last_name', 'lima'),
(28, 2, 'description', ''),
(29, 2, 'rich_editing', 'true'),
(30, 2, 'syntax_highlighting', 'true'),
(31, 2, 'comment_shortcuts', 'false'),
(32, 2, 'admin_color', 'fresh'),
(33, 2, 'use_ssl', '0'),
(34, 2, 'show_admin_bar_front', 'true'),
(35, 2, 'locale', ''),
(36, 2, 'wd_capabilities', 'a:1:{s:8:"customer";b:1;}'),
(37, 2, 'wd_user_level', '0'),
(38, 2, 'last_update', '1535241767'),
(40, 2, '_woocommerce_persistent_cart_1', 'a:1:{s:4:"cart";a:0:{}}'),
(41, 2, 'wc_last_active', '1535241600'),
(42, 1, 'wd_media_library_mode', 'list'),
(43, 1, 'wpfm_total_filesize_used', '524924'),
(44, 1, 'woosw_key', 'P5372Q'),
(45, 1, 'managenav-menuscolumnshidden', 'a:3:{i:0;s:15:"title-attribute";i:1;s:3:"xfn";i:2;s:11:"description";}'),
(46, 1, 'metaboxhidden_nav-menus', 'a:4:{i:0;s:20:"add-post-type-slider";i:1;s:25:"add-post-type-slider_blog";i:2;s:12:"add-post_tag";i:3;s:15:"add-product_tag";}'),
(47, 1, 'closedpostboxes_nav-menus', 'a:0:{}'),
(48, 1, 'last_update', '1538073166'),
(49, 1, 'billing_first_name', 'Vanssiler'),
(50, 1, 'billing_last_name', 'Gonçalves'),
(51, 1, 'billing_address_1', 'Rua Nove de Julho 533'),
(52, 1, 'billing_city', 'Marília'),
(53, 1, 'billing_state', 'SP'),
(54, 1, 'billing_postcode', '17509-110'),
(55, 1, 'billing_country', 'BR'),
(56, 1, 'billing_email', 'vanssiler.goncalves@gmail.com'),
(57, 1, 'billing_phone', '997669642'),
(58, 1, 'shipping_first_name', 'Vanssiler'),
(59, 1, 'shipping_last_name', 'Gonçalves'),
(60, 1, 'shipping_address_1', 'Rua Nove de Julho 533'),
(61, 1, 'shipping_city', 'Marília'),
(62, 1, 'shipping_state', 'SP'),
(63, 1, 'shipping_postcode', '17509110'),
(64, 1, 'shipping_country', 'BR'),
(65, 1, 'shipping_method', 'a:1:{i:0;s:15:"free_shipping:1";}'),
(67, 1, '_woocommerce_persistent_cart_1', 'a:1:{s:4:"cart";a:3:{s:32:"d1f491a404d6854880943e5c3cd9ca25";a:11:{s:3:"key";s:32:"d1f491a404d6854880943e5c3cd9ca25";s:10:"product_id";i:129;s:12:"variation_id";i:0;s:9:"variation";a:0:{}s:8:"quantity";i:5;s:9:"data_hash";s:32:"b5c1d5ca8bae6d4896cf1807cdf763f0";s:13:"line_tax_data";a:2:{s:8:"subtotal";a:0:{}s:5:"total";a:0:{}}s:13:"line_subtotal";d:100;s:17:"line_subtotal_tax";i:0;s:10:"line_total";d:100;s:8:"line_tax";i:0;}s:32:"ac627ab1ccbdb62ec96e702f07f6425b";a:11:{s:3:"key";s:32:"ac627ab1ccbdb62ec96e702f07f6425b";s:10:"product_id";i:99;s:12:"variation_id";i:0;s:9:"variation";a:0:{}s:8:"quantity";i:1;s:9:"data_hash";s:32:"b5c1d5ca8bae6d4896cf1807cdf763f0";s:13:"line_tax_data";a:2:{s:8:"subtotal";a:0:{}s:5:"total";a:0:{}}s:13:"line_subtotal";d:20;s:17:"line_subtotal_tax";i:0;s:10:"line_total";d:20;s:8:"line_tax";i:0;}s:32:"be1aa3eaeaff193544a35c3acc95df2e";a:20:{s:13:"wccpf_imagens";a:6:{s:5:"fname";s:7:"imagens";s:5:"ftype";s:4:"file";s:8:"user_val";s:776:"[{"file":"/Users/Vanssiler/Desktop/TRABALHOS/SMARTUX/CLIENTES/AGENCIA 904/Arquivos Wall Done/Website Localhost/walldone-new/wp-content/uploads/2018/10/imagem_teste.jpg","url":"http://walldone/wp-content/uploads/2018/10/imagem_teste.jpg","type":"image/jpeg"},{"file":"/Users/Vanssiler/Desktop/TRABALHOS/SMARTUX/CLIENTES/AGENCIA 904/Arquivos Wall Done/Website Localhost/walldone-new/wp-content/uploads/2018/10/imagem_teste2.jpg","url":"http://walldone/wp-content/uploads/2018/10/imagem_teste2.jpg","type":"image/jpeg"},{"file":"/Users/Vanssiler/Desktop/TRABALHOS/SMARTUX/CLIENTES/AGENCIA 904/Arquivos Wall Done/Website Localhost/walldone-new/wp-content/uploads/2018/10/imagem_teste3.jpg","url":"http://walldone/wp-content/uploads/2018/10/imagem_teste3.jpg","type":"image/jpeg"}]";s:9:"fee_rules";a:0:{}s:13:"pricing_rules";a:0:{}s:6:"format";s:0:"";}s:13:"wccpf_largura";a:6:{s:5:"fname";s:7:"largura";s:5:"ftype";s:4:"text";s:8:"user_val";s:4:"1.6m";s:9:"fee_rules";a:0:{}s:13:"pricing_rules";a:0:{}s:6:"format";s:0:"";}s:12:"wccpf_altura";a:6:{s:5:"fname";s:6:"altura";s:5:"ftype";s:4:"text";s:8:"user_val";s:4:"2.3m";s:9:"fee_rules";a:0:{}s:13:"pricing_rules";a:0:{}s:6:"format";s:0:"";}s:26:"wccpf_informaes_adicionais";a:6:{s:5:"fname";s:20:"informaes_adicionais";s:5:"ftype";s:4:"text";s:8:"user_val";s:20:"informações parede";s:9:"fee_rules";a:0:{}s:13:"pricing_rules";a:0:{}s:6:"format";s:0:"";}s:35:"wccpf_dos_produtos_que_voc_escolheu";a:6:{s:5:"fname";s:29:"dos_produtos_que_voc_escolheu";s:5:"ftype";s:5:"radio";s:8:"user_val";s:12:"Somente eles";s:9:"fee_rules";a:0:{}s:13:"pricing_rules";a:0:{}s:6:"format";s:0:"";}s:34:"wccpf_sobre_os_produtos_escolhidos";a:6:{s:5:"fname";s:28:"sobre_os_produtos_escolhidos";s:5:"ftype";s:8:"textarea";s:8:"user_val";s:17:"asdaaffsad s sfg ";s:9:"fee_rules";a:0:{}s:13:"pricing_rules";a:0:{}s:6:"format";s:0:"";}s:25:"wccpf_sobre_outro_produto";a:6:{s:5:"fname";s:19:"sobre_outro_produto";s:5:"ftype";s:8:"textarea";s:8:"user_val";s:16:"sd fsdh sdh sdh ";s:9:"fee_rules";a:0:{}s:13:"pricing_rules";a:0:{}s:6:"format";s:0:"";}s:20:"wccpf_mais_informaes";a:6:{s:5:"fname";s:14:"mais_informaes";s:5:"ftype";s:8:"textarea";s:8:"user_val";s:16:"s dhsdh sdh sdh ";s:9:"fee_rules";a:0:{}s:13:"pricing_rules";a:0:{}s:6:"format";s:0:"";}s:21:"wccpf_produtos_do_kit";a:6:{s:5:"fname";s:15:"produtos_do_kit";s:5:"ftype";s:8:"textarea";s:8:"user_val";s:135:"Quarto de Casal Menina e Menino | Quadro - Nosso Lar | Adesivo para Parede - Bolas Irregulares | Quadro Gato Frida Aquarela (cópia) | ";s:9:"fee_rules";a:0:{}s:13:"pricing_rules";a:0:{}s:6:"format";s:0:"";}s:3:"key";s:32:"be1aa3eaeaff193544a35c3acc95df2e";s:10:"product_id";i:242;s:12:"variation_id";i:0;s:9:"variation";a:0:{}s:8:"quantity";i:1;s:9:"data_hash";s:32:"b5c1d5ca8bae6d4896cf1807cdf763f0";s:13:"line_tax_data";a:2:{s:8:"subtotal";a:0:{}s:5:"total";a:0:{}}s:13:"line_subtotal";d:100;s:17:"line_subtotal_tax";i:0;s:10:"line_total";d:100;s:8:"line_tax";i:0;}}}');

-- --------------------------------------------------------

--
-- Table structure for table `wd_users`
--

CREATE TABLE `wd_users` (
  `ID` bigint(20) unsigned NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wd_users`
--

INSERT INTO `wd_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'agencia904', '$P$BmKHdSg56yoHTtVErXg5bIU6/kVcKK0', 'agencia904', 'web@904.ag', '', '2018-08-23 14:05:40', '', 0, 'agencia904'),
(2, 'felipe', '$P$BbBHrRLEIvrccZxDLhD7hLNpQb.Hkt1', 'felipe', 'felipe@904.ag', '', '2018-08-26 00:02:47', '', 0, 'felipe');

-- --------------------------------------------------------

--
-- Table structure for table `wd_wc_download_log`
--

CREATE TABLE `wd_wc_download_log` (
  `download_log_id` bigint(20) unsigned NOT NULL,
  `timestamp` datetime NOT NULL,
  `permission_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `user_ip_address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wd_wc_webhooks`
--

CREATE TABLE `wd_wc_webhooks` (
  `webhook_id` bigint(20) unsigned NOT NULL,
  `status` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `delivery_url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `topic` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_created_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `api_version` smallint(4) NOT NULL,
  `failure_count` smallint(10) NOT NULL DEFAULT '0',
  `pending_delivery` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wd_wfu_dbxqueue`
--

CREATE TABLE `wd_wfu_dbxqueue` (
  `iddbxqueue` mediumint(9) NOT NULL,
  `fileid` mediumint(9) NOT NULL,
  `priority` mediumint(9) NOT NULL,
  `status` mediumint(9) NOT NULL,
  `jobid` varchar(10) NOT NULL,
  `start_time` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wd_wfu_log`
--

CREATE TABLE `wd_wfu_log` (
  `idlog` mediumint(9) NOT NULL,
  `userid` int(11) NOT NULL,
  `uploaduserid` int(11) NOT NULL,
  `uploadtime` bigint(20) DEFAULT NULL,
  `sessionid` varchar(40) DEFAULT NULL,
  `filepath` text NOT NULL,
  `filehash` varchar(100) NOT NULL,
  `filesize` bigint(20) NOT NULL,
  `uploadid` varchar(20) NOT NULL,
  `pageid` mediumint(9) DEFAULT NULL,
  `blogid` mediumint(9) DEFAULT NULL,
  `sid` varchar(10) DEFAULT NULL,
  `date_from` datetime DEFAULT NULL,
  `date_to` datetime DEFAULT NULL,
  `action` varchar(20) NOT NULL,
  `linkedto` mediumint(9) DEFAULT NULL,
  `filedata` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wd_wfu_userdata`
--

CREATE TABLE `wd_wfu_userdata` (
  `iduserdata` mediumint(9) NOT NULL,
  `uploadid` varchar(20) NOT NULL,
  `property` varchar(100) NOT NULL,
  `propkey` mediumint(9) NOT NULL,
  `propvalue` text,
  `date_from` datetime DEFAULT NULL,
  `date_to` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wd_woocommerce_api_keys`
--

CREATE TABLE `wd_woocommerce_api_keys` (
  `key_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `description` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permissions` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `consumer_key` char(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `consumer_secret` char(43) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nonces` longtext COLLATE utf8mb4_unicode_ci,
  `truncated_key` char(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_access` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wd_woocommerce_attribute_taxonomies`
--

CREATE TABLE `wd_woocommerce_attribute_taxonomies` (
  `attribute_id` bigint(20) unsigned NOT NULL,
  `attribute_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_label` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attribute_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_orderby` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_public` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wd_woocommerce_attribute_taxonomies`
--

INSERT INTO `wd_woocommerce_attribute_taxonomies` (`attribute_id`, `attribute_name`, `attribute_label`, `attribute_type`, `attribute_orderby`, `attribute_public`) VALUES
(1, 'molduras', 'Molduras', 'select', 'menu_order', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wd_woocommerce_downloadable_product_permissions`
--

CREATE TABLE `wd_woocommerce_downloadable_product_permissions` (
  `permission_id` bigint(20) unsigned NOT NULL,
  `download_id` varchar(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` bigint(20) unsigned NOT NULL,
  `order_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `order_key` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_email` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `downloads_remaining` varchar(9) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `access_granted` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access_expires` datetime DEFAULT NULL,
  `download_count` bigint(20) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wd_woocommerce_log`
--

CREATE TABLE `wd_woocommerce_log` (
  `log_id` bigint(20) unsigned NOT NULL,
  `timestamp` datetime NOT NULL,
  `level` smallint(4) NOT NULL,
  `source` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `context` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wd_woocommerce_order_itemmeta`
--

CREATE TABLE `wd_woocommerce_order_itemmeta` (
  `meta_id` bigint(20) unsigned NOT NULL,
  `order_item_id` bigint(20) unsigned NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wd_woocommerce_order_itemmeta`
--

INSERT INTO `wd_woocommerce_order_itemmeta` (`meta_id`, `order_item_id`, `meta_key`, `meta_value`) VALUES
(1, 1, '_product_id', '129'),
(2, 1, '_variation_id', '0'),
(3, 1, '_qty', '1'),
(4, 1, '_tax_class', ''),
(5, 1, '_line_subtotal', '20'),
(6, 1, '_line_subtotal_tax', '0'),
(7, 1, '_line_total', '20'),
(8, 1, '_line_tax', '0'),
(9, 1, '_line_tax_data', 'a:2:{s:5:"total";a:0:{}s:8:"subtotal";a:0:{}}'),
(10, 2, 'method_id', 'free_shipping'),
(11, 2, 'instance_id', '1'),
(12, 2, 'cost', '0.00'),
(13, 2, 'total_tax', '0'),
(14, 2, 'taxes', 'a:1:{s:5:"total";a:0:{}}'),
(15, 2, 'Itens', 'Quadro Gato Frida Aquarela (cópia) &times; 1');

-- --------------------------------------------------------

--
-- Table structure for table `wd_woocommerce_order_items`
--

CREATE TABLE `wd_woocommerce_order_items` (
  `order_item_id` bigint(20) unsigned NOT NULL,
  `order_item_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_item_type` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `order_id` bigint(20) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wd_woocommerce_order_items`
--

INSERT INTO `wd_woocommerce_order_items` (`order_item_id`, `order_item_name`, `order_item_type`, `order_id`) VALUES
(1, 'Quadro Gato Frida Aquarela (cópia)', 'line_item', 353),
(2, 'Frete grátis', 'shipping', 353);

-- --------------------------------------------------------

--
-- Table structure for table `wd_woocommerce_payment_tokenmeta`
--

CREATE TABLE `wd_woocommerce_payment_tokenmeta` (
  `meta_id` bigint(20) unsigned NOT NULL,
  `payment_token_id` bigint(20) unsigned NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wd_woocommerce_payment_tokens`
--

CREATE TABLE `wd_woocommerce_payment_tokens` (
  `token_id` bigint(20) unsigned NOT NULL,
  `gateway_id` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `type` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wd_woocommerce_sessions`
--

CREATE TABLE `wd_woocommerce_sessions` (
  `session_id` bigint(20) unsigned NOT NULL,
  `session_key` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_expiry` bigint(20) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=144 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wd_woocommerce_sessions`
--

INSERT INTO `wd_woocommerce_sessions` (`session_id`, `session_key`, `session_value`, `session_expiry`) VALUES
(143, '1', 'a:13:{s:4:"cart";s:3992:"a:3:{s:32:"d1f491a404d6854880943e5c3cd9ca25";a:11:{s:3:"key";s:32:"d1f491a404d6854880943e5c3cd9ca25";s:10:"product_id";i:129;s:12:"variation_id";i:0;s:9:"variation";a:0:{}s:8:"quantity";i:5;s:9:"data_hash";s:32:"b5c1d5ca8bae6d4896cf1807cdf763f0";s:13:"line_tax_data";a:2:{s:8:"subtotal";a:0:{}s:5:"total";a:0:{}}s:13:"line_subtotal";d:100;s:17:"line_subtotal_tax";i:0;s:10:"line_total";d:100;s:8:"line_tax";i:0;}s:32:"ac627ab1ccbdb62ec96e702f07f6425b";a:11:{s:3:"key";s:32:"ac627ab1ccbdb62ec96e702f07f6425b";s:10:"product_id";i:99;s:12:"variation_id";i:0;s:9:"variation";a:0:{}s:8:"quantity";i:1;s:9:"data_hash";s:32:"b5c1d5ca8bae6d4896cf1807cdf763f0";s:13:"line_tax_data";a:2:{s:8:"subtotal";a:0:{}s:5:"total";a:0:{}}s:13:"line_subtotal";d:20;s:17:"line_subtotal_tax";i:0;s:10:"line_total";d:20;s:8:"line_tax";i:0;}s:32:"be1aa3eaeaff193544a35c3acc95df2e";a:20:{s:13:"wccpf_imagens";a:6:{s:5:"fname";s:7:"imagens";s:5:"ftype";s:4:"file";s:8:"user_val";s:845:"[{"file":"\\/Users\\/Vanssiler\\/Desktop\\/TRABALHOS\\/SMARTUX\\/CLIENTES\\/AGENCIA 904\\/Arquivos Wall Done\\/Website Localhost\\/walldone-new\\/wp-content\\/uploads\\/2018\\/10\\/imagem_teste.jpg","url":"http:\\/\\/walldone\\/wp-content\\/uploads\\/2018\\/10\\/imagem_teste.jpg","type":"image\\/jpeg"},{"file":"\\/Users\\/Vanssiler\\/Desktop\\/TRABALHOS\\/SMARTUX\\/CLIENTES\\/AGENCIA 904\\/Arquivos Wall Done\\/Website Localhost\\/walldone-new\\/wp-content\\/uploads\\/2018\\/10\\/imagem_teste2.jpg","url":"http:\\/\\/walldone\\/wp-content\\/uploads\\/2018\\/10\\/imagem_teste2.jpg","type":"image\\/jpeg"},{"file":"\\/Users\\/Vanssiler\\/Desktop\\/TRABALHOS\\/SMARTUX\\/CLIENTES\\/AGENCIA 904\\/Arquivos Wall Done\\/Website Localhost\\/walldone-new\\/wp-content\\/uploads\\/2018\\/10\\/imagem_teste3.jpg","url":"http:\\/\\/walldone\\/wp-content\\/uploads\\/2018\\/10\\/imagem_teste3.jpg","type":"image\\/jpeg"}]";s:9:"fee_rules";a:0:{}s:13:"pricing_rules";a:0:{}s:6:"format";s:0:"";}s:13:"wccpf_largura";a:6:{s:5:"fname";s:7:"largura";s:5:"ftype";s:4:"text";s:8:"user_val";s:4:"1.6m";s:9:"fee_rules";a:0:{}s:13:"pricing_rules";a:0:{}s:6:"format";s:0:"";}s:12:"wccpf_altura";a:6:{s:5:"fname";s:6:"altura";s:5:"ftype";s:4:"text";s:8:"user_val";s:4:"2.3m";s:9:"fee_rules";a:0:{}s:13:"pricing_rules";a:0:{}s:6:"format";s:0:"";}s:26:"wccpf_informaes_adicionais";a:6:{s:5:"fname";s:20:"informaes_adicionais";s:5:"ftype";s:4:"text";s:8:"user_val";s:20:"informações parede";s:9:"fee_rules";a:0:{}s:13:"pricing_rules";a:0:{}s:6:"format";s:0:"";}s:35:"wccpf_dos_produtos_que_voc_escolheu";a:6:{s:5:"fname";s:29:"dos_produtos_que_voc_escolheu";s:5:"ftype";s:5:"radio";s:8:"user_val";s:12:"Somente eles";s:9:"fee_rules";a:0:{}s:13:"pricing_rules";a:0:{}s:6:"format";s:0:"";}s:34:"wccpf_sobre_os_produtos_escolhidos";a:6:{s:5:"fname";s:28:"sobre_os_produtos_escolhidos";s:5:"ftype";s:8:"textarea";s:8:"user_val";s:17:"asdaaffsad s sfg ";s:9:"fee_rules";a:0:{}s:13:"pricing_rules";a:0:{}s:6:"format";s:0:"";}s:25:"wccpf_sobre_outro_produto";a:6:{s:5:"fname";s:19:"sobre_outro_produto";s:5:"ftype";s:8:"textarea";s:8:"user_val";s:16:"sd fsdh sdh sdh ";s:9:"fee_rules";a:0:{}s:13:"pricing_rules";a:0:{}s:6:"format";s:0:"";}s:20:"wccpf_mais_informaes";a:6:{s:5:"fname";s:14:"mais_informaes";s:5:"ftype";s:8:"textarea";s:8:"user_val";s:16:"s dhsdh sdh sdh ";s:9:"fee_rules";a:0:{}s:13:"pricing_rules";a:0:{}s:6:"format";s:0:"";}s:21:"wccpf_produtos_do_kit";a:6:{s:5:"fname";s:15:"produtos_do_kit";s:5:"ftype";s:8:"textarea";s:8:"user_val";s:135:"Quarto de Casal Menina e Menino | Quadro - Nosso Lar | Adesivo para Parede - Bolas Irregulares | Quadro Gato Frida Aquarela (cópia) | ";s:9:"fee_rules";a:0:{}s:13:"pricing_rules";a:0:{}s:6:"format";s:0:"";}s:3:"key";s:32:"be1aa3eaeaff193544a35c3acc95df2e";s:10:"product_id";i:242;s:12:"variation_id";i:0;s:9:"variation";a:0:{}s:8:"quantity";i:1;s:9:"data_hash";s:32:"b5c1d5ca8bae6d4896cf1807cdf763f0";s:13:"line_tax_data";a:2:{s:8:"subtotal";a:0:{}s:5:"total";a:0:{}}s:13:"line_subtotal";d:100;s:17:"line_subtotal_tax";i:0;s:10:"line_total";d:100;s:8:"line_tax";i:0;}}";s:11:"cart_totals";s:408:"a:15:{s:8:"subtotal";s:6:"220.00";s:12:"subtotal_tax";d:0;s:14:"shipping_total";s:4:"0.00";s:12:"shipping_tax";d:0;s:14:"shipping_taxes";a:0:{}s:14:"discount_total";d:0;s:12:"discount_tax";d:0;s:19:"cart_contents_total";s:6:"220.00";s:17:"cart_contents_tax";d:0;s:19:"cart_contents_taxes";a:0:{}s:9:"fee_total";s:4:"0.00";s:7:"fee_tax";d:0;s:9:"fee_taxes";a:0:{}s:5:"total";s:6:"220.00";s:9:"total_tax";d:0;}";s:15:"applied_coupons";s:6:"a:0:{}";s:22:"coupon_discount_totals";s:6:"a:0:{}";s:26:"coupon_discount_tax_totals";s:6:"a:0:{}";s:21:"removed_cart_contents";s:6:"a:0:{}";s:8:"customer";s:919:"a:26:{s:2:"id";s:1:"1";s:13:"date_modified";s:25:"2018-09-27T15:32:46-03:00";s:8:"postcode";s:9:"17509-110";s:4:"city";s:8:"Marília";s:9:"address_1";s:21:"Rua Nove de Julho 533";s:7:"address";s:21:"Rua Nove de Julho 533";s:9:"address_2";s:0:"";s:5:"state";s:2:"SP";s:7:"country";s:2:"BR";s:17:"shipping_postcode";s:9:"17509-110";s:13:"shipping_city";s:8:"Marília";s:18:"shipping_address_1";s:21:"Rua Nove de Julho 533";s:16:"shipping_address";s:21:"Rua Nove de Julho 533";s:18:"shipping_address_2";s:0:"";s:14:"shipping_state";s:2:"SP";s:16:"shipping_country";s:2:"BR";s:13:"is_vat_exempt";s:0:"";s:19:"calculated_shipping";s:1:"1";s:10:"first_name";s:9:"Vanssiler";s:9:"last_name";s:10:"Gonçalves";s:7:"company";s:0:"";s:5:"phone";s:9:"997669642";s:5:"email";s:29:"vanssiler.goncalves@gmail.com";s:19:"shipping_first_name";s:9:"Vanssiler";s:18:"shipping_last_name";s:10:"Gonçalves";s:16:"shipping_company";s:0:"";}";s:22:"shipping_for_package_0";s:472:"a:2:{s:12:"package_hash";s:40:"wc_ship_3b0437b3ffe702dc09e97eb7037a8f16";s:5:"rates";a:1:{s:15:"free_shipping:1";O:16:"WC_Shipping_Rate":2:{s:7:"\0*\0data";a:6:{s:2:"id";s:15:"free_shipping:1";s:9:"method_id";s:13:"free_shipping";s:11:"instance_id";i:1;s:5:"label";s:13:"Frete grátis";s:4:"cost";s:4:"0.00";s:5:"taxes";a:0:{}}s:12:"\0*\0meta_data";a:1:{s:5:"Itens";s:98:"Quadro Gato Frida Aquarela (cópia) &times; 5, Quadro Gato Frida Aquarela &times; 1, Kit &times; 1";}}}}";s:25:"previous_shipping_methods";s:43:"a:1:{i:0;a:1:{i:0;s:15:"free_shipping:1";}}";s:23:"chosen_shipping_methods";s:33:"a:1:{i:0;s:15:"free_shipping:1";}";s:22:"shipping_method_counts";s:14:"a:1:{i:0;i:1;}";s:10:"wc_notices";N;s:21:"chosen_payment_method";s:4:"bacs";}', 1538617517);

-- --------------------------------------------------------

--
-- Table structure for table `wd_woocommerce_shipping_zones`
--

CREATE TABLE `wd_woocommerce_shipping_zones` (
  `zone_id` bigint(20) unsigned NOT NULL,
  `zone_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zone_order` bigint(20) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wd_woocommerce_shipping_zones`
--

INSERT INTO `wd_woocommerce_shipping_zones` (`zone_id`, `zone_name`, `zone_order`) VALUES
(1, 'Brasil', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wd_woocommerce_shipping_zone_locations`
--

CREATE TABLE `wd_woocommerce_shipping_zone_locations` (
  `location_id` bigint(20) unsigned NOT NULL,
  `zone_id` bigint(20) unsigned NOT NULL,
  `location_code` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location_type` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wd_woocommerce_shipping_zone_locations`
--

INSERT INTO `wd_woocommerce_shipping_zone_locations` (`location_id`, `zone_id`, `location_code`, `location_type`) VALUES
(1, 1, 'BR', 'country');

-- --------------------------------------------------------

--
-- Table structure for table `wd_woocommerce_shipping_zone_methods`
--

CREATE TABLE `wd_woocommerce_shipping_zone_methods` (
  `zone_id` bigint(20) unsigned NOT NULL,
  `instance_id` bigint(20) unsigned NOT NULL,
  `method_id` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `method_order` bigint(20) unsigned NOT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wd_woocommerce_shipping_zone_methods`
--

INSERT INTO `wd_woocommerce_shipping_zone_methods` (`zone_id`, `instance_id`, `method_id`, `method_order`, `is_enabled`) VALUES
(1, 1, 'free_shipping', 1, 1),
(0, 2, 'free_shipping', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `wd_woocommerce_tax_rates`
--

CREATE TABLE `wd_woocommerce_tax_rates` (
  `tax_rate_id` bigint(20) unsigned NOT NULL,
  `tax_rate_country` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate_state` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate_priority` bigint(20) unsigned NOT NULL,
  `tax_rate_compound` int(1) NOT NULL DEFAULT '0',
  `tax_rate_shipping` int(1) NOT NULL DEFAULT '1',
  `tax_rate_order` bigint(20) unsigned NOT NULL,
  `tax_rate_class` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wd_woocommerce_tax_rate_locations`
--

CREATE TABLE `wd_woocommerce_tax_rate_locations` (
  `location_id` bigint(20) unsigned NOT NULL,
  `location_code` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tax_rate_id` bigint(20) unsigned NOT NULL,
  `location_type` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wd_woo_file_dropzone`
--

CREATE TABLE `wd_woo_file_dropzone` (
  `id` int(11) unsigned NOT NULL,
  `extra_fields` longtext,
  `file_urls` longtext,
  `product_id` int(11) unsigned DEFAULT NULL,
  `order_id` int(11) unsigned DEFAULT NULL,
  `order_status` varchar(50) DEFAULT NULL,
  `session_id` varchar(300) DEFAULT NULL,
  `session_expiry_time` varchar(300) DEFAULT NULL,
  `variation_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wd_cf7_vdata`
--
ALTER TABLE `wd_cf7_vdata`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `wd_cf7_vdata_entry`
--
ALTER TABLE `wd_cf7_vdata_entry`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `wd_commentmeta`
--
ALTER TABLE `wd_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wd_comments`
--
ALTER TABLE `wd_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10)),
  ADD KEY `woo_idx_comment_type` (`comment_type`);

--
-- Indexes for table `wd_links`
--
ALTER TABLE `wd_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `wd_options`
--
ALTER TABLE `wd_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Indexes for table `wd_postmeta`
--
ALTER TABLE `wd_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wd_posts`
--
ALTER TABLE `wd_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `wd_termmeta`
--
ALTER TABLE `wd_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wd_terms`
--
ALTER TABLE `wd_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `wd_term_relationships`
--
ALTER TABLE `wd_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `wd_term_taxonomy`
--
ALTER TABLE `wd_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `wd_usermeta`
--
ALTER TABLE `wd_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wd_users`
--
ALTER TABLE `wd_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- Indexes for table `wd_wc_download_log`
--
ALTER TABLE `wd_wc_download_log`
  ADD PRIMARY KEY (`download_log_id`),
  ADD KEY `permission_id` (`permission_id`),
  ADD KEY `timestamp` (`timestamp`);

--
-- Indexes for table `wd_wc_webhooks`
--
ALTER TABLE `wd_wc_webhooks`
  ADD PRIMARY KEY (`webhook_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `wd_wfu_dbxqueue`
--
ALTER TABLE `wd_wfu_dbxqueue`
  ADD PRIMARY KEY (`iddbxqueue`);

--
-- Indexes for table `wd_wfu_log`
--
ALTER TABLE `wd_wfu_log`
  ADD PRIMARY KEY (`idlog`);

--
-- Indexes for table `wd_wfu_userdata`
--
ALTER TABLE `wd_wfu_userdata`
  ADD PRIMARY KEY (`iduserdata`);

--
-- Indexes for table `wd_woocommerce_api_keys`
--
ALTER TABLE `wd_woocommerce_api_keys`
  ADD PRIMARY KEY (`key_id`),
  ADD KEY `consumer_key` (`consumer_key`),
  ADD KEY `consumer_secret` (`consumer_secret`);

--
-- Indexes for table `wd_woocommerce_attribute_taxonomies`
--
ALTER TABLE `wd_woocommerce_attribute_taxonomies`
  ADD PRIMARY KEY (`attribute_id`),
  ADD KEY `attribute_name` (`attribute_name`(20));

--
-- Indexes for table `wd_woocommerce_downloadable_product_permissions`
--
ALTER TABLE `wd_woocommerce_downloadable_product_permissions`
  ADD PRIMARY KEY (`permission_id`),
  ADD KEY `download_order_key_product` (`product_id`,`order_id`,`order_key`(16),`download_id`),
  ADD KEY `download_order_product` (`download_id`,`order_id`,`product_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `wd_woocommerce_log`
--
ALTER TABLE `wd_woocommerce_log`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `level` (`level`);

--
-- Indexes for table `wd_woocommerce_order_itemmeta`
--
ALTER TABLE `wd_woocommerce_order_itemmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `order_item_id` (`order_item_id`),
  ADD KEY `meta_key` (`meta_key`(32));

--
-- Indexes for table `wd_woocommerce_order_items`
--
ALTER TABLE `wd_woocommerce_order_items`
  ADD PRIMARY KEY (`order_item_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `wd_woocommerce_payment_tokenmeta`
--
ALTER TABLE `wd_woocommerce_payment_tokenmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `payment_token_id` (`payment_token_id`),
  ADD KEY `meta_key` (`meta_key`(32));

--
-- Indexes for table `wd_woocommerce_payment_tokens`
--
ALTER TABLE `wd_woocommerce_payment_tokens`
  ADD PRIMARY KEY (`token_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `wd_woocommerce_sessions`
--
ALTER TABLE `wd_woocommerce_sessions`
  ADD PRIMARY KEY (`session_key`),
  ADD UNIQUE KEY `session_id` (`session_id`);

--
-- Indexes for table `wd_woocommerce_shipping_zones`
--
ALTER TABLE `wd_woocommerce_shipping_zones`
  ADD PRIMARY KEY (`zone_id`);

--
-- Indexes for table `wd_woocommerce_shipping_zone_locations`
--
ALTER TABLE `wd_woocommerce_shipping_zone_locations`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `location_id` (`location_id`),
  ADD KEY `location_type_code` (`location_type`(10),`location_code`(20));

--
-- Indexes for table `wd_woocommerce_shipping_zone_methods`
--
ALTER TABLE `wd_woocommerce_shipping_zone_methods`
  ADD PRIMARY KEY (`instance_id`);

--
-- Indexes for table `wd_woocommerce_tax_rates`
--
ALTER TABLE `wd_woocommerce_tax_rates`
  ADD PRIMARY KEY (`tax_rate_id`),
  ADD KEY `tax_rate_country` (`tax_rate_country`),
  ADD KEY `tax_rate_state` (`tax_rate_state`(2)),
  ADD KEY `tax_rate_class` (`tax_rate_class`(10)),
  ADD KEY `tax_rate_priority` (`tax_rate_priority`);

--
-- Indexes for table `wd_woocommerce_tax_rate_locations`
--
ALTER TABLE `wd_woocommerce_tax_rate_locations`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `tax_rate_id` (`tax_rate_id`),
  ADD KEY `location_type_code` (`location_type`(10),`location_code`(20));

--
-- Indexes for table `wd_woo_file_dropzone`
--
ALTER TABLE `wd_woo_file_dropzone`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wd_cf7_vdata`
--
ALTER TABLE `wd_cf7_vdata`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wd_cf7_vdata_entry`
--
ALTER TABLE `wd_cf7_vdata_entry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wd_commentmeta`
--
ALTER TABLE `wd_commentmeta`
  MODIFY `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `wd_comments`
--
ALTER TABLE `wd_comments`
  MODIFY `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `wd_links`
--
ALTER TABLE `wd_links`
  MODIFY `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wd_options`
--
ALTER TABLE `wd_options`
  MODIFY `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2152;
--
-- AUTO_INCREMENT for table `wd_postmeta`
--
ALTER TABLE `wd_postmeta`
  MODIFY `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2143;
--
-- AUTO_INCREMENT for table `wd_posts`
--
ALTER TABLE `wd_posts`
  MODIFY `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=366;
--
-- AUTO_INCREMENT for table `wd_termmeta`
--
ALTER TABLE `wd_termmeta`
  MODIFY `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=80;
--
-- AUTO_INCREMENT for table `wd_terms`
--
ALTER TABLE `wd_terms`
  MODIFY `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `wd_term_taxonomy`
--
ALTER TABLE `wd_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `wd_usermeta`
--
ALTER TABLE `wd_usermeta`
  MODIFY `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=68;
--
-- AUTO_INCREMENT for table `wd_users`
--
ALTER TABLE `wd_users`
  MODIFY `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `wd_wc_download_log`
--
ALTER TABLE `wd_wc_download_log`
  MODIFY `download_log_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wd_wc_webhooks`
--
ALTER TABLE `wd_wc_webhooks`
  MODIFY `webhook_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wd_wfu_dbxqueue`
--
ALTER TABLE `wd_wfu_dbxqueue`
  MODIFY `iddbxqueue` mediumint(9) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wd_wfu_log`
--
ALTER TABLE `wd_wfu_log`
  MODIFY `idlog` mediumint(9) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wd_wfu_userdata`
--
ALTER TABLE `wd_wfu_userdata`
  MODIFY `iduserdata` mediumint(9) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wd_woocommerce_api_keys`
--
ALTER TABLE `wd_woocommerce_api_keys`
  MODIFY `key_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wd_woocommerce_attribute_taxonomies`
--
ALTER TABLE `wd_woocommerce_attribute_taxonomies`
  MODIFY `attribute_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wd_woocommerce_downloadable_product_permissions`
--
ALTER TABLE `wd_woocommerce_downloadable_product_permissions`
  MODIFY `permission_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wd_woocommerce_log`
--
ALTER TABLE `wd_woocommerce_log`
  MODIFY `log_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wd_woocommerce_order_itemmeta`
--
ALTER TABLE `wd_woocommerce_order_itemmeta`
  MODIFY `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `wd_woocommerce_order_items`
--
ALTER TABLE `wd_woocommerce_order_items`
  MODIFY `order_item_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `wd_woocommerce_payment_tokenmeta`
--
ALTER TABLE `wd_woocommerce_payment_tokenmeta`
  MODIFY `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wd_woocommerce_payment_tokens`
--
ALTER TABLE `wd_woocommerce_payment_tokens`
  MODIFY `token_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wd_woocommerce_sessions`
--
ALTER TABLE `wd_woocommerce_sessions`
  MODIFY `session_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=144;
--
-- AUTO_INCREMENT for table `wd_woocommerce_shipping_zones`
--
ALTER TABLE `wd_woocommerce_shipping_zones`
  MODIFY `zone_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wd_woocommerce_shipping_zone_locations`
--
ALTER TABLE `wd_woocommerce_shipping_zone_locations`
  MODIFY `location_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wd_woocommerce_shipping_zone_methods`
--
ALTER TABLE `wd_woocommerce_shipping_zone_methods`
  MODIFY `instance_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `wd_woocommerce_tax_rates`
--
ALTER TABLE `wd_woocommerce_tax_rates`
  MODIFY `tax_rate_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wd_woocommerce_tax_rate_locations`
--
ALTER TABLE `wd_woocommerce_tax_rate_locations`
  MODIFY `location_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wd_woo_file_dropzone`
--
ALTER TABLE `wd_woo_file_dropzone`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `wd_wc_download_log`
--
ALTER TABLE `wd_wc_download_log`
  ADD CONSTRAINT `fk_wc_download_log_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `wd_woocommerce_downloadable_product_permissions` (`permission_id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
