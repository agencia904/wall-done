<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'wall_done');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'lh wwGcrn5NA.c5c*5t=l817=tP`]:Nm4n*keU`<I[]^~}8fCU#zPqylkHGuKe+[');
define('SECURE_AUTH_KEY',  '{*Z}xcO^|Qn}]:IQ3D|^%==q!j[t?;CDrS 2bZ~Xib9>CCs*EwfE?oGm8+[dee}I');
define('LOGGED_IN_KEY',    ',T87m5+aUTHF2!l%mN^hV~)=hvB<X9):am|0B<>(?qXc?aQBy0dbm2*UCM5R&,Wd');
define('NONCE_KEY',        'W)}NN3VK2ldvjHS`FY*bODeco>D O*vOkW}9M0kwLU7>l].g#4bA>B5&]z)*4u(b');
define('AUTH_SALT',        'q@w,IcFWNxETPIG9R01Y#X&_A=h3j-_!sLOIQ1NIw_Xb)rG`ed}=!2S<m+~YQf:0');
define('SECURE_AUTH_SALT', 'Vplh<mwj#F*aD f&uag>OlnS2Y)&GU<<zd)K`>Hn/|Yf5PRX[:{Tq];qwl7n{0PE');
define('LOGGED_IN_SALT',   '^w%2K:km=jy?{B0>qY# `R7<v>mJCAx5?PA/o^wm1W9AwH@FJaWGS[[7BZq&:&8C');
define('NONCE_SALT',       'L)@?_^gJzt`(V0@D42,)DNQ>yahy>*TPG< #Q8HL+| soe4+IP3@;=xkjt{z~=NU');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wd_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
