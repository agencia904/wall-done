<?php
/**
 * 404.php
 *
 * Desenvolvido por Agência 904
 * Copyright © Todos os direitos reservados
 *
 * @link https://www.walldone.com.br/
 * @author https://www.walldone.com.br/
 * @package Wall Done
 * @subpackage Wall Done
 * @since Wall Done 3.0
 */
?>
<?php get_header(); ?>

<section class="common-header">
	<div class="wrap">
		<div class="common-header-container">		
			<nav class="breadcrumb">
				<?php if ( function_exists( 'breadcrumb_trail' ) ) breadcrumb_trail(); ?>
			</nav>
			<h1> Erro 404, página não encontrada  </h1>
		</div>
	</div>
</section>

<section class="common-error-404">
	<div class="wrap">
		<div class="common-error-404-container">
			<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>

			<div class="common-title-404">
				Fique Tranquilo!
			</div>
			<div class="common-description-404">
				Calma, este não é o fim do caminho, existem outras<br>
				direções para continuar. Navegue pelo menu. ou entre em contato!
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>