<?php
/**
 * Template Name: As paredes também falam
 * as-paredes-tambem-falam.php
 *
 * Desenvolvido por Agência 904
 * Copyright © Todos os direitos reservados
 *
 * @link https://www.walldone.com.br/
 * @author https://www.walldone.com.br/
 * @package Wall Done
 * @subpackage Wall Done
 * @since Wall Done 3.0
 */
?>
<?php get_header(); ?>
<section class="common-header">
	<div class="wrap">
		<div class="common-header-container">
			<?php do_action( 'woocommerce_before_main_content' ); ?>
			<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
				<h1 class="woocommerce-products-header__title page-title"><?php the_title(); ?></h1>
			<?php endif; ?>
		</div>
	</div>
</section>

<section class="page-walk-talk">
	<div class="wrap">
		<div class="page-walk-talk-container">
			<div class="page-walk-talk-content">
				<div class="page-walk-talk-row">
					<div class="column">
						<iframe src="https://player.vimeo.com/video/189330068" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
					</div>
					<div class="column">
						<h2>Wall Lovers</h2>
						<p>Todos os que fazem parte da família Wall Done receberam carinhosamente esse título. São parceiros que partilham dos mesmos ideais e acreditam no poder das paredes para transformação dos ambientes e da relação que as pessoas tem com o lugar onde vivem.</p>

						<p>Acredito que muitos de vocês sejam também Wall Lovers sem nunca ter se dado conta disso. Esse vídeo mostra como a decoração das paredes esteve sempre presente em nossas vidas. Tenho certeza que você irá se identificar em algum momento.</p>
					</div>
				</div>
			</div>

			<div class="page-walk-talk-content">
				<figure>
					<img src="<?php bloginfo('template_directory');?>/assets/images/paredes-falam/as-paredes-tambem-falam-walldone.jpg" alt="As paredes também falam | Wall Done" />
				</figure>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>