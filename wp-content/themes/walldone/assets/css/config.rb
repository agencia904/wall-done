# Require any additional compass plugins here.
require "compass-normalize"
require "susy"
require "breakpoint"

# Set this to the root of your project when deployed:
project_type = :stand_alone
http_path = "../../"
sass_dir = "sass"
css_dir = "../../"
images_dir = "../images"
fonts_dir = "../fonts"
javascripts_dir = "../js"
line_comments = false
preferred_syntax = :scss
output_style = :compressed
relative_assets = true
sass_options = {:debug_info=>false}
