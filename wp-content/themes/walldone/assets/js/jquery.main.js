

$(document).ready(function(){


	$('.header-bottom-content-nav .meu-kit').appendTo('.navigation');
	$('.header-bottom-content-nav .search-mobile').prependTo('.navigation');

	// hamburger menu mobile
	var forEach=function(t,o,r){if("[object Object]"===Object.prototype.toString.call(t))for(var c in t)Object.prototype.hasOwnProperty.call(t,c)&&o.call(r,t[c],c,t);else for(var e=0,l=t.length;l>e;e++)o.call(r,t[e],e,t)};
	var hamburgers = document.querySelectorAll(".hamburger");
	if (hamburgers.length > 0) {
		forEach(hamburgers, function(hamburger) {
			hamburger.addEventListener("click", function() {
				this.classList.toggle("is-active");
			}, false);
		});
	}

	// Fecha menu ao pressionar tecla esc
	$(document).keyup(function(e) {
		if (e.keyCode == 27) { // esc keycode

			// remove class do menu-mobile
			$('#open-menu').removeClass('open');
			$('.wall-cart').removeClass("open-wall-cart");
			$('.header-bottom-wall-cart-mobile').removeClass('active');

			$('#menu-mobile').addClass('transition-out');
			setTimeout(function () {
				$('#menu-mobile').removeClass('active');
				$('#menu-mobile').removeClass('transition-out');
			}, 500);

			//fecha o modal
			if ($('#show-modal-open').hasClass('show')) {
				$('#show-modal-open').toggleClass('show');
			}
		}
	});
	
	//MENU MOBILE

	var $mobTrigger = $('.menu-mobile .menu-item-has-children');
	$mobTrigger.append('<i class="fal fa-angle-down"></i>');
	$mobTrigger.each(function (index) {
		var $this = $(this),
			$trigger = $this.find('> i');
		$trigger.on('click', function () {
			var $thisTr = $(this);
			$thisTr.toggleClass('active');
			$this.find('>.sub-menu').slideToggle();
		});
	});

	$(".hamburger-box").click(function(){
		$(this).toggleClass("open");
		$('.wall-cart').removeClass("open-wall-cart");
	});
	
	$(".header-bottom-wall-cart-mobile .fa-cart-plus").click(function(){		
		$('.wall-cart').toggleClass("open-wall-cart");
		$('#menu-mobile').removeClass("active");
		$('.header-bottom-wall-cart-mobile').toggleClass('active');
	});

	// Toggle Menu
    var toggles = document.querySelectorAll(".hamburger-box");

    for (var i = toggles.length - 1; i >= 0; i--) {
		var toggle = toggles[i];
		toggleHandler(toggle);
	};

    function toggleHandler(toggle) {
		toggle.addEventListener( "click", function(e) {
			e.preventDefault();

			if ($('#menu-mobile').hasClass('active')) {
				$('#menu-mobile').addClass('transition-out');

				setTimeout(function () {
					$('#menu-mobile').removeClass('active');
					$('#menu-mobile').removeClass('transition-out');
				}, 500);

			} else {
				$('#menu-mobile').addClass('active');
			}
		});
	}

	//FILTROS ACCORDION

	var $mobTrigger = $('.product-categories .cat-parent');
	$mobTrigger.prepend('<i class="fal fa-angle-right"></i>');
	$mobTrigger.each(function (index) {
		var $this = $(this),
			$trigger = $this.find('> i');
		$trigger.on('click', function () {
			var $thisTr = $(this);
			$thisTr.toggleClass('active');
			$this.find('>.children').slideToggle();
		});
	});

	$(window).scroll(function() {
		if(!$('#menu').hasClass('active')) {
			if ($(this).scrollTop() > 50) {  
				$('header#header').addClass("sticky");
				$('#menu-mobile').addClass("sticky");
			}
			else {
				$('header#header').removeClass("sticky");
				$('#menu-mobile').removeClass("sticky");
			}
		}
	});

	// slider home
	$('section.home-slider .home-slider-item').slick({	    
		arrows: true,
		dots: true,
		infinite: true,
		speed: 800,		
		fade: true,
		autoplay: true,
		autoplaySpeed: 15000,
		cssEase: 'linear',
		lazyLoad: 'ondemand'
	});

	$('section.blog-slider .blog-slider-item').slick({	    
		arrows: true,
		dots: true,
		infinite: true,
		speed: 800,		
		fade: true,
		autoplay: true,
		autoplaySpeed: 15000,
		cssEase: 'linear',
		lazyLoad: 'ondemand'
	});

	$('section.promocoes-slider .promocoes-slider-item').slick({
		arrows: true,
		dots: true,
		infinite: true,
		speed: 800,
		fade: true,
		autoplay: true,
		autoplaySpeed: 15000,
		cssEase: 'linear',
		lazyLoad: 'ondemand'
	});

	// titulo home blog
	$('.home-blog-title-switch').slick({	    
		arrows: false,
		dots: false,
		infinite: true,
		speed: 100,		
		fade: true,
		autoplay: true,
		autoplaySpeed: 1000,
		cssEase: 'linear',
		pauseOnHover:false
	});

	// slider home depoimentos
	$('.home-depoimentos-container').slick({	    
		dots: true,
		infinite: true,
		speed: 800,
		slidesToShow: 1,
		adaptiveHeight: true
	});

	//busca mobile
	$(".search-mobile").click(function () {
	    $('.header-bottom-search-mobile').toggleClass("mobile-hidden");
	    $(this).toggleClass('active');
	});

	// Add focus campo de busca
	$('.search-field').focus(function () {
	    $('.search-submit .fal').addClass('search-submit-color');
	}).blur(function () {
	    $('.search-submit .fal').removeClass('search-submit-color');
	});

	//add auto play vido home sobre
	function updateQueryStringParameter(uri, key, value) {
	  var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
	  var separator = uri.indexOf('?') !== -1 ? "&" : "?";
	  if (uri.match(re)) {
	    return uri.replace(re, '$1' + key + "=" + value + '$2');
	  } else {
	    return uri + separator + key + "=" + value;
	  }
	}

	$('.home-intro-active-video, .meu-kit-intro-active-video').on('click', function() {
	  var url = $('#video-player').attr('src');
	  var Updatedurl = updateQueryStringParameter(url, 'autoplay', 'true');
	  $('#video-player').attr('src', Updatedurl);
	  $('#video-player').addClass('show');
	  $('#video-remove').addClass('hide');
	  $(this).addClass('hide');
	});


	$('#tab-description iframe').wrap('<div class="video-wrapper"></div>');

	$('.checkout-content select').wrap('<div class="select"></div>');

	$(window).resize(function () {
		var $viewWidth = $('.flex-viewport').width(),
			$viewElement = $('.woocommerce-product-gallery__image');

		$viewElement.css('width', $viewWidth);
	});


}); // End Jquery