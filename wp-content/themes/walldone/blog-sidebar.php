<?php
/**
 * blog-sidebar.php
 *
 * Desenvolvido por Agência 904
 * Copyright © Todos os direitos reservados
 *
 * @link https://www.walldone.com.br/
 * @author https://www.walldone.com.br/
 * @package Wall Done
 * @subpackage Wall Done
 * @since Wall Done 3.0
 */
?>
<div class="blog-main-sidebar-content">
	<div class="sidebar-content">
		<div class="sidebar-intro">
			<figure>
				<img src="<?php bloginfo('template_directory');?>/assets/images/perfil-larissa-rothen-walldone.jpg" alt="Larissa Rothen | Wall Done" />
			</figure>
			<h4>Larissa Rothen</h4>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis.</p>

			<ul>
				<li>
					<a href="">
						<i class="fas fa-comment-alt-lines"></i>
						<span>Início</span>
					</a>
				</li>
				<li>
					<a href="https://open.spotify.com/user/movimentocasafeliz?si=e8GJQ_DISMKRypmvlIyl5w" target="_blank">
						<i class="fab fa-spotify"></i>
						<span>Playlists</span>
					</a>
				</li>
				<li>
					<a href="">
						<i class="fas fa-download"></i>
						<span>Downloads</span>
					</a>
				</li>
				<li>
					<a href="">
						<i class="fas fa-star"></i>
						<span>Parcerias</span>
					</a>
				</li>
			</ul>

			<div class="sidebar-intro-social">
				<h5>Siga a Wall Done</h5>
				<a href="">
					<i class="fab fa-instagram"></i>
				</a>

				<a href="">
					<i class="fab fa-facebook"></i>
				</a>

				<a href="">
					<i class="fab fa-pinterest"></i>
				</a>
			</div>
		</div>
		
		<div class="sidebar-categories">
			<h4>Categorias:</h4>

			<ul>
				<?php
                foreach (get_categories() as $category){
                    echo "<li><a href='".get_category_link( $category->term_id )."'>";
                    echo $category->name;
                    echo "</a></li>";
                  }
				?>
			</ul>
		</div>

		<div class="sidebar-search">
			<h4>Busca:</h4>
			<form role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>" class="sidebar-search-form">
				<input type="search" class="search-field" required placeholder="<?php echo esc_attr_x( 'Busque por posts', 'placeholder', 'Wall Done' ); ?>" value="<?php echo get_search_query(); ?>" name="s" autocomplete="off" />
				<button type="submit" class="search-submit">
					<?php echo _x( '<i class="fal fa-search"></i>', 'submit button', 'Wall Done' ); ?>
				</button>
			</form>
		</div>

		<div class="sidebar-newsletter">
			<h4>Newsletter</h4>
			
			<?php echo do_shortcode('[contact-form-7 id="102" title="Newsletter"]'); ?>
		</div>
	</div>
</div>