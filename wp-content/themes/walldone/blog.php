<?php
/**
 * Template Name: Blog da Larissa
 * blog.php
 *
 * Desenvolvido por Agência 904
 * Copyright © Todos os direitos reservados
 *
 * @link https://www.walldone.com.br/
 * @author https://www.walldone.com.br/
 * @package Wall Done
 * @subpackage Wall Done
 * @since Wall Done 3.0
 */
?>
<?php get_header(); ?>
<section class="blog-header">
	<div class="wrap">
		<div class="blog-header-container">
			<?php do_action( 'woocommerce_before_main_content' ); ?>
			<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
				<h1>
					<i class="fal fa-comment-alt-lines"></i>
					<span><?php the_title(); ?></span>
				</h1>
			<?php endif; ?>
		</div>
	</div>
</section>

<section class="blog-slider">
	<div class="wrap">
		<div class="blog-slider-item">
			<?php
			$args = array(
			  'posts_per_page'	=>  '-1',
			  'post_type' 		=> 'slider_blog',
			  'orderby'   		=> 'meta_value_num',
			  'order'     		=> 'DESC'
			  );
			$query = new WP_Query( $args );
			if($query->have_posts()) : while($query->have_posts()) : $query->the_post(); ?>
				<div class="blog-slider-item-container">
					<div class="blog-slider-bg" style="background-image: url(<?php echo the_post_thumbnail_url('full'); ?>);"></div>
					<div class="blog-slider-container">
						<div class="blog-slider-content">
							<div class="blog-slider-title"><?php the_title(); ?></div>
							<div class="blog-slider-desc"><?php the_content(); ?></div>
							<?php if(get_post_meta( get_the_ID(), 'slider_link', true) && get_post_meta( get_the_ID(), 'slider_link_title', true)) : ?>
							<div class="box-button">
								<a href="<?php echo get_post_meta( get_the_ID(), 'slider_link', true); ?>" class="btn-sing">
									<span><i class="fas fa-caret-right"></i> <?php echo get_post_meta( get_the_ID(), 'slider_link_title', true); ?></span>
								</a>
							</div>
							<?php endif; ?>
						</div>	
					</div>
				</div>
			 	<?php endwhile; ?>
			<?php endif;?>		
		</div>
	</div>
</section>

<section class="blog-main">
	<div class="wrap">
		<div class="blog-main-container">
			<div class="blog-main-content">
				<div class="blog-list-row purple">
					<div class="blog-list-row-title">
						<div class="column">
							<?php 
							  $cat_id = get_category_by_slug('categoria-1'); 
							  $cat_title = $cat_id->name;
							?>
							<a href="<?php echo get_category_link($cat_id); ?>">
								<h2><i class="fas fa-caret-right"></i> <span><?php echo $cat_title; ?></span></h2>
							</a>
						</div>
						<div class="column">
							<a href="<?php echo get_category_link($cat_id); ?>">
								<i class="fal fa-list-alt"></i>
								<span>VER TODOS</span>
							</a>
						</div>
					</div>
					<div class="blog-list-row-content">
						<?php
						$blog = array(
						'posts_per_page' 	=>  '2',
						'post_type'			=> 'post',
						'category_name' 	=> 'categoria-1',
						'order'    			=> 'DESC',
						'oderby'			=> 'date menu_order'
						);
						$query = new WP_Query( $blog ); 

						if($query->have_posts()) : while($query->have_posts()) : $query->the_post(); ?>
							<div class="blog-list-post">
								<a href="<?php print get_permalink($post->ID) ?>">
									<figure class="blog-list-post-image">
										<?php if (has_post_thumbnail()) : ?>
											<?php the_post_thumbnail(); ?>
										<?php else : ?>
											<img src="<?php bloginfo('template_directory');?>/assets/images/imagem-nao-disponivel-walldone.jpg" alt="Imagem não disponível | Wall Done" />	
										<?php endif; ?>    
									</figure>
									<div class="blog-list-post-desc">
										<div class="blog-list-post-info">
											<span>por <b><?php echo get_the_author(); ?></b> em <b><?php echo get_the_date(); ?></b></span> 
										</div>
										<h3><?php echo the_title(); ?></h3>
									</div>
								</a>						
							</div>
						<?php endwhile; ?>
						<?php else : ?>
							<div class="woocommerce-message woocommerce-message--info woocommerce-Message woocommerce-Message--info woocommerce-info">
								Ainda não existem posts a serem listados para esta categoria.
							</div>
						<?php endif; ?>
						<?php wp_reset_query(); ?>
					</div>
				</div>

				<div class="blog-list-row yellow">
					<div class="blog-list-row-title">
						<div class="column">
							<?php 
							  $cat_id = get_category_by_slug('categoria-2'); 
							  $cat_title = $cat_id->name;
							?>
							<a href="<?php echo get_category_link($cat_id); ?>">
								<h2><i class="fas fa-caret-right"></i> <span><?php echo $cat_title; ?></span></h2>
							</a>
						</div>
						<div class="column">
							<a href="<?php echo get_category_link($cat_id); ?>">
								<i class="fal fa-list-alt"></i>
								<span>VER TODOS</span>
							</a>
						</div>
					</div>
					<div class="blog-list-row-content">
						<?php
						$blog = array(
						'posts_per_page' 	=>  '2',
						'post_type'			=> 'post',
						'category_name' 	=> 'categoria-2',
						'order'    			=> 'DESC',
						'oderby'			=> 'date menu_order'
						);
						$query = new WP_Query( $blog ); 

						if($query->have_posts()) : while($query->have_posts()) : $query->the_post(); ?>
							<div class="blog-list-post">
								<a href="<?php print get_permalink($post->ID) ?>">
									<figure class="blog-list-post-image">
										<?php if (has_post_thumbnail()) : ?>
											<?php the_post_thumbnail(); ?>
										<?php else : ?>
											<img src="<?php bloginfo('template_directory');?>/assets/images/imagem-nao-disponivel-walldone.jpg" alt="Imagem não disponível | Wall Done" />	
										<?php endif; ?>    
									</figure>
									<div class="blog-list-post-desc">
										<div class="blog-list-post-info">
											<span>por <b><?php echo get_the_author(); ?></b> em <b><?php echo get_the_date(); ?></b></span> 
										</div>
										<h3><?php echo the_title(); ?></h3>
									</div>
								</a>						
							</div>
						<?php endwhile; ?>
						<?php else : ?>
							<div class="woocommerce-message woocommerce-message--info woocommerce-Message woocommerce-Message--info woocommerce-info">
								Ainda não existem posts a serem listados para esta categoria.
							</div>
						<?php endif; ?>
						<?php wp_reset_query(); ?>
					</div>
				</div>

				<div class="blog-list-row red">
					<div class="blog-list-row-title">
						<div class="column">
							<?php 
							  $cat_id = get_category_by_slug('categoria-3'); 
							  $cat_title = $cat_id->name;
							?>
							<a href="<?php echo get_category_link($cat_id); ?>">
								<h2><i class="fas fa-caret-right"></i> <span><?php echo $cat_title; ?></span></h2>
							</a>
						</div>
						<div class="column">
							<a href="<?php echo get_category_link($cat_id); ?>">
								<i class="fal fa-list-alt"></i>
								<span>VER TODOS</span>
							</a>
						</div>
					</div>
					<div class="blog-list-row-content">
						<?php
						$blog = array(
						'posts_per_page' 	=>  '2',
						'post_type'			=> 'post',
						'category_name' 	=> 'categoria-3',
						'order'    			=> 'DESC',
						'oderby'			=> 'date menu_order'
						);
						$query = new WP_Query( $blog ); 

						if($query->have_posts()) : while($query->have_posts()) : $query->the_post(); ?>
							<div class="blog-list-post">
								<a href="<?php print get_permalink($post->ID) ?>">
									<figure class="blog-list-post-image">
										<?php if (has_post_thumbnail()) : ?>
											<?php the_post_thumbnail(); ?>
										<?php else : ?>
											<img src="<?php bloginfo('template_directory');?>/assets/images/imagem-nao-disponivel-walldone.jpg" alt="Imagem não disponível | Wall Done" />	
										<?php endif; ?>    
									</figure>
									<div class="blog-list-post-desc">
										<div class="blog-list-post-info">
											<span>por <b><?php echo get_the_author(); ?></b> em <b><?php echo get_the_date(); ?></b></span> 
										</div>
										<h3><?php echo the_title(); ?></h3>
									</div>
								</a>						
							</div>
						<?php endwhile; ?>
						<?php else : ?>
							<div class="woocommerce-message woocommerce-message--info woocommerce-Message woocommerce-Message--info woocommerce-info">
								Ainda não existem posts a serem listados para esta categoria.
							</div>
						<?php endif; ?>
						<?php wp_reset_query(); ?>
					</div>
				</div>
			</div>
			<aside class="blog-main-sidebar">
				<?php include_once('blog-sidebar.php'); ?>
			</aside>
		</div>
	</div>
</section>
<?php get_footer(); ?>