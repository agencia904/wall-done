<?php
/**
 * Template Name: Cadastrar
 * cadastrar.php
 *
 * Desenvolvido por Agência 904
 * Copyright © Todos os direitos reservados
 *
 * @link https://www.walldone.com.br/
 * @author https://www.walldone.com.br/
 * @package Wall Done
 * @subpackage Wall Done
 * @since Wall Done 3.0
 */
?>
<?php get_header(); ?>

<section class="common-header">
	<div class="wrap">
		<div class="common-header-container">		
			<nav class="breadcrumb">
				<?php if ( function_exists( 'breadcrumb_trail' ) ) breadcrumb_trail(); ?>
			</nav>
			<h1> Seja um Wall Lover! </h1>
		</div>
	</div>
</section>

<section class="meu-kit-video">
	<div class="wrap">
		<div class="meu-kit-video-container">
			<div class="column">

				<?php echo do_shortcode('[woocommerce_simple_registration]'); ?>

			</div>	
		</div>
	</div>
</section>


<?php get_footer(); ?>