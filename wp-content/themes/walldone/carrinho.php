<?php
/**
 * Template Name: Carrinho de Compras
 * carrinho.php
 *
 * Desenvolvido por Agência 904
 * Copyright © Todos os direitos reservados
 *
 * @link https://www.walldone.com.br/
 * @author https://www.walldone.com.br/
 * @package Wall Done
 * @subpackage Wall Done
 * @since Wall Done 3.0
 */
?>
<?php get_header(); ?>

<section class="common-header">
	<div class="wrap">
		<div class="common-header-container">
			<?php do_action( 'woocommerce_before_main_content' ); ?>
			<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
				<h1 class="woocommerce-products-header__title page-title"><?php the_title(); ?></h1>
			<?php endif; ?>
		</div>
	</div>
</section>

<section class="carrinho">
	<div class="wrap">
		<div class="carrinho-container">
			<div class="carrinho-title">
				<h2>Estamos quase lá...</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididut ut.</p>
			</div>
			<div class="carrinho-content">
				<?php echo do_shortcode("[woocommerce_cart]"); ?>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>
