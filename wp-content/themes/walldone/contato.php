<?php
/**
 * Template Name: Contato
 * contato.php
 *
 * Desenvolvido por Agência 904
 * Copyright © Todos os direitos reservados
 *
 * @link https://www.walldone.com.br/
 * @author https://www.walldone.com.br/
 * @package Wall Done
 * @subpackage Wall Done
 * @since Wall Done 3.0
 */
?>
<?php get_header(); ?>
<section class="common-header">
	<div class="wrap">
		<div class="common-header-container">
			<?php do_action( 'woocommerce_before_main_content' ); ?>
			<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
				<h1 class="woocommerce-products-header__title page-title"><?php the_title(); ?></h1>
			<?php endif; ?>
		</div>
	</div>
</section>

<section class="page-contato">
	<div class="wrap">
		<div class="page-contato-container">
			<div class="page-contato-content">
				<div class="column">
					<div class="column-title">
						<h2>Fale com a Wall Done</h2>
						<p>Olá Wall Lover, tudo bem? Tem alguma dúvida, aconteceu algum problema ou quer dar alguma sugestão? Entre em contato através do formulário.</p>
					</div>

					<div class="column-info">
						<ul>
							<li>
								<div class="column-info-icon">
									<i class="fal fa-file-signature"></i>
								</div>
								<div class="column-info-content">
									<b>Razão social:</b>
									<span>Wall Done</span>
								</div>
							</li>
							<li>
								<div class="column-info-icon">
									<i class="fal fa-file-alt"></i>
								</div>
								<div class="column-info-content">
									<b>CNPJ:</b>
									<span>25.336.655/0001-66</span>
								</div>
							</li>
							<li>
								<div class="column-info-icon">
									<i class="fal fa-phone"></i>
								</div>
								<div class="column-info-content">
									<b>Telefone:</b>
									<span>(41) 3049-1881</span>
								</div>
							</li>
							<li>
								<div class="column-info-icon">
									<i class="fab fa-whatsapp"></i>
								</div>
								<div class="column-info-content">
									<b>WhatsApp:</b>
									<span>(41) 99903-0711</span>
								</div>
							</li>
							<li>
								<div class="column-info-icon">
									<i class="fal fa-map-marker-alt"></i>
								</div>
								<div class="column-info-content">
									<b>Endereço:</b>
									<span>Av. Presidente Affonso Camargo, 633, sala 13 <small>Cristo Rei - Curitiba/PR</small></span>
								</div>
							</li>
							<li>
								<div class="column-info-icon">
									<i class="fal fa-at"></i>
								</div>
								<div class="column-info-content">
									<b>E-mail:</b>
									<span>falecom@walldone.com.br</span>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="column">
					<div class="column-form">
						<?php echo do_shortcode('[contact-form-7 id="392" title="Contato"]'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>