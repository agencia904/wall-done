<?php
/**
 * Template Name: Finalizar
 * finalizar.php
 *
 * Desenvolvido por Agência 904
 * Copyright © Todos os direitos reservados
 *
 * @link https://www.walldone.com.br/
 * @author https://www.walldone.com.br/
 * @package Wall Done
 * @subpackage Wall Done
 * @since Wall Done 3.0
 */
?>
<?php get_header(); ?>

<section class="common-header">
	<div class="wrap">
		<div class="common-header-container">		
			<nav class="breadcrumb">
				<?php if ( function_exists( 'breadcrumb_trail' ) ) breadcrumb_trail(); ?>
			</nav>
			<h1> Monte seu Kit com a Larissa! </h1>
		</div>
	</div>
</section>

<section class="common-passos">
	<div class="wrap">
		<div class="common-passos-container">
			<div class="column">
				<div class="box active">
					1
				</div>
				<h2 class="active">briefing do projeto</h2>
			</div>		
			<div class="column">
				<div class="box active">
					2
				</div>
				<h2 class="active">Tudo certo?</h2>
			</div>
			<div class="column">
				<div class="box active">
					3
				</div>
				<h2 class="active">Finalizar</h2>
			</div>		
		</div>
	</div>
</section>

<section id="finalizar-titulo" class="common-passos-checkout">
	<div class="wrap">
		<div class="common-passos-checkout-container">
			<h3>Confira ser está tudo certinho</h3>
			<p>Lorem ipsum dolor sit ame, consectetur adipisicing elit, sed do eiusmod tempor.</p>
		</div>
	</div>
</section>

<section class="carrinho-de-compras">
	<div class="wrap">
		<div class="carrinho-de-compras-container">
			<div class="woocommerce">
				
				<?php
					wc_print_notices();
					do_action( 'woocommerce_before_cart' );
				?>

				<form class="woocommerce-cart-form" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">
					<?php do_action( 'woocommerce_before_cart_table' ); ?>

					<div class="shop_table shop_table_responsive cart woocommerce-cart-form__contents" cellspacing="0">
						<div class="common-title">
							<ul>
								<li><?php esc_html_e( 'Product', 'woocommerce' ); ?></li>
								<li><?php esc_html_e( 'Price', 'woocommerce' ); ?></li>
								<li><?php esc_html_e( 'Quantity', 'woocommerce' ); ?></li>
								<li><?php esc_html_e( 'Total', 'woocommerce' ); ?></li>
							</ul>
						</div>
						<div id="box-produtos-contant">
							<?php do_action( 'woocommerce_before_cart_contents' ); ?>

							<?php
							foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
								$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
								$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

								if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
									$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
									?>
									<div class="informacoes-produtos woocommerce-cart-form__cart-item <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">
										<div class="column">
											<div class="box">								
												<div class="product-remove">
													<?php
														echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
															'<a title="Remover" href="%s" class="remove" aria-label="%s" data-product_id="%s" data-product_sku="%s"><i class="fal fa-times"></i></a>',
															esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
															__( 'Remove this item', 'woocommerce' ),
															esc_attr( $product_id ),
															esc_attr( $_product->get_sku() )
														), $cart_item_key );
													?>
												</div>
												<div class="product-thumbnail">
													<?php
													$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

													if ( ! $product_permalink ) {
														echo wp_kses_post( $thumbnail );
													} else {
														printf( '<a href="%s">%s</a>', esc_url( $product_permalink ), wp_kses_post( $thumbnail ) );
													}
													?>
												</div>
											</div>
										</div>

										<div class="column">
											<div class="box">								
												<div class="product-price" data-title="<?php esc_attr_e( 'Price', 'woocommerce' ); ?>">
													<?php
														echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
													?>
												</div>
											</div>
										</div>

										<div class="column">
											<div class="box">								
												<div class="product-quantity" data-title="<?php esc_attr_e( 'Quantity', 'woocommerce' ); ?>">
													<?php
													if ( $_product->is_sold_individually() ) {
														$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
													} else {
														$product_quantity = woocommerce_quantity_input( array(
															'input_name'   => "cart[{$cart_item_key}][qty]",
															'input_value'  => $cart_item['quantity'],
															'max_value'    => $_product->get_max_purchase_quantity(),
															'min_value'    => '0',
															'product_name' => $_product->get_name(),
														), $_product, false );
													}
													echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item );
													?>
												</div>
											</div>
										</div>
										<div class="column">
											<div class="box">								
												<div class="product-subtotal" data-title="<?php esc_attr_e( 'Total', 'woocommerce' ); ?>">
													<?php
														echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key );
													?>
												</div>
											</div>
										</div>
									</div>
									<?php
								}
							}
							?>

							<?php do_action( 'woocommerce_cart_contents' ); ?>

							<div class="product-cupom">
								<div colspan="6" class="actions">
									<?php if ( wc_coupons_enabled() ) { ?>
										<div class="coupon">
											<label for="coupon_code"><?php esc_html_e( 'Coupon:', 'woocommerce' ); ?></label>

											<input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="CUPON DE DESCONTO" />

											<button type="submit" class="button" name="apply_coupon" value="<?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?>"><?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?></button>

											<?php do_action( 'woocommerce_cart_coupon' ); ?>
										</div>
									<?php } ?>

									<button type="submit" class="button update-cart" name="update_cart" value="<?php esc_attr_e( 'Update cart', 'woocommerce' ); ?>"><?php esc_html_e( 'Update cart', 'woocommerce' ); ?></button>

									<?php do_action( 'woocommerce_cart_actions' ); ?>

									<?php wp_nonce_field( 'woocommerce-cart', 'woocommerce-cart-nonce' ); ?>
								</div>
							</div>

							<?php do_action( 'woocommerce_after_cart_contents' ); ?>
						</div>
					</div>
					<?php do_action( 'woocommerce_after_cart_table' ); ?>
				</form>

				<div class="cart-collaterals">
					<?php do_action( 'woocommerce_cart_collaterals' ); ?>
				</div>

				<?php do_action( 'woocommerce_after_cart' ); ?>

			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>