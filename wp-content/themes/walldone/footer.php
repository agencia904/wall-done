<?php
/**
 * footer.php
 *
 * Desenvolvido por Agência 904
 * Copyright © Todos os direitos reservados
 *
 * @link https://www.walldone.com.br/
 * @author https://www.walldone.com.br/
 * @package Wall Done
 * @subpackage Wall Done
 * @since Wall Done 3.0
 */
?>

<div class="clearfix footer"></div>

<footer id="footer">
	<div class="footer-social">
		<ul>
			<li>
				<a href="https://www.instagram.com/walldonedecor/" target="_blank" title="Instagram | Wall Done" target="_blank" class="social-icon">
					<i class="fab fa-instagram"></i>
				</a>
			</li>
			<li>
				<a href="https://www.facebook.com/walldonedecor/" target="_blank" title="Facebook | Wall Done" target="_blank" class="social-icon">
					<i class="fab fa-facebook-f"></i>
				</a>
			</li>
			<li>
				<a href="https://br.pinterest.com/walldonedecor/" target="_blank" title="Pinterest | Wall Done" target="_blank" class="social-icon">
					<i class="fab fa-pinterest"></i>
				</a>
			</li>
		</ul>
	</div>

	<div class="footer-container">
		<div class="wrap">
			<div class="footer-content">
				<div class="column small">
					<h5>Sobre</h5>
					<ul class="footer-about">
						<li>
							<a href="<?php echo site_url(); ?>/blog-da-larissa">Blog da Larissa</a>
						</li>
						<li>
							<a href="<?php echo site_url(); ?>/sobre">A Wall Done</a>
						</li>
						<li>
							<a href="<?php echo site_url(); ?>/as-paredes-tambem-falam">As paredes também falam</a>
						</li>
						<li>
							<a href="<?php echo site_url(); ?>/promocoes">Promoções</a>
						</li>
						<li>
							<a href="<?php echo site_url(); ?>/vendas-atacado">Vendas no atacado</a>
						</li>
						<li>
							<a href="<?php echo site_url(); ?>/minha-conta">Minha conta</a>
						</li>
						<li>
							<a href="<?php echo site_url(); ?>/trocas-e-devolucoes">Trocas e Devoluções</a>
						</li>
					</ul>
				</div>
				<div class="column">
					<h5>Pagamento e Segurança</h5>				
					<figure class="pagamento-e-seguranca">
						<img src="<?php bloginfo('template_directory');?>/assets/images/footer-pagamento-e-seguranca-walldone.png" alt="Pagamento e Segurança | Wall Done">
					</figure>

					<ul class="footer-info">
						<li>
							<i class="fas fa-shield-check"></i> 
							Lorem ipsum dolor sit amet, consectetur.
						</li>
						<li>
							<i class="fas fa-exclamation-circle"></i> 
							Wall Done - CNPJ: 25.336.655/0001-66
						</li>
					</ul>

					<figure class="verificado-e-protegido">
						<img src="<?php bloginfo('template_directory');?>/assets/images/footer-verificado-e-protegido-walldone.png" alt="Pagamento e Segurança | Wall Done">						
					</figure>
				</div>
				<div class="column">
					<h5>Newsletter</h5>				
					<p>Cadastre-se e receba as melhores novidades e ofertas!</p>
					<div class="footer-newsletter">			
						<?php echo do_shortcode('[contact-form-7 id="102" title="Newsletter"]'); ?>
					</div>
				</div>
			</div>

			<div class="footer-contato">
				<ul>
					<li><i class="fal fa-phone"></i> 41 3049 1881</li>
					<li><i class="fab fa-whatsapp"></i> 41 99903 0711</li>
					<li><i class="fal fa-comment-alt-lines"></i> Contato online</li>
				</ul>
			</div> 
		</div>
	</div>

	<div class="footer-copyright">
		<a href="http://904.ag" target="_blank">Desenvolvido por 904.ag © Copyright <?php echo date('Y'); ?></a>
	</div>
</footer>
</div>

<script>
	// mensagens ao adicionar ao carrinho
	// $(document).on('click','.add_to_cart_button, .single_add_to_cart_button',function(e) {
	// 	e.preventDefault();
	// 	addedToCart();
	// });

	// $(document).on('click','.remove_from_cart_button',function(e) {
	// 	e.preventDefault();
	// 	removeFromCart();
	// });

	// function addedToCart(){
	// 	$.notify("<i class='fal fa-grin-hearts'></i> <b>Sucesso!</b> Produto adicionado ao carrinho.", {type: "success", class:"notify-success tr", verticalAlign:"top", align: "right", animationType:"drop", close: true});
	// }

	// function removeFromCart(){
	// 	$.notify("<i class='fal fa-sad-cry'></i> <b>Ahhh!</b> Produto removido do carrinho.", {type: "error", class:"notify-error tr", verticalAlign:"top", align: "right", animationType:"drop", close: true});
	// }

	// jQuery.noConflict();
	// jQuery( document ).ready(function( $ ) {
	// // menu mobile accordion
	// jQuery('#menu-mobile > ul > li > a, #accordion > ul > li > a').click(function() {
	// 	var checkElement = jQuery(this).next();
	//   	jQuery('#menu-mobile li, #accordion li').removeClass('active');
	//   	jQuery(this).closest('li').addClass('active');

	//   	if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
	//   		jQuery(this).closest('li').removeClass('active');
	//   		checkElement.slideUp('normal');
	//   	}
	//   	if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
	//   		jQuery('#menu-mobile ul ul:visible, #accordion ul ul:visible').slideUp('normal');
	//   		checkElement.slideDown('normal');
	//   	}
	//   	if(jQuery(this).closest('li').find('ul').children().length == 0) {
	//   		return true;
	//   	} else {
	//   		return false;
	//   	}
	// });

	// if(jQuery('#accordion > ul > li').hasClass('active')){
	// 	var checkElement = jQuery('#accordion > ul > li.active > a').next();
	//   	jQuery('#accordion li').removeClass('active');
	//   	jQuery('#accordion > ul > li > a').closest('li').addClass('active');

	//   	if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
	//   		jQuery('#accordion > ul > li > a').closest('li').removeClass('active');
	//   		checkElement.slideUp('normal');
	//   	}
	//   	if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
	//   		jQuery('#accordion ul ul:visible').slideUp('normal');
	//   		checkElement.slideDown('normal');
	//   	}
	//   	if(jQuery('#accordion > ul > li > a').closest('li').find('ul').children().length == 0) {
	//   		return true;
	//   	} else {
	//   		return false;
	//   	}
	// } else {
	//   	var checkElement = jQuery('#accordion > ul > li:first-child > a').next();
	//   	jQuery('#accordion li').removeClass('active');
	//   	jQuery('#accordion > ul > li > a').closest('li').addClass('active');

	//   	if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
	//   		jQuery('#accordion > ul > li > a').closest('li').removeClass('active');
	//   		checkElement.slideUp('normal');
	//   	}
	//   	if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
	//   		jQuery('#accordion ul ul:visible').slideUp('normal');
	//   		checkElement.slideDown('normal');
	//   	}
	//   	if(jQuery('#accordion > ul > li > a').closest('li').find('ul').children().length == 0) {
	//   		return true;
	//   	} else {
	//   		return false;
	//   	}
	//   }
	// });
</script>
<?php wp_footer(); ?>
</body>
</html>
