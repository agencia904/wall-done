<?php
/**
 * functions.php
 *
 * Desenvolvido por Agência 904
 * Copyright © Todos os direitos reservados
 *
 * @link https://www.walldone.com.br/
 * @author https://www.walldone.com.br/
 * @package Wall Done
 * @subpackage Wall Done
 * @since Wall Done 3.0
 */


add_theme_support( 'woocommerce' );

add_filter( 'woocommerce_enqueue_styles', 'jk_dequeue_styles' );
function jk_dequeue_styles( $enqueue_styles ) {
    // unset( $enqueue_styles['woocommerce-general'] );    // Remove the gloss
    unset( $enqueue_styles['woocommerce-layout'] );     // Remove the layout
    // unset( $enqueue_styles['woocommerce-smallscreen'] );    // Remove the smallscreen optimisation
    return $enqueue_styles;
}

// Adiciona funcionalidades a imagem do produto
add_action( 'after_setup_theme', 'walldone_setup' );

function walldone_setup() {
    add_theme_support( 'wc-product-gallery-zoom' );
    // add_theme_support( 'wc-product-gallery-lightbox' );
    add_theme_support( 'wc-product-gallery-slider' );

    //Menu da loja
    register_nav_menus( array(
        'store_menu' => __( 'Menu da Loja' ),
    ) );

    //Menu da loja
    register_nav_menus( array(
        'mobile_menu' => __( 'Menu Mobile' ),
    ) );

    //Menu do rodapé
    register_nav_menus( array(
        'footer_menu' => __( 'Menu do Rodapé' ),
    ) );

}

function walldone_widgets_init() {

    register_sidebar( array(
        'name' => __( 'Coluna lateral da loja', 'walldone' ),
        'id' => 'sidebar-1',
        'before_widget' => '<aside id="%1$s" class="widget %2$s panel panel-default">',
        'after_widget' => "</aside>",
        'before_title' => '<div class="panel-heading"> <h3 class="panel-title">',
        'after_title' => '</h3></div>',
    ) );
}
add_action( 'init', 'walldone_widgets_init' );

function filter_ptags_on_images($content) {
    $content = preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
    return preg_replace('/<p>\s*(<iframe .*>*.<\/iframe>)\s*<\/p>/iU', '\1', $content);
}
add_filter('acf_the_content', 'filter_ptags_on_images');
add_filter('the_content', 'filter_ptags_on_images');

// hide wordpress dashboard bar
show_admin_bar( false );

// adiciona placeholders nos inputs do formulário do contato
add_filter( 'comment_form_default_fields', 'comment_placeholders' );
function comment_placeholders( $fields )
{
    $fields['author'] = str_replace(
        '<input',
        '<input placeholder="Nome"',
        $fields['author']
        );
    $fields['email'] = str_replace(
        '<input',
        '<input placeholder="Email"',
        $fields['email']
        );
    unset($fields['url']);

    return $fields;
}

// adiciona placeholders no textarea do formulário do contato
add_filter( 'comment_form_defaults', 'textarea_placeholder' );

function textarea_placeholder( $fields )
{
    $fields['comment_field'] = str_replace(
        '<textarea',
        '<textarea placeholder="Mensagem"',
        $fields['comment_field']
        );

    return $fields;
}

// remove o css do breadcrumbs
add_action( 'after_setup_theme', 'bct_theme_setup' );

function bct_theme_setup() {
    add_theme_support( 'breadcrumb-trail' );
}

function wpdocs_custom_excerpt_length( $length ) {
    return 15;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );

function wpdocs_excerpt_more( $more ) {
    return '...';
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );

add_filter( 'use_default_gallery_style', '__return_false' );

add_filter('wpcf7_form_elements', function($content) {
    $content = preg_replace('/<(span).*?class="\s*(?:.*\s)?wpcf7-form-control-wrap(?:\s[^"]+)?\s*"[^\>]*>(.*)<\/\1>/i', '\2', $content);

    return $content;
});

function my_login_logo() { ?>
<style type="text/css">
    #login {
        padding: 6% 0 0 !important;
    }

    #login h1 a, .login h1 a {
        background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo-walldone.png);
        height: 60px;
        width: 305px;
        background-size: auto auto;
        background-repeat: no-repeat;
        padding-bottom: 10px;
    }
</style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );


if ( function_exists( 'add_theme_support' ) ) {
    add_theme_support( 'post-thumbnails', array( 'post' ) );
    set_post_thumbnail_size( 870, 550, true );
}

// Imagens Posts
if ( function_exists( 'add_theme_support' ) ) {
    add_theme_support( 'post-thumbnails', array( 'slider') );
    set_post_thumbnail_size( 1600, 800, true );
}

if ( function_exists( 'add_theme_support' ) ) {
    add_theme_support( 'post-thumbnails', array( 'slider_blog') );
    set_post_thumbnail_size( 1200, 600, true );
}

// Post Types sliders
function setup_sliders_cpt(){
    $labels = array(
        'name' => _x('Slider', 'post type general name'),
        'singular_name' => _x('Slider', 'post type singular name'),
        'add_new' => _x('Adicionar novo', 'Novo Item'),
        'add_new_item' => __('Adicionar Novo Slider'),
        'edit_item' => __('Editar Slider'),
        'new_item' => __('Novo Slider'),
        'all_items' => __('Todos os Sliders'),
        'view_item' => __('Ver Slider'),
        'search_items' => __('Procurar Slider'),
        'not_found' => __('Nenhum Slider encontrado'),
        'not_found_in_trash' => __('Nenhum Slider encontrado no lixo'),
        'parent_item_colon' => '',
        'menu_name' => 'Slider'
        );
    $args = array(
        'labels' => $labels,
        'description' => 'Slider',
        'rewrite' => array('slug' => 'slider'),
        'public' => true,
        'exclude_from_search' => true,
        'menu_position' => 7,
        'supports' => array('title', 'editor', 'thumbnail', 'excerpt', 'custom-fields', 'page-attributes'),
        'has_archive' => false,
        'taxonomies' => array(''),
        'menu_icon' => 'dashicons-welcome-view-site',
        );
    register_post_type('slider', $args);
}

add_action('init', 'setup_sliders_cpt');

function setup_sliders_blog_cpt(){
    $labels = array(
        'name' => _x('Slider Blog', 'post type general name'),
        'singular_name' => _x('Slider Blog', 'post type singular name'),
        'add_new' => _x('Adicionar novo', 'Novo Item'),
        'add_new_item' => __('Adicionar Novo Slider'),
        'edit_item' => __('Editar Slider'),
        'new_item' => __('Novo Slider'),
        'all_items' => __('Todos os Sliders'),
        'view_item' => __('Ver Slider'),
        'search_items' => __('Procurar Slider'),
        'not_found' => __('Nenhum Slider encontrado'),
        'not_found_in_trash' => __('Nenhum Slider encontrado no lixo'),
        'parent_item_colon' => '',
        'menu_name' => 'Slider Blog'
        );
    $args = array(
        'labels' => $labels,
        'description' => 'Slider Blog',
        'rewrite' => array('slug' => 'slider-blog'),
        'public' => true,
        'exclude_from_search' => true,
        'menu_position' => 8,
        'supports' => array('title', 'editor', 'thumbnail', 'excerpt', 'custom-fields', 'page-attributes'),
        'has_archive' => false,
        'taxonomies' => array(''),
        'menu_icon' => 'dashicons-welcome-view-site',
        );
    register_post_type('slider_blog', $args);
}

add_action('init', 'setup_sliders_blog_cpt');

//Slider de produtos em promoção
function setup_sliders_promo_cpt(){
    $labels = array(
        'name' => _x('Slider Promoções', 'post type general name'),
        'singular_name' => _x('Slider Blog', 'post type singular name'),
        'add_new' => _x('Adicionar novo', 'Novo Item'),
        'add_new_item' => __('Adicionar Novo Slider'),
        'edit_item' => __('Editar Slider'),
        'new_item' => __('Novo Slider'),
        'all_items' => __('Todos os Sliders'),
        'view_item' => __('Ver Slider'),
        'search_items' => __('Procurar Slider'),
        'not_found' => __('Nenhum Slider encontrado'),
        'not_found_in_trash' => __('Nenhum Slider encontrado no lixo'),
        'parent_item_colon' => '',
        'menu_name' => 'Slider Promoções'
        );
    $args = array(
        'labels' => $labels,
        'description' => 'Slider Promoções',
        'rewrite' => array('slug' => 'slider-promo'),
        'public' => true,
        'exclude_from_search' => true,
        'menu_position' => 9,
        'supports' => array('title', 'editor', 'thumbnail', 'excerpt', 'custom-fields', 'page-attributes'),
        'has_archive' => false,
        'taxonomies' => array(''),
        'menu_icon' => 'dashicons-welcome-view-site',
        );
    register_post_type('slider_promo', $args);
}

add_action('init', 'setup_sliders_promo_cpt');


// Depoimentos
function depoimentos_register() {

    $labels = array(
        'name' => _x('Depoimentos', 'post type general name'),
        'singular_name' => _x('depoimento', 'post type singular name'),
        'add_new' => _x('Adicionar novo', 'depoimento item'),
        'add_new_item' => __('Adicionar novo Depoimento'),
        'edit_item' => __('Editar Depoimento'),
        'new_item' => __('Novo Depoimento'),
        'view_item' => __('Ver Depoimento'),
        'search_items' => __('Buscar Depoimento'),
        'not_found' => __('Nenhum Depoimento Encontrado'),
        'not_found_in_trash' => __('Nenhum Depoimento Encontrado no Lixo'),
        'parent_item_colon' => ''
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'menu_position'       => 8,
        'menu_icon' => 'dashicons-admin-comments',
        'rewrite' => array( 'slug' => 'depoimento', 'with_front'=> false ),
        'capability_type' => 'post',
        'hierarchical' => true,
        'has_archive' => true,
        'supports' => array('title','editor','thumbnail','comments'),
    );

    register_post_type( 'depoimento' , $args );

}

add_action( 'init', 'depoimentos_register' );

// ajax wall cart
add_filter( 'woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_mini_cart_fragment' );
function woocommerce_header_add_to_mini_cart_fragment( $mini_cart ) { ob_start(); ?>
    <div class="wall-cart-container">
        <?php if ( ! WC()->cart->is_empty() ) : ?>

            <div class="wall-cart-content woocommerce-mini-cart cart_list product_list_widget <?php echo esc_attr( $args['list_class'] ); ?>">
                <?php
                    do_action( 'woocommerce_before_mini_cart_contents' );

                    foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
                        $_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
                        $product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

                        if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_widget_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
                            $product_name      = apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key );
                            $thumbnail         = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );
                            $product_price     = apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
                            $product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
                            ?>

                            <div class="wall-cart-box woocommerce-mini-cart-item <?php echo esc_attr( apply_filters( 'woocommerce_mini_cart_item_class', 'mini_cart_item', $cart_item, $cart_item_key ) ); ?>">

                                <div class="wall-cart-box-image">
                                    <?php if ( empty( $product_permalink ) ) : ?>
                                        <?php echo $thumbnail; ?>
                                    <?php else : ?>
                                        <a href="<?php echo esc_url( $product_permalink ); ?>">
                                            <?php echo $thumbnail; ?>
                                        </a>
                                    <?php endif; ?>
                                </div>

                                <div class="wall-cart-box-title">
                                    <div class="wall-cart-box-title-text">
                                        <!-- <?php echo wc_get_formatted_cart_item_data( $cart_item ); ?> -->
                                        <?php if ( empty( $product_permalink ) ) : ?>
                                            <?php echo $product_name; ?>
                                        <?php else : ?>
                                            <a href="<?php echo esc_url( $product_permalink ); ?>">
                                                <?php echo $product_name; ?>
                                            </a>
                                        <?php endif; ?>
                                    </div>
                                    <div class="wall-cart-box-qtde">
                                        <?php echo apply_filters( 'woocommerce_widget_cart_item_quantity', '<span class="quantity">' . sprintf( '%s &times; %s', $cart_item['quantity'], $product_price ) . '</span>', $cart_item, $cart_item_key ); ?>
                                    </div>
                                </div>

                                <div class="wall-cart-remove-itens" onclick="(function(){ $('#loader').removeClass('hide'); })()">
                                   <?php
                                   echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
                                       '<a href="%s" class="remove remove_from_cart_button" aria-label="%s" data-product_id="%s" data-cart_item_key="%s" data-product_sku="%s"><i class="fal fa-times"></i></a>',
                                       esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
                                       __( 'Remove this item', 'woocommerce' ),
                                       esc_attr( $product_id ),
                                       esc_attr( $cart_item_key ),
                                       esc_attr( $_product->get_sku() )
                                   ), $cart_item_key );
                                   ?>
                                </div>
                            </div>
                            <?php
                        }
                    }

                    do_action( 'woocommerce_mini_cart_contents' );
                ?>
                <div id="loader" class="wall-cart-load hide"></div>
            </div>

            <?php do_action( 'woocommerce_widget_shopping_cart_before_buttons' ); ?>

            <div class="wall-cart-total woocommerce-mini-cart__total total">
              <p>
                <span class="woocommerce-Price-subtotal"><?php _e( 'Subtotal', 'woocommerce' ); ?>:</span>
                <?php echo WC()->cart->get_cart_subtotal(); ?>
              </p>
            </div>
            <div class="wall-cart-buy">
                <a class="btn-sing" href="<?php echo site_url(); ?>/carrinho">
                    <span>
                        <i class="fal fa-cart-plus"></i>
                        FINALIZAR COMPRA
                    </span>
                </a>
            </div>
            <div class="wall-cart-meu-kit">
                <a href="<?php echo site_url(); ?>/meu-kit" class="btn-sing" >
                    <span>
                        <i class="fal fa-box-heart"></i> Meu Kit
                    </span>
                </a>
            </div>

        <?php else : ?>
            <div class="wall-cart-empty woocommerce-mini-cart__empty-message">
                <i class="fal fa-frown"></i>
                <p>Seu carrinho está vazio e triste.</p>
            </div>
        <?php endif; ?>
    </div>
<?php
$mini_cart['.wall-cart-container'] = ob_get_clean();
    return $mini_cart;
}

function tutsplus_excerpt_in_product_archives() {
    echo '<span class="description">';
    the_excerpt();
    echo '</span>';
}
add_action( 'woocommerce_after_shop_loop_item_title', 'tutsplus_excerpt_in_product_archives', 40 );

// Cria novos campos customizados para o produto
add_action( 'woocommerce_product_options_general_product_data', 'woo_add_custom_general_fields' );
add_action( 'woocommerce_process_product_meta', 'woo_add_custom_general_fields_save' );

function woo_add_custom_general_fields() {

    global $woocommerce, $post;

    echo '<div class="options_group">';

    // Textarea
    woocommerce_wp_textarea_input(
        array(
            'id'          => '_single_product_quote',
            'label'       => __( 'Citação do Produto', 'woocommerce' ),
            'placeholder' => '',
            'description' => __( 'Digite a citação do produto aqui.', 'woocommerce' )
        )
    );
    echo '</div>';
}

function woo_add_custom_general_fields_save( $post_id ){

    // Textarea
    $woocommerce_single_product_quote = $_POST['_single_product_quote'];
    if( !empty( $woocommerce_single_product_quote ) )
        update_post_meta( $post_id, '_single_product_quote', esc_html( $woocommerce_single_product_quote ) );

}

// Remove seções woocomerce
// remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50);
remove_action('woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action('woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);
remove_action('woocommerce_sidebar', 'woocommerce_get_sidebar', 10);


// Adicione texto aos preços
add_filter( 'woocommerce_variable_price_html', 'bbloomer_variation_price_format', 10, 2 );
function bbloomer_variation_price_format( $price, $product ) {
$min_var_reg_price = $product->get_variation_regular_price( 'min', true );
$min_var_sale_price = $product->get_variation_sale_price( 'min', true );

if ( $min_var_sale_price < $min_var_reg_price ) {
    $price = sprintf( __( '<span class="price_range">DE: <del>%1$s</del> POR:</span><ins>%2$s</ins>', 'woocommerce' ), wc_price( $min_var_reg_price ), wc_price( $min_var_sale_price ) );
} else {
    $price = sprintf( __( '<span class="price_from">A PARTIR DE:</span> %1$s', 'woocommerce' ), wc_price( $min_var_reg_price ) );
}

return $price;
}

// atualizar número de items no carrinho via ajax
add_filter( 'woocommerce_add_to_cart_fragments', 'iconic_cart_count_fragments', 10, 1 );

function iconic_cart_count_fragments( $fragments ) {

    $fragments['div.header-wall-cart-count'] = '<div class="header-wall-cart-count">' . WC()->cart->get_cart_contents_count() . '</div>';

    return $fragments;

}

// Remove links das imagens do produto
add_filter('woocommerce_single_product_image_thumbnail_html','wc_remove_link_on_thumbnails' );

function wc_remove_link_on_thumbnails( $html ) {
     return strip_tags( $html,'<div><img>' );
}

add_filter( 'wc_nested_category_layout_category_title_html', 'wc_nested_category_layout_category_title_html', 10, 3 );
function wc_nested_category_layout_category_title_html( $title, $categories, $term ) {
    $category = $categories[ count( $categories ) - 1 ];
    $url = esc_attr( get_term_link( $category ) );
    $link = '<a href="' . $url . '">' . wptexturize( $category->name ) . '</a>';
    return sprintf( '<h2 class="wc-nested-category-layout-category-title">%s</h2>', $link );
}

/**
 * Allow HTML in term (category, tag) descriptions
 */
foreach ( array( 'pre_term_description' ) as $filter ) {
    remove_filter( $filter, 'wp_filter_kses' );
}

foreach ( array( 'term_description' ) as $filter ) {
    remove_filter( $filter, 'wp_kses_data' );
}

/**
 * Restricts which products can be added to the cart at the same time.
 * Version for WooCommerce 3.x and later.
 *
 * HOW TO USE THIS CODE
 * 1. Add the code to the bottom of your theme's functions.php file (see https://www.skyverge.com/blog/add-custom-code-to-wordpress/).
 * 2. Set the IDs of the products that are allowed to be added to the cart at the same time.
 * 3. Amend the message displayed to customers when products are unavailable after the specified
 *    products have been added to the cart (see function woocommerce_get_price_html(), below).
 *
 * GPL DISCLAIMER
 * Because this code program is free of charge, there is no warranty for it, to the extent permitted by applicable law.
 * Except when otherwise stated in writing the copyright holders and/or other parties provide the program "as is"
 * without warranty of any kind, either expressed or implied, including, but not limited to, the implied warranties of
 * merchantability and fitness for a particular purpose. The entire risk as to the quality and performance of the program
 * is with you. should the program prove defective, you assume the cost of all necessary servicing, repair or correction.
 *
 * Need a consultation, or assistance to customise this code? Find us on Codeable: https://aelia.co/hire_us
 */

/**
 * Retrieves the cart contents. We can't just call WC_Cart::get_cart(), because
 * such method runs multiple actions and filters, which we don't want to trigger
 * at this stage.
 *
 * @author Aelia <support@aelia.co>
 */
function aelia_get_cart_contents() {
  $cart_contents = array();
  /**
   * Load the cart object. This defaults to the persistant cart if null.
   */
  $cart = WC()->session->get( 'cart', null );

    if ( is_null( $cart ) && ( $saved_cart = get_user_meta( get_current_user_id(), '_woocommerce_persistent_cart_' . get_current_blog_id(), true ) ) ) { // @codingStandardsIgnoreLine
        $cart                = $saved_cart['cart'];
    }
    elseif ( is_null( $cart ) ) {
        $cart = array();
    }
    elseif ( is_array( $cart ) && ( $saved_cart = get_user_meta( get_current_user_id(), '_woocommerce_persistent_cart_' . get_current_blog_id(), true ) ) ) { // @codingStandardsIgnoreLine
        $cart                = array_merge( $saved_cart['cart'], $cart );
    }

  if ( is_array( $cart ) ) {
    foreach ( $cart as $key => $values ) {
      $_product = wc_get_product( $values['variation_id'] ? $values['variation_id'] : $values['product_id'] );

      if ( ! empty( $_product ) && $_product->exists() && $values['quantity'] > 0 ) {
        if ( $_product->is_purchasable() ) {
          // Put session data into array. Run through filter so other plugins can load their own session data
          $session_data = array_merge( $values, array( 'data' => $_product ) );
          $cart_contents[ $key ] = apply_filters( 'woocommerce_get_cart_item_from_session', $session_data, $values, $key );
        }
      }
    }
  }
  return $cart_contents;
}

// Step 1 - Keep track of cart contents
add_action('wp_loaded', function() {
  // If there is no session, then we don't have a cart and we should not take
  // any action
  if(!is_object(WC()->session)) {
    return;
  }

  // This variable must be global, we will need it later. If this code were
  // packaged as a plugin, a property could be used instead
  global $allowed_cart_items;
  // We decided that products with ID 737 and 832 can go together. If any of them
  // is in the cart, all other products cannot be added to it
  global $restricted_cart_items;
  $restricted_cart_items = array(
        // Set the IDs of the products that can be added to the cart at the same time
    // 1,
    // 2,
    // 3,
  );

  // "Snoop" into the cart contents, without actually loading the whole cart
  foreach(aelia_get_cart_contents() as $item) {
    if(in_array($item['data']->get_id(), $restricted_cart_items)) {
      $allowed_cart_items[] = $item['data']->get_id();

      // If you need to allow MULTIPLE restricted items in the cart, comment
      // the line below
      break;
    }
  }

  // Step 2 - Make disallowed products "not purchasable"
  add_filter('woocommerce_is_purchasable', function($is_purchasable, $product) {
    global $restricted_cart_items;
    global $allowed_cart_items;

    // If any of the restricted products is in the cart, any other must be made
    // "not purchasable"
    if(!empty($allowed_cart_items)) {
      // To allow MULTIPLE products from the restricted ones, use the line below
      //$is_purchasable = in_array($product->id, $allowed_cart_items) || in_array($product->id, $restricted_cart_items);

      // To allow a SINGLE  products from the restricted ones, use the line below
      $is_purchasable = in_array($product->get_id(), $allowed_cart_items);
    }
    return $is_purchasable;
  }, 10, 2);
}, 10);

// Step 3 - Explain customers why they can't add some products to the cart
add_filter('woocommerce_get_price_html', function($price_html, $product) {
  if(!$product->is_purchasable() && is_product()) {
    $price_html .= '<p>' . __('This product cannot be purchased together with "Product X" or "Product Y". If you wish to buy this product, please remove the other products from the cart.', 'woocommerce') . '</p>';
  }
  return $price_html;
}, 10, 2);

//include_once ('includes/product-custom-fields.php');


add_filter('woocommerce_add_to_cart_redirect', 'redirect_to_checkout');
function redirect_to_checkout($redirect_url) {
    if (isset($_REQUEST['is_buy_now']) && $_REQUEST['is_buy_now']) {
        global $woocommerce;
        $redirect_url = wc_get_checkout_url();
    }
    return $redirect_url;
}


class Sublevel_Walker extends Walker_Nav_Menu
{
    function start_lvl( &$output, $depth = 0, $args = array() ) {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<div class='submenu'><ul class='sub-menu'>\n";
    }
    function end_lvl( &$output, $depth = 0, $args = array() ) {
        $indent = str_repeat("\t", $depth);
        $output .= "$indent</ul></div>\n";
    }
}

/**
 * Rename product data tabs
 */
add_filter( 'woocommerce_product_tabs', 'woo_rename_tabs', 98 );
function woo_rename_tabs( $tabs ) {
    $tabs['additional_information']['title'] = __( 'Informações Adicionais' );	// Rename the additional information tab
    return $tabs;
}


/*remove term descriptions from post editor */

function wall_hide_cat_descr() { ?>

    <style type="text/css">
        .term-description-wrap {
            display: none;
        }
    </style>

<?php }

add_action( 'admin_head-term.php', 'wall_hide_cat_descr' );
add_action( 'admin_head-edit-tags.php', 'wall_hide_cat_descr' );


add_action( 'wp_enqueue_scripts', 'wsis_dequeue_stylesandscripts_select2', 100 );

function wsis_dequeue_stylesandscripts_select2() {
    if ( class_exists( 'woocommerce' ) ) {
        wp_dequeue_style( 'select2' );
        wp_deregister_style( 'select2' );

        wp_dequeue_script( 'select2');
        wp_deregister_script('select2');

    }
}

add_filter( 'woocommerce_billing_fields' , 'custom_override_billing_fields' );
add_filter( 'woocommerce_shipping_fields' , 'custom_override_shipping_fields' );

function custom_override_billing_fields( $fields ) {
    unset($fields['billing_country']);
    return $fields;
}

function custom_override_shipping_fields( $fields ) {
    unset($fields['shipping_country']);
    return $fields;
}
