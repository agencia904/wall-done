<?php
/**
 * header.php
 *
 * Desenvolvido por Agência 904
 * Copyright © Todos os direitos reservados
 *
 * @link https://www.walldone.com.br/
 * @author https://www.walldone.com.br/
 * @package Wall Done
 * @subpackage Wall Done
 * @since Wall Done 3.0
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<meta name="format-detection" content="telephone=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="url" content="<?php echo site_url(); ?>" />
	<link href="<?php bloginfo('template_directory');?>/style.css" rel="stylesheet">
	<link href="<?php bloginfo('template_directory');?>/fontawesome.css" rel="stylesheet">

	<script
	  src="https://code.jquery.com/jquery-3.3.1.min.js"
	  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
	  crossorigin="anonymous"></script>
	<script type="text/javascript" src="<?php bloginfo('template_directory');?>/assets/js/jquery.main.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_directory');?>/assets/js/slick/slick.min.js"></script>

	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory');?>/assets/js/notify/notify.css">
	<script src="<?php bloginfo('template_directory');?>/assets/js/notify/notify.js" type="text/javascript"></script>
	
	<title><?php wp_title('|', true, 'right'); bloginfo() ?></title>
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>
</head>

<body <?php body_class('bg-init'); ?>>
	<header id="header">
		<div class="header-top">
			<div class="header-top-container">
				<div class="column">
					<div class="header-top-social">
						<ul>
							<li>
								<a href="https://www.instagram.com/walldonedecor/" target="_blank" title="Instagram | Wall Done" target="_blank">
									<i class="fab fa-instagram"></i>
								</a>
							</li>
							<li>
								<a href="https://www.facebook.com/walldonedecor/" target="_blank" title="Facebook | Wall Done" target="_blank">
									<i class="fab fa-facebook-f"></i>
								</a>
							</li>
							<li>
								<a href="https://br.pinterest.com/walldonedecor/" target="_blank" title="Pinterest | Wall Done" target="_blank">
									<i class="fab fa-pinterest"></i>
								</a>
							</li>
							<li class="header-top-social-blog">
								<a href="<?php echo site_url(); ?>/blog-da-larissa" title="Blog da Larissa | Wall Done">
									<i class="fal fa-comment-alt-lines"></i> <span>Blog da Larissa</span>
								</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="column"> 
					<div class="header-top-cart">
						<ul>
							<li class="header-wall-lover">
								<a href="javascript:void(0)">
									<span>Seja um Wall Lover</span> <i class="fal fa-heart"></i>
								</a>
								<div class="header-wall-lover-submenu">
									<ul>
										<li>
											<a href="<?php echo site_url(); ?>/minha-conta">Entrar</a>
										</li>
										<li>
											<a href="<?php echo site_url(); ?>/minha-conta">Minha conta</a>
										</li>
										<li>
											<a href="#">Meus pedidos</a>
										</li>
										<li>
											<a href="#">Lista de desejos</a>
										</li>
										<li>
											<a href="#">Meus Kits com a Larissa</a>
										</li>
									</ul>
								</div>
							</li>
							<li class="header-wall-cart">
								<a href="/carrinho">
									<span>Wall Cart</span> <i class="fal fa-cart-plus"></i>
									<div class="header-wall-cart-count"><?php echo WC()->cart->get_cart_contents_count(); ?></div>
								</a>
								<div class="wall-cart">
									<div class="wall-cart-wrap">
										<div class="wall-cart-title">													
											<span>Wall Cart</span>
											<i class="fal fa-cart-plus" aria-hidden="true"></i>
										</div>
										<?php require('wall-cart.php');?>
									</div>										
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		
		<div class="header-bottom">
			<div class="header-bottom-container">
				<div class="header-bottom-menu-mobile" id="open-menu">
					<button class="hamburger hamburger--spin" type="button">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</button>
				</div>
				<div class="header-bottom-logo">
					<a href="<?php echo site_url(); ?>">
						<img src="<?php bloginfo('template_directory');?>/assets/images/logo-walldone.png" alt="Logo | Wall Done" />
					</a>
				</div>
				<div class="header-bottom-wall-cart-mobile">
					<a class="hover-social-cart">
						<i class="fal fa-cart-plus cart-mobile"></i>
						<div class="header-wall-cart-count"><?php echo WC()->cart->get_cart_contents_count(); ?></div>
					</a>
				</div>
				<div class="header-bottom-content">
					<div class="header-bottom-content-search">
						<?php echo get_product_search_form(); ?>
					</div>
					<nav class="header-bottom-content-nav">
						<ul role="navigation">
							<li class="search-mobile">
							</li>
							<li itemscope itemtype="http://schema.org/SiteNavigationElement" class="first">
								<a itemprop="url" href="<?php echo site_url(); ?>/ambientes">
									<span itemprop="name">Ambientes</span>
								</a>
								<div class="submenu">
									<ul>
										<li><a href="#">Corredores e Halls de Entrada</a></li>
										<li><a href="#">Cozinhas</a></li>
										<li><a href="#">Escritórios</a></li>
										<li><a href="#">Quartos de Casal</a></li>
										<li><a href="#">Quartos de Solteiro</a></li>
										<li><a href="#">Quartos Infantis</a></li>
										<li><a href="#">Sacadas e Churrasqueiras</a></li>
										<li><a href="#">Salas</a></li>
									</ul>
								</div>
							</li>
							<li itemscope itemtype="http://schema.org/SiteNavigationElement">
								<a itemprop="url" href="<?php echo site_url(); ?>/temas">
									<span itemprop="name">Temas</span>
								</a>
								<div class="submenu">
									<ul>
										<li><a href="#">Amor</a></li>
										<li><a href="#">Comes e Bebes</a></li>
										<li><a href="#">Fé</a></li>
										<li><a href="#">Good Vibes</a></li>
										<li><a href="#">Infantil</a></li>
										<li><a href="#">Lar Doce Lar</a></li>
										<li><a href="#">Pet</a></li>
									</ul>
								</div>
							</li>
							<li itemscope itemtype="http://schema.org/SiteNavigationElement">
								<a itemprop="url" href="<?php echo site_url(); ?>/colecoes">
									<span itemprop="name">Coleções</span>
								</a>
								<div class="submenu">
									<ul>
										<li><a href="#">Aquarela - Mariana Terleski</a></li>
										<li><a href="#">Bença - Borogodó</a></li>
										<li><a href="#">Mãos ao Alto - Marcos Bernardes</a></li>
										<li><a href="#">Tô de Olho</a></li>
									</ul>
								</div>
							</li>
							<li itemscope itemtype="http://schema.org/SiteNavigationElement" class="last">
								<a itemprop="url" href="<?php echo site_url(); ?>/produtos">
									<span itemprop="name">Produtos</span>
								</a>
								<div class="submenu sub">
									<ul>
										<li><a href="#">Adesivos</a></li>
										<li><a href="#">Aromatizador de Ambientes</a></li>
										<li><a href="#">Espelhos</a></li>
										<li><a href="#">Flâmulas</a></li>
										<li><a href="#">Letreiros</a></li>
										<li><a href="#">Penduradores</a></li>
										<li><a href="#">Plaquinhas</a></li>
										<li><a href="#">Prateleiras</a></li>
										<li><a href="#">Quadros</a></li>
										<li><a href="#">Quadros Luminosos</a></li>
										<li><a href="#">Relógios</a></li>
										<li><a href="#">Vasinhos</a></li>
									</ul>
								</div>
							</li>
							<li itemscope itemtype="http://schema.org/SiteNavigationElement" class="meu-kit">
								<a href="<?php echo site_url(); ?>/meu-kit">
									<i class="fal fa-box-heart"></i> Meu Kit
								</a>
							</li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
		<div class="header-bottom-search-mobile mobile-hidden">
			<?php echo get_product_search_form(); ?>
		</div>
	</header>
	<div class="clearfix header"></div>
	
	<div id="menu-mobile">
		<div class="menu-mobile-search">
			<?php echo get_product_search_form(); ?>
		</div>
		<ul>
			<span class="wall-lover">
				<a href="#">Acesse</a><span class="border">/</span>					
				<a href="#">Seja um Wall Lover <i class="fal fa-heart"></i></a>
			</span>
			<li class="blog">
				<a href="<?php echo site_url(); ?>/blog">
					<i class="fal fa-comment-alt-lines"></i> Blog da Larissa
				</a>
			</li>
			<li>
				<a href="<?php echo site_url(); ?>">
					Ambientes
					<ul class="submenu submenu-mobile">
						<li><a href="#">Corredores e Halls de Entrada</a></li>
						<li><a href="#">Cozinhas</a></li>
						<li><a href="#">Escritórios</a></li>
						<li><a href="#">Quartos de Casal</a></li>
						<li><a href="#">Quartos de Solteiro</a></li>
						<li><a href="#">Quartos Infantis</a></li>
						<li><a href="#">Sacadas e Churrasqueiras</a></li>
						<li><a href="#">Salas</a></li>
					</ul>		
				</a>
			</li>
			<li>
				<a href="<?php echo site_url(); ?>">
					Temas
					<ul class="submenu submenu-mobile">
						<li><a href="#">Amor</a></li>
						<li><a href="#">Comes e Bebes</a></li>
						<li><a href="#">Fé</a></li>
						<li><a href="#">Good Vibes</a></li>
						<li><a href="#">Infantil</a></li>
						<li><a href="#">Lar Doce Lar</a></li>
						<li><a href="#">Pet</a></li>
					</ul>		
				</a>
			</li>
			<li>
				<a href="<?php echo site_url(); ?>">
					Coleções
					<ul class="submenu submenu-mobile">
						<li><a href="#">Aquarela - Mariana Terleski</a></li>
						<li><a href="#">Bença - Borogodó</a></li>
						<li><a href="#">Mãos ao Alto - Marcos Bernardes</a></li>
						<li><a href="#">Tô de Olho</a></li>
					</ul>
				</a>
			</li>
			<li>
				<a href="<?php echo site_url(); ?>">
					Produtos
					<ul class="submenu submenu-mobile">
						<li><a href="#">Adesivos</a></li>
						<li><a href="#">Aromatizador de Ambientes</a></li>
						<li><a href="#">Espelhos</a></li>
						<li><a href="#">Flâmulas</a></li>
						<li><a href="#">Letreiros</a></li>
						<li><a href="#">Penduradores</a></li>
						<li><a href="#">Plaquinhas</a></li>
						<li><a href="#">Prateleiras</a></li>
						<li><a href="#">Quadros</a></li>
						<li><a href="#">Quadros Luminosos</a></li>
						<li><a href="#">Relógios</a></li>
						<li><a href="#">Vasinhos</a></li>
					</ul>
				</a>
			</li>
			<li class="meu-kit">
				<a href="<?php echo site_url(); ?>/meu-kit">
					<span><i class="fal fa-box-heart"></i> Meu Kit</span>
				</a>
			</li>
		</ul>
	</div>

	<div id="container">