<?php
/**
 * index.php
 *
 * Desenvolvido por Agência 904
 * Copyright © Todos os direitos reservados
 *
 * @link https://www.walldone.com.br/
 * @author https://www.walldone.com.br/
 * @package Wall Done
 * @subpackage Wall Done
 * @since Wall Done 3.0
 */
?>
<?php get_header(); ?>

<section class="home-slider">
	<div class="home-slider-item">
		<?php
		$args = array(
		  'posts_per_page'	=>  '-1',
		  'post_type' 		=> 'slider',
		  'orderby'   		=> 'meta_value_num',
		  'order'     		=> 'DESC'
		  );
		$query = new WP_Query( $args );
		if($query->have_posts()) : while($query->have_posts()) : $query->the_post(); ?>
			<div class="home-slider-item-container">
				<div class="home-slider-bg" style="background-image: url(<?php echo the_post_thumbnail_url('full'); ?>);"></div>
				<div class="home-slider-container">
					<div class="wrap">
						<div class="home-slider-content">
							<div class="home-slider-title"><?php the_title(); ?></div>
							<div class="home-slider-desc"><?php the_content(); ?></div>
							<?php if(get_post_meta( get_the_ID(), 'slider_link', true) && get_post_meta( get_the_ID(), 'slider_link_title', true)) : ?>
							<div class="box-button">
								<a href="<?php echo get_post_meta( get_the_ID(), 'slider_link', true); ?>" class="btn-sing">
									<span><i class="fas fa-caret-right"></i> <?php echo get_post_meta( get_the_ID(), 'slider_link_title', true); ?></span>
								</a>
							</div>
							<?php endif; ?>
						</div>	
					</div>	
				</div>
			</div>
		 	<?php endwhile; ?>
		<?php endif;?>		
	</div>
</section>
<section class="home-intro">
	<div class="wrap">
		<div class="home-intro-container">
			<div class="column">
				<div class="home-intro-iframe">
					<img id="video-remove" src="<?php bloginfo('template_directory');?>/assets/images/home-video-walldone.jpg" alt="Video | Wall Done" />
					<iframe id="video-player" src="https://player.vimeo.com/video/189330068" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

					<div class="home-intro-active-video">
						<i class="fal fa-play"></i>
					</div>
				</div> 
			</div>		
			<div class="column">
				<h2>Minhas inspirações</h2>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolo.
				</p>
				<a href="<?php echo site_url(); ?>" class="btn-sing">
					<span><i class="fas fa-caret-right"></i> CONHEÇA A WALL DONE!</span>
				</a>
			</div>		
		</div>
	</div>
</section>
<section class="home-destaques">
	<div class="home-destaques-container">
		<div class="home-destaques-content">
			<div class="home-destaques-title">
				<h3>Frete <span>grátis</span> em compras acima de R$ 200,00</h3>
			</div>
			<div class="home-destaques-row">
				<div class="column">
					<div class="destaque-icon">
						<i class="fal fa-truck"></i>
					</div>
					<div class="destaque-desc">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.</p>
					</div>
				</div>
				<div class="column">
					<div class="destaque-icon">
						<i class="fal fa-smile"></i>
					</div>
					<div class="destaque-desc">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.</p>
					</div>
				</div>
				<div class="column">
					<div class="destaque-icon">
						<i class="fal fa-heart-circle"></i>
					</div>
					<div class="destaque-desc">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="bg-home-destaques"></div>
	</div>
</section>
<section class="home-lancamentos">
	<div class="wrap">
		<div class="home-lancamentos-container">
			<div class="home-lancamentos-description">
				<h4>Lançamentos</h4>
				<span></span>
				<p>Que tal estas novidades fresquinhas feitas a mão?</p>
			</div>
			<div class="common-list-products">
				<div class="common-list-products-box">
					<ul class="products">
						<?php
							$args = array(
								'post_type' => 'product',
								'posts_per_page' => 8,
								'meta_query' => array(
									array(
										'key' => 'lancamento',
										'compare' => '=',
										'value' => '1'
									)
								)
								);
							$loop = new WP_Query( $args );
							if ( $loop->have_posts() ) {
								while ( $loop->have_posts() ) : $loop->the_post();
									wc_get_template_part( 'content', 'product' );
								endwhile;
							} else {
								echo __( 'Não existem produtos cadastrados.' );
							}
							wp_reset_postdata();
						?>	
					</ul>		
				</div>
			</div>
		</div>
	</div>
</section>
<section class="home-wall-talk">
	<div class="wrap">
		<div class="home-wall-talk-container">
			<div class="home-wall-talk-title">
				<h4>As paredes também falam</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adispisicing elit.</p>
			</div>
			<div class="home-wall-talk-content">
				<div class="column">
					<div class="box-text">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque molestias eos dolorem similique placeat, excepturi minima perspiciatis assumenda! Nulla voluptas quae est quasi aut odio ipsam rerum corporis earum veritatis.</p>
						<div class="box-button">
							<a href="<?php echo site_url(); ?>" class="btn-sing">
								<span><i class="fas fa-caret-right"></i> SAIBA MAIS</span>
							</a>
						</div>
					</div>
				</div>
				<div class="column">
					<figure>
						<img src="<?php bloginfo('template_directory');?>/assets/images/home-sobre-larissa-walldone.jpg" alt="Larissa | Wall Done" />
					</figure>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="home-mais-vendidos">
	<div class="wrap">
		<div class="home-mais-vendidos-container">
			<div class="home-mais-vendidos-description">
				<h4>Mais Vendidos</h4>
				<span></span>
				<p>Estes são os mais queridos pelos meus clientes</p>
			</div>
			<div class="common-list-products">
				<div class="common-list-products-box">
					<ul class="products">
						<?php
							$args = array(
								'post_type' => 'product',
								'posts_per_page' => 8,
								'meta_query' => array(
									array(
										'key' => 'mais_vendidos',
										'compare' => '=',
										'value' => '1'
									)
								)
								);
							$loop = new WP_Query( $args );
							if ( $loop->have_posts() ) {
								while ( $loop->have_posts() ) : $loop->the_post();
									wc_get_template_part( 'content', 'product' );
								endwhile;
							} else {
								echo __( 'Não existem produtos cadastrados..' );
							}
							wp_reset_postdata();
						?>	
					</ul>			
				</div>
			</div>
		</div>
	</div>
</section>
<section class="home-depoimentos">
	<div class="home-depoimentos-title">
		<h4>Depoimentos</h4>
		<span></span>
		<p>É o carinho de vocês que me faz querer melhorar todos os dias</p>
	</div>	
	<div class="home-depoimentos-wrap">
		<div class="home-depoimentos-container">
			<?php
				$depoimentos = new WP_Query(
					array(
						'post_type' => 'depoimento',
						'posts_per_page' => 8,
					)
				);
			?>
			<?php if ( $depoimentos->have_posts() ) :;?>
				<?php while ( $depoimentos->have_posts() ) : $depoimentos->the_post();?>
					<div class="home-depoimentos-content">
						<div class="home-depoimentos-box">
							<div class="column">
								<figure>
									<?php the_post_thumbnail();?>
								</figure>
							</div>
							<div class="column">
								<div class="home-depoimentos-desc">
									<p><?php the_content() ;?></p>
									<h6><?php the_title();?></h6>
									<i><?php the_time('j F, Y')?></i>
								</div>
							</div>
						</div>
					</div>
				<?php endwhile; ;?>
			<?php endif ;?>
			<?php wp_reset_postdata(); ?>
		</div>
	</div>
</section>
<section class="home-blog">
	<div class="home-blog-title">
		<div class="home-blog-title-content">
			<h4>Casa feliz tem</h4>
			<span></span>
			<div class="home-blog-title-switch">
				<p>FELICIDADE</p>
				<p>ALEGRIA</p>
				<p>BOM GOSTO</p>
				<p>AMIGOS</p>
			</div>
		</div>
	</div>
	<div class="home-blog-container">
		<div class="wrap">
			<div class="home-blog-content">
				<div class="home-blog-post-container">
					<?php
					$blog = array(
					'posts_per_page' =>  '3',
					'post_type' => 'post',
					'order'     => 'DESC',
					'oderby'	=> 'date menu_order'
					);
					$query = new WP_Query( $blog ); 

					if($query->have_posts()) : while($query->have_posts()) : $query->the_post(); ?>
						<div class="home-blog-post">
							<a href="<?php print get_permalink($post->ID) ?>">
								<figure class="home-blog-post-image">
									<?php if (has_post_thumbnail()) : ?>
										<?php the_post_thumbnail(); ?>
									<?php else : ?>
										<img src="<?php bloginfo('template_directory');?>/assets/images/imagem-nao-disponivel-walldone.jpg" alt="Imagem não disponível | Wall Done" />	
									<?php endif; ?>    
								</figure>
								<div class="home-blog-post-desc">
									<div class="home-blog-post-info">
										<span>por <b><?php echo get_the_author(); ?></b> em <b><?php echo get_the_date(); ?></b></span> 
									</div>
									<h3><?php echo the_title(); ?></h3>
								</div>
							</a>						
						</div>
					<?php endwhile; ?>
					<?php endif; ?>
					<?php wp_reset_query(); ?>
				</div>
	
				<div class="home-blog-post-more">
					<a href="<?php echo site_url(); ?>/blog" class="btn-sing">
						<span><i class="fas fa-caret-right"></i> ACESSE MEU BLOG</span>
					</a>					
				</div>
			</div>
		</div>
	</div>
</section>
<section class="home-instagram">
	<div class="wrap">
		<div class="home-instagram-container">
			<div class="column">
				<div class="home-instagram-title">
					<p>Me acompanhe também no</p>
					<h5>Instagram</h5>
					<h6>@walldonedecor</h6>
				</div>
			</div>
			<div class="column">
				<div class="home-instagram-posts">
					<!-- InstaWidget -->
					<a href="https://instawidget.net/v/user/walldonedecor" id="link-b38c7f3cab45b6fb8130a0e8e3fbba64979d9d4a1c82c17431337323ebf66f90">@walldonedecor</a>
<!--					<script src="https://instawidget.net/js/instawidget.js?u=b38c7f3cab45b6fb8130a0e8e3fbba64979d9d4a1c82c17431337323ebf66f90&width=500px"></script>						-->
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>