<?php
/**
 * Template Name: Kits prontos
 * kits-prontos.php
 *
 * Desenvolvido por Agência 904
 * Copyright © Todos os direitos reservados
 *
 * @link https://www.walldone.com.br/
 * @author https://www.walldone.com.br/
 * @package Wall Done
 * @subpackage Wall Done
 * @since Wall Done 3.0
 */
?>
<?php get_header(); ?>
<section class="common-header">
	<div class="wrap">
		<div class="common-header-container">
			<?php do_action( 'woocommerce_before_main_content' ); ?>
			<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
				<h1 class="woocommerce-products-header__title page-title"><?php the_title(); ?></h1>
			<?php endif; ?>
		</div>
	</div>
</section>

<section class="page-kits-prontos">
	<div class="wrap">
		<div class="page-kits-prontos-container">
			<div class="page-kits-prontos-content">
				<div class="page-kits-prontos-row">
					<div class="column">
						<iframe src="https://player.vimeo.com/video/189330068" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
					</div>
					<div class="column">
						<h2>Kits prontos</h2>
						<p>Essas composições foram feitas pela arquiteta e criadora do site (conheça sua história na aba SOBRE A WALL DONE). Você pode encontrá-las na aba KITS PRONTOS no menu inicial.</p>

						<p><b>Os kits foram montados com os produtos que estão disponíveis individualmente no site, porém, ao adquiri-los, você terá as seguintes vantagens:</b></p>
					</div>
				</div>
			</div>

			<div class="page-kits-prontos-content">
				<ul>
					<li>Os valores dos kits são promocionais: você paga menos do que se somasse o valor de todos os produtos individuais que fazem parte deles;</li>
					<li>O pagamento pode ser feito em até 3x sem juros;</li>
					<li>O frete é grátis para as regiões Sul e Sudeste (nos kits acima de R$299,00);</li>
				</ul>

				<p><b>Os kits foram pensados com muito cuidado e carinho: você será orientado na escolha de acordo com o tamanho da sua parede e o ambiente:</b></p>

				<ul>
					<li>Não precisa se preocupar se a quantidade e o tamanho dos objetos vão ficar proporcionais ao tamanho da parede;</li>
					<li>Não precisa se preocupar em saber a distância mais adequada entre os objetos, bem como sua melhor disposição: você receberá, juntamente com o kit, uma sugestão de layout para sua parede e um gabarito com os pontos onde os objetos devem ser colocados para que a composição fique em perfeita harmonia: nunca foi tão fácil dar aquele "up" naquela sua parede sem graça e deixá-la com a sua cara!</li>
				</ul>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>