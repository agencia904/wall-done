<?php
/**
 * Template Name: Meu Kit Briefing
 * meu-kit-briefing.php
 *
 * Desenvolvido por Agência 904
 * Copyright © Todos os direitos reservados
 *
 * @link https://www.walldone.com.br/
 * @author https://www.walldone.com.br/
 * @package Wall Done
 * @subpackage Wall Done
 * @since Wall Done 3.0
 */
?>
<?php get_header(); ?>
<script type="text/javascript" src="<?php echo plugins_url();?>/wc-fields-factory/assets/js/spectrum.js"></script>
<script type="text/javascript" src="<?php echo plugins_url();?>/wc-fields-factory/assets/js/wcff-client.js"></script>
<section class="common-header">
	<div class="wrap">
		<div class="common-header-container">
			<?php do_action( 'woocommerce_before_main_content' ); ?>
			<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
				<h1 class="woocommerce-products-header__title page-title"><?php the_title(); ?></h1>
			<?php endif; ?>
		</div>
	</div>
</section>

<?php
$kit = new WP_Query(
	array(
		'post_type' => 'product',
		'posts_per_page' => 1,
		'id'=> 242,
	)
);
;?>
<?php if( $kit->have_posts() ):?>
	<?php while( $kit->have_posts() ) : $kit->the_post(); ?>
		<?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>
		<form class="cart product-summary-cart" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data'>
			<section class="meu-kit-walk">
				<div class="wrap">
					<div class="meu-kit-walk-container">
						<div class="walk active" id="passo1">
							<div class="walk-content">
								<div class="walk-number">
									1
								</div>
								<div class="walk-text">
									Briefing do<br>Projeto
								</div>
							</div>
						</div>
						<div class="walk" id="passo2">
							<div class="walk-content">
								<div class="walk-number">
									2
								</div>
								<div class="walk-text">
									Tudo certo?
								</div>
							</div>
						</div>
						<div class="walk" id="passo3">
							<div class="walk-content">
								<div class="walk-number">
									3
								</div>
								<div class="walk-text">
									Finalizar
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

			<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

			<!-- Inicio coluna Briefing-->
			<section class="meu-kit-briefing" id="sectionbriefing">
				<div class="meu-kit-briefing-row">
					<div class="wrap">
						<div class="temp-values">

						</div>
						<div class="meu-kit-briefing-row-container">
							<div class="meu-kit-briefing-row-title">
								<h3><span>1.1</span> Confira seus protudos</h3>
								<p>Se você selecionou produtos para montar seu Kit comigo, eles irão aparecer abaixo.</p>
							</div>
							<div class="meu-kit-briefing-row-content">
								<div class="meu-kit-briefing-confere-produtos">
									<script type="text/javascript">
									    $(document).ready(function(){
									    	$(".woosw-content-item--image.confere-item-image").find("a").contents().unwrap();
									    });
									</script>
									<?php echo do_shortcode('[woosw_list]');?>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="meu-kit-briefing-row">
					<div class="wrap">
						<div class="meu-kit-briefing-row-container">
							<div class="meu-kit-briefing-row-title">
								<h3><span>1.2</span>  Envie fotos da sua parede.</h3>
							</div>
							<div class="meu-kit-briefing-row-content">
								<div class="meu-kit-briefing-envia-produtos">
									<div class="envia-produtos-box">
										<div class="file-upload">
											<div class="file-upload-content">
												<i class="fal fa-upload"></i>
												<span>Clique aqui para carregar imagens da sua parede</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="meu-kit-briefing-row">
					<div class="wrap">
						<div class="meu-kit-briefing-row-container">
							<div class="meu-kit-briefing-row-title">
								<h3><span>1.3</span>  Me diga as medidas da sua parede</h3>
								<p>Lorem ipsum dolor sit ame, consectetur adipisicing elit, sed do eiusmod tempor.</p>
							</div>
							<div class="meu-kit-briefing-row-content">
								<div class="meu-kit-briefing-medida-parede">
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="meu-kit-briefing-row">
					<div class="wrap">
						<div class="meu-kit-briefing-row-container">
							<div class="meu-kit-briefing-row-title">
								<h3><span>1.4</span>  A parte mais gostosa. Me fale do que você gosta!</h3>
							</div>
							<div class="meu-kit-briefing-row-content">
								<div class="meu-kit-briefing-sobre-kit">
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="meu-kit-briefing-row sobre-os-produtos">
					<div class="wrap">
						<div class="meu-kit-briefing-row-container">
							<div class="meu-kit-briefing-row-title">
								<p>Caso tenha escolhido poucos itens ou nenhum, por favor me conte um pouco melhor sobre sua ideia para essa parede. Por exemplo, você prefere cores sóbrias ou mais alegres? Gostaria de uma composição mais minimalista ou mais descolada? <span>Lembrando que a idéia é que a parede reflita a sua personalidade, então por mais que eu monte ela para você, é importante que me dê as diretrizes. ;)</span></p>
							</div>
							<div class="meu-kit-briefing-row-content">
								<div class="meu-kit-briefing-sobre-projeto">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="meu-kit-briefing-row">
					<div class="wrap">
						<div class="meu-kit-briefing-row-container">
							<div class="meu-kit-briefing-row-title">
								<p>Você já possui algum outro item que também deseja incluir na composição?</p>
							</div>
							<div class="meu-kit-briefing-row-content">
								<div class="meu-kit-briefing-sobre-outros-itens">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="meu-kit-briefing-row">
					<div class="wrap">
						<div class="meu-kit-briefing-row-container">
							<div class="meu-kit-briefing-row-title">
								<p>Deseja incluir mais alguma informação relevante para a montagem do kit?</p>
							</div>
							<div class="meu-kit-briefing-row-content">
								<div class="meu-kit-briefing-mais-informacoes">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="box-button">
								
								<div class="product-summary-cart-row">
									
									<button type="submit" name="vamosla" value="<?php echo esc_attr($product->get_id()); ?>" class="single_add_to_cart_button button alt btn-sing" id="vamosla">
										<i></i>
										<span><?php echo esc_html('Vamos lá?!'); ?></span>
									</button>
									<input type="hidden" name="is_buy_now" id="is_buy_now" value="0" />
								</div>
							</div>
			</section>
			<!-- Fim coluna Briefing-->

			<!-- Inicio coluna tudo certo-->
			<section class="meu-kit-briefing" id="sectionconfere" style="display: none;">
				<div class="meu-kit-briefing-row">
					<div class="wrap">
						<div class="temp-values">

						</div>
						<div class="meu-kit-briefing-row-container">
							<div class="meu-kit-briefing-row-title">
								<h3>Confira se está tudo certinho</h3>
								<p>Se você selecionou produtos para montar seu Kit comigo, eles irão aparecer abaixo.</p>
							</div>
							<div class="meu-kit-briefing-row-content">
								<div class="meu-kit-briefing-confere-produtos">
									<script type="text/javascript">
									    $(document).ready(function(){
									    	$(".woosw-content-item--image.confere-item-image").find("a").contents().unwrap();
									    });
									</script>
									<?php echo do_shortcode('[woosw_list]');?>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="meu-kit-briefing-row">
					<div class="wrap">
						<div class="meu-kit-briefing-row-container">
							<div class="meu-kit-briefing-row-title">
								<h3>Paredes</h3>
								<p>Que legal que você optou por eu escolher os seus produtos. Já estou adorando! :) Confira as fotos da sua parede abaixo:</p>
							</div>
							<div class="meu-kit-briefing-row-content">
								<div class="meu-kit-briefing-envia-produtos">
									<div class="envia-produtos-box">
										<div class="file-upload">
											<div class="file-upload-content">
												<i class="fal fa-upload"></i>
												<span>Carregar as fotos aqui.</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="meu-kit-briefing-row">
					<div class="wrap">
						<div class="meu-kit-briefing-row-container">
							<div class="meu-kit-briefing-row-title">
								<h3>Confira as medidas da sua parede</h3>
								<p>Esse passo é bem importante pra não enviarmos nada fora da medida ok?</p>
							</div>
							<div class="meu-kit-briefing-row-content">
								<div class="meu-kit-briefing-medida-parede">
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="meu-kit-briefing-row sobre-os-produtos">
					<div class="wrap">
						<div class="meu-kit-briefing-row-container">
							<div class="meu-kit-briefing-row-title">
								<h3>Suas ideias</h3>
							</div>
							<div class="meu-kit-briefing-row-content">
								<div class="meu-kit-briefing-sobre-projeto">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="meu-kit-briefing-row">
					<div class="wrap">
						<div class="meu-kit-briefing-row-container">
							<div class="meu-kit-briefing-row-title">
								<p>Outros produtos incluídos:</p>
							</div>
							<div class="meu-kit-briefing-row-content">
								<div class="meu-kit-briefing-sobre-outros-itens">
								</div>
							</div>
						</div>
					</div>
				</div>

				<section class="common-passos-checkout">
					<div class="wrap">
						<div class="common-passos-checkout-container">
							<h3>Tudo certo?!</h3>
							<p>Agora vamos finalizar e decorar logo logo sua casa :)</p>

							<div class="box-button">
								<a href="<?php echo site_url(); ?>/tudo-certo" class="btn-voltar">
									<span><i class="fas fa-caret-left"></i> VOLTAR</span>
								</a>
								<div class="product-summary-cart-row">
									<input type="hidden" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" />
									<button type="submit" name="add-to-cart" value="<?php echo esc_attr($product->get_id()); ?>" class="single_add_to_cart_button button alt btn-sing" id="buy_now_button">
										<i></i>
										<span><?php echo esc_html('Vamos lá?!'); ?></span>
									</button>
									<input type="hidden" name="is_buy_now" id="is_buy_now" value="0" />
								</div>
							</div>
						</div>
					</div>
				</section>

			</section>
			<!-- Fim coluna tudo certo-->

			<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
		</form>

		<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>
	<?php endwhile;?>
<?php endif;?>




	<script type="text/javascript">
		$(document).ready(function () {

			//Ações do briefing ========================

			//Reposiciona os elementos

			$('.imagens-wrapper').appendTo('.envia-produtos-box');
			$('.largura-wrapper').appendTo('.meu-kit-briefing-medida-parede');
			$('.altura-wrapper').appendTo('.meu-kit-briefing-medida-parede');
			$('.informaes_adicionais-wrapper').appendTo('.meu-kit-briefing-medida-parede');
			$('.dos_produtos_que_voc_escolheu-wrapper').appendTo('.meu-kit-briefing-sobre-kit');
			$('.sobre_os_produtos_escolhidos-wrapper').appendTo('.meu-kit-briefing-sobre-projeto');
			$('.sobre_outro_produto-wrapper').appendTo('.meu-kit-briefing-sobre-outros-itens');
			$('.mais_informaes-wrapper').appendTo('.meu-kit-briefing-mais-informacoes');

			//Adiciona produtos da wishlist ao textarea

			var $product = $('.meu-kit-briefing-confere-produtos .confere-item'),
				$selectProduct = $product.find('input[type=checkbox]'),
				$productsKit = $('textarea[name=produtos_do_kit]'),
				$productsValue = $('.temp-values'),
				$processa = $('.button-processa');

			$selectProduct.each(function () {
				var _this = $(this);
				var $id = _this.attr('id');
				_this.change(function () {
					if($(this).prop('checked')) {
						$productsValue.append('<span class="'+ _this.attr('id') +'">' + _this.val() + '</span>');
					} else {
						$('span.'+ $id +'').remove();
					}
				});
			});

			//Carrega Imagens

			$('.file-upload').click(function () {
				$('.imagens-wrapper input[type=file').click();
			});

			//Processa Briefing

			$('#buy_now_button').click(function(){
				var $text = $productsValue.find('span');
				$productsKit.text('')
				$text.each(function () {
					$productsKit.append($(this).text() + ' | ')
				});
				// set value to 1
				$('#is_buy_now').val('1');
				//submit the form
				$('form.cart').submit();
			});

			//esconde section 1
			jQuery(function(){
		        jQuery("#vamosla").click(function(e){
		            e.preventDefault();
		            if( jQuery('#sectionconfere').is(':visible') ) {
					  jQuery("#sectionconfere").css("display","block");
					} else {
					   jQuery("#sectionconfere").fadeToggle("slow");
					   jQuery("#sectionbriefing").hide("slow");
					   jQuery("#passo2").addClass( "active" );
					}	            
		           
		        });
	    	});

			//esconde section 2

		});
	</script>
<?php get_footer(); ?>