<?php
/**
 * Template Name: Meu Kit Briefing BACKUP
 * meu-kit-briefing.php
 *
 * Desenvolvido por Agência 904
 * Copyright © Todos os direitos reservados
 *
 * @link https://www.walldone.com.br/
 * @author https://www.walldone.com.br/
 * @package Wall Done
 * @subpackage Wall Done
 * @since Wall Done 3.0
 */
?>
<?php get_header(); ?>
<section class="common-header">
	<div class="wrap">
		<div class="common-header-container">
			<?php do_action( 'woocommerce_before_main_content' ); ?>
			<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
				<h1 class="woocommerce-products-header__title page-title"><?php the_title(); ?></h1>
			<?php endif; ?>
		</div>
	</div>
</section>

<section class="meu-kit-walk">
	<div class="wrap">
		<div class="meu-kit-walk-container">
			<div class="walk active">
				<div class="walk-content">
					<div class="walk-number">
						1
					</div>
					<div class="walk-text">
						Briefing do<br>Projeto
					</div>
				</div>
			</div>		
			<div class="walk">
				<div class="walk-content">
					<div class="walk-number">
						2
					</div>
					<div class="walk-text">
						Tudo certo?
					</div>
				</div>
			</div>
			<div class="walk">
				<div class="walk-content">
					<div class="walk-number">
						3
					</div>
					<div class="walk-text">
						Finalizar
					</div>
				</div>
			</div>		
		</div>
	</div>
</section>

<section class="meu-kit-briefing">
	<div class="meu-kit-briefing-row">
		<div class="wrap">
			<div class="meu-kit-briefing-row-container">
				<div class="meu-kit-briefing-row-title">
					<h3><span>1.1</span> Selecione os produtos que você deseja montar o Kit</h3>
					<p>Se você selecionou produtos para montar seu Kit comigo, eles irão aparecer abaixo.</p>
				</div>
				<div class="meu-kit-briefing-row-content">
					<div class="meu-kit-briefing-confere-produtos">
						<?php
						
						
						$cartItemKey = WC()->cart->find_product_in_cart( 129 );
						WC()->cart->remove_cart_item( $cartItemKey );


						    global $woocommerce;
						    $items = $woocommerce->cart->get_cart();

						        foreach($items as $item => $values) { 
						        	echo '<div class="confere-item">';
						        	echo '<input type="checkbox" value="'.$values['product_id'].'" id="cb'.$values['product_id'].'">'; 
						        	echo '<label for="cb'.$values['product_id'].'">';
						            $_product =  wc_get_product( $values['data']->get_id() );
						            $getProductDetail = wc_get_product( $values['product_id'] );
						            echo '<div class="confere-item-image">' . $getProductDetail->get_image() . '</div>';
						            echo '<div class="confere-item-title">' . $_product->get_title() .'</div>';
						            echo '</label>';
						            echo '</div>';
						        }
						?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="meu-kit-briefing-row">
		<div class="wrap">
			<div class="meu-kit-briefing-row-container">
				<div class="meu-kit-briefing-row-title">
					<h3><span>1.2</span>  Envie fotos da sua parede</h3>
					<p>Se você selecionou produtos para montar seu Kit comigo, eles irão aparecer abaixo.</p>
				</div>
				<div class="meu-kit-briefing-row-content">
					<div class="meu-kit-briefing-envia-produtos">
						<div class="envia-produtos-box">
							<?php echo do_shortcode('[wp-dropzone title="Arraste imagens da sua parede aqui :)" desc="Your file will be uploaded to media" border-width="2" border-style="dashed" border-color="#dd102d" background="#fbfbfb" max-file-size="5" accepted-files="image/*" max-files="5" max-files-alert="One file is enough!" auto-process="true" upload-button-text="Click to Upload" guest-upload="true" remove-links="false" clickable="true" id="321"]'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="meu-kit-briefing-row">
		<div class="wrap">
			<div class="meu-kit-briefing-row-container">
				<div class="meu-kit-briefing-row-title">
					<h3><span>1.3</span>  Me diga as medidas da sua parede</h3>
					<p>Lorem ipsum dolor sit ame, consectetur adipisicing elit, sed do eiusmod tempor.</p>
				</div>
				<div class="meu-kit-briefing-row-content">
					<div class="meu-kit-briefing-medida-parede">
						<input type="text" class="form-control" name="wall_width" placeholder="Largura">
						<input type="text" class="form-control" name="wall_height" placeholder="Altura">
						<input type="text" class="form-control" name="wall_comments" placeholder="Comentários adicionais">
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="meu-kit-briefing-row">
		<div class="wrap">
			<div class="meu-kit-briefing-row-container">
				<div class="meu-kit-briefing-row-title">
					<h3><span>1.4</span>  A parte mais gostosa. Me fale do que você gosta!</h3>
					<p>Lorem ipsum dolor sit ame, consectetur adipisicing elit, sed do eiusmod tempor. Dos produtos que você escolheu:</p>
				</div>
				<div class="meu-kit-briefing-row-content">
					<div class="meu-kit-briefing-sobre-kit">
						<label for="">
							<input type="checkbox" name="1" value="1"/> 
							<span>Somente eles farão parte do Kit.</span>
						</label>

						<label for="">
							<input type="checkbox" name="2" value="2" /> 
							Serão parte do kit, gostaria que me ajudasse a escolher mais alguns para compor.
						</label>

						<label for="">
							<input type="checkbox" name="3" value="3" /> 
							Não escolhi nenhum, preciso de ajuda começando do zero.
						</label>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="meu-kit-briefing-row">
		<div class="wrap">
			<div class="meu-kit-briefing-row-container">
				<div class="meu-kit-briefing-row-title">
					<h3><span>1.4</span>  A parte mais gostosa. Me fale do que você gosta!</h3>
					<p>Lorem ipsum dolor sit ame, consectetur adipisicing elit, sed do eiusmod tempor. Dos produtos que você escolheu:</p>
				</div>
				<div class="meu-kit-briefing-row-content">
					<div class="meu-kit-briefing-sobre-projeto">
						<textarea name="Mensagem" cols="40" rows="10" class="textarea-control" placeholder="Digite sua resposta aqui"></textarea>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>