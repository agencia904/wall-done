<?php
/**
 * Template Name: Meu Kit
 * meu-kit.php
 *
 * Desenvolvido por Agência 904
 * Copyright © Todos os direitos reservados
 *
 * @link https://www.walldone.com.br/
 * @author https://www.walldone.com.br/
 * @package Wall Done
 * @subpackage Wall Done
 * @since Wall Done 3.0
 */
?>
<?php get_header(); ?>
<section class="common-header">
	<div class="wrap">
		<div class="common-header-container">
			<?php do_action( 'woocommerce_before_main_content' ); ?>
			<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
				<h1 class="woocommerce-products-header__title page-title"><?php the_title(); ?></h1>
			<?php endif; ?>
		</div>
	</div>
</section>

<section class="meu-kit-intro">
	<div class="wrap">
		<div class="meu-kit-intro-container">
			<div class="column">
				<div class="meu-kit-intro-iframe">
					<img id="video-remove" src="<?php bloginfo('template_directory');?>/assets/images/meu-kit/meu-kit-video-walldone.jpg" alt="Video | Wall Done" />
					<iframe id="video-player" src="https://player.vimeo.com/video/189330068" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
					
					<div class="meu-kit-intro-active-video">
						<i class="fal fa-play"></i>
					</div>
				</div> 
			</div>		
			<div class="column">
				<h2>Como funciona?</h2>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolo.
				</p>
				<div class="box-button">
					<a href="<?php echo site_url(); ?>/briefing-do-projeto" class="btn-sing">
						<span><i class="fas fa-caret-right"></i> QUERO COMEÇAR</span>
					</a>
				</div>
			</div>		
		</div>
	</div>
</section>

<section class="meu-kit-steps">
	<div class="wrap">
		<div class="meu-kit-steps-container">
			<div class="meu-kit-steps-row right">
				<div class="meu-kit-step">
					<figure>
						<img src="<?php bloginfo('template_directory');?>/assets/images/meu-kit/meu-kit-passo-1-briefing-do-projeto-walldone.png" alt="Briefing do Projeto - Passo 1 - Meu Kit | Wall Done" />
					</figure>
					<div class="meu-kit-step-title">
						<span>1.</span><h2>BRIEFING<br> DO PROJETO</h2>
					</div>
					<ul>
						<li>Escolha dos produtos.</li>
						<li>Upload das imagens.</li>
						<li>Medidas da parede.</li>
						<li>Ambiente em que ela está.</li>
						<li>Demais perguntas (questionário).</li>
					</ul>
				</div>
				<div class="meu-kit-step">
					<figure>
						<img src="<?php bloginfo('template_directory');?>/assets/images/meu-kit/meu-kit-passo-2-tudo-certo-walldone.png" alt="Tudo certo - Passo 2 - Meu Kit | Wall Done" />
					</figure>
					<div class="meu-kit-step-title">
						<span>2.</span><h2>TUDO<br> CERTO?</h2>
					</div>
					<ul>
						<li>Compilado de todas as informações que você preencheu e botões para voltar e preencher novamente ou prosseguir.</li>
					</ul>
				</div>
				<div class="meu-kit-step">
					<figure>
						<img src="<?php bloginfo('template_directory');?>/assets/images/meu-kit/meu-kit-passo-3-pagamento-walldone.png" alt="Pagamento - Passo 3 - Meu Kit | Wall Done" />
					</figure>
					<div class="meu-kit-step-title">
						<span>3.</span><h2>PAGAMENTO</h2>
					</div>
					<ul>
						<li>Compilado de todas as informações que você preencheu e botões para voltar e preencher novamente ou prosseguir.</li>
					</ul>
				</div>
				<div class="meu-kit-step">
					<figure>
						<img src="<?php bloginfo('template_directory');?>/assets/images/meu-kit/meu-kit-passo-4-confirmacao-do-pagamento-walldone.png" alt="Confirmacão do Pagamento - Passo 4 - Meu Kit | Wall Done" />
					</figure>
					<div class="meu-kit-step-title">
						<span>4.</span><h2>CONFIRMAÇÃO DO PAGAMENTO</h2>
					</div>
					<ul>
						<li>Após o pagamento, você receberá um e-mail de confirmação.</li>
					</ul>
				</div>
			</div>
			<div class="meu-kit-steps-row left">
				<div class="meu-kit-step">
					<figure>
						<img src="<?php bloginfo('template_directory');?>/assets/images/meu-kit/meu-kit-passo-5-envio-do-projeto-walldone.png" alt="Envio do Projeto - Passo 5 - Meu Kit | Wall Done" />
					</figure>
					<div class="meu-kit-step-title">
						<span>5.</span><h2>ENVIO DO<br> PROJETO</h2>
					</div>
					<ul>
						<li>Irei enviar por e-mail 3 opções de kits para sua validação. Você terá direito a até 2 alterações.</li>
						<li>A comunicação segue por e-mail até a validação final.</li>
					</ul>
				</div>
				<div class="meu-kit-step">
					<figure>
						<img src="<?php bloginfo('template_directory');?>/assets/images/meu-kit/meu-kit-passo-6-compra-voucher-walldone.png" alt="Compra + Voucher - Passo 6 - Meu Kit | Wall Done" />
					</figure>
					<div class="meu-kit-step-title">
						<span>6.</span><h2>COMPRA +<br> VOUCHER</h2>
					</div>
					<ul>
						<li>Cliente efetua a compra e aplica o cupom de desconto referente ao valor do voucher.</li>
					</ul>
				</div>
				<div class="meu-kit-step">
					<figure>
						<img src="<?php bloginfo('template_directory');?>/assets/images/meu-kit/meu-kit-passo-7-envio-dos-produtos-walldone.png" alt="Envio dos produtos do Kit + Gabarito - Passo 7 - Meu Kit | Wall Done" />
					</figure>
					<div class="meu-kit-step-title">
						<span>7.</span><h2>ENVIO DOS<br> PRODUTOS DO<br> KIT + GABARITO</h2>
					</div>
				</div>
				<div class="meu-kit-step start">
					<a href="<?php echo site_url(); ?>/briefing-do-projeto" class="btn-sing">
						<span><i class="fas fa-play"></i></span>
					</a>
					<img class="click-aqui" src="<?php bloginfo('template_directory');?>/assets/images/meu-kit/meu-kit-passo-8-clique-para-comecar-walldone.png" alt="Clique aqui para começar | Wall Done" />
					<h3>Clique aqui<br> para começar!</h3>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>