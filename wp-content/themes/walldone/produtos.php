<?php
/**
 * Template Name: Produtos
 * produtos.php
 *
 * Desenvolvido por Agência 904
 * Copyright © Todos os direitos reservados
 *
 * @link https://www.walldone.com.br/
 * @author https://www.walldone.com.br/
 * @package Wall Done
 * @subpackage Wall Done
 * @since Wall Done 3.0
 */
?>
<?php get_header(); ?>

<section class="common-header">
	<div class="wrap">
		<div class="common-header-container">		
			<nav class="breadcrumb">
				<?php if ( function_exists( 'breadcrumb_trail' ) ) breadcrumb_trail(); ?>
			</nav>
			<h1> Produtos </h1>
		</div>
	</div>
</section>

<section class="produto-common first">
	<div class="wrap">
		<div class="produto-common-container">
			<div class="produto-common-description base-color-1">
				<h4>Lançamentos</h4>
				<span></span>
				<p>Que tal estas novidades fresquinhas feitas a mão?</p>
			</div>
			<div class="common-list-produtos">
				<div class="box-produtos">
					<?php
						$args = array(
							'post_type' => 'product',
							'posts_per_page' => 8,
							// 'category_name'  => 'A CATEGORIA TEM QUE SER CRIADA ( APÓS SER CRIADA COLOCAR NOME AQUI )',
							'orderby'        => 'rand'
							);
						$loop = new WP_Query( $args );
						if ( $loop->have_posts() ) {
							while ( $loop->have_posts() ) : $loop->the_post();
								wc_get_template_part( 'content', 'product' );
							endwhile;
						} else {
							echo __( 'Sem Produtos Cadastrado' );
						}
						wp_reset_postdata();
					?>			
				</div>
			</div>
		</div>
	</div>
</section>

<section class="produto-common">
	<div class="wrap">
		<div class="produto-common-container">
			<div class="produto-common-description base-color-2">
				<h4>Mais Vendidos</h4>
				<span></span>
				<p>Estes são os mais vendidos :)</p>
			</div>
			<div class="common-list-produtos">
				<div class="box-produtos">
					<?php
						$args = array(
							'post_type' => 'product',
							'posts_per_page' => 8,
							// 'category_name'  => 'A CATEGORIA TEM QUE SER CRIADA ( APÓS SER CRIADA COLOCAR NOME AQUI )',
							'orderby'        => 'rand'
							);
						$loop = new WP_Query( $args );
						if ( $loop->have_posts() ) {
							while ( $loop->have_posts() ) : $loop->the_post();
								wc_get_template_part( 'content', 'product' );
							endwhile;
						} else {
							echo __( 'Sem Produtos Cadastrado' );
						}
						wp_reset_postdata();
					?>			
				</div>
			</div>
		</div>
	</div>
</section>

<section class="produto-common">
	<div class="wrap">
		<div class="produto-common-container">
			<div class="produto-common-description base-color-3">
				<h4>Mais Queridos</h4>
				<span></span>
				<p>Estes são os mais queridos pelos meus clientes</p>
			</div>
			<div class="common-list-produtos">
				<div class="box-produtos">
					<?php
						$args = array(
							'post_type' => 'product',
							'posts_per_page' => 8,
							// 'category_name'  => 'A CATEGORIA TEM QUE SER CRIADA ( APÓS SER CRIADA COLOCAR NOME AQUI )',
							'orderby'        => 'rand'
							);
						$loop = new WP_Query( $args );
						if ( $loop->have_posts() ) {
							while ( $loop->have_posts() ) : $loop->the_post();
								wc_get_template_part( 'content', 'product' );
							endwhile;
						} else {
							echo __( 'Sem Produtos Cadastrado' );
						}
						wp_reset_postdata();
					?>			
				</div>
			</div>
		</div>
	</div>
</section>

<section class="produto-common">
	<div class="wrap">
		<div class="produto-common-container">
			<div class="produto-common-description base-color-4">
				<h4>Feito para você</h4>
				<span></span>
				<p>Estes são os mais queridos feito para você</p>
			</div>
			<div class="common-list-produtos">
				<div class="box-produtos">
					<?php
						$args = array(
							'post_type' => 'product',
							'posts_per_page' => 8,
							// 'category_name'  => 'A CATEGORIA TEM QUE SER CRIADA ( APÓS SER CRIADA COLOCAR NOME AQUI )',
							'orderby'        => 'rand'
							);
						$loop = new WP_Query( $args );
						if ( $loop->have_posts() ) {
							while ( $loop->have_posts() ) : $loop->the_post();
								wc_get_template_part( 'content', 'product' );
							endwhile;
						} else {
							echo __( 'Sem Produtos Cadastrado' );
						}
						wp_reset_postdata();
					?>			
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>
