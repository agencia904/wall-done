<?php
/**
 * Template Name: promocoesções
 * promocoes.php
 *
 * Desenvolvido por Agência 904
 * Copyright © Todos os direitos reservados
 *
 * @link https://www.walldone.com.br/
 * @author https://www.walldone.com.br/
 * @package Wall Done
 * @subpackage Wall Done
 * @since Wall Done 3.0
 */
?>
<?php get_header(); ?>
<?php
$args = array(
  'posts_per_page'	=>  '-1',
  'post_type' 		=> 'slider_promo',
  'orderby'   		=> 'meta_value_num',
  'order'     		=> 'DESC'
  );
$query = new WP_Query( $args );
if($query->have_posts()) : ?>
<section class="promocoes-slider">
	<div class="promocoes-slider-item">
		<?php while($query->have_posts()) : $query->the_post(); ?>
			<div class="promocoes-slider-item-container">
				<div class="promocoes-slider-bg" style="background-image: url(<?php echo the_post_thumbnail_url('full'); ?>);"></div>
				<div class="promocoes-slider-container">
					<div class="wrap">
						<div class="promocoes-slider-content">
							<div class="promocoes-slider-title"><?php the_title(); ?></div>
							<div class="promocoes-slider-desc"><?php the_content(); ?></div>
							<?php if(get_post_meta( get_the_ID(), 'slider_link', true) && get_post_meta( get_the_ID(), 'slider_link_title', true)) : ?>
							<div class="box-button">
								<a href="<?php echo get_post_meta( get_the_ID(), 'slider_link', true); ?>" class="btn-sing">
									<span><i class="fas fa-caret-right"></i> <?php echo get_post_meta( get_the_ID(), 'slider_link_title', true); ?></span>
								</a>
							</div>
							<?php endif; ?>
						</div>	
					</div>	
				</div>
			</div>
		 <?php endwhile; ?>
	</div>
</section>
<?php endif;?>

<section class="archive-product">
	<div class="wrap">
		<div class="archive-product-container">
			<aside class="archive-product-sidebar">
				<?php dynamic_sidebar('sidebar-1'); ?>

				<div class="banners">
					<a href="<?php echo $link; ?>">
						<img src="<?php echo $imagem; ?>" alt="">
					</a>
				</div>
			</aside>
			<div class="archive-product-content">
				<?php 
					if ( woocommerce_product_loop() ) { 

						$args = array(
						    'post_type'      => 'product',
						    'posts_per_page' => 12,
						    'paged'=>$paged,
						    'meta_query'     => array(
						        'relation' => 'OR',
						        array( // Simple products type
						            'key'           => '_sale_price',
						            'value'         => 0,
						            'compare'       => '>',
						            'type'          => 'numeric'
						        ),
						        array( // Variable products type
						            'key'           => '_min_variation_sale_price',
						            'value'         => 0,
						            'compare'       => '>',
						            'type'          => 'numeric'
						        )
						    )
						);
						$loop = new WP_Query( $args );
						if ( $loop->have_posts() ) {
				?>
				<div class="archive-product-list">
					<div class="common-list-products">
						<div class="common-list-products-box columns-3">
							<ul class="products">
								<?php
								while ( $loop->have_posts() ) : $loop->the_post();
									wc_get_template_part( 'content', 'product' );
								endwhile;
								?>	
							</ul>
						</div>
					</div>
				</div>
				<?php
				$big = 999999999;
				$the_query = new WP_Query( $args );

				if ($the_query->max_num_pages > 1) :
				?>
				<nav class="common-nav">
				    <div class="wrap">
				    	<div class="common-nav-container">
				    		<?php
				    		echo paginate_links( array(
				    		    'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
				    		    'format' => '?paged=%#%',
				    		    'current' => max( 1, get_query_var('paged') ),
				    		    'total' => $the_query->max_num_pages,
				    		    'prev_text' => __( '←', 'textdomain' ),
				    		    'next_text' => __( '→', 'textdomain' ),
				    		) );
				    		?>
				    	</div>
				    </div>
				</nav>
				<?php endif; ?>
				<?php
					} else {
						do_action( 'woocommerce_no_products_found' );
					}
					wp_reset_postdata();
				} else {
					do_action( 'woocommerce_no_products_found' );
				}
				?>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>