<?php
/**
 * search.php
 *
 * Desenvolvido por Agência 904
 * Copyright © Todos os direitos reservados
 *
 * @link https://www.walldone.com.br/
 * @author https://www.walldone.com.br/
 * @package Wall Done
 * @subpackage Wall Done
 * @since Wall Done 3.0
 */
?>
<?php get_header(); ?>
<section class="blog-header single">
	<div class="wrap">
		<div class="blog-header-container">
			<?php do_action( 'woocommerce_before_main_content' ); ?>
			<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
				<h1>
					<i class="fal fa-comment-alt-lines"></i>
					<span><?php printf( esc_html__( 'Resultados para: %s', stackstar ), get_search_query() ); ?></span>
				</h1>
			<?php endif; ?>
		</div>
	</div>
</section>

<section class="blog-main">
	<div class="wrap">
		<div class="blog-main-container">
			<div class="blog-main-content">
				<div class="blog-list-archive">
					<?php
					$blog = array(
					'posts_per_page' 	=>  '10',
					'post_type'			=> 'post',
					's'					=> $s,
					'orderby'   		=> 'date menu_order',
					'order'     		=> 'DESC',
					'paged'=>$paged
					);
					$query = new WP_Query( $blog ); 

					if($query->have_posts()) : while($query->have_posts()) : $query->the_post(); ?>
						<div class="blog-list-post">
							<a href="<?php print get_permalink($post->ID) ?>">
								<figure class="blog-list-post-image">
									<?php if (has_post_thumbnail()) : ?>
										<?php the_post_thumbnail(); ?>
									<?php else : ?>
										<img src="<?php bloginfo('template_directory');?>/assets/images/imagem-nao-disponivel-walldone.jpg" alt="Imagem não disponível | Wall Done" />	
									<?php endif; ?>    
								</figure>
								<div class="blog-list-post-desc">
									<div class="blog-list-post-info">
										<span>por <b><?php echo get_the_author(); ?></b> em <b><?php echo get_the_date(); ?></b></span> 
									</div>
									<h3><?php echo the_title(); ?></h3>
								</div>
							</a>						
						</div>
					<?php endwhile; ?>
					<?php else : ?>
						<div class="woocommerce-message woocommerce-message--error woocommerce-Message woocommerce-Message--error woocommerce-error">
							Não foram encontrados resultados para o termo <b><?php echo get_search_query(); ?></b>.
						</div>
					<?php endif; ?>
					<?php wp_reset_query(); ?>
				</div>
				<?php
				$big = 999999999;
				$the_query = new WP_Query( $blog );

				if ($the_query->max_num_pages > 1) :
				?>
				<nav class="common-nav">
				    <div class="wrap">
				    	<div class="common-nav-container">
				    		<?php
				    		echo paginate_links( array(
				    		    'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
				    		    'format' => '?paged=%#%',
				    		    'current' => max( 1, get_query_var('paged') ),
				    		    'total' => $the_query->max_num_pages,
				    		    'prev_text' => __( '←', 'textdomain' ),
				    		    'next_text' => __( '→', 'textdomain' ),
				    		) );
				    		?>
				    	</div>
				    </div>
				</nav>
				<?php endif; ?>
			</div>
			<aside class="blog-main-sidebar">
				<?php include_once('blog-sidebar.php'); ?>
			</aside>
		</div>
	</div>
</section>
<?php get_footer(); ?>