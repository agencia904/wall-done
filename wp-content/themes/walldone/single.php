<?php
/**
 * single.php
 *
 * Desenvolvido por Agência 904
 * Copyright © Todos os direitos reservados
 *
 * @link https://www.walldone.com.br/
 * @author https://www.walldone.com.br/
 * @package Wall Done
 * @subpackage Wall Done
 * @since Wall Done 3.0
 */
?>
<?php get_header(); ?>
<section class="blog-header single">
	<div class="wrap">
		<div class="blog-header-container">
			<?php do_action( 'woocommerce_before_main_content' ); ?>
			<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
				<h1>
					<i class="fal fa-comment-alt-lines"></i>
					<span><?php the_title(); ?></span>
				</h1>
			<?php endif; ?>
		</div>
	</div>
</section>

<section class="blog-main">
	<div class="wrap">
		<div class="blog-main-container">
			<div class="blog-main-content">
				<?php while (have_posts()) { the_post(); ?>
					<div class="article-header">
						<div class="article-header-image">
							<figure>
								<?php if (has_post_thumbnail()) { the_post_thumbnail('full'); } else { ?>
								    <img src="<?php bloginfo('template_directory');?>/assets/images/imagem-nao-disponivel-walldone.jpg" alt="Imagem não disponível | Wall Done" />
								<?php } ?>
							</figure>
						</div>

						<div class="article-header-info">
							<div class="column">
								<div class="header-info-item author">
									<i class="fal fa-user"></i>
									<span><?php echo get_the_author(); ?></span>
								</div>
								<div class="header-info-item cat">
									<i class="fal fa-location"></i>
									<span><?php $cat = get_the_category(); $cat_name = $cat[0]->cat_name; echo $cat_name; ?></span>
								</div>
								<div class="header-info-item date">
									<i class="fal fa-calendar-alt"></i>
									<span><?php echo get_the_date(); ?></span>
								</div>
							</div>
							<div class="column">
								<div class="common-social">
									<i class="share">Compartilhe:</i>
									<?php echo do_shortcode('[cresta-social-share]'); ?>
								</div>
							</div>
						</div>

						<div class="article-header-title">
							<h2><?php echo get_the_title(); ?></h2>
						</div>
					</div>

					<article class="article-content">
						<?php echo the_content(); ?>
					</article>
				<?php } ?>
			</div>
			<aside class="blog-main-sidebar">
				<?php include_once('blog-sidebar.php'); ?>
			</aside>
		</div>
	</div>
</section>
<?php get_footer(); ?>