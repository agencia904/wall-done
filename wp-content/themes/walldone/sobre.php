<?php
/**
 * Template Name: Sobre
 * sobre.php
 *
 * Desenvolvido por Agência 904
 * Copyright © Todos os direitos reservados
 *
 * @link https://www.walldone.com.br/
 * @author https://www.walldone.com.br/
 * @package Wall Done
 * @subpackage Wall Done
 * @since Wall Done 3.0
 */
?>
<?php get_header(); ?>
<section class="common-header">
	<div class="wrap">
		<div class="common-header-container">
			<?php do_action( 'woocommerce_before_main_content' ); ?>
			<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
				<h1 class="woocommerce-products-header__title page-title"><?php the_title(); ?></h1>
			<?php endif; ?>
		</div>
	</div>
</section>

<section class="page-sobre">
	<div class="wrap">
		<div class="page-sobre-container">

			<div class="page-sobre-content">
				<div class="page-sobre-content-left">
					<figure class="page-sobre-image-top">
						<img src="<?php bloginfo('template_directory');?>/assets/images/sobre/era-uma-vez-sobre-walldone.jpg" alt="Era uma vez - Sobre | Wall Done" />
					</figure>
					
					<h2>Era uma vez</h2>

					<p>Olá, sou Larissa Rothen, arquiteta e criadora da Wall Done. Quero dividir com vocês como nasceu essa minha grande paixão pela decoração de paredes.</p>

					<p>Tudo começou logo após a lua-de-mel, quando o Edu e eu fomos morar no nosso primeiro apê. Nossa grana era bem curta para decorar a casa (acho que isso acontece com a maioria dos recém casados né?!) e de início compramos apenas o essencial.</p>

					<p>Alguns meses depois (o orçamento continuava apertado =/) surgiu o grande desejo de deixar nossa casa do nosso jeitinho: viva, alegre, colorida e feliz.</p>

					<p>Aí veio o grande desafio: como fazer isso sem gastar muito?</p>

					<p>Foi então que entrou em cena a minha criatividade de arquiteta e a resposta veio num clique: decorar as paredes!</p>

					<p>A ideia foi se concretizando e as peças começaram a se encaixar de forma perfeita. Seriam mudanças simples, mas que dariam “aquela renovada nos ambientes”, através de cenários montados com objetos que refletiam nossa essência, nosso modo de vida.</p>

					<p>Decidi então começar pela parede que divide a sala do corredor, pois era a que mais pedia socorro. Era pálida, sem vida, sem graça. E, como a sala é o ambiente onde mais ficamos e recebemos nossos amigos, não tive dúvidas de que precisava dar aquele “up” para criar uma atmosfera de alegria e aconchego.</p>

					<p>O resultado é esse aí que você está vendo na foto. Uma composição de objetos com cores, formas, texturas e que são a nossa cara. Comecei com poucos itens, já adicionamos vários outros (agora no plural porque o Edu me ajudou) e queremos ainda mais. Afinal, as paredes são sempre espaços para algo novo entrar =)</p>

					<p>E foi assim que a Wall Done veio ao mundo. Para ser muito mais que um negócio: uma filosofia de vida. Uma maneira de oferecer às pessoas a mesma experiência que tive ao decorar minhas paredes e deixar minha casa ainda mais feliz.</p>
				</div>
				<div class="page-sobre-content-right">
					<h2>Sobre a Wall Done</h2>
					
					<p>Olá, nós somos a Wall Done e amamos decorar paredes.</p>

					<p>Nosso desejo é dar um toque todo especial na decoração do seu cantinho, com muita criatividade, design e uma pitada de personalidade.</p>

					<p>Acreditamos no poder que as paredes têm de transformar e valorizar qualquer ambiente e deixar sua casa com o jeitinho das pessoas que lá vivem.</p>

					<p>Decorar paredes é muito mais do que pendurar quadros e outros objetos. É viajar pelo tempo e dar asas à imaginação. Voltar ao passado para relembrar nossas origens, as lembranças dos momentos mais especiais e das pessoas queridas que moram no nosso coração. Vivenciar o presente, o que somos, nossas escolhas, conquistas, amores e tudo que revela nossa essência e inspira nosso dia a dia. E, depositar no futuro todos nossos sonhos e desejos.</p>

					<p>Queremos levar cada pedacinho do nosso mundo para o seu. Pensando nisso, fizemos com muito carinho uma seleção de objetos estilosos e modernos, especialmente para você!</p>

					<p>Passeie por nossas cores, formas e texturas e experimento o prazer de levar mais beleza e aconchego para o seu lar.</p>

					<p>Fazemos tudo com muito amor e carinho!</p>

					<div class="page-sobre-social">
						<a href="https://www.instagram.com/walldonedecor/" target="_blank" title="Instagram | Wall Done" target="_blank" class="social-icon">
							<i class="fab fa-instagram"></i>
							<span>SIGA-NOS NO INSTAGRAM</span>
						</a>

						<a href="https://www.facebook.com/walldonedecor/" target="_blank" title="Facebook | Wall Done" target="_blank" class="social-icon">
							<i class="fab fa-facebook-f"></i>
							<span>SIGA-NOS NO FACEBOOK</span>
						</a>
					</div>

					<div class="page-sobre-navigate">
						<h5>Continue navegando:</h5>

						<a href="<?php echo site_url(); ?>/as-paredes-tambem-falam" class="btn-simple">
							<i class="fas fa-caret-right"></i>
							<span>
								As paredes também falam
							</span>
						</a>

						<a href="<?php echo site_url(); ?>/blog-da-larissa" class="btn-simple">
							<i class="fas fa-caret-right"></i>
							<span>
								Blog da Larissa
							</span>
						</a>

						<a href="<?php echo site_url(); ?>/produtos" class="btn-simple">
							<i class="fas fa-caret-right"></i>
							<span>
								Loja Wall Done
							</span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>