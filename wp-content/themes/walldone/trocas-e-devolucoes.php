<?php
/**
 * Template Name: Trocas e devoluções
 * trocas-e-devolucoes.php
 *
 * Desenvolvido por Agência 904
 * Copyright © Todos os direitos reservados
 *
 * @link https://www.walldone.com.br/
 * @author https://www.walldone.com.br/
 * @package Wall Done
 * @subpackage Wall Done
 * @since Wall Done 3.0
 */
?>
<?php get_header(); ?>
<section class="common-header">
	<div class="wrap">
		<div class="common-header-container">
			<?php do_action( 'woocommerce_before_main_content' ); ?>
			<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
				<h1 class="woocommerce-products-header__title page-title"><?php the_title(); ?></h1>
			<?php endif; ?>
		</div>
	</div>
</section>

<section class="page-trocas-devolucoes">
	<div class="wrap">
		<div class="page-trocas-devolucoes-container">
			<div class="page-trocas-devolucoes-content">
				<div class="page-trocas-devolucoes-row">
					<h2>Trocas e Devoluções</h2>
					<p>Sua tão esperada encomenda chegou mas você não gostou? Está danificada? A cor não te agradou?</p>

					<p>É simples! Entre em contato com a gente clicando <a href="<?php echo site_url(); ?>">aqui</a> no prazo de até 7 (sete) dias corridos após o recebimento.</p>

					<p>Nós realizaremos a troca ou devolução de produtos segundo as condições citadas abaixo e de acordo com o Código de Defesa do Consumidor:</p>

					<ul>
						<li>O produto deve estar sem indícios de uso e na caixa original ou em outra caixa que garanta a proteção do item;</li>
						<li>É obrigatório enviar a Nota Fiscal junto com o produto devolvido. Em caso de devolução parcial: enviar uma cópia da Nota Fiscal destacando o produto devolvido. Em caso de devolução total: enviar todas as vias da Nota Fiscal original.</li>
						<li>Caso os produtos sejam recebidos de outra forma que não atenda as condições acima, o cliente será notificado e receberá a devolução do mesmo item em seu endereço de entrega. O custo do frete será por conta dele e a troca não será mais feita;</li>
						<li>A troca deverá ser comunicada somente através do e-mail falecom@walldone.com.br, para que o pedido fique registrado e documentado. No e-mail o cliente deve informar o motivo da troca e o novo item escolhido;</li>
						<li>Caso o cliente queira trocar pelo mesmo produto e ele não esteja mais disponível em estoque, o comprador terá a opção de escolher outro produto, solicitar crédito em nossa loja para ser usado posteriormente ou solicitar o reembolso integral;</li>
						<li>O cliente poderá fazer a troca por qualquer outro item da nossa loja virtual, desde que o mesmo esteja disponível em estoque. Nesse caso não serão liberados cupons para compras futuras;</li>
						<li>O custo do frete para retorno da mercadoria até nós é por nossa conta. As despesas de postagens para o envio do produto trocado são de inteira responsabilidade do cliente.</li>
						<li>A devolução dos produtos no prazo e condições acima estipuladas garante ao cliente o direito ao reembolso do valor das peças e do frete correspondente;</li>
						<li>Após receber e conferir corretamente a mercadoria, realizaremos o reembolso diretamente no cartão de crédito utilizado na compra.</li>
					</ul>
				</div>
				<div class="page-trocas-devolucoes-row">
					<h2>Sua encomenda chegou danificada?</h2>
					<p>Infelizmente o pessoal dos Correios não tem o mesmo carinho que nós ao manusear as embalagens.</p>

					<p>Nós caprichamos bastante na proteção dos produtos dentro das caixas, mas mesmo assim às vezes pode acontecer alguma avaria durante o transporte.</p>

					<p>Se isso acontecer, não se preocupe. Tire a foto do produto na hora e mande para nós através do e-mail <a href="mailto:falecom@walldone.com.br">falecom@walldone.com.br</a>. Envie também fotos de como a embalagem chegou na sua casa.</p>

					<p>O cliente poderá optar pelo reenvio de um produto igual (se disponível em estoque) sem nenhum custo adicional ou pela devolução integral do valor da compra.</p>
				</div>
				<div class="page-trocas-devolucoes-row">
					<h2>Prazos de Entrega</h2>
					<p>Atenção! O prazo de POSTAGEM é diferente do prazo de ENTREGA referente à opção de frete selecionada.</p>

					<p>O prazo de postagem é o tempo que a Wall Done leva para carinhosamente cuidar de todos os pedidos, embalar e postar nos Correios. Esse prazo começa a ser contado a partir da aprovação do seu pagamento.</p>

					<p>O prazo de entrega ( que você escolhe ao comprar) é o prazo dos Correios para entrega do produto após a postagem.</p>

					<p>Uma vez que o pedido é enviado pelos Correios, você receberá automaticamente o código de rastreio para acompanhá-lo.</p>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>