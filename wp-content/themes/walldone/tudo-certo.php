<?php
/**
 * Template Name: Tudo Certo
 * tudo-certo.php
 *
 * Desenvolvido por Agência 904
 * Copyright © Todos os direitos reservados
 *
 * @link https://www.walldone.com.br/
 * @author https://www.walldone.com.br/
 * @package Wall Done
 * @subpackage Wall Done
 * @since Wall Done 3.0
 */
?>
<?php get_header(); ?>

<section class="common-header">
	<div class="wrap">
		<div class="common-header-container">		
			<nav class="breadcrumb">
				<?php if ( function_exists( 'breadcrumb_trail' ) ) breadcrumb_trail(); ?>
			</nav>
			<h1> Monte seu Kit com a Larissa! </h1>
		</div>
	</div>
</section>

<section class="common-passos">
	<div class="wrap">
		<div class="common-passos-container">
			<div class="column">
				<div class="box active">
					1
				</div>
				<h2 class="active">briefing do projeto</h2>
			</div>		
			<div class="column">
				<div class="box active ">
					2
				</div>
				<h2 class="active">Tudo certo?</h2>
			</div>
			<div class="column">
				<div class="box">
					3
				</div>
				<h2>Finalizar</h2>
			</div>		
		</div>
	</div>
</section>

<section class="common-passos-checkout">
	<div class="wrap">
		<div class="common-passos-checkout-container">
			<h3>Confira ser está tudo certinho</h3>
			<p>Lorem ipsum dolor sit ame, consectetur adipisicing elit, sed do eiusmod tempor.</p>

			<h3>Produtos e paredes</h3>
			<p>
				Que legal, você optou por eu escolher os seus produtos. Já estou adorando! :)<br>
				Confira as fotos das suas paredes abaixo.
			</p>
			<div class="imagens-selecionadas">
				<ul>
					<li>
						<div class="box-fotos">
							
						</div>
						<p>Parede 1</p>
						<div class="medidas">
							2 x 3 m
						</div>
					</li>
					<li>
						<div class="box-fotos">
							
						</div>
						<p>Parede 2</p>
						<div class="medidas">
							2 x 3 m
						</div>
					</li>
					<li>
						<div class="box-fotos">
							
						</div>
						<p>Parede 3</p>
						<div class="medidas">
							2 x 3 m
						</div>
					</li>
					<li>
						<div class="box-fotos">
							
						</div>
						<p>Parede 4</p>
						<div class="medidas">
							2 x 3 m
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>

<section class="common-passos-checkout dark">
	<div class="wrap">
		<div class="common-passos-checkout-container">
			<h3>Suas ideias</h3>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
		</div>
	</div>
</section>

<section class="common-passos-checkout">
	<div class="wrap">
		<div class="common-passos-checkout-container">
			<h3>Tudo certo?!</h3>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>

			<div class="box-button">
				<a href="<?php echo site_url(); ?>/tudo-certo" class="btn-voltar">
					<span><i class="fas fa-caret-left"></i> VOLTAR</span>
				</a>
				<a href="<?php echo site_url(); ?>/finalizar" class="btn-sing">					
					<span><i class="fas fa-caret-right"></i> VAMOS LÁ?!</span>
				</a>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>