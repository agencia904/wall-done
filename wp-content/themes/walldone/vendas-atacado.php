<?php
/**
 * Template Name: Vendas no atacado
 * vendas-atacado.php
 *
 * Desenvolvido por Agência 904
 * Copyright © Todos os direitos reservados
 *
 * @link https://www.walldone.com.br/
 * @author https://www.walldone.com.br/
 * @package Wall Done
 * @subpackage Wall Done
 * @since Wall Done 3.0
 */
?>
<?php get_header(); ?>
<section class="common-header">
	<div class="wrap">
		<div class="common-header-container">
			<?php do_action( 'woocommerce_before_main_content' ); ?>
			<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
				<h1 class="woocommerce-products-header__title page-title"><?php the_title(); ?></h1>
			<?php endif; ?>
		</div>
	</div>
</section>

<section class="page-vendas-atacado">
	<div class="wrap">
		<div class="page-vendas-atacado-container">
			<div class="page-vendas-atacado-content">
				<div class="page-vendas-atacado-row">
					<div class="column">
						<h2>Caro lojista,</h2>
						
						<p><b>Quer ter a Wall Done na sua loja? =)</b></p>

						<p>Vocês estão convidados a construir essa parceria de sucesso!</p>

						<p>Queremos fazer parte da sua loja e deixá-la ainda mais incrível e cheia de personalidade. Nossos produtos são vendidos em todo Brasil e tem excelente giro em todas as regiões.</p>

						<p>Nos preocupamos em analisar os locais de venda dos nossos produtos, para que você lojista possa sempre oferecer itens diferenciados.</p>

						<p>Para se cadastrar, preencha o formulário.</p>
					</div>
					<div class="column">
						<h4>O que está esperando? Torne-se um Wall Lover!</h4>
						<p>Cadastre-se agora mesmo.</p>
						<?php echo do_shortcode('[contact-form-7 id="385" title="Vendas no atacado"]'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>