<?php
/**
 * wall-cart.php
 *
 * Desenvolvido por Agência 904
 * Copyright © Todos os direitos reservados
 *
 * @link https://www.walldone.com.br/
 * @author https://www.walldone.com.br/
 * @package Wall Done
 * @subpackage Wall Done
 * @since Wall Done 3.0
 */
?>

<div class="wall-cart-container">
    <?php if ( ! WC()->cart->is_empty() ) : ?>

        <div class="wall-cart-content woocommerce-mini-cart cart_list product_list_widget <?php echo esc_attr( $args['list_class'] ); ?>">
            <?php
                do_action( 'woocommerce_before_mini_cart_contents' );

                foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
                    $_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
                    $product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

                    if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_widget_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
                        $product_name      = apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key );
                        $thumbnail         = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );
                        $product_price     = apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
                        $product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
                        ?>

                        <div class="wall-cart-box woocommerce-mini-cart-item <?php echo esc_attr( apply_filters( 'woocommerce_mini_cart_item_class', 'mini_cart_item', $cart_item, $cart_item_key ) ); ?>">

                            <div class="wall-cart-box-image">
                                <?php if ( empty( $product_permalink ) ) : ?>
                                    <?php echo $thumbnail; ?>
                                <?php else : ?>
                                    <a href="<?php echo esc_url( $product_permalink ); ?>">
                                        <?php echo $thumbnail; ?>
                                    </a>
                                <?php endif; ?>
                            </div>

                            <div class="wall-cart-box-title">
                                <div class="wall-cart-box-title-text">
                                    <!-- <?php echo wc_get_formatted_cart_item_data( $cart_item ); ?> -->
                                    <?php if ( empty( $product_permalink ) ) : ?>
                                        <?php echo $product_name; ?>
                                    <?php else : ?>
                                        <a href="<?php echo esc_url( $product_permalink ); ?>">
                                            <?php echo $product_name; ?>
                                        </a>
                                    <?php endif; ?>
                                </div> 
                                <div class="wall-cart-box-qtde">
                                    <?php echo apply_filters( 'woocommerce_widget_cart_item_quantity', '<span class="quantity">' . sprintf( '%s &times; %s', $cart_item['quantity'], $product_price ) . '</span>', $cart_item, $cart_item_key ); ?> 
                                </div>
                            </div>

                            <div class="wall-cart-remove-itens" onclick="(function(){ $('#loader').removeClass('hide'); })()">
                               <?php
                               echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
                                   '<a href="%s" class="remove remove_from_cart_button" aria-label="%s" data-product_id="%s" data-cart_item_key="%s" data-product_sku="%s"><i class="fal fa-times"></i></a>',
                                   esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
                                   __( 'Remove this item', 'woocommerce' ),
                                   esc_attr( $product_id ),
                                   esc_attr( $cart_item_key ),
                                   esc_attr( $_product->get_sku() )
                               ), $cart_item_key );
                               ?>                                     
                            </div>
                        </div>
                        <?php
                    }
                }

                do_action( 'woocommerce_mini_cart_contents' );
            ?>
            <div id="loader" class="wall-cart-load hide"></div>
        </div>

        <?php do_action( 'woocommerce_widget_shopping_cart_before_buttons' ); ?>

        <div class="wall-cart-total woocommerce-mini-cart__total total">
          <p>
            <span class="woocommerce-Price-subtotal"><?php _e( 'Subtotal', 'woocommerce' ); ?>:</span>
            <?php echo WC()->cart->get_cart_subtotal(); ?>
          </p>
        </div>
        <div class="wall-cart-buy">
            <a class="btn-sing" href="<?php echo site_url(); ?>/carrinho">
                <span>
                    <i class="fal fa-cart-plus"></i>
                    FINALIZAR COMPRA
                </span>
            </a>
        </div>
        <div class="wall-cart-meu-kit">
            <a href="<?php echo site_url(); ?>/meu-kit" class="btn-sing" >
                <span>
                    <i class="fal fa-box-heart"></i> Meu Kit
                </span>
            </a>
        </div>

    <?php else : ?>
        <div class="wall-cart-empty woocommerce-mini-cart__empty-message">
            <i class="fal fa-frown"></i>
            <p>Seu carrinho está vazio e triste.</p>
        </div>
    <?php endif; ?>
</div>