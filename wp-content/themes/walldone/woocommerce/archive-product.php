<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );
?>

<?php

// $term = get_queried_object();
// $imagem = get_field('imagem', $term);
// $link = get_field('link', $term);
// $title = get_field('titulo_descricao', $term);
// $desc = get_field('conteudo_descricao', $term);

?>

<section class="common-header">
	<div class="wrap">
		<div class="common-header-container">
			<div class="column">
				<?php do_action( 'woocommerce_before_main_content' ); ?>
				<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
					<h1 class="woocommerce-products-header__title page-title"><?php woocommerce_page_title(); ?></h1>
				<?php endif; ?>

				<?php
				$cat 		= get_queried_object();
				$cat_id 	= $cat->term_id;
				$cat_desc 	= $cat->description;
				?>
			</div>

			<div class="column">
				<?php if ( woocommerce_product_loop() ) : ?>
					<i><?php woocommerce_result_count(); ?></i>
					<div class="select">
						<?php woocommerce_catalog_ordering(); ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>
<section class="archive-product">
	<div class="wrap">
		<div class="archive-product-container">
			<aside class="archive-product-sidebar">
				<?php dynamic_sidebar('sidebar-1'); ?>
				<div class="banners">
					<a href="<?php echo $link; ?>">
						<img src="<?php echo $imagem; ?>" alt="">
					</a>
				</div>
			</aside>
			<div class="archive-product-content">
				<?php if ( woocommerce_product_loop() ) { ?>
				<div class="archive-product-list">
					<div class="common-list-products">
						<div class="common-list-products-box columns-3">
							<?php
								woocommerce_product_loop_start();
								if ( wc_get_loop_prop( 'total' ) ) {
									$counter = 1;
									while ( have_posts() ) {
										the_post();
										do_action( 'woocommerce_shop_loop' );

										wc_get_template_part( 'content', 'product' );

										if( $title && $counter === 4 ) { ?>

											<div class="archive-product-list-quote"><h3><?php echo $title; ?></h3><p><?php echo $desc ;?></p></div>

										<?php }
										$counter++;
									}
								}

								woocommerce_product_loop_end();
							?>
						</div>
					</div>
				</div>
				<?php 
					do_action( 'woocommerce_after_shop_loop' );
				} else {
					do_action( 'woocommerce_no_products_found' );
				}
				?>
			</div>
		</div>
	</div>
</section>
<?php
do_action( 'woocommerce_after_main_content' );

get_footer( 'shop' );
