<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );
?>
<section class="common-header">
	<div class="wrap">
		<div class="common-header-container">
			<div class="column">
				<?php do_action( 'woocommerce_before_main_content' ); ?>
				<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
					<h1 class="woocommerce-products-header__title page-title"><?php woocommerce_page_title(); ?></h1>
				<?php endif; ?>

				<?php
				$cat 		= get_queried_object();
				$cat_id 	= $cat->term_id;
				$cat_desc 	= $cat->description;
				?>
			</div>

			<div class="column">
				<?php if ( woocommerce_product_loop() ) : ?>
					<i><?php woocommerce_result_count(); ?></i>
					<div class="select">
						<?php woocommerce_catalog_ordering(); ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>
<section class="archive-product">
	<div class="wrap">
		<div class="archive-product-container">
			<aside class="archive-product-sidebar">
				<div class="common-sidebar-accordion" id="accordion">
					<?php
					$taxonomy     = 'product_cat';
					$orderby      = 'name';  
					$show_count   = 0;      
					$pad_counts   = 0;      
					$hierarchical = 1;      
					$title        = '';  
					$empty        = 0;

					$args = array(
					    'taxonomy'     => $taxonomy,
					    'orderby'      => $orderby,
					    'show_count'   => $show_count,
					    'pad_counts'   => $pad_counts,
					    'hierarchical' => $hierarchical,
					    'title_li'     => $title,
					    'hide_empty'   => $empty
					);

					$all_categories = get_categories( $args );

					foreach ($all_categories as $cat) {

					    if($cat->category_parent == 0) {

					        $category_id = $cat->term_id;

					        // echo '<br /> ('. $category_id .') <a href="'. get_term_link($cat->slug, 'product_cat') .'">'. $cat->name .'</a>';

					        $args2 = array(
					            'taxonomy'     => $taxonomy,
					            'parent'       => $category_id,
					            'orderby'      => $orderby,
					            'show_count'   => $show_count,
					            'pad_counts'   => $pad_counts,
					            'hierarchical' => $hierarchical,
					            'title_li'     => $title,
					            'hide_empty'   => $empty
					        );

					        $sub_cats = get_categories( $args2 );

					        echo '<ul>';
					        if($sub_cats) {

					            foreach($sub_cats as $sub_category) {

					            	if($cat_id == $sub_category->term_id) {
					            		$active = ' class="active"';
					            	} else {
					            		$active = '';
					            	}

					                echo  '<li' . $active . '><a href="#">' .  $sub_category->name .'</a>';
					                // echo apply_filters( 'woocommerce_subcategory_count_html', ' (' . $sub_category->count . ')', $category );

					                 $args3 = array(
					            'taxonomy'     => $taxonomy,
					            'parent'       =>  $sub_category->term_id,
					            'orderby'      => $orderby,
					            'show_count'   => $show_count,
					            'pad_counts'   => $pad_counts,
					            'hierarchical' => $hierarchical,
					            'title_li'     => $title,
					            'hide_empty'   => $empty
					        );

					        $sub_cats3 = get_categories( $args3 );

					        if($sub_cats3) {
					        	echo '<ul>';
					            foreach($sub_cats3 as $sub_category3) {
					                echo  '<li><a href="'. get_term_link($sub_category3->slug, 'product_cat') .'">'. $sub_category3->name .'</a></li>';
					                // echo apply_filters( 'woocommerce_subcategory_count_html', ' (' . $sub_category3->count . ')', $category );

					            }
					            echo '</ul></li>';


					                 }

					            }
					            echo '</ul>';
					        }
					    }       
					}
					?>
				</div>
				<div class="common-sidebar-filters">
					<?php do_shortcode('[br_filters attribute=price type=slider title="Filtrar por preço"]'); ?>
					<?php do_shortcode('[br_filters attribute=pa_molduras type=checkbox title="Filtrar por atributos"]'); ?>
				</div>
			</aside>
			<div class="archive-product-content">
				<?php if ( woocommerce_product_loop() ) { ?>
				<div class="archive-product-list">
					<div class="common-list-products">
						<div class="common-list-products-box columns-3">
							<?php if(isset($cat_desc)): ?>
							<script>
								$(document).ready(function(){
									$("ul.products li:nth-child(4n)").after('<div class="archive-product-list-quote"><?php echo $cat_desc; ?></div>');
								});
							</script>
						<?php endif; ?>
							<?php
								woocommerce_product_loop_start();

								if ( wc_get_loop_prop( 'total' ) ) {
									while ( have_posts() ) {
										the_post();

										do_action( 'woocommerce_shop_loop' );

										wc_get_template_part( 'content', 'product' );
									}
								}

								woocommerce_product_loop_end();
							?>
						</div>
					</div>
				</div>
				<?php 
					do_action( 'woocommerce_after_shop_loop' );
				} else {
					do_action( 'woocommerce_no_products_found' );
				}
				?>
			</div>
		</div>
	</div>
</section>
<?php
do_action( 'woocommerce_after_main_content' );

get_footer( 'shop' );
