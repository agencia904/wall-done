<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>
<section class="single-wrapper product" id="product-<?php the_ID(); ?>" <?php wc_product_class(); ?>>
	<div class="wrap">
		<div class="single-product-container">
			<div class="column">
				<?php
					/**
					 * Hook: woocommerce_before_single_product_summary.
					 *
					 * @hooked woocommerce_show_product_sale_flash - 10
					 * @hooked woocommerce_show_product_images - 20
					 */
					do_action( 'woocommerce_before_single_product_summary' );
				?>
			</div>
			<div class="column">
				<div class="product-summary summary entry-summary">
					<?php
						/**
						 * Hook: woocommerce_single_product_summary.
						 *
						 * @hooked woocommerce_template_single_title - 5
						 * @hooked woocommerce_template_single_rating - 10
						 * @hooked woocommerce_template_single_price - 10
						 * @hooked woocommerce_template_single_excerpt - 20
						 * @hooked woocommerce_template_single_add_to_cart - 30
						 * @hooked woocommerce_template_single_meta - 40
						 * @hooked woocommerce_template_single_sharing - 50
						 * @hooked WC_Structured_Data::generate_product_data() - 60
						 */
						do_action( 'woocommerce_single_product_summary' );
					?>
				</div>
			</div>
		</div>
	</div>
</section>

<?php
	/**
	 * Hook: woocommerce_after_single_product_summary.
	 *
	 * @hooked woocommerce_output_product_data_tabs - 10
	 * @hooked woocommerce_upsell_display - 15
	 * @hooked woocommerce_output_related_products - 20
	 */
	do_action( 'woocommerce_after_single_product_summary' );
?>

<?php do_action( 'woocommerce_after_single_product' ); ?>

<script type="text/javascript">
	jQuery(function($){

		// Ajax add to cart on the product page
		var $warp_fragment_refresh = {
		    url: wc_cart_fragments_params.wc_ajax_url.toString().replace( '%%endpoint%%', 'get_refreshed_fragments' ),
		    type: 'POST',
		    success: function( data ) {
		        if ( data && data.fragments ) {

		            $.each( data.fragments, function( key, value ) {
		                $( key ).replaceWith( value );
		            });

		            $( document.body ).trigger( 'wc_fragments_refreshed' );
		        }
		    }
		};

		$('.product-summary form.cart').on('submit', function (e)
		{
		    e.preventDefault();

		    $('.single_add_to_cart_button').prop('disabled', true);
		    $('.single_add_to_cart_button').addClass("loading");

		    var product_url = window.location,
		        form = $(this);

		    $.post(product_url, form.serialize() + '&_wp_http_referer=' + product_url, function (result)
		    {
		        // update fragments
		        $.ajax($warp_fragment_refresh);

		        $('.single_add_to_cart_button').removeClass("loading");
		        $('.single_add_to_cart_button').prop('disabled', false);

		    });
		});

		$( ".single_variation_wrap" ).on( "show_variation", function ( event, variation ) {
			var _newPrice = $('.woocommerce-variation-price').html();
			$('.product-summary-price-text').html(_newPrice);
		} );

	});
</script>