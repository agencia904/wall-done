<?php
/**
 * Single Product title
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/title.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @author     WooThemes
 * @package    WooCommerce/Templates
 * @version    1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

the_title( '<h2 class="product_title entry-title">', '</h2>' );
?>
<?php
	global $product;
	$product_id=$product->id;
	$attachment_ids = $product->get_gallery_attachment_ids();
?>
<div class="product-cat">
	<p>
		Categoria:
		<?php
		    foreach( wp_get_post_terms( $product_id, 'product_cat' ) as $product_cat ){
		        if( $product_cat ){
		            echo '<span>' . $product_cat->name . '</span>';
		        }
		    }
		?>

		<?php if($product->get_sku()) { echo " / Ref: " . $product->get_sku(); } ?>
	</p>
</div>		
<?php if(get_post_meta( $product_id, '_single_product_quote', true )) : ?>
	<div class="product-quote">
		<p><?php echo get_post_meta( $product_id, '_single_product_quote', true ); ?></p>
	</div>
<?php endif; ?>
